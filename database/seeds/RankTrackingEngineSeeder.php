<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\RankTrackingEngine;

class RankTrackingEngineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RankTrackingEngine::updateOrCreate(
            ['id' => 1],
            [
                'url' => 'google.com',
                'is_enabled' => 1,
                'name' => 'Google',
                'country' => 'US',
            ]
        );
    }
}
