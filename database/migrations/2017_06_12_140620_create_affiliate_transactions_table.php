<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliate_transactions', function (Blueprint $table) {
            $table->integer('affiliate_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('transacted_at');
            $table->integer('type_id');
            $table->boolean('is_mature')->default(false)->index();
            $table->date('mature_at')->index();
            $table->json('details')->nullable();
            $table->string('reward_type');
            $table->json('reward_details')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('affiliate_id')->references('id')->on('affiliates')->onDelete('cascade');
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate_transactions', function (Blueprint $table) {
            $table->dropForeign('affiliate_transactions_user_id_foreign');
            $table->dropForeign('affiliate_transactions_affiliate_id_foreign');
        });

        Schema::dropIfExists('affiliate_transactions');
    }
}
