<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;

class CreatePaypalAndCoinbasePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = Permission::create(['name' => 'administer paypal']);
        $permission = Permission::create(['name' => 'administer coinbase']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'administer paypal')->first()->delete();
        Permission::where('name', 'administer coinbase')->first()->delete();
    }
}
