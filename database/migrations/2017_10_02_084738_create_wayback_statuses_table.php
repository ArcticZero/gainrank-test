<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybackStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wayback_statuses', function (Blueprint $table) {
            $table->smallInteger('id');
            $table->string('name', 255);
            $table->string('code', 255);
            $table->string('cust_message', 255);
            $table->string('staff_message', 255);
            $table->string('cust_url', 255);

            $table->primary('id');
            $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wayback_statuses');
    }
}
