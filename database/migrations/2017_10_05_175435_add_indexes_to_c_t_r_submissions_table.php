<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToCTRSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ctr_submissions', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('keyword');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ctr_submissions', function (Blueprint $table) {
            $table->dropIndex('ctr_submissions_user_id_index');
            $table->dropIndex('ctr_submissions_keyword_index');
        });
    }
}
