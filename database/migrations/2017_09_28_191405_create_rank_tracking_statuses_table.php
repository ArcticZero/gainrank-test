<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTrackingStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_tracking_statuses', function (Blueprint $table) {
            $table->smallInteger('id');
            $table->string('name', 255);
            $table->string('code', 255);
            $table->string('cust_message', 255);
            $table->string('staff_message', 255);
            $table->string('cust_url', 255);

            $table->primary('id');
            $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank_tracking_statuses');
    }
}
