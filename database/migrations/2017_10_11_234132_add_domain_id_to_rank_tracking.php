<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDomainIdToRankTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rank_tracking', function (Blueprint $table) {
            $table->integer('domain_id')
                ->unsigned()
                ->nullable(); // in case we already have domains

            $table->foreign('domain_id')
                ->references('id')
                ->on('rank_tracking_domains')
                ->onDelete('cascade');

            $table->dropColumn('domain');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_domain_id_foreign');
            $table->dropColumn('domain_id');
            $table->string('domain', 255);
        });
    }
}
