<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTrackingRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_tracking_ranks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('track_id')
                ->unsigned();
            $table->smallInteger('rank');

            $table->foreign('track_id')
                ->references('id')
                ->on('rank_tracking')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking_ranks', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_ranks_track_id_foreign');
            $table->dropColumn('track_id');
        });

        Schema::dropIfExists('rank_tracking_ranks');
    }
}
