<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropVersionForeignKeyFromRankTrackingSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rank_tracking_settings', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_settings_version_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking_settings', function (Blueprint $table) {
            $table->foreign('version')
                    ->references('id')
                    ->on('rank_tracking_versions')
                    ->onDelete('cascade');
        });
    }
}
