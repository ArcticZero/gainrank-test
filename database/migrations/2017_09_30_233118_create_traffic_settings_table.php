<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrafficSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->smallInteger('version')
                ->unsigned();
            $table->string('name', 255);
            $table->string('value', 255);
            $table->unique(['name', 'version']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic_settings');
    }
}
