<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\AdCampaignBonusType;
use App\Reward;

class AddRewardTypeToAdCampaignBonusTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_campaign_bonus_types', function (Blueprint $table) {
            $table->integer('reward_type')->unsigned()->nullable();
        });
        
        AdCampaignBonusType::where('type', '% off on first month')
            ->update(['reward_type' => Reward::TYPE_DISCOUNT_INITIAL_PERCENT]);
        AdCampaignBonusType::where('type', '% off recurring')
            ->update(['reward_type' => Reward::TYPE_DISCOUNT_RECURRING_PERCENT]);
        AdCampaignBonusType::where('type', 'flat $ off on first month')
            ->update(['reward_type' => Reward::TYPE_DISCOUNT_INITIAL_AMOUNT]);
        AdCampaignBonusType::where('type', 'flat $ off recurring')
            ->update(['reward_type' => Reward::TYPE_DISCOUNT_RECURRING_AMOUNT]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_campaign_bonus_types', function (Blueprint $table) {
            $table->dropColumn('reward_type');
        });
    }
}
