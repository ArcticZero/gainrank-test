<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Laravel\Spark\Spark;
use App\User;

class AddDefaultRolesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = Role::create(['name' => 'admin']);
        $editor = Role::create(['name' => 'editor']);
        $regular = Role::create(['name' => 'regular user']);
        $blocked = Role::create(['name' => 'blocked']);
        $permission = Permission::create(['name' => 'access kiosk']);
        $permission = Permission::create(['name' => 'administer announcements']);
        $permission = Permission::create(['name' => 'administer posts']);
        $permission = Permission::create(['name' => 'administer users']);
        $permission = Permission::create(['name' => 'assign roles']);
        // The admin role will have all permissions via a Gate Facade
        // So we're not assigning it any.
        // See App\Providers\AuthServiceProvider.php
        $editor->givePermissionTo('access kiosk');
        $editor->givePermissionTo('administer announcements');
        $editor->givePermissionTo('administer posts');
        $users = User::all();
        foreach ($users as $user) {
            if (Spark::developer($user->email)) {
                $user->assignRole($admin);
            } else {
                $user->assignRole($regular);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $users = User::role('blocked')->get();
        foreach ($users as $user) {
            $user->removeRole('blocked');
        }
        $users = User::role('regular user')->get();
        foreach ($users as $user) {
            $user->removeRole('regular user');
        }
        $users = User::role('editor')->get();
        foreach ($users as $user) {
            $user->removeRole('editor');
        }
        $users = User::role('admin')->get();
        foreach ($users as $user) {
            $user->removeRole('admin');
        }
        $admin = Role::findByName('admin');
        $editor = Role::findByName('editor');
        $regular = Role::findByName('regular user');
        $blocked = Role::findByName('blocked');
        $editor->revokePermissionTo('access kiosk');
        $editor->revokePermissionTo('administer announcements');
        $editor->revokePermissionTo('administer posts');
        $regular->delete();
        $blocked->delete();
        $editor->delete();
        $admin->delete();
        Permission::where('name', 'access kiosk')->first()->delete();
        Permission::where('name', 'administer announcements')->first()->delete();
        Permission::where('name', 'administer posts')->first()->delete();
        Permission::where('name', 'administer users')->first()->delete();
        Permission::where('name', 'assign roles')->first()->delete();
    }
}
