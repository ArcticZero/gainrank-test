<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('order_type');
            $table->string('billing_provider')
                ->nullable();
            $table->string('paypal_payer_id')
                ->nullable();
            $table->string('paypal_payer_email')
                ->nullable();
            $table->string('paypal_agreement_id')
                ->nullable();
            $table->integer('pending_subscription_id')
                ->unsigned()
                ->nullable();
            $table->json('request_details')
                ->nullable();
            $table->tinyInteger('status')
                ->default(1);

            $table->foreign('pending_subscription_id')
                ->references('id')
                ->on('user_pending_subscriptions')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pending_orders');
        Schema::enableForeignKeyConstraints();
    }
}
