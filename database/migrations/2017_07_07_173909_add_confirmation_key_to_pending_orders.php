<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmationKeyToPendingOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_orders', function (Blueprint $table) {
            $table->string('confirmation_key')->nullable();
            $table->timestamp('confirmation_key_expires_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_orders', function (Blueprint $table) {
            $table->dropColumn('confirmation_key');
            $table->dropColumn('confirmation_key_expires_at');
        });
    }
}
