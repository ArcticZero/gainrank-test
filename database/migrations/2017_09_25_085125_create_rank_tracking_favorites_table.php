<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTrackingFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_tracking_favorites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                ->unsigned();
            $table->integer('track_id')
                ->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('track_id')
                ->references('id')
                ->on('rank_tracking')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking_favorites', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_favorites_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('rank_tracking_favorites_track_id_foreign');
            $table->dropColumn('track_id');
        });

        Schema::dropIfExists('rank_tracking_favorites');
    }
}
