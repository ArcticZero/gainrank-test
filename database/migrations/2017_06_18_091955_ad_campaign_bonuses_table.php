<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdCampaignBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_campaign_bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bonus_amount');
            $table->integer('ad_campaign_bonus_type_id')->unsigned();
            $table->integer('ad_campaign_id')->unsigned();
            $table->foreign('ad_campaign_bonus_type_id')
                ->references('id')->on('ad_campaign_bonus_types')
                ->onDelete('cascade');
            $table->foreign('ad_campaign_id')
                ->references('id')->on('ad_campaigns')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_campaign_bonuses');
    }
}
