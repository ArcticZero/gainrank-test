<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTrackingSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_tracking_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->smallInteger('version')
                ->unsigned();
            $table->string('name', 255);
            $table->string('value', 255);
            $table->unique(['name', 'version']);

            $table->foreign('version')
                ->references('id')
                ->on('rank_tracking_versions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking_settings', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_settings_version_foreign');
            $table->dropColumn('version');
        });

        Schema::dropIfExists('rank_tracking_settings');
    }
}
