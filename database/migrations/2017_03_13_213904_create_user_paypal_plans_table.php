<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaypalPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_paypal_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('paypal_agreement_id', 255)->nullable()->index();
            $table->string('provider_plan_name', 255)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_paypal_plans');
    }
}
