<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FingerprintRecordingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fingerprint_recordings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fingerprint_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable()->default(0);
            $table->timestamps();
            $table->foreign('fingerprint_id')
                ->references('id')->on('fingerprints')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->nullable()
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fingerprint_recordings');
    }
}
