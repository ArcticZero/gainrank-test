<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\AdCampaignBonusType;

class AdCampaignBonusTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_campaign_bonus_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->timestamps();
        });
        AdCampaignBonusType::create(['type' => '% bonus on first deposit']);
        AdCampaignBonusType::create(['type' => '% off on first month']);
        AdCampaignBonusType::create(['type' => '% off recurring']);
        AdCampaignBonusType::create(['type' => 'flat $ off on first month']);
        AdCampaignBonusType::create(['type' => 'flat $ off recurring']);
        AdCampaignBonusType::create(['type' => 'flat # bonus keywords to track']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_campaign_bonus_types');
    }
}
