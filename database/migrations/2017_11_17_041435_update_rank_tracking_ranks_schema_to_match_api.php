<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRankTrackingRanksSchemaToMatchApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement('alter table rank_tracking_ranks modify id int(10)');
        Schema::enableForeignKeyConstraints();

        Schema::table('rank_tracking_ranks', function (Blueprint $table) {
            $table->date('applied_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement('alter table rank_tracking_ranks modify id int(10) AUTO_INCREMENT');
        Schema::enableForeignKeyConstraints();

        Schema::table('rank_tracking_ranks', function (Blueprint $table) {
            $table->dropColumn('applied_at');
        });
    }
}
