<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('sales_invoice_id')
                ->unsigned()
                ->nullable();
            $table->integer('user_id')
                ->unsigned()
                ->nullable();
            $table->string('payment_processor_id');
            $table->string('reason');
            $table->json('reason_data');
            $table->json('notification_data');
            $table->decimal('dollar_amount', 16, 8)
                ->default(0);
            $table->decimal('amount', 16, 8)
                ->default(0);
            $table->string('currency');

            $table->foreign('sales_invoice_id')
                ->references('id')
                ->on('sales_invoices')
                ->onDelete('set null');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('payment_logs');
        Schema::enableForeignkeyConstraints();
    }
}
