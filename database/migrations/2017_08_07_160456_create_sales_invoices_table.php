<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('apply_at')
                ->nullable();
            $table->timestamp('due_at')
                ->nullable();
            $table->decimal('total_amount', 16, 8)
                ->default(0);
            $table->string('currency');
            $table->string('pay_url')
                ->nullable();
            $table->integer('user_id')
                ->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('sales_invoices');
        Schema::enableForeignkeyConstraints();
    }
}
