<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybackSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wayback_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')
                ->unsigned();
            $table->string('submission_id', 255);
            $table->string('url', 255);
            $table->string('zipfile', 255);
            $table->smallInteger('id_status');
            $table->smallInteger('version');
            $table->string('ip_addr', 100);
            $table->boolean('flag_api_add');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wayback_submissions', function (Blueprint $table) {
            $table->dropForeign('wayback_submissions_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('wayback_submissions');
    }
}
