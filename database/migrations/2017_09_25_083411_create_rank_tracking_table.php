<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')
                ->unsigned();
            $table->string('submission_id', 255);
            $table->string('domain', 255);
            $table->string('keyword', 255);
            $table->integer('engine_id')
                ->unsigned();
            $table->string('country', 2);
            $table->smallInteger('version')
                ->unsigned();
            $table->timestamp('stopped_at')
                ->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('engine_id')
                ->references('id')
                ->on('rank_tracking_engines')
                ->onDelete('cascade');

            $table->index('domain');

            $table->foreign('version')
                ->references('id')
                ->on('rank_tracking_versions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropForeign('rank_tracking_engine_id_foreign');
            $table->dropColumn('engine_id');
            $table->dropForeign('rank_tracking_version_foreign');
            $table->dropColumn('version');
        });

        Schema::dropIfExists('rank_tracking');
    }
}
