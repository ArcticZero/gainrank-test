<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class HelpdeskPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'open new support ticket']);
        Permission::create(['name' => 'reply to support ticket']);
        $new_ticket = Role::create(['name' => 'can open new support ticket']);
        $reply_ticket = Role::create(['name' => 'can reply to support ticket']);
        $new_ticket->givePermissionTo('open new support ticket');
        $reply_ticket->givePermissionTo('reply to support ticket');
        $users = User::all();
        foreach ($users as $user) {
            $user->assignRole($new_ticket);
            $user->assignRole($reply_ticket);
        }
        Schema::table('users', function (Blueprint $table) {
            $table->string('helpdesk_user_id', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $users = User::role('can open new support ticket')->get();
        foreach ($users as $user) {
            $user->removeRole('can open new support ticket');
        }
        $users = User::role('can reply to support ticket')->get();
        foreach ($users as $user) {
            $user->removeRole('can reply to support ticket');
        }
        $new_ticket = Role::findByName('can open new support ticket');
        $reply_ticket = Role::findByName('can reply to support ticket');
        $new_ticket->revokePermissionTo('open new support ticket');
        $reply_ticket->revokePermissionTo('reply to support ticket');
        $new_ticket->delete();
        $reply_ticket->delete();
        Permission::where('name', 'open new support ticket')->first()->delete();
        Permission::where('name', 'reply to support ticket')->first()->delete();
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('helpdesk_user_id');
        });
    }
}
