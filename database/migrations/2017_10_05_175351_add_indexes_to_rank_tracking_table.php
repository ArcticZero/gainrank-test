<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToRankTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rank_tracking', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('submission_id');
            $table->index('keyword');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking', function (Blueprint $table) {
            $table->dropIndex('rank_tracking_user_id_index');
            $table->dropIndex('rank_tracking_submission_id_index');
            $table->dropIndex('rank_tracking_keyword_index');
        });
    }
}
