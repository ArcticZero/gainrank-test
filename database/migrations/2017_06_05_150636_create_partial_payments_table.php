<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartialPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partial_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')->index();
            $table->integer('subscription_id')->nullable()->index();
            $table->integer('pending_subscription_id')->nullable()->index();
            $table->string('order_id')->index();
            $table->decimal('amount_paid', 16, 8)->default(0);
            $table->decimal('total_amount', 16, 8)->default(0);
            $table->boolean('is_paid')->default(false);
            $table->string('currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partial_payments');
    }
}
