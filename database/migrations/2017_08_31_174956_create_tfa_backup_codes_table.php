<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTfaBackupCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tfa_backup_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('code', 255);
            $table->timestamp('used_at')
                ->nullable();
            $table->integer('user_id')
                ->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tfa_backup_codes', function (Blueprint $table) {
            $table->dropForeign('tfa_backup_codes_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('tfa_backup_codes');
    }
}
