<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpdeskConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')
                ->unsigned();
            $table->string('conv_id', 25);
            $table->integer('read_reply_count');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('helpdesk_conversations', function (Blueprint $table) {
            $table->dropForeign('helpdesk_conversations_user_id_foreign');
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('helpdesk_conversations');
    }
}
