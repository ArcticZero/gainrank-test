<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Spatie\Permission\Models\Role;

class AddFrauderRoleAndCaptureIpAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fingerprint_recordings', function (Blueprint $table) {
            $table->string('ip_address', 45)->comment('Stores both IPv4 and IPv6 Addresses hence VARCHAR(45)')->nullable();
        });
        Role::create(['name' => 'frauder']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fingerprint_recordings', function (Blueprint $table) {
            $table->dropColumn('ip_address');
        });
        $users = User::role('frauder')->get();
        foreach ($users as $user) {
            $user->removeRole('frauder');
        }
        Role::findByName('frauder')->delete();
    }
}
