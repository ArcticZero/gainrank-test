<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->decimal('payout_available', 10, 2)->default(0);
            $table->decimal('payout_total', 10, 2)->default(0);
            $table->tinyInteger('status')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliates', function (Blueprint $table) {
            $table->dropForeign('affiliates_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('affiliates');
    }
}
