<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTrackingDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_tracking_domains', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('url', 255);
            $table->integer('user_id')
                ->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rank_tracking_domains', function (Blueprint $table) {
            $table->dropForeign('rank_tracking_domains_user_id_foreign');
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('rank_tracking_domains');
    }
}
