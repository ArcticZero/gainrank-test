<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateSoftLaunchInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soft_launch_invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('invitee_user_id')->unsigned()->nullable();
            $table->string('token', 40)->unique();
            $table->boolean('used')->default(0);
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->nullable()
                ->onDelete('set null');
            $table->foreign('invitee_user_id')
                ->references('id')->on('users')
                ->nullable()
                ->onDelete('cascade');
        });

        /*
         * If you get an error while running migrations in a fresh db import remember to run the following
         * right after repopulating the db:
         * php artisan cache:forget spatie.permission.cache
         * php artisan cache:clear
         */
        Permission::create(['name' => 'invite users']);
        $regular = Role::findByName('regular user');
        $regular->givePermissionTo('invite users');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soft_launch_invitations');
        $regular = Role::findByName('regular user');
        $regular->revokePermissionTo('invite users');
        Permission::where('name', 'invite users')->first()->delete();
    }
}
