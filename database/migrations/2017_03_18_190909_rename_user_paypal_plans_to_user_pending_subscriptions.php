<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameUserPaypalPlansToUserPendingSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_paypal_plans', function (Blueprint $table) {
            $table->string('billing_provider', 255);
            $table->renameColumn('paypal_agreement_id', 'agreement_id');
        });

        Schema::rename('user_paypal_plans', 'user_pending_subscriptions');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_pending_subscriptions', function (Blueprint $table) {
            $table->dropColumn('billing_provider');
            $table->renameColumn('agreement_id', 'paypal_agreement_id');
        });

        Schema::rename('user_pending_subscriptions', 'user_paypal_plans');
    }
}
