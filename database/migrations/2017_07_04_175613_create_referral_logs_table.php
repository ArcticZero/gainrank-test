<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->ipAddress('ip_address')->nullable();
            $table->string('referer')->nullable();
            $table->string('user_agent')->nullable();
            $table->json('details')->nullable();
            $table->integer('fingerprint_id')->unsigned()->nullable();
            $table->integer('affiliate_id')->unsigned()->nullable();
            $table->integer('referred_user_id')->unsigned()->nullable();
            $table->foreign('fingerprint_id')->references('id')->on('fingerprints')->onDelete('set null');
            $table->foreign('affiliate_id')->references('id')->on('affiliates')->onDelete('set null');
            $table->foreign('referred_user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referral_logs', function (Blueprint $table) {
            $table->dropForeign('referral_logs_affiliate_id_foreign');
            $table->dropForeign('referral_logs_fingerprint_id_foreign');
            $table->dropForeign('referral_logs_referred_user_id_foreign');
            $table->dropColumn('affiliate_id');
            $table->dropColumn('fingerprint_id');
            $table->dropColumn('referred_user_id');
        });

        Schema::dropIfExists('referral_logs');
    }
}
