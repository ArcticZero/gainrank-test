<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_invoice_id')
                ->unsigned();
            $table->string('description');
            $table->decimal('amount', 16, 8)
                ->default(0);
            $table->string('currency');
            $table->integer('quantity')
                ->unsigned();

            $table->foreign('sales_invoice_id')
                ->references('id')
                ->on('sales_invoices')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('sales_invoice_items');
        Schema::enableForeignkeyConstraints();
    }
}
