<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCTRSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctr_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id')
                ->unsigned();
            $table->string('submission_id', 255);
            $table->string('url', 255);
            $table->string('keyword', 255);
            $table->integer('wanted_count');
            $table->integer('sent_count');
            $table->integer('cost_per_x');
            $table->integer('cost_per_amt');
            $table->smallInteger('id_status');
            $table->smallInteger('version');
            $table->string('ip_addr', 100);
            $table->boolean('flag_api_add');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ctr_submissions', function (Blueprint $table) {
            $table->dropForeign('ctr_submissions_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('ctr_submissions');
    }
}
