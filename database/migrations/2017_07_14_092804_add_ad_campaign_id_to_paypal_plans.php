<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdCampaignIdToPaypalPlans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paypal_plans', function (Blueprint $table) {
            $table->dropPrimary();
            $table->integer('ad_campaign_id')
                ->unsigned()
                ->nullable();
            $table->foreign('ad_campaign_id')
                ->references('id')
                ->on('ad_campaigns')
                ->onDelete('cascade');
        });

        Schema::table('paypal_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_id')
                ->nullable()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paypal_plans', function (Blueprint $table) {
            $table->integer('id')
                ->unsigned()
                ->change();
            $table->dropPrimary();
            $table->dropColumn('id');
            $table->dropForeign('paypal_plans_ad_campaign_id_foreign');
            $table->dropColumn('ad_campaign_id');
            $table->string('plan_id')
                ->nullable(false)
                ->change();
            $table->primary('plan_id');
        });
    }
}
