#!/usr/bin/env bash

echo "php-cs-fixer pre commit hook start"

PHP_CS_FIXER="vendor/friendsofphp/php-cs-fixer/php-cs-fixer"
HAS_PHP_CS_FIXER=false
PHP_CODECEPTION="vendor/bin/codecept"
HAS_PHP_CODECEPTION=false

if [ -x $PHP_CS_FIXER ]; then
    HAS_PHP_CS_FIXER=true
fi

if [ -x $PHP_CODECEPTION ]; then
    HAS_PHP_CODECEPTION=true
fi

if $HAS_PHP_CS_FIXER && $HAS_PHP_CODECEPTION; then
    git status --porcelain | grep -e '^[ \t]*[A]\(.*\).php$' | cut -c 3- | while read line; do
        $PHP_CS_FIXER fix --verbose "$line";
        git add "$line";
    done
    $PHP_CODECEPTION run -s acceptance;

    EXITSTATUS=$?

    if [ $EXITSTATUS -ne 0 ]
    then
      echo ""
      echo "Test run failed, aborting commit!"
      echo ""
      echo "Please fix error and try again."
      echo ""
      exit 1
    fi
else
    echo ""
    echo "Please install php-cs-fixer and codeception, e.g.:"
    echo ""
    echo "  composer update"
    echo ""
    exit 1
fi

echo "php-cs-fixer pre commit hook finish"