# Git Hooks

This directory contains git hooks, which are installed to the current project via the [`shared-git-hooks`](https://github.com/kilianc/shared-git-hooks) npm package.
All you need to do to activate them is run `npm install`.

At the time of writing the following hooks are available (EXT optional):
```
applypatch-msg.EXT
pre-applypatch.EXT
post-applypatch.EXT
pre-commit.EXT
prepare-commit-msg.EXT
commit-msg.EXT
post-commit.EXT
pre-rebase.EXT
post-checkout.EXT
post-merge.EXT
pre-push.EXT
pre-receive.EXT
update.EXT
post-receive.EXT
post-update.EXT
pre-auto-gc.EXT
post-rewrite.EXT
```
