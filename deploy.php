<?php
namespace Deployer;
use Deployer\Task\Context;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

require 'recipe/laravel.php';
option('db-pwd', null, InputOption::VALUE_OPTIONAL, 'Password for the MySQL test user.');

// Configuration
// Servers
inventory('servers.yml');

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@gitlab.com:gainrank/GainrankDash.git');

set('shared_files', ['.env']);

set('shared_dirs', [
    'storage/app',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);

set('writable_dirs', ['bootstrap/cache', 'storage', 'vendor']);

// Tasks
desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    run('service php-fpm restart');
});

desc('Create temporary database');
task('temporary-site:create-db', function () {
    if (input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            run("mysql -u root -e 'DROP DATABASE IF EXISTS test_$revision; CREATE DATABASE test_$revision;'");
        } else {
            echo "\nNo revision specified, performing default deployment.\n";
        }
    }
})->onStage('test');

desc('Create temporary directory');
task('temporary-site:create-tmp-dir', function () {
    if (input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            $hostname = get('hostname');
            $host = host($hostname);
            $dir = "/var/www/$revision.gainrank.com";
            run("mkdir -p $dir");

            $host->set('deploy_path', $dir);
        }
    }
})->onStage('test');

desc('Populate .env file');
task('temporary-site:populate-env-file', function () {
    $revision = '';
    $pwd = '';
    if (input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
    }
    if (input()->hasOption('db-pwd')) {
        $pwd = input()->getOption('db-pwd');
    }
    if (!empty($revision) && !empty($pwd)) {
        run("mysql -u root test_$revision < {{release_path}}/tests/_data/dump.sql");
        run("cp {{release_path}}/.env.testing {{deploy_path}}/shared; mv {{deploy_path}}/shared/.env.testing {{deploy_path}}/shared/.env");
        run('sed -i \'/DB_DATABASE=.*/c\DB_DATABASE=test_'.$revision.'\' {{deploy_path}}/shared/.env');
        run('sed -i \'/DB_USERNAME=.*/c\DB_USERNAME=testuser\' {{deploy_path}}/shared/.env');
        run('sed -i \'/DB_PASSWORD=.*/c\DB_PASSWORD='.$pwd.'\' {{deploy_path}}/shared/.env');
        run('sed -i \'/APP_URL=.*/c\APP_URL='.$revision.'.gainrank.com\' {{deploy_path}}/shared/.env');
    }
});

desc('Delete tmp site and database');
task('temporary-site:delete', function () {
    if (input()->hasOption('revision')) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            run('mysql -u root -e \'DROP DATABASE test_' . $revision . ';\'');
            run('rm -fr /var/www/' . $revision . '.gainrank.com/');
        }
    }
})->onStage('test');

desc('Execute artisan config:clear');
task('artisan:config:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan config:clear');
});


task('temporary-site', [
    'temporary-site:create-db',
    'temporary-site:create-tmp-dir',
])->onStage('test');

before('deploy', 'temporary-site');
after('deploy:shared', 'temporary-site:populate-env-file');

after('deploy:symlink', 'php-fpm:restart');
// Need to clear the config cache as for some reason it's not loading properly after deployment
after('artisan:optimize', 'artisan:config:clear');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
after('deploy:failed', 'temporary-site:delete');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');
