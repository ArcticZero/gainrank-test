<?php

return [
    'currency' => 'USD',

    'providers' => [
        'paypal_recurring',
        'paypal_manual',
        'coinbase'
    ]
];
