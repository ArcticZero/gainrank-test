var tour = {
    "id": "tutorial-home",
    "steps": [
        {
            "title": "Active Users",
            "content": "This shows you a history of active users.",
            "target": "active-users",
            "placement": "top"
        },
        {
            "content": "This is a bar chart for domains.",
            "target": "bar-domains",
            "placement": "left"
        },
        {
            "content": "This is a bar chart for keywords.",
            "target": "bar-keywords",
            "placement": "left"
        },
        {
            "content": "This is a bar chart for another stat.",
            "target": "bar-another-stat",
            "placement": "left"
        },
        {
            "content": "This is a bar chart for one more stat.",
            "target": "bar-one-more",
            "placement": "left"
        }
    ]
}