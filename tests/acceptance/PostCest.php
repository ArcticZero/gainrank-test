<?php


class PostCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function seeCreateBlogPostUI(\Step\Acceptance\Login $I)
    {
        $I->wantTo('See the form for creating Blog Posts');
        $I->loginAsAdmin();
        $I->amOnPage('spark/kiosk');
        $I->click('Blog Posts');
        $I->see('Blog Posts you create here will be displayed in the GainRank homepage.');
        $I->see('Title');
        $I->see('Categories');
        $I->see('Select Image');
        $I->see('Body');
    }

    public function confirmRequiredFields(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Confirm that title, image and body for Blog Posts are required');
        $I->loginAsAdmin();
        $I->amOnPage('spark/kiosk#/posts');
        $I->click('#posts .create-button');
        $I->waitForText('The title field is required.');
        // Since no text is displayed for image uploads we're going to check if the container gets the has-error class
        $I->waitForElement('//div[@id=\'uploadbox\' and contains(@class,\'form-group upload has-error\')]');
        $I->waitForText('The body field is required.');
    }

    // Leaving test for the future but couldn't get it to work (there's some incompatibility with PhantomJS/VueJS/Quill)
/*    public function createBlogPost(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Create a Blog Post');
        $I->loginAsAdmin();
        $I->amOnPage('spark/kiosk#/posts');
        $I->wait(60);
        $I->fillField('title', 'Test Blog Post');
        //$I->click('//div[contains(@class,\'ql-editor ql-blank\')]');
        //$I->fillField('div.ql-editor.ql-blank', 'This is the body from fillfield.');
        // Since Quill editor doesn't use regular input/textarea fields we have to update the value via JS
        //$I->executeJS('$(\'div.ql-editor\').html(\'<p><u>This</u> is the <em>body</em> of the test <strong>Blog Post<strong>.</p>\');');
        // TODO Check required fields something fishy error msgs
        $I->executeJS('$(\'#upload-input\').show();');
        $I->wait(3);
        //$I->attachFile('#upload-input', '1.png');
        // TODO categories
        $I->click('#posts .create-button');
        //$I->wait(60);
        $I->waitForText('Test Blog Post');
        $I->waitForText(date('F jS, Y h:i A'));
    }*/
}
