<?php


class CoinbaseCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function subscribeTest(AcceptanceTester $I)
    {
        $I->wantTo('subscribe via Coinbase');
        $I->lookForwardTo('have a pending subscription to a paid plan using Coinbase');

        $I->registerAccount('Subscriber', 'subscriber@gainrank.com', 'testpassword');

        $I->wait(3);
        $I->seeInCurrentUrl('home');

        $this->subscribeViaCoinbase($I);
    }

    /*
    public function activateTest(AcceptanceTester $I)
    {
        $I->wantTo('subscribe via Coinbase and activate');
        $I->lookForwardTo('have an activated subscription to a paid plan using Coinbase');

        $I->registerAccount('Subscriber', 'subscriber@gainrank.com', 'testpassword');

        $I->wait(3);
        $I->seeInCurrentUrl('home');

        $this->subscribeViaCoinbase($I);

        $this->activateSubscription($I, 'coinbase');

        $I->see('Cancel Subscription');
    }

    public function cancelTest(AcceptanceTester $I)
    {
        $I->wantTo('cancel a Coinbase subscription');
        $I->lookForwardTo('have a cancelled Coinbase subscription');

        $I->registerAccount('Subscriber', 'subscriber@gainrank.com', 'testpassword');

        $I->wait(3);
        $I->seeInCurrentUrl('home');

        $this->subscribeViaCoinbase($I);

        $this->activateSubscription($I, 'coinbase');

        $I->click('Cancel Subscription');
        $I->wait(5);
        $I->seeElement('.btn.btn-danger');
        $I->click('.btn.btn-danger');
        $I->waitForElement('.btn.btn-plan.btn-warning', 40);
        $I->dontSee('Cancel Subscription');
    }

    public function resumeTest(AcceptanceTester $I)
    {
        $I->wantTo('resume a cancelled Coinbase subscription');
        $I->lookForwardTo('resume a cancelled Coinbase subscription');

        $I->registerAccount('Subscriber', 'subscriber@gainrank.com', 'testpassword');

        $I->wait(3);
        $I->seeInCurrentUrl('home');

        $this->subscribeViaCoinbase($I);

        $this->activateSubscription($I, 'coinbase');

        $I->click('Cancel Subscription');
        $I->wait(5);
        $I->seeElement('.btn.btn-danger');
        $I->click('.btn.btn-danger');
        $I->waitForElement('.btn.btn-plan.btn-warning', 40);
        $I->dontSee('Cancel Subscription');

        $I->click('.btn.btn-plan.btn-warning');
        $I->waitForElement('.btn.btn-danger-outline', 40);
    }
    */

    // helper methods
    private function subscribeViaCoinbase(AcceptanceTester $I)
    {
        $I->amOnPage('/settings');
        $I->click('Subscription');
        $I->waitForElement('.btn.btn-primary-outline.btn-plan');
        $I->click('.btn.btn-primary-outline.btn-plan');
        $I->see('Coinbase');
        $I->selectOption('form input[name="payment_method"]', 'coinbase');
        $I->click('Checkout');

        $this->coinbaseLogin($I);

        /*
        $I->waitForElement('#send_using_coinbase', 30);
        $I->click('#send_using_coinbase');
        $I->waitForelement('#return_button', 30);
        $I->click('#return_button');
        $I->waitForText('Your subscription is currently pending, and will be activated once payment has been cleared.', 40);
        */
    }

    private function activateSubscription(AcceptanceTester $I, $billing_provider)
    {
        $I->amOnPage('/settings/subscription/activate/' . $billing_provider);
        $I->wait(5);
        $I->moveBack();
        $I->reloadPage();
    }

    private function coinbaseLogin(AcceptanceTester $I)
    {
        $I->waitForElement('#pay_tabs', 30);
        $I->click('Use Coinbase Wallet');
        $I->waitForElement('#sign_in', 30);
        $I->click('#sign_in');
        $I->switchToWindow('signin_window');
        $I->waitForElement("#signin_button", 30);
        $I->fillField('email', 'gainrank_test@mail.com');
        $I->fillField('password', 'zq2AixK67hPomQU2kIyT');
        $I->click('#signin_button');
        $I->switchToWindow();
    }
}
