<?php


class AdCampaignsCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function adminCanAdministerAdCampaigns(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Create a new Ad Campaign');
        $I->loginAsAdmin();
        $I->see('Ad Campaigns');
        $I->click('Ad Campaigns');
        $I->fillField('name', 'Test Campaign');
        $I->fillField('//*[@id="campaigns"]/div/div[1]/div[2]/form/div[3]/div/div/div[1]/input', '15');
        $I->fillField('description', 'This is a test Ad Campaign');
        $I->fillField('start_date', '2017-01-01');
        $I->fillField('end_date', '2018-01-01');
        $I->fillField('max_uses', '2000');
        $I->fillField('path', 'test-campaign');
        $I->checkOption('active');
        $I->click('//*[@id="campaigns"]/div/div[1]/div[2]/form/div[10]/div/button');
        $I->wait(20);
        $I->see('Campaigns');
        $I->see('Name', '//*[@id="campaigns"]/div/div[2]/div[2]/table/thead/tr/th[1]');
        $I->see('Start Date', '//*[@id="campaigns"]/div/div[2]/div[2]/table/thead/tr/th[2]');
        $I->see('End Date', '//*[@id="campaigns"]/div/div[2]/div[2]/table/thead/tr/th[3]');
        $I->see('Max Uses', '//*[@id="campaigns"]/div/div[2]/div[2]/table/thead/tr/th[4]');
        $I->see('Path', '//*[@id="campaigns"]/div/div[2]/div[2]/table/thead/tr/th[5]');
        $I->see('Active', '//*[@id="campaigns"]/div/div[2]/div[2]/table/thead/tr/th[6]');
        $I->see('Test Campaign', '//*[@id="campaigns"]/div/div[2]/div[2]/table/tbody/tr/td[1]/div');
        $I->see('2017-01-01', '//*[@id="campaigns"]/div/div[2]/div[2]/table/tbody/tr/td[2]/div');
        $I->see('2018-01-01', '//*[@id="campaigns"]/div/div[2]/div[2]/table/tbody/tr/td[3]/div');
        $I->see('2000', '//*[@id="campaigns"]/div/div[2]/div[2]/table/tbody/tr/td[4]/div');
        $I->see('test-campaign', '//*[@id="campaigns"]/div/div[2]/div[2]/table/tbody/tr/td[5]/div');
    }
}
