<?php


class FingerprintsCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    /**
     * @param \Step\Acceptance\Login $I
     */
    public function confirmFingerprintGeneration(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Confirm that fingerprints are being generated');
        $I->amOnPage('logout');
        $I->loginAsRegular();
        $I->amOnPage('logout');
        $I->loginAsAdmin();
        $I->amOnPage('spark/kiosk');
        $I->see('Fingerprints');
        $I->click('Fingerprints');
        $I->seeElement('#fingerprints-search');
        $I->pressKey('#fingerprints-search', '*', WebDriverKeys::ENTER);
        $I->wait(3);
        $I->see('Hash');
        $I->click('//*[@id="fingerprints"]/div/div/div[2]/div[2]/table/tbody/tr/td[3]/button/i');
        $I->see('spyroskregular@mindaria.com');
        $I->see('spyrosktestadmin@mindaria.com');
    }

    /**
     * @param \Step\Acceptance\Login $I
     */
    public function confirmUserAssociationViaFingerprints(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Confirm that users can be associated via fingerprints');
        $I->amOnPage('logout');
        $I->loginAsRegular();
        $I->amOnPage('logout');
        $I->loginAsAdmin();
        $I->amOnPage('spark/kiosk');
        $I->see('Users');
        $I->click('Users');
        $I->seeElement('#kiosk-users-search');
        $I->pressKey('#kiosk-users-search', '*', WebDriverKeys::ENTER);
        $I->wait(3);
        $I->see('spyroskregular@mindaria.com');
        $I->amOnPage('spark/kiosk#/users/18');
        $I->see('Related Users By Fingerprint');
        $I->see('Related Users By IP Address');
        $I->see('spyroskregular@mindaria.com');
        $I->see('spyrosktestadmin@mindaria.com');
    }
}
