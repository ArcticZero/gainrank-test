<?php

use Codeception\Example;

class RegistrationCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function registerTest(AcceptanceTester $I)
    {
        $email = 'subscriber1a@gainrank.com';
        $I->wantTo('Register a user account');
        $I->lookForwardTo('get to the user dashboard');

        $I->registerAccount('Subscriber 1A', $email, 'testpassword');

        $I->wait(3);
        $I->seeInCurrentUrl('home');
        $I->see('active users: last 24 hours');
    }

    public function gmailTest(AcceptanceTester $I)
    {
        $email = 't.est+extrastring@gmail.com';
        $clean_email = 'test@gmail.com';
        $I->wantTo('Register an account with a Gmail address');
        $I->lookForwardTo('see Gmail normalizer removing dots and any text after a plus from my email address');
        $I->amOnPage('/');
        $I->see('Get your free trial');
        $I->click('Get your free trial');
        $I->fillField('name', 'Tester');
        $I->fillField('email', $email);
        $I->fillField('password', 'testpassword');
        $I->fillField('password_confirmation', 'testpassword');
        $I->checkOption('[name=terms]');
        $I->click('.btn-primary');
        $I->wait(5);
        $I->seeInCurrentUrl('thank-you-for-registering');
        $I->see('Thank you for creating an account on Gainrank.
        A message with further instructions has been sent to your email address.');
        $I->receiveAnEmailToEmail($clean_email);
        $I->receiveAnEmailWithSubject('Gainrank Email Verification');
        $I->seeInEmailHtmlBody('Click here to verify your account');
        $matches = array();
        $body = $I->fetchLastMessage()['html_body'];
        preg_match_all('/<a[^>]+href=[\'"]http:\/\/.+?(\/.+?)[\'"][^>]*>/', $body, $matches);
        $I->amOnPage($matches[1][0]);
        $I->wait(3);
        $I->see('Account');
        $I->click('Account');
        $I->wait(3);
        $I->see('Active Users');
        $I->seeInCurrentUrl('home');
        $I->amOnPage('/settings');
        $I->wait(3);
        $I->seeInField('email', $clean_email);
    }

    /**
     * @dataprovider blockedEmailProvider
     */
    public function emailFilterTest(AcceptanceTester $I, Example $example)
    {
        $I->wantTo('Register an account using a blocked domain');
        $I->lookForwardTo('get an error saying the domain is blocked');
        $I->amOnPage('/');
        $I->see('Get your free trial');
        $I->click('Get your free trial');
        $I->fillField('name', 'Tester');
        $I->fillField('email', $example['email']);
        $I->fillField('password', 'testpassword');
        $I->fillField('password_confirmation', 'testpassword');
        $I->checkOption('[name=terms]');
        $I->click('.btn-primary');
        $I->wait(3);
        $I->see('domain is not allowed');
    }

    protected function blockedEmailProvider()
    {
        return [
            [ 'email' => 'test@mailinator.com' ],
            [ 'email' => 'test@zippymail.info' ],
        ];
    }
}
