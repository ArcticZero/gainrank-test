<?php


class HelpdeskCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function viewHelpdesk(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Access the Helpdesk');
        $I->loginAsRegular();
        $I->amOnPage('/settings');
        $I->see('My Tickets');
        $I->see('New Ticket');
        $I->click('New Ticket');
        $I->see('Subject');
        $I->see('Body');
        $I->see('Attachments');
    }
}
