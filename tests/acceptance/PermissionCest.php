<?php

use Step\Acceptance\Login;

class PermissionCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function adminCanAccessKiosk(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Access Kiosk as an admin');
        $I->loginAsAdmin();
    }

    public function adminCanAdministerAnnouncements(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Create a new Announcement');
        $I->loginAsAdmin();
        $I->see('Announcements');
        $I->click('Announcements');
        $I->fillField('announcement', 'Admin Test Announcement');
        $I->fillField('action_text', 'Test Text');
        $I->fillField('action_url', 'http://www.google.com');
        $I->click('Create');
        $I->wait(3);
        $I->see('Recent Announcements');
        $I->see(date('F jS, Y'));
        $I->amOnPage('/home');
        $I->click('//*[@id="spark-navbar-collapse"]/ul[2]/li[1]/a/div/i');
        $I->click('Announcements');
        $I->wait(3);
        $I->see('Admin Test Announcement');
    }

    // Blog posts have their own separate tests so here we're just checking access permissions
    public function adminCanAccessBlogPosts(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Access Blog Post Form as an admin');
        $I->loginAsAdmin();
        $I->see('Blog Posts');
        $I->click('Blog Posts');
        $I->wait(3);
        $I->see('Create Blog Post');
        $I->see('Blog Posts you create here will be displayed in the GainRank homepage.');
    }

    public function adminCanAdministerUsers(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Confirm that I can administer users as an admin');
        $I->loginAsAdmin();
        $I->see('Users');
        $I->click('Users');
        $I->seeElement('#kiosk-users-search');
        $I->pressKey('#kiosk-users-search', '*', WebDriverKeys::ENTER);
        $I->wait(3);
        $I->see('Blocked');
        $I->click('//*[@id="users"]/div/div[1]/div[2]/div[2]/table/tbody/tr[4]/td[4]/button');
        $I->wait(3);
        $I->see('Email Address: spyrosktestblocked@mindaria.com');
        $I->dontSeeCheckboxIsChecked('#admin');
        $I->dontSeeCheckboxIsChecked('#editor');
        $I->seeCheckboxIsChecked('#blocked');
        $I->dontSeeCheckboxIsChecked('#frauder');
        $I->checkOption('#frauder');
        $I->wait(3);
        $I->reloadPage();
        $I->dontSeeCheckboxIsChecked('#admin');
        $I->dontSeeCheckboxIsChecked('#editor');
        $I->seeCheckboxIsChecked('#blocked');
        $I->seeCheckboxIsChecked('#frauder');
        $I->uncheckOption('#frauder');
        $I->wait(3);
        $I->reloadPage();
        $I->dontSeeCheckboxIsChecked('#admin');
        $I->dontSeeCheckboxIsChecked('#editor');
        $I->seeCheckboxIsChecked('#blocked');
        $I->dontSeeCheckboxIsChecked('#frauder');
    }

    public function editorCanAccessKiosk(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Access Kiosk as an editor');
        $I->loginAsEditor();
    }

    public function editorCanAdministerAnnouncements(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Create a new Announcement');
        $I->loginAsEditor();
        $I->see('Announcements');
        $I->click('Announcements');
        $I->fillField('announcement', 'Editor Test Announcement');
        $I->fillField('action_text', 'Test Text');
        $I->fillField('action_url', 'http://www.google.com');
        $I->click('Create');
        $I->wait(3);
        $I->see('Recent Announcements');
        $I->see(date('F jS, Y'));
        $I->amOnPage('/home');
        $I->click('//*[@id="spark-navbar-collapse"]/ul[2]/li[1]/a/div/i');
        $I->click('Announcements');
        $I->wait(3);
        $I->see('Editor Test Announcement');
    }

    // Blog posts have their own separate tests so here we're just checking access permissions
    public function editorCanAccessBlogPosts(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Access Blog Post Form as an editor');
        $I->loginAsEditor();
        $I->see('Blog Posts');
        $I->click('Blog Posts');
        $I->wait(3);
        $I->see('Create Blog Post');
        $I->see('Blog Posts you create here will be displayed in the GainRank homepage.');
    }

    public function editorCanNotAdministerUsers(\Step\Acceptance\Login $I)
    {
        $I->wantTo('Confirm that I can not administer users as an editor');
        $I->loginAsEditor();
        $I->dontSee('Users', 'a');
        $I->amOnPage('/spark/kiosk#/users');
        $I->dontSee('Users', 'a');
    }

    public function regularUserCanNotAccessKiosk(\Step\Acceptance\Login $I)
    {
        $email = 'regularuser@example.com';
        $I->wantTo('Confirm that Kiosk is not accessible to regular users');
        $I->lookForwardTo('get to the user dashboard');

        $I->registerAccount('Regular User', $email, 'testpassword');

        $I->wait(3);
        $I->seeInCurrentUrl('home');
        $I->see('active users: last 24 hours');
        $I->click('//*[@id="spark-navbar-collapse"]/ul[2]/li[2]/a/img');
        $I->dontSee('Kiosk');
    }

    public function blockedUserCanNotAccessApplication(\Step\Acceptance\Login $I)
    {
        $I->loginAsBlocked();
    }

    public function adminCanAccessPaypalKiosk(Login $I)
    {
        $I->wantTo('Confirm that PayPal kiosk is accessible to admin');
        $I->loginAsAdmin();
        $I->see('PayPal Notification History');
        $I->click('PayPal Notification History');
        $I->wait(3);
        $I->see('Date Received');
    }

    public function editorCannotAccessPaypalKiosk(Login $I)
    {
        $I->wantTo('Confirm that PayPal kiosk is not accessible to editor');
        $I->loginAsEditor();
        $I->dontSee('PayPal Notification History');
    }

    public function adminCanAccessCoinbaseKiosk(Login $I)
    {
        $I->wantTo('Confirm that Coinbase kiosk is accessible to admin');
        $I->loginAsAdmin();
        $I->see('Coinbase Notification History');
        $I->click('Coinbase Notification History');
        $I->wait(3);
        $I->see('Date Received');
    }

    public function editorCannotAccessCoinbaseKiosk(Login $I)
    {
        $I->wantTo('Confirm that Coinbase kiosk is not accessible to editor');
        $I->loginAsEditor();
        $I->dontSee('Coinbase Notification History');
    }
}
