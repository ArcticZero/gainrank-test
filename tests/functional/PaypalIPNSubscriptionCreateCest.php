<?php


class PaypalIPNSubscriptionCreateCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    /* Commenting out as it was breaking locally
    TODO Ken
    // tests
    public function subscribe(FunctionalTester $I)
    {
        $I->wantTo('activate a subscription by sending an IPN');
        $I->lookForwardTo('get an active user subscription');
        $I->sendPOST('/settings/subscription/paypal/ipn', [
            "payment_cycle" => "Monthly",
            "txn_type" => "recurring_payment_profile_created",
            "last_name" => "Zamora",
            "initial_payment_status" => "Completed",
            "next_payment_date" => "03:00:00 Aug 09, 2017 PDT",
            "residence_country" => "US",
            "initial_payment_amount" => "0.01",
            "currency_code" => "USD",
            "time_created" => "14:31:21 Jul 09, 2017 PDT",
            "verify_sign" => "AQU0e5vuZCvSg-XJploSa.sGUDlpA4x9yATka3h9kZiaXcR6rKcODy4U",
            "period_type" => " Regular",
            "payer_status" => "verified",
            "test_ipn" => "1",
            "tax" => "0.00",
            "payer_email" => "gainrank-buyer@gainrank.com",
            "first_name" => "Victor",
            "receiver_email" => "gainrank-facilitator@gainrank.com",
            "payer_id" => "AJ4K72XBAUKB2",
            "product_type" => "1",
            "initial_payment_txn_id" => "2HW63188AE014254P",
            "shipping" => "0.00",
            "amount_per_cycle" => "0.01",
            "profile_status" => "Active",
            "charset" => "windows-1252",
            "notify_version" => "3.8",
            "amount" => "0.01",
            "outstanding_balance" => "0.00",
            "recurring_payment_id" => "I-XC609LGAFV23",
            "product_name" => "Gainrank Subscription (Basic)",
            "ipn_track_id" => "a22bdbfd6e67a"
        ]);
        $I->dontSeeResponseCodeIs(404);
        $I->dontSeeResponseCodeIs(500);
    }
    */
}
