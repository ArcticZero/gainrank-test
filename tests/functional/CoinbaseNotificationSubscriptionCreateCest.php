<?php


class CoinbaseNotificationSubscriptionCreateCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    // tests
    public function subscribe(FunctionalTester $I)
    {
        $payload = '
{"id":"7983f5e0-6030-5370-b4a3-bcb37b4b257b","type":"wallet:orders:paid","data":{"id":"18cfe6a5-d5f7-560a-8eb3-7de9e32099cb","code":"P2LMZSVS","type":"order","name":"Balance for Gainrank Pending Subscription ID# 1","description":"Basic","amount":{"amount":"0.00000007","currency":"BTC"},"receipt_url":"https:\/\/www.coinbase.com\/orders\/a12ba7b550b43f9ce4eed6cb582740ce\/receipt","resource":"order","resource_path":"\/v2\/orders\/18cfe6a5-d5f7-560a-8eb3-7de9e32099cb","status":"paid","overpaid":false,"bitcoin_amount":{"amount":"0.00000007","currency":"BTC"},"total_amount_received":{"amount":"0.00000007","currency":"BTC"},"payout_amount":null,"bitcoin_address":"1BrLMPjxifHxB1j6ZzFS8ugi9cL48xtp2C","refund_address":"1HVDxj7GfVr7smpLbeiHeX8jnCBdqVBQK3","bitcoin_uri":"bitcoin:1BrLMPjxifHxB1j6ZzFS8ugi9cL48xtp2C?amount=0.00000007&r=https:\/\/www.coinbase.com\/r\/5937151200724d067c928d7d","notifications_url":null,"paid_at":"2017-06-06T20:48:21Z","mispaid_at":null,"expires_at":"2017-06-06T21:03:18Z","metadata":[],"created_at":"2017-06-06T20:48:18Z","updated_at":"2017-06-06T20:48:21Z","customer_info":null,"transaction":{"id":"a07b7732-a666-54a8-ba8e-83330e1dbc98","resource":"transaction","resource_path":"\/v2\/accounts\/bc07534f-b5cb-58e5-a762-383b265819c8\/transactions\/a07b7732-a666-54a8-ba8e-83330e1dbc98"},"mispayments":[],"refunds":[]},"user":{"id":"58581c37-9680-5286-a1fe-48fd0f9690e3","resource":"user","resource_path":"\/v2\/users\/58581c37-9680-5286-a1fe-48fd0f9690e3"},"account":{"id":"bc07534f-b5cb-58e5-a762-383b265819c8","resource":"account","resource_path":"\/v2\/accounts\/bc07534f-b5cb-58e5-a762-383b265819c8"},"delivery_attempts":0,"created_at":"2017-06-06T20:48:21Z","resource":"notification","resource_path":"\/v2\/notifications\/7983f5e0-6030-5370-b4a3-bcb37b4b257b","additional_data":[]}';

        $I->wantTo('make sure the Coinbase notification listener is properly accepting input');
        $I->lookForwardTo('get a 200 or 422 response code from the notification listener');
        $I->sendPOST('/settings/subscription/coinbase/notification', json_decode($payload, true));
        $I->dontSeeResponseCodeIs(404);
        $I->dontSeeResponseCodeIs(500);
    }
}
