-- MySQL dump 10.13  Distrib 5.7.17-11, for Linux (x86_64)
--
-- Host: localhost    Database: gainrank
-- ------------------------------------------------------
-- Server version	5.7.17-11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad_campaign_bonus_types`
--

DROP TABLE IF EXISTS `ad_campaign_bonus_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_campaign_bonus_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reward_type` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_campaign_bonus_types`
--

LOCK TABLES `ad_campaign_bonus_types` WRITE;
/*!40000 ALTER TABLE `ad_campaign_bonus_types` DISABLE KEYS */;
INSERT INTO `ad_campaign_bonus_types` VALUES (1,'% bonus on first deposit','2017-07-10 07:57:30','2017-07-10 07:57:30',NULL),(2,'% off on first month','2017-07-10 07:57:30','2017-07-19 09:43:21',4),(3,'% off recurring','2017-07-10 07:57:30','2017-07-19 09:43:21',6),(4,'flat $ off on first month','2017-07-10 07:57:30','2017-07-19 09:43:21',3),(5,'flat $ off recurring','2017-07-10 07:57:30','2017-07-19 09:43:21',5),(6,'flat # bonus keywords to track','2017-07-10 07:57:30','2017-07-10 07:57:30',NULL);
/*!40000 ALTER TABLE `ad_campaign_bonus_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ad_campaign_bonuses`
--

DROP TABLE IF EXISTS `ad_campaign_bonuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_campaign_bonuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bonus_amount` int(11) NOT NULL,
  `ad_campaign_bonus_type_id` int(10) unsigned NOT NULL,
  `ad_campaign_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ad_campaign_bonuses_ad_campaign_bonus_type_id_foreign` (`ad_campaign_bonus_type_id`),
  KEY `ad_campaign_bonuses_ad_campaign_id_foreign` (`ad_campaign_id`),
  CONSTRAINT `ad_campaign_bonuses_ad_campaign_bonus_type_id_foreign` FOREIGN KEY (`ad_campaign_bonus_type_id`) REFERENCES `ad_campaign_bonus_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ad_campaign_bonuses_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_campaign_bonuses`
--

LOCK TABLES `ad_campaign_bonuses` WRITE;
/*!40000 ALTER TABLE `ad_campaign_bonuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ad_campaign_bonuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ad_campaigns`
--

DROP TABLE IF EXISTS `ad_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `max_uses` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_campaigns_path_unique` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_campaigns`
--

LOCK TABLES `ad_campaigns` WRITE;
/*!40000 ALTER TABLE `ad_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `ad_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliate_transactions`
--

DROP TABLE IF EXISTS `affiliate_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate_transactions` (
  `affiliate_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transacted_at` datetime NOT NULL,
  `type_id` int(11) NOT NULL,
  `is_mature` tinyint(1) NOT NULL DEFAULT '0',
  `mature_at` date NOT NULL,
  `details` json DEFAULT NULL,
  `reward_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reward_details` json DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `affiliate_transactions_user_id_foreign` (`user_id`),
  KEY `affiliate_transactions_affiliate_id_foreign` (`affiliate_id`),
  KEY `affiliate_transactions_is_mature_index` (`is_mature`),
  KEY `affiliate_transactions_mature_at_index` (`mature_at`),
  CONSTRAINT `affiliate_transactions_affiliate_id_foreign` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliates` (`id`) ON DELETE CASCADE,
  CONSTRAINT `affiliate_transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliate_transactions`
--

LOCK TABLES `affiliate_transactions` WRITE;
/*!40000 ALTER TABLE `affiliate_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliate_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliates`
--

DROP TABLE IF EXISTS `affiliates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payout_available` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payout_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliates_ref_id_unique` (`ref_id`),
  KEY `affiliates_user_id_foreign` (`user_id`),
  CONSTRAINT `affiliates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliates`
--

LOCK TABLES `affiliates` WRITE;
/*!40000 ALTER TABLE `affiliates` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alexa_settings`
--

DROP TABLE IF EXISTS `alexa_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alexa_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alexa_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alexa_settings`
--

LOCK TABLES `alexa_settings` WRITE;
/*!40000 ALTER TABLE `alexa_settings` DISABLE KEYS */;
INSERT INTO `alexa_settings` VALUES (1,NULL,NULL,1,'update_interval','60'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,0,'current_version','1'),(6,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_alexa.php'),(7,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_alexa.php'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'cost_per','1000'),(10,NULL,'2017-10-02 06:41:17',1,'cost_per_x','100');
/*!40000 ALTER TABLE `alexa_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alexa_statuses`
--

DROP TABLE IF EXISTS `alexa_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alexa_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alexa_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alexa_statuses`
--

LOCK TABLES `alexa_statuses` WRITE;
/*!40000 ALTER TABLE `alexa_statuses` DISABLE KEYS */;
INSERT INTO `alexa_statuses` VALUES (123,'ji','i','nin','i','in');
/*!40000 ALTER TABLE `alexa_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alexa_submissions`
--

DROP TABLE IF EXISTS `alexa_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alexa_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wanted_count` int(11) NOT NULL,
  `sent_count` int(11) NOT NULL,
  `cost_per_x` int(11) NOT NULL,
  `cost_per_amt` int(11) NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alexa_submissions_user_id_foreign` (`user_id`),
  KEY `alexa_submissions_user_id_index` (`user_id`),
  CONSTRAINT `alexa_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alexa_submissions`
--

LOCK TABLES `alexa_submissions` WRITE;
/*!40000 ALTER TABLE `alexa_submissions` DISABLE KEYS */;
INSERT INTO `alexa_submissions` VALUES (1,'2017-10-02 20:13:16','2017-10-02 20:13:16',1,'14','gainrank.com',123,0,100,1000,1,1,'192.168.50.101',1);
/*!40000 ALTER TABLE `alexa_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
INSERT INTO `announcements` VALUES ('171c47d4-69cd-4ab4-a33f-142feb3cff90',1,'test','','','2017-09-22 15:27:07','2017-09-22 15:27:07');
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_tokens`
--

DROP TABLE IF EXISTS `api_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_tokens` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `transient` tinyint(4) NOT NULL DEFAULT '0',
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_tokens_token_unique` (`token`),
  KEY `api_tokens_user_id_expires_at_index` (`user_id`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_tokens`
--

LOCK TABLES `api_tokens` WRITE;
/*!40000 ALTER TABLE `api_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coinbase_checkouts`
--

DROP TABLE IF EXISTS `coinbase_checkouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coinbase_checkouts` (
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkout_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkout_embed_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_campaign_id` int(10) unsigned DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `discount_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coinbase_checkouts_ad_campaign_id_foreign` (`ad_campaign_id`),
  CONSTRAINT `coinbase_checkouts_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coinbase_checkouts`
--

LOCK TABLES `coinbase_checkouts` WRITE;
/*!40000 ALTER TABLE `coinbase_checkouts` DISABLE KEYS */;
INSERT INTO `coinbase_checkouts` VALUES ('provider-id-1','cae996ce-552e-599b-8eb9-4a86cd4ece93','0526085f665b9a328a5529ffd10ba710',NULL,1,NULL);
/*!40000 ALTER TABLE `coinbase_checkouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coinbase_notifications`
--

DROP TABLE IF EXISTS `coinbase_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coinbase_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_details` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coinbase_notifications`
--

LOCK TABLES `coinbase_notifications` WRITE;
/*!40000 ALTER TABLE `coinbase_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `coinbase_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctr_settings`
--

DROP TABLE IF EXISTS `ctr_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctr_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ctr_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctr_settings`
--

LOCK TABLES `ctr_settings` WRITE;
/*!40000 ALTER TABLE `ctr_settings` DISABLE KEYS */;
INSERT INTO `ctr_settings` VALUES (1,NULL,NULL,0,'current_version','1'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_ctr.php'),(7,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_ctr.php'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'cost_per','1000'),(10,NULL,NULL,1,'cost_per_x','100');
/*!40000 ALTER TABLE `ctr_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctr_statuses`
--

DROP TABLE IF EXISTS `ctr_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctr_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ctr_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctr_statuses`
--

LOCK TABLES `ctr_statuses` WRITE;
/*!40000 ALTER TABLE `ctr_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ctr_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctr_submissions`
--

DROP TABLE IF EXISTS `ctr_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctr_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wanted_count` int(11) NOT NULL,
  `sent_count` int(11) NOT NULL,
  `cost_per_x` int(11) NOT NULL,
  `cost_per_amt` int(11) NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ctr_submissions_user_id_foreign` (`user_id`),
  KEY `ctr_submissions_user_id_index` (`user_id`),
  KEY `ctr_submissions_keyword_index` (`keyword`),
  CONSTRAINT `ctr_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctr_submissions`
--

LOCK TABLES `ctr_submissions` WRITE;
/*!40000 ALTER TABLE `ctr_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ctr_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fingerprint_recordings`
--

DROP TABLE IF EXISTS `fingerprint_recordings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fingerprint_recordings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fingerprint_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Stores both IPv4 and IPv6 Addresses hence VARCHAR(45)',
  PRIMARY KEY (`id`),
  KEY `fingerprint_recordings_fingerprint_id_foreign` (`fingerprint_id`),
  KEY `fingerprint_recordings_user_id_foreign` (`user_id`),
  CONSTRAINT `fingerprint_recordings_fingerprint_id_foreign` FOREIGN KEY (`fingerprint_id`) REFERENCES `fingerprints` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fingerprint_recordings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=677 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fingerprint_recordings`
--

LOCK TABLES `fingerprint_recordings` WRITE;
/*!40000 ALTER TABLE `fingerprint_recordings` DISABLE KEYS */;
/*!40000 ALTER TABLE `fingerprint_recordings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fingerprints`
--

DROP TABLE IF EXISTS `fingerprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fingerprints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fingerprints_hash_unique` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fingerprints`
--

LOCK TABLES `fingerprints` WRITE;
/*!40000 ALTER TABLE `fingerprints` DISABLE KEYS */;
/*!40000 ALTER TABLE `fingerprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitations`
--

DROP TABLE IF EXISTS `invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitations` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invitations_token_unique` (`token`),
  KEY `invitations_team_id_index` (`team_id`),
  KEY `invitations_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitations`
--

LOCK TABLES `invitations` WRITE;
/*!40000 ALTER TABLE `invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_reminders`
--

DROP TABLE IF EXISTS `invoice_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` int(11) NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_reminders_subscription_id_index` (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_reminders`
--

LOCK TABLES `invoice_reminders` WRITE;
/*!40000 ALTER TABLE `invoice_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `card_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoices_created_at_index` (`created_at`),
  KEY `invoices_user_id_index` (`user_id`),
  KEY `invoices_team_id_index` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2017_02_11_173613_create_performance_indicators_table',1),(2,'2017_02_11_173614_create_announcements_table',1),(3,'2017_02_11_173616_create_users_table',1),(4,'2017_02_11_173619_create_password_resets_table',1),(5,'2017_02_11_173623_create_api_tokens_table',1),(6,'2017_02_11_173628_create_subscriptions_table',1),(7,'2017_02_11_173634_create_invoices_table',1),(8,'2017_02_11_173641_create_notifications_table',1),(9,'2017_02_11_173649_create_teams_table',1),(10,'2017_02_11_173658_create_team_users_table',1),(11,'2017_02_11_173708_create_invitations_table',1),(12,'2017_02_26_155459_add_is_subscribed_to_user_table',1),(13,'2017_02_17_162416_create_posts_table',2),(14,'2017_03_04_173635_add_paypal_fields_to_subscription',3),(15,'2017_03_04_175247_make_stripe_fields_nullable_in_subscriptions',3),(16,'2017_03_04_182545_create_paypal_plans_table',3),(17,'2017_03_09_100348_add_provider_plan_name_to_subscriptions',3),(18,'2017_03_09_160613_add_billing_provider_to_subscriptions',3),(19,'2017_03_12_173021_add_last_payment_at_to_subscriptions',3),(20,'2017_03_13_213904_create_user_paypal_plans_table',3),(21,'2017_03_18_190909_rename_user_paypal_plans_to_user_pending_subscriptions',3),(22,'2017_04_02_201515_add_has_used_trial_to_users',3),(23,'2017_04_09_205803_create_invoice_reminders_table',3),(24,'2017_04_22_191553_add_is_cancelled_to_subscriptions',3),(25,'2017_04_23_203337_make_paypal_plan_id_nullable_in_paypal_plans',4),(26,'2017_03_29_145006_create_tag_tables',5),(27,'2017_04_03_054651_create_media_table',5),(28,'2017_05_07_174545_create_coinbase_checkouts_table',6),(29,'2017_05_08_182042_create_coinbase_notifications_table',6),(30,'2017_05_08_183446_create_paypal_notifications_table',7),(31,'2017_05_20_093449_create_permission_tables',8),(32,'2017_05_20_103808_add_default_roles_permissions',8),(33,'2017_05_21_070520_add_verification_to_user_table',8),(34,'2017_06_08_165936_create_paypal_and_coinbase_permissions',9),(35,'2017_06_02_063006_fingerprints_table',10),(36,'2017_06_02_063727_fingerprint_recordings_table',10),(37,'2017_06_05_074303_permissions_to_view_fingerprints',10),(38,'2017_06_15_135541_add_frauder_role_and_capture_ip_address',11),(39,'2017_06_05_150636_create_partial_payments_table',12),(40,'2017_06_09_113326_add_pending_reason_to_user_pending_subscriptions',12),(41,'2017_06_09_113442_add_pending_reason_to_subscriptions',12),(42,'2017_06_12_040827_add_is_affiliate_to_users_table',12),(43,'2017_06_12_140550_create_affiliates_table',12),(44,'2017_06_12_140620_create_affiliate_transactions_table',12),(45,'2017_06_14_081938_add_affiliate_id_to_users_table',12),(46,'2017_06_16_200945_add_credits_to_users_table',12),(47,'2017_06_22_070851_create_soft_launch_invitations_table',12),(48,'2017_06_22_092753_soft_launch_shortlist_table',12),(49,'2017_06_18_091736_ad_campaign_bonus_types_table',13),(50,'2017_06_18_091816_ad_campaigns_table',13),(51,'2017_06_18_091955_ad_campaign_bonuses_table',13),(52,'2017_06_18_092449_add_ad_campaign_bonus_to_user_table',13),(53,'2017_07_04_175613_create_referral_logs_table',14),(54,'2017_07_05_180554_add_referrer_affiliate_id_to_users_table',14),(55,'2017_07_13_170745_remove_affiliate_id_from_users_table',14),(56,'2017_07_14_092804_add_ad_campaign_id_to_paypal_plans',14),(57,'2017_07_14_100509_add_ad_campaign_id_to_coinbase_checkouts',14),(58,'2017_07_14_142026_add_reward_type_to_ad_campaign_bonus_types_table',14),(59,'2017_07_14_225311_add_discount_type_to_coinbase_checkouts_table',14),(60,'2017_06_27_083423_helpdesk_permissions',15),(61,'2017_05_29_201724_add_banned_at_to_users_table',16),(62,'2017_08_30_103743_email_marketing',16),(63,'2017_08_26_143313_add_tfa_secret_to_users_table',17),(64,'2017_08_30_103227_add_tfa_enable_to_users_table',17),(65,'2017_08_31_174956_create_tfa_backup_codes_table',17),(66,'2017_08_31_203940_add_generated_backup_codes_at_to_users_table',17),(67,'2017_09_11_210041_add_verified_role',18),(68,'2017_08_22_132856_add_show_guide_to_users_table',19),(69,'2017_08_07_160456_create_sales_invoices_table',20),(70,'2017_08_07_165033_create_sales_invoice_items_table',20),(71,'2017_08_14_031818_create_payment_logs_table',20),(72,'2017_08_14_084206_add_ext_invoice_id_to_sales_invoices_table',20),(73,'2017_06_22_181013_add_paypal_payer_info_to_users_table',21),(74,'2017_06_25_062714_create_pending_orders_table',21),(75,'2017_06_26_093835_create_pending_order_actions_table',21),(76,'2017_07_07_131625_create_user_paypal_verified_emails_table',21),(77,'2017_07_07_173909_add_confirmation_key_to_pending_orders',21),(78,'2017_07_09_174809_add_user_id_to_pending_orders_table',21),(91,'2017_09_20_082328_create_rank_tracking_versions_table',22),(92,'2017_09_20_082953_create_rank_tracking_settings_table',22),(93,'2017_09_25_083149_create_rank_tracking_engines_table',22),(94,'2017_09_25_083411_create_rank_tracking_table',22),(95,'2017_09_25_083422_create_rank_tracking_ranks_table',22),(96,'2017_09_25_085125_create_rank_tracking_favorites_table',23),(97,'2017_09_27_021649_create_alexa_submissions_table',24),(98,'2017_09_27_024102_create_alexa_settings_table',24),(99,'2017_09_28_042106_create_c_t_r_settings_table',25),(100,'2017_09_28_042123_create_c_t_r_submissions_table',25),(101,'2017_09_28_191405_create_rank_tracking_statuses_table',26),(104,'2017_09_28_192603_create_alexa_statuses_table',27),(105,'2017_09_28_192721_add_id_status_to_rank_tracking_table',27),(107,'2017_09_28_193020_create_c_t_r_statuses_table',28),(110,'2017_09_28_195931_drop_version_foreign_key_from_rank_tracking_settings_table',29),(111,'2017_09_30_233109_create_traffic_submissions_table',30),(112,'2017_09_30_233118_create_traffic_settings_table',30),(113,'2017_10_02_012618_create_wayback_settings_table',31),(114,'2017_10_02_012625_create_wayback_submissions_table',31),(115,'2017_10_02_082628_create_traffic_statuses_table',32),(116,'2017_10_02_084738_create_wayback_statuses_table',33),(117,'2017_10_05_175351_add_indexes_to_rank_tracking_table',34),(118,'2017_10_05_175404_add_indexes_to_rank_tracking_ranks_table',34),(119,'2017_10_05_175417_add_indexes_to_rank_tracking_favorites_table',34),(120,'2017_10_05_175424_add_indexes_to_alexa_submissions_table',34),(121,'2017_10_05_175435_add_indexes_to_c_t_r_submissions_table',34),(122,'2017_10_05_175447_add_indexes_to_traffic_submissions_table',34),(123,'2017_10_05_175454_add_indexes_to_wayback_submissions_table',34);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,1,'App\\User'),(6,1,'App\\User'),(7,1,'App\\User'),(1,6,'App\\User'),(6,6,'App\\User'),(7,6,'App\\User'),(1,11,'App\\User'),(6,11,'App\\User'),(7,11,'App\\User'),(2,16,'App\\User'),(6,16,'App\\User'),(7,16,'App\\User'),(4,17,'App\\User'),(6,17,'App\\User'),(7,17,'App\\User'),(3,18,'App\\User'),(6,18,'App\\User'),(7,18,'App\\User'),(3,37,'App\\User'),(6,37,'App\\User'),(7,37,'App\\User'),(3,38,'App\\User'),(6,38,'App\\User'),(7,38,'App\\User'),(3,61,'App\\User'),(6,61,'App\\User'),(7,61,'App\\User'),(3,62,'App\\User'),(6,62,'App\\User'),(7,62,'App\\User'),(3,63,'App\\User'),(6,63,'App\\User'),(7,63,'App\\User'),(3,64,'App\\User'),(6,64,'App\\User'),(7,64,'App\\User'),(3,65,'App\\User'),(6,65,'App\\User'),(7,65,'App\\User'),(3,66,'App\\User'),(6,66,'App\\User'),(7,66,'App\\User'),(3,67,'App\\User'),(6,67,'App\\User'),(7,67,'App\\User'),(3,68,'App\\User'),(6,68,'App\\User'),(7,68,'App\\User'),(3,70,'App\\User'),(6,70,'App\\User'),(7,70,'App\\User');
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_url` text COLLATE utf8mb4_unicode_ci,
  `read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_user_id_created_at_index` (`user_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES ('4bb7a4ba-42f9-4e17-907d-9434e7e5a2f6',61,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,0,'2017-10-03 14:19:13','2017-10-03 14:19:13'),('ab5e0a7c-530f-42ee-a9b3-eae2a6884a2e',11,NULL,'fa-clock-o','Your trial period will expire on April 18th.','Subscribe','/settings#/subscription',0,'2017-04-08 17:08:43','2017-04-08 17:08:43'),('bf6036d4-6de4-4940-96c4-2cc56054dc60',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-09-27 19:25:45','2017-09-27 19:25:48'),('cf88638f-2cf8-4e30-bc21-ffc858bb4a44',6,NULL,'fa-clock-o','Your trial period will expire on March 24th.','Subscribe','/settings#/subscription',1,'2017-03-14 14:31:02','2017-03-14 15:03:58'),('f7c3533f-1527-4add-a853-5fb88bcd4c3f',1,NULL,'fa-clock-o','Your trial period will expire on March 11th.','Subscribe','/settings#/subscription',1,'2017-03-01 15:26:23','2017-09-27 19:25:48');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partial_payments`
--

DROP TABLE IF EXISTS `partial_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partial_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `pending_subscription_id` int(11) DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_paid` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `total_amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `partial_payments_user_id_index` (`user_id`),
  KEY `partial_payments_subscription_id_index` (`subscription_id`),
  KEY `partial_payments_pending_subscription_id_index` (`pending_subscription_id`),
  KEY `partial_payments_order_id_index` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partial_payments`
--

LOCK TABLES `partial_payments` WRITE;
/*!40000 ALTER TABLE `partial_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `partial_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_logs`
--

DROP TABLE IF EXISTS `payment_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sales_invoice_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `payment_processor_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason_data` json NOT NULL,
  `notification_data` json NOT NULL,
  `dollar_amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_logs_sales_invoice_id_foreign` (`sales_invoice_id`),
  KEY `payment_logs_user_id_foreign` (`user_id`),
  CONSTRAINT `payment_logs_sales_invoice_id_foreign` FOREIGN KEY (`sales_invoice_id`) REFERENCES `sales_invoices` (`id`) ON DELETE SET NULL,
  CONSTRAINT `payment_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_logs`
--

LOCK TABLES `payment_logs` WRITE;
/*!40000 ALTER TABLE `payment_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_notifications`
--

DROP TABLE IF EXISTS `paypal_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_details` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_notifications`
--

LOCK TABLES `paypal_notifications` WRITE;
/*!40000 ALTER TABLE `paypal_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_plans`
--

DROP TABLE IF EXISTS `paypal_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_plans` (
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_campaign_id` int(10) unsigned DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `paypal_plans_ad_campaign_id_foreign` (`ad_campaign_id`),
  CONSTRAINT `paypal_plans_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_plans`
--

LOCK TABLES `paypal_plans` WRITE;
/*!40000 ALTER TABLE `paypal_plans` DISABLE KEYS */;
INSERT INTO `paypal_plans` VALUES ('provider-id-1','P-4PE90897064052640HCRFFEQ',NULL,1);
/*!40000 ALTER TABLE `paypal_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pending_order_actions`
--

DROP TABLE IF EXISTS `pending_order_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_order_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pending_order_actions_user_id_foreign` (`user_id`),
  KEY `pending_order_actions_order_id_foreign` (`order_id`),
  CONSTRAINT `pending_order_actions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `pending_orders` (`id`),
  CONSTRAINT `pending_order_actions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pending_order_actions`
--

LOCK TABLES `pending_order_actions` WRITE;
/*!40000 ALTER TABLE `pending_order_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pending_order_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pending_orders`
--

DROP TABLE IF EXISTS `pending_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_type` int(11) NOT NULL,
  `billing_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_agreement_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pending_subscription_id` int(10) unsigned DEFAULT NULL,
  `request_details` json DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `confirmation_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation_key_expires_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pending_orders_pending_subscription_id_foreign` (`pending_subscription_id`),
  KEY `pending_orders_user_id_foreign` (`user_id`),
  CONSTRAINT `pending_orders_pending_subscription_id_foreign` FOREIGN KEY (`pending_subscription_id`) REFERENCES `user_pending_subscriptions` (`id`) ON DELETE SET NULL,
  CONSTRAINT `pending_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pending_orders`
--

LOCK TABLES `pending_orders` WRITE;
/*!40000 ALTER TABLE `pending_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `pending_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performance_indicators`
--

DROP TABLE IF EXISTS `performance_indicators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `performance_indicators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `monthly_recurring_revenue` decimal(8,2) NOT NULL,
  `yearly_recurring_revenue` decimal(8,2) NOT NULL,
  `daily_volume` decimal(8,2) NOT NULL,
  `new_users` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `performance_indicators_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performance_indicators`
--

LOCK TABLES `performance_indicators` WRITE;
/*!40000 ALTER TABLE `performance_indicators` DISABLE KEYS */;
/*!40000 ALTER TABLE `performance_indicators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'access kiosk','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(2,'administer announcements','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(3,'administer posts','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(4,'administer users','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(5,'assign roles','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(6,'administer paypal','web','2017-06-08 17:08:43','2017-06-08 17:08:43'),(7,'administer coinbase','web','2017-06-08 17:08:43','2017-06-08 17:08:43'),(8,'view fingerprints','web','2017-06-13 16:53:17','2017-06-13 16:53:17'),(9,'invite users','web','2017-07-10 07:41:02','2017-07-10 07:41:02'),(10,'administer ad campaigns','web','2017-07-10 07:57:30','2017-07-10 07:57:30'),(11,'open new support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49'),(12,'reply to support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_title_unique` (`title`),
  UNIQUE KEY `posts_slug_unique` (`slug`),
  KEY `posts_user_id_foreign` (`user_id`),
  CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (8,6,'Try this','<p>Ahahah</p>','try-this',1,'2017-04-08 19:13:02','2017-04-08 19:10:35','2017-04-08 19:13:02'),(9,6,'one more','<p>Ahahah</p>','one-more',1,'2017-04-08 19:12:59','2017-04-08 19:12:47','2017-04-08 19:12:59');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking`
--

DROP TABLE IF EXISTS `rank_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `engine_id` int(10) unsigned NOT NULL,
  `country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `stopped_at` timestamp NULL DEFAULT NULL,
  `id_status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_user_id_foreign` (`user_id`),
  KEY `rank_tracking_engine_id_foreign` (`engine_id`),
  KEY `rank_tracking_domain_index` (`domain`),
  KEY `rank_tracking_version_foreign` (`version`),
  KEY `rank_tracking_user_id_index` (`user_id`),
  KEY `rank_tracking_submission_id_index` (`submission_id`),
  KEY `rank_tracking_keyword_index` (`keyword`),
  CONSTRAINT `rank_tracking_engine_id_foreign` FOREIGN KEY (`engine_id`) REFERENCES `rank_tracking_engines` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_version_foreign` FOREIGN KEY (`version`) REFERENCES `rank_tracking_versions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking`
--

LOCK TABLES `rank_tracking` WRITE;
/*!40000 ALTER TABLE `rank_tracking` DISABLE KEYS */;
INSERT INTO `rank_tracking` VALUES (6,'2017-10-02 19:23:53','2017-10-02 19:23:53',1,'12','gainrank.com','alexa seo',1,'US',1,NULL,1),(7,'2017-10-02 19:23:54','2017-10-02 19:23:54',1,'13','gainrank.com','awesome seo',1,'US',1,NULL,1);
/*!40000 ALTER TABLE `rank_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_engines`
--

DROP TABLE IF EXISTS `rank_tracking_engines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_engines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_engines`
--

LOCK TABLES `rank_tracking_engines` WRITE;
/*!40000 ALTER TABLE `rank_tracking_engines` DISABLE KEYS */;
INSERT INTO `rank_tracking_engines` VALUES (1,'Google','google.com','US',1);
/*!40000 ALTER TABLE `rank_tracking_engines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_favorites`
--

DROP TABLE IF EXISTS `rank_tracking_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `track_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_favorites_user_id_foreign` (`user_id`),
  KEY `rank_tracking_favorites_track_id_foreign` (`track_id`),
  KEY `rank_tracking_favorites_user_id_index` (`user_id`),
  CONSTRAINT `rank_tracking_favorites_track_id_foreign` FOREIGN KEY (`track_id`) REFERENCES `rank_tracking` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_favorites`
--

LOCK TABLES `rank_tracking_favorites` WRITE;
/*!40000 ALTER TABLE `rank_tracking_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `rank_tracking_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_ranks`
--

DROP TABLE IF EXISTS `rank_tracking_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `track_id` int(10) unsigned NOT NULL,
  `rank` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_ranks_track_id_foreign` (`track_id`),
  KEY `rank_tracking_ranks_rank_index` (`rank`),
  CONSTRAINT `rank_tracking_ranks_track_id_foreign` FOREIGN KEY (`track_id`) REFERENCES `rank_tracking` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_ranks`
--

LOCK TABLES `rank_tracking_ranks` WRITE;
/*!40000 ALTER TABLE `rank_tracking_ranks` DISABLE KEYS */;
/*!40000 ALTER TABLE `rank_tracking_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_settings`
--

DROP TABLE IF EXISTS `rank_tracking_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_settings_name_version_unique` (`name`,`version`),
  KEY `rank_tracking_settings_version_foreign` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_settings`
--

LOCK TABLES `rank_tracking_settings` WRITE;
/*!40000 ALTER TABLE `rank_tracking_settings` DISABLE KEYS */;
INSERT INTO `rank_tracking_settings` VALUES (1,NULL,'2017-10-01 21:21:55',1,'update_interval','60'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,1,'current_version','1'),(6,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_alexa.php'),(7,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_alexa.php'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'cost_per','1000'),(10,NULL,'2017-10-01 21:22:05',1,'cost_per_x','100');
/*!40000 ALTER TABLE `rank_tracking_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_statuses`
--

DROP TABLE IF EXISTS `rank_tracking_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_statuses`
--

LOCK TABLES `rank_tracking_statuses` WRITE;
/*!40000 ALTER TABLE `rank_tracking_statuses` DISABLE KEYS */;
INSERT INTO `rank_tracking_statuses` VALUES (111,'1233','4532','435','634','345'),(200,'Ok','Ok!!','Ok!sdfsdf','Ok Staff','http://url.com'),(345,'ij','iji','ih','hnb','ii');
/*!40000 ALTER TABLE `rank_tracking_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_versions`
--

DROP TABLE IF EXISTS `rank_tracking_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_versions` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_versions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_versions`
--

LOCK TABLES `rank_tracking_versions` WRITE;
/*!40000 ALTER TABLE `rank_tracking_versions` DISABLE KEYS */;
INSERT INTO `rank_tracking_versions` VALUES (NULL,NULL,1,'1.0');
/*!40000 ALTER TABLE `rank_tracking_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referral_logs`
--

DROP TABLE IF EXISTS `referral_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referral_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` json DEFAULT NULL,
  `fingerprint_id` int(10) unsigned DEFAULT NULL,
  `affiliate_id` int(10) unsigned DEFAULT NULL,
  `referred_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `referral_logs_fingerprint_id_foreign` (`fingerprint_id`),
  KEY `referral_logs_affiliate_id_foreign` (`affiliate_id`),
  KEY `referral_logs_referred_user_id_foreign` (`referred_user_id`),
  CONSTRAINT `referral_logs_affiliate_id_foreign` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliates` (`id`) ON DELETE SET NULL,
  CONSTRAINT `referral_logs_fingerprint_id_foreign` FOREIGN KEY (`fingerprint_id`) REFERENCES `fingerprints` (`id`) ON DELETE SET NULL,
  CONSTRAINT `referral_logs_referred_user_id_foreign` FOREIGN KEY (`referred_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referral_logs`
--

LOCK TABLES `referral_logs` WRITE;
/*!40000 ALTER TABLE `referral_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `referral_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,2),(2,2),(3,2),(9,3),(11,6),(12,7);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(2,'editor','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(3,'regular user','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(4,'blocked','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(5,'frauder','web','2017-06-15 16:29:24','2017-06-15 16:29:24'),(6,'can open new support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49'),(7,'can reply to support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49'),(8,'verified','web','2017-09-16 16:37:34','2017-09-16 16:37:34');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoice_items`
--

DROP TABLE IF EXISTS `sales_invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sales_invoice_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_invoice_items_sales_invoice_id_foreign` (`sales_invoice_id`),
  CONSTRAINT `sales_invoice_items_sales_invoice_id_foreign` FOREIGN KEY (`sales_invoice_id`) REFERENCES `sales_invoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoice_items`
--

LOCK TABLES `sales_invoice_items` WRITE;
/*!40000 ALTER TABLE `sales_invoice_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoice_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoices`
--

DROP TABLE IF EXISTS `sales_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `apply_at` timestamp NULL DEFAULT NULL,
  `due_at` timestamp NULL DEFAULT NULL,
  `total_amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `ext_invoice_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_invoices_user_id_foreign` (`user_id`),
  CONSTRAINT `sales_invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoices`
--

LOCK TABLES `sales_invoices` WRITE;
/*!40000 ALTER TABLE `sales_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soft_launch_invitations`
--

DROP TABLE IF EXISTS `soft_launch_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soft_launch_invitations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `invitee_user_id` int(10) unsigned DEFAULT NULL,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `soft_launch_invitations_token_unique` (`token`),
  KEY `soft_launch_invitations_user_id_foreign` (`user_id`),
  KEY `soft_launch_invitations_invitee_user_id_foreign` (`invitee_user_id`),
  CONSTRAINT `soft_launch_invitations_invitee_user_id_foreign` FOREIGN KEY (`invitee_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `soft_launch_invitations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soft_launch_invitations`
--

LOCK TABLES `soft_launch_invitations` WRITE;
/*!40000 ALTER TABLE `soft_launch_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `soft_launch_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soft_launch_shortlist`
--

DROP TABLE IF EXISTS `soft_launch_shortlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soft_launch_shortlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `soft_launch_shortlist_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soft_launch_shortlist`
--

LOCK TABLES `soft_launch_shortlist` WRITE;
/*!40000 ALTER TABLE `soft_launch_shortlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `soft_launch_shortlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_agreement_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_plan_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_payment_at` timestamp NULL DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `pending_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggables`
--

DROP TABLE IF EXISTS `taggables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggables` (
  `tag_id` int(10) unsigned NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `taggables_tag_id_foreign` (`tag_id`),
  CONSTRAINT `taggables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggables`
--

LOCK TABLES `taggables` WRITE;
/*!40000 ALTER TABLE `taggables` DISABLE KEYS */;
INSERT INTO `taggables` VALUES (1,9,'App\\Post');
/*!40000 ALTER TABLE `taggables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` json NOT NULL,
  `slug` json NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_column` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'{\"en\": \"dsds\"}','{\"en\": \"dsds\"}',NULL,1,'2017-04-08 19:12:47','2017-04-08 19:12:47');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_subscriptions`
--

DROP TABLE IF EXISTS `team_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_subscriptions`
--

LOCK TABLES `team_subscriptions` WRITE;
/*!40000 ALTER TABLE `team_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_users`
--

DROP TABLE IF EXISTS `team_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_users` (
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `team_users_team_id_user_id_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_users`
--

LOCK TABLES `team_users` WRITE;
/*!40000 ALTER TABLE `team_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_url` text COLLATE utf8mb4_unicode_ci,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_billing_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_billing_information` text COLLATE utf8mb4_unicode_ci,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teams_slug_unique` (`slug`),
  KEY `teams_owner_id_index` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tfa_backup_codes`
--

DROP TABLE IF EXISTS `tfa_backup_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tfa_backup_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tfa_backup_codes_user_id_foreign` (`user_id`),
  CONSTRAINT `tfa_backup_codes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tfa_backup_codes`
--

LOCK TABLES `tfa_backup_codes` WRITE;
/*!40000 ALTER TABLE `tfa_backup_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tfa_backup_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_settings`
--

DROP TABLE IF EXISTS `traffic_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `traffic_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_settings`
--

LOCK TABLES `traffic_settings` WRITE;
/*!40000 ALTER TABLE `traffic_settings` DISABLE KEYS */;
INSERT INTO `traffic_settings` VALUES (1,NULL,NULL,0,'current_version','1'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_traffic.php'),(6,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_traffic.php'),(7,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(8,NULL,NULL,1,'cost_per','1000'),(9,NULL,NULL,1,'cost_per_x','100'),(10,NULL,NULL,1,'update_interval','60');
/*!40000 ALTER TABLE `traffic_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_statuses`
--

DROP TABLE IF EXISTS `traffic_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `traffic_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_statuses`
--

LOCK TABLES `traffic_statuses` WRITE;
/*!40000 ALTER TABLE `traffic_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `traffic_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_submissions`
--

DROP TABLE IF EXISTS `traffic_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wanted_count` int(11) NOT NULL,
  `sent_count` int(11) NOT NULL,
  `cost_per_x` int(11) NOT NULL,
  `cost_per_amt` int(11) NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `traffic_submissions_user_id_foreign` (`user_id`),
  KEY `traffic_submissions_user_id_index` (`user_id`),
  CONSTRAINT `traffic_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_submissions`
--

LOCK TABLES `traffic_submissions` WRITE;
/*!40000 ALTER TABLE `traffic_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `traffic_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_paypal_verified_emails`
--

DROP TABLE IF EXISTS `user_paypal_verified_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_paypal_verified_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_paypal_verified_emails_user_id_foreign` (`user_id`),
  CONSTRAINT `user_paypal_verified_emails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_paypal_verified_emails`
--

LOCK TABLES `user_paypal_verified_emails` WRITE;
/*!40000 ALTER TABLE `user_paypal_verified_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_paypal_verified_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_pending_subscriptions`
--

DROP TABLE IF EXISTS `user_pending_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_pending_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `agreement_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_plan_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `billing_provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pending_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_paypal_plans_user_id_index` (`user_id`),
  KEY `user_paypal_plans_paypal_agreement_id_index` (`agreement_id`),
  KEY `user_paypal_plans_provider_plan_name_index` (`provider_plan_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_pending_subscriptions`
--

LOCK TABLES `user_pending_subscriptions` WRITE;
/*!40000 ALTER TABLE `user_pending_subscriptions` DISABLE KEYS */;
INSERT INTO `user_pending_subscriptions` VALUES (1,1,NULL,'provider-id-1','2017-07-10 14:49:15','2017-07-10 14:49:15','paypal_recurring',NULL);
/*!40000 ALTER TABLE `user_pending_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_url` text COLLATE utf8mb4_unicode_ci,
  `uses_two_factor_auth` tinyint(4) NOT NULL DEFAULT '0',
  `authy_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_reset_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` int(11) DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_billing_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_billing_information` text COLLATE utf8mb4_unicode_ci,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `last_read_announcements_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT '0',
  `has_used_trial` tinyint(1) NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `verification_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helpdesk_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_affiliate` tinyint(1) NOT NULL DEFAULT '0',
  `credits` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ad_campaign_id` int(10) unsigned DEFAULT NULL,
  `referrer_affiliate_id` int(10) unsigned DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  `email_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tfa_secret` varchar(17) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tfa_enable` tinyint(1) NOT NULL DEFAULT '0',
  `generated_backup_codes_at` timestamp NULL DEFAULT NULL,
  `show_guide` tinyint(1) NOT NULL DEFAULT '1',
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_ad_campaign_id_foreign` (`ad_campaign_id`),
  KEY `users_referrer_affiliate_id_foreign` (`referrer_affiliate_id`),
  CONSTRAINT `users_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE SET NULL,
  CONSTRAINT `users_referrer_affiliate_id_foreign` FOREIGN KEY (`referrer_affiliate_id`) REFERENCES `affiliates` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Kendrick Chan','kc@jankstudio.com','$2y$10$cJLbLUVbBUPQi8GV1YwzXeIs01nFOfpDBO4idamrgoBh.X5K4aXoG','E4gDBZzFaqm2GshBfgQP5mbgcFAWBPNtHos8VMN4qXGcdKt5Xo043XcIPsKz',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-11 15:26:23','2017-09-26 12:03:31','2017-03-01 15:26:23','2017-09-27 19:25:41',1,0,0,'096a3b082735a96bbcb52362350412f4fdf525eb5dbc7d9494696d05a2c59560',NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(6,'Spyros K','spyrosk@mindaria.com','$2y$10$dw/GV1r..wBukKtpMMs/iOo3B01JOEbbNg7VGN.Fv0AjFhPjGBhk6','64qKOlqPJZONeuOjanWUR9aBIUUJFfXilllqR5g98cWpyyUOSDuRNXJOcUQS',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-24 14:31:02','2017-03-14 14:31:02','2017-03-14 14:31:02','2017-03-14 14:31:02',1,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(11,'Test Admin','spyrosktestadmin@mindaria.com','$2y$10$2uBy3P1dbT0JkV9VsZMUv.AC6Idao2cjeTvFGIy/3qDPv06VSTO2G',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-18 17:08:43','2017-04-08 17:08:43','2017-04-08 17:08:43','2017-04-08 17:08:43',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(16,'Test Editor','spyrosktesteditor@mindaria.com','$2y$10$ryjxI7wVFDPDgMQu9DuhLO0V1uMGq.XEKso2SEOjXYEPSNIv8iSae',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-31 07:51:03','2017-05-31 07:51:03','2017-05-31 07:51:03','2017-05-31 07:51:03',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(17,'Blocked','spyrosktestblocked@mindaria.com','$2y$10$DE0OV2d1g9pvdBDe70QDqeX5uWqNKF7t0E8IHqx3B5TVEN8fJJKfW',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-31 08:47:01','2017-05-31 08:47:01','2017-05-31 08:47:01','2017-05-31 08:47:01',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(18,'Regular User','spyroskregular@mindaria.com','$2y$10$d.W.4Tbb24u2mydiDpXTL.Vm.3EO3vJqwHRBsYpEh9Tae9iIvGe5a',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-06-13 16:54:00','2017-06-13 16:54:00','2017-06-13 16:54:00','2017-06-13 16:54:00',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(37,'Test','test4@jankstudio.com','$2y$10$JjikR.MnPvS9VZnQn3oRhu8j9dJHQXp3330bltiscQoVo6P3D1DrK',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-14 04:59:16','2017-09-14 04:59:16','2017-09-14 04:59:16','2017-09-14 04:59:20',0,0,0,'cffcf75831cf9359892686201ca6b7b907c9029b64eef836f0b243d740fac3a6','920ab0a2',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(38,'Test Ticket Sender','test8@jankstudio.com','$2y$10$OdT8buMNoxERj9CH8E67EuMWosI5CJC8pw4iHnEZJ8euviRUqnX.6','14uf7ZVE8KyGVFAJ49TR2BYRrlkcYKaRmDIoAGbFO9aTeMUBsZ3imxtz0vmG',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-15 04:42:50','2017-09-15 04:42:50','2017-09-15 04:42:50','2017-09-15 04:42:56',0,0,1,NULL,'181e674e',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(61,'Test Person X','testpersonx@gmail.com','$2y$10$qRBCUP8TTCgKtyUbHGXAf.lEhTAd6RuDpbcpouUG.LLWZoSQE1Eoe','vBSG3ayyq9Tvffg6QPpZUiinQ5dPCyWy2NUYJTzGjvY8fyGLHaHnF4CITSU9',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:18:32','2017-10-03 14:18:32','2017-10-03 14:18:32','2017-10-03 14:19:10',0,0,0,'e43971e0bc9e73051b86ad4ea76b16c7cb33731bae343fa697ccf14114987016','9e4fd87b',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(62,'Test Person Z','testpersonz@gmail.com','$2y$10$6tW3jDJ8BiNOMi5ezQUUQ.A5ejZX5FS9nX4IVhcyMoarrGdxByv.S','kSCQivMlOBHjkgkkEUWx1QlgTnjdNpfLzbMZxLRd0V4TPgC90XpjzZb5jptP',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:22:17','2017-10-03 14:22:17','2017-10-03 14:22:17','2017-10-03 14:22:23',0,0,0,'e29bc6a3c88fae5338ce972914110487dc3cbf6ac8be4ebb98fe38ae3cd6d1da','c8db349e',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(63,'Test Person A','testpersona@gmail.com','$2y$10$xJksF9n2EOltpKNWLeXReOxxhX4YseErcAMoirTnspMiIns.qc6M2','jk0QU3uy86ZdyKyqsHOiXVw4fXjRbbRMMt9B2WLF4Q9amVU2jLhjb5sJm6ze',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:22:38','2017-10-03 14:22:38','2017-10-03 14:22:38','2017-10-03 14:22:44',0,0,0,'eabb595eb13bf4fd72992e5f8e201c2fe1b57feb5ec8ee58a8aa4a9ba46bfa64','501e0497',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(64,'Test Person AB','testpersonab@gmail.com','$2y$10$5TbSFEfKld0NAjeuLcrbWu.iPXOadqN5tBODz8FHf6oKJjopmpkeS','VKJt9Fm17OaVYRgdr4MLszA6XXafeihuK1aWkzeQ81u1fPSiYbpQJWv73sYI',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:59:07','2017-10-03 14:59:07','2017-10-03 14:59:07','2017-10-03 14:59:14',0,0,0,'72c8724ee41e01f7d7bd2a8308ad36a136d244c72af4bd861de44c05f06b6ae0','7ec2c30c',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(65,'Test Person BA','testpersonba@gmail.com','$2y$10$i4gQvaI71qHecqO3/0UuGepLkEiIaMpzvsxs7rB8S/u1vn7dJoOgi','uR34654GolJ48m6Y29YDdenU0BD4njXUTZWQHh40UFZFZgvWGmQlek0luyF9',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:00:53','2017-10-03 15:00:53','2017-10-03 15:00:53','2017-10-03 15:00:57',0,0,0,'9d7f64c5a8a40b084f9de3beb1c024de57f787f24f1666b6ac2e278b4cba048b','4b2e2ef8',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(66,'Test User ABA','testuseraba@gmail.com','$2y$10$qxPrLSZ7a9bUZRKAO2IDouQrsEYILDMazlo9LmD/TM1f79A0Pu6Q6','VvD2dOGJpAEnzFtAg93LwDGLnIz79fgMW2jcjY1CkPNIxjze9JrEZXeCJa6F',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:17:51','2017-10-03 15:17:51','2017-10-03 15:17:51','2017-10-03 15:17:56',0,0,0,'ff1b1220f16539eebe5ebd52be3a5cb30d355638d9d448a48d93b9944eae319f','79c8ea41',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(67,'Test User ABB','testuserabb@gmail.com','$2y$10$7xZVb9hP9eqviuxynflzFOW4Q0srNlyOG8Ubnh3RtEg.Jgg2Ujik2','Yv0d1uFQmyI3zFxCwu5AyY4avz9LrS5uRTpkkkIfTDeVdHwtEZTXAjjcXUxz',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:18:11','2017-10-03 15:18:11','2017-10-03 15:18:11','2017-10-03 15:18:14',0,0,0,'a38441c62a70c5c73c6afc111f0e3d3c9760a0f1ae25823ba8718a58a365a760','64cb916f',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(68,'Test Person Y','testpersony@gmail.com','$2y$10$GoZvkyWMY53KMhv/GHo4IOzxFxJ0hCaCCz0n.CSMqlBOJ1kuVcBDm','leFgZCLENxTE2WTXSh9lpUxF0aBvSGTteSVX5GqlN4fhoFRlV0i4qZ9QjCll',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:23:37','2017-10-03 15:23:37','2017-10-03 15:23:37','2017-10-03 15:23:43',0,0,0,'68ca44b67097e58b25f3c00120a55be575f5c70bb2ccb0da0a4da715771dfb9e','d9fa0f44',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(70,'Test Person AAA','testpersonaaa@gmail.com','$2y$10$lVUxQlCeHZzowMpeprxCgO/ULCSGXTKRyCr9SfXtaj90I.zLmzxTu',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:28:50','2017-10-03 15:28:50','2017-10-03 15:28:50','2017-10-03 15:28:56',0,0,0,'435a8dcaeb03ca6c493a040b4226d3fe674ba6078e9f00d75816890e08547251','1175b22c',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wayback_settings`
--

DROP TABLE IF EXISTS `wayback_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wayback_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wayback_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wayback_settings`
--

LOCK TABLES `wayback_settings` WRITE;
/*!40000 ALTER TABLE `wayback_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `wayback_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wayback_statuses`
--

DROP TABLE IF EXISTS `wayback_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wayback_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wayback_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wayback_statuses`
--

LOCK TABLES `wayback_statuses` WRITE;
/*!40000 ALTER TABLE `wayback_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `wayback_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wayback_submissions`
--

DROP TABLE IF EXISTS `wayback_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wayback_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipfile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wayback_submissions_user_id_foreign` (`user_id`),
  KEY `wayback_submissions_user_id_index` (`user_id`),
  CONSTRAINT `wayback_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wayback_submissions`
--

LOCK TABLES `wayback_submissions` WRITE;
/*!40000 ALTER TABLE `wayback_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wayback_submissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-05 18:01:21
