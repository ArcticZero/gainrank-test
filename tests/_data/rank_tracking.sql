-- MySQL dump 10.13  Distrib 5.7.17-11, for Linux (x86_64)
--
-- Host: localhost    Database: gainrank
-- ------------------------------------------------------
-- Server version	5.7.17-11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rank_tracking_settings`
--

DROP TABLE IF EXISTS `rank_tracking_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_settings_name_version_unique` (`name`,`version`),
  KEY `rank_tracking_settings_version_foreign` (`version`),
  CONSTRAINT `rank_tracking_settings_version_foreign` FOREIGN KEY (`version`) REFERENCES `rank_tracking_versions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_settings`
--

LOCK TABLES `rank_tracking_settings` WRITE;
/*!40000 ALTER TABLE `rank_tracking_settings` DISABLE KEYS */;
INSERT INTO `rank_tracking_settings` VALUES (1,NULL,NULL,1,'update_interval','60'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,0,'current_version','1'),(6,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_ranker.php'),(7,NULL,NULL,1,'max_favorites','20'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_ranker.php');
/*!40000 ALTER TABLE `rank_tracking_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_versions`
--

DROP TABLE IF EXISTS `rank_tracking_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_versions` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_versions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_versions`
--

LOCK TABLES `rank_tracking_versions` WRITE;
/*!40000 ALTER TABLE `rank_tracking_versions` DISABLE KEYS */;
INSERT INTO `rank_tracking_versions` VALUES (NULL,NULL,0,'general'),(NULL,NULL,1,'beta');
/*!40000 ALTER TABLE `rank_tracking_versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-20 17:23:31
