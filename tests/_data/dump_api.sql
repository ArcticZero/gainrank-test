-- MySQL dump 10.13  Distrib 5.7.17-11, for Linux (x86_64)
--
-- Host: localhost    Database: gainrank
-- ------------------------------------------------------
-- Server version	5.7.17-11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad_campaign_bonus_types`
--

DROP TABLE IF EXISTS `ad_campaign_bonus_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_campaign_bonus_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `reward_type` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_campaign_bonus_types`
--

LOCK TABLES `ad_campaign_bonus_types` WRITE;
/*!40000 ALTER TABLE `ad_campaign_bonus_types` DISABLE KEYS */;
INSERT INTO `ad_campaign_bonus_types` VALUES (1,'% bonus on first deposit','2017-07-10 07:57:30','2017-07-10 07:57:30',NULL),(2,'% off on first month','2017-07-10 07:57:30','2017-07-19 09:43:21',4),(3,'% off recurring','2017-07-10 07:57:30','2017-07-19 09:43:21',6),(4,'flat $ off on first month','2017-07-10 07:57:30','2017-07-19 09:43:21',3),(5,'flat $ off recurring','2017-07-10 07:57:30','2017-07-19 09:43:21',5),(6,'flat # bonus keywords to track','2017-07-10 07:57:30','2017-07-10 07:57:30',NULL);
/*!40000 ALTER TABLE `ad_campaign_bonus_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ad_campaign_bonuses`
--

DROP TABLE IF EXISTS `ad_campaign_bonuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_campaign_bonuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bonus_amount` int(11) NOT NULL,
  `ad_campaign_bonus_type_id` int(10) unsigned NOT NULL,
  `ad_campaign_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ad_campaign_bonuses_ad_campaign_bonus_type_id_foreign` (`ad_campaign_bonus_type_id`),
  KEY `ad_campaign_bonuses_ad_campaign_id_foreign` (`ad_campaign_id`),
  CONSTRAINT `ad_campaign_bonuses_ad_campaign_bonus_type_id_foreign` FOREIGN KEY (`ad_campaign_bonus_type_id`) REFERENCES `ad_campaign_bonus_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ad_campaign_bonuses_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_campaign_bonuses`
--

LOCK TABLES `ad_campaign_bonuses` WRITE;
/*!40000 ALTER TABLE `ad_campaign_bonuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ad_campaign_bonuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ad_campaigns`
--

DROP TABLE IF EXISTS `ad_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_campaigns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `max_uses` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_campaigns_path_unique` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_campaigns`
--

LOCK TABLES `ad_campaigns` WRITE;
/*!40000 ALTER TABLE `ad_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `ad_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliate_transactions`
--

DROP TABLE IF EXISTS `affiliate_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliate_transactions` (
  `affiliate_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transacted_at` datetime NOT NULL,
  `type_id` int(11) NOT NULL,
  `is_mature` tinyint(1) NOT NULL DEFAULT '0',
  `mature_at` date NOT NULL,
  `details` json DEFAULT NULL,
  `reward_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reward_details` json DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `affiliate_transactions_user_id_foreign` (`user_id`),
  KEY `affiliate_transactions_affiliate_id_foreign` (`affiliate_id`),
  KEY `affiliate_transactions_is_mature_index` (`is_mature`),
  KEY `affiliate_transactions_mature_at_index` (`mature_at`),
  CONSTRAINT `affiliate_transactions_affiliate_id_foreign` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliates` (`id`) ON DELETE CASCADE,
  CONSTRAINT `affiliate_transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliate_transactions`
--

LOCK TABLES `affiliate_transactions` WRITE;
/*!40000 ALTER TABLE `affiliate_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliate_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliates`
--

DROP TABLE IF EXISTS `affiliates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payout_available` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payout_total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliates_ref_id_unique` (`ref_id`),
  KEY `affiliates_user_id_foreign` (`user_id`),
  CONSTRAINT `affiliates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliates`
--

LOCK TABLES `affiliates` WRITE;
/*!40000 ALTER TABLE `affiliates` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alexa_settings`
--

DROP TABLE IF EXISTS `alexa_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alexa_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alexa_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alexa_settings`
--

LOCK TABLES `alexa_settings` WRITE;
/*!40000 ALTER TABLE `alexa_settings` DISABLE KEYS */;
INSERT INTO `alexa_settings` VALUES (1,NULL,NULL,1,'update_interval','60'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,0,'current_version','1'),(6,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_alexa.php'),(7,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_alexa.php'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'cost_per','1000'),(10,NULL,'2017-10-02 06:41:17',1,'cost_per_x','100');
/*!40000 ALTER TABLE `alexa_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alexa_statuses`
--

DROP TABLE IF EXISTS `alexa_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alexa_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alexa_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alexa_statuses`
--

LOCK TABLES `alexa_statuses` WRITE;
/*!40000 ALTER TABLE `alexa_statuses` DISABLE KEYS */;
INSERT INTO `alexa_statuses` VALUES (123,'ji','i','nin','i','in');
/*!40000 ALTER TABLE `alexa_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alexa_submissions`
--

DROP TABLE IF EXISTS `alexa_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alexa_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wanted_count` int(11) NOT NULL,
  `sent_count` int(11) NOT NULL,
  `cost_per_x` int(11) NOT NULL,
  `cost_per_amt` int(11) NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alexa_submissions_user_id_foreign` (`user_id`),
  KEY `alexa_submissions_user_id_index` (`user_id`),
  CONSTRAINT `alexa_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alexa_submissions`
--

LOCK TABLES `alexa_submissions` WRITE;
/*!40000 ALTER TABLE `alexa_submissions` DISABLE KEYS */;
INSERT INTO `alexa_submissions` VALUES (1,'2017-10-02 20:13:16','2017-11-21 08:19:02',1,'14','gainrank.com',123,0,100,1000,0,1,'192.168.50.101',1);
/*!40000 ALTER TABLE `alexa_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
INSERT INTO `announcements` VALUES ('171c47d4-69cd-4ab4-a33f-142feb3cff90',1,'test','','','2017-09-22 15:27:07','2017-09-22 15:27:07');
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_tokens`
--

DROP TABLE IF EXISTS `api_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_tokens` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `transient` tinyint(4) NOT NULL DEFAULT '0',
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_tokens_token_unique` (`token`),
  KEY `api_tokens_user_id_expires_at_index` (`user_id`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_tokens`
--

LOCK TABLES `api_tokens` WRITE;
/*!40000 ALTER TABLE `api_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coinbase_checkouts`
--

DROP TABLE IF EXISTS `coinbase_checkouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coinbase_checkouts` (
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkout_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkout_embed_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_campaign_id` int(10) unsigned DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `discount_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coinbase_checkouts_ad_campaign_id_foreign` (`ad_campaign_id`),
  CONSTRAINT `coinbase_checkouts_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coinbase_checkouts`
--

LOCK TABLES `coinbase_checkouts` WRITE;
/*!40000 ALTER TABLE `coinbase_checkouts` DISABLE KEYS */;
INSERT INTO `coinbase_checkouts` VALUES ('provider-id-1','cae996ce-552e-599b-8eb9-4a86cd4ece93','0526085f665b9a328a5529ffd10ba710',NULL,1,NULL);
/*!40000 ALTER TABLE `coinbase_checkouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coinbase_notifications`
--

DROP TABLE IF EXISTS `coinbase_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coinbase_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_details` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coinbase_notifications`
--

LOCK TABLES `coinbase_notifications` WRITE;
/*!40000 ALTER TABLE `coinbase_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `coinbase_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctr_settings`
--

DROP TABLE IF EXISTS `ctr_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctr_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ctr_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctr_settings`
--

LOCK TABLES `ctr_settings` WRITE;
/*!40000 ALTER TABLE `ctr_settings` DISABLE KEYS */;
INSERT INTO `ctr_settings` VALUES (1,NULL,NULL,0,'current_version','1'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_ctr.php'),(7,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_ctr.php'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'cost_per','1000'),(10,NULL,NULL,1,'cost_per_x','100');
/*!40000 ALTER TABLE `ctr_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctr_statuses`
--

DROP TABLE IF EXISTS `ctr_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctr_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ctr_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctr_statuses`
--

LOCK TABLES `ctr_statuses` WRITE;
/*!40000 ALTER TABLE `ctr_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ctr_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctr_submissions`
--

DROP TABLE IF EXISTS `ctr_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctr_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wanted_count` int(11) NOT NULL,
  `sent_count` int(11) NOT NULL,
  `cost_per_x` int(11) NOT NULL,
  `cost_per_amt` int(11) NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ctr_submissions_user_id_foreign` (`user_id`),
  KEY `ctr_submissions_user_id_index` (`user_id`),
  KEY `ctr_submissions_keyword_index` (`keyword`),
  CONSTRAINT `ctr_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctr_submissions`
--

LOCK TABLES `ctr_submissions` WRITE;
/*!40000 ALTER TABLE `ctr_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ctr_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fingerprint_recordings`
--

DROP TABLE IF EXISTS `fingerprint_recordings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fingerprint_recordings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fingerprint_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Stores both IPv4 and IPv6 Addresses hence VARCHAR(45)',
  PRIMARY KEY (`id`),
  KEY `fingerprint_recordings_fingerprint_id_foreign` (`fingerprint_id`),
  KEY `fingerprint_recordings_user_id_foreign` (`user_id`),
  CONSTRAINT `fingerprint_recordings_fingerprint_id_foreign` FOREIGN KEY (`fingerprint_id`) REFERENCES `fingerprints` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fingerprint_recordings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=887 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fingerprint_recordings`
--

LOCK TABLES `fingerprint_recordings` WRITE;
/*!40000 ALTER TABLE `fingerprint_recordings` DISABLE KEYS */;
INSERT INTO `fingerprint_recordings` VALUES (1,1,71,'2017-10-08 10:03:15','2017-10-08 10:03:15','192.168.50.101'),(2,1,72,'2017-10-08 10:05:54','2017-10-08 10:05:54','192.168.50.101'),(3,1,72,'2017-10-08 11:30:26','2017-10-08 11:30:26','192.168.50.101'),(4,1,72,'2017-10-08 11:31:47','2017-10-08 11:31:47','192.168.50.101'),(5,1,72,'2017-10-08 11:32:25','2017-10-08 11:32:25','192.168.50.101'),(6,1,72,'2017-10-08 11:32:58','2017-10-08 11:32:58','192.168.50.101'),(7,1,72,'2017-10-08 11:34:08','2017-10-08 11:34:08','192.168.50.101'),(8,1,72,'2017-10-08 11:34:22','2017-10-08 11:34:22','192.168.50.101'),(9,1,72,'2017-10-08 11:35:08','2017-10-08 11:35:08','192.168.50.101'),(10,1,72,'2017-10-08 11:36:47','2017-10-08 11:36:47','192.168.50.101'),(11,1,72,'2017-10-08 11:37:00','2017-10-08 11:37:00','192.168.50.101'),(12,1,72,'2017-10-08 11:37:06','2017-10-08 11:37:06','192.168.50.101'),(13,1,72,'2017-10-08 11:38:55','2017-10-08 11:38:55','192.168.50.101'),(14,1,72,'2017-10-08 11:39:00','2017-10-08 11:39:00','192.168.50.101'),(15,1,72,'2017-10-08 11:39:02','2017-10-08 11:39:02','192.168.50.101'),(16,1,72,'2017-10-08 11:39:05','2017-10-08 11:39:05','192.168.50.101'),(17,2,72,'2017-10-08 12:05:56','2017-10-08 12:05:56','192.168.50.101'),(18,2,72,'2017-10-08 12:06:42','2017-10-08 12:06:42','192.168.50.101'),(19,2,72,'2017-10-08 12:07:02','2017-10-08 12:07:02','192.168.50.101'),(20,2,72,'2017-10-08 12:07:11','2017-10-08 12:07:11','192.168.50.101'),(21,2,72,'2017-10-08 12:07:15','2017-10-08 12:07:15','192.168.50.101'),(22,2,72,'2017-10-08 12:07:48','2017-10-08 12:07:48','192.168.50.101'),(23,2,72,'2017-10-08 12:07:55','2017-10-08 12:07:55','192.168.50.101'),(24,2,72,'2017-10-08 12:08:06','2017-10-08 12:08:06','192.168.50.101'),(25,2,72,'2017-10-08 12:08:24','2017-10-08 12:08:24','192.168.50.101'),(26,2,72,'2017-10-08 12:08:35','2017-10-08 12:08:35','192.168.50.101'),(27,2,72,'2017-10-08 14:29:47','2017-10-08 14:29:47','192.168.50.101'),(28,2,72,'2017-10-08 14:30:44','2017-10-08 14:30:44','192.168.50.101'),(29,2,1,'2017-10-08 14:31:14','2017-10-08 14:31:14','192.168.50.101'),(30,2,1,'2017-10-08 14:35:13','2017-10-08 14:35:13','192.168.50.101'),(31,2,1,'2017-10-08 15:09:33','2017-10-08 15:09:33','192.168.50.101'),(32,2,1,'2017-10-08 15:09:36','2017-10-08 15:09:36','192.168.50.101'),(33,2,1,'2017-10-08 15:10:47','2017-10-08 15:10:47','192.168.50.101'),(34,2,1,'2017-10-08 15:11:11','2017-10-08 15:11:11','192.168.50.101'),(35,2,1,'2017-10-08 15:12:31','2017-10-08 15:12:31','192.168.50.101'),(36,2,1,'2017-10-08 15:13:10','2017-10-08 15:13:10','192.168.50.101'),(37,2,1,'2017-10-08 16:56:15','2017-10-08 16:56:15','192.168.50.101'),(38,3,1,'2017-10-08 17:01:41','2017-10-08 17:01:41','192.168.50.101'),(39,2,1,'2017-10-08 17:02:25','2017-10-08 17:02:25','192.168.50.101'),(40,2,1,'2017-10-08 17:03:01','2017-10-08 17:03:01','192.168.50.101'),(41,2,1,'2017-10-08 17:03:30','2017-10-08 17:03:30','192.168.50.101'),(42,2,1,'2017-10-08 17:07:25','2017-10-08 17:07:25','192.168.50.101'),(43,2,1,'2017-10-08 17:07:31','2017-10-08 17:07:31','192.168.50.101'),(44,2,1,'2017-10-08 17:08:07','2017-10-08 17:08:07','192.168.50.101'),(45,2,1,'2017-10-08 17:08:17','2017-10-08 17:08:17','192.168.50.101'),(46,2,1,'2017-10-08 17:08:23','2017-10-08 17:08:23','192.168.50.101'),(47,2,1,'2017-10-08 17:09:35','2017-10-08 17:09:35','192.168.50.101'),(48,2,1,'2017-10-08 17:10:20','2017-10-08 17:10:20','192.168.50.101'),(49,2,1,'2017-10-08 17:19:13','2017-10-08 17:19:13','192.168.50.101'),(50,2,1,'2017-10-08 17:19:21','2017-10-08 17:19:21','192.168.50.101'),(51,2,1,'2017-10-08 17:19:54','2017-10-08 17:19:54','192.168.50.101'),(52,2,1,'2017-10-08 17:22:16','2017-10-08 17:22:16','192.168.50.101'),(53,2,1,'2017-10-08 17:29:26','2017-10-08 17:29:26','192.168.50.101'),(54,2,1,'2017-10-08 18:07:43','2017-10-08 18:07:43','192.168.50.101'),(55,2,1,'2017-10-08 18:07:45','2017-10-08 18:07:45','192.168.50.101'),(56,2,1,'2017-10-08 18:09:02','2017-10-08 18:09:02','192.168.50.101'),(57,2,1,'2017-10-08 18:09:03','2017-10-08 18:09:03','192.168.50.101'),(58,2,1,'2017-10-08 18:09:37','2017-10-08 18:09:37','192.168.50.101'),(59,2,1,'2017-10-08 18:09:41','2017-10-08 18:09:41','192.168.50.101'),(60,2,1,'2017-10-08 18:09:44','2017-10-08 18:09:44','192.168.50.101'),(61,2,1,'2017-10-08 18:11:31','2017-10-08 18:11:31','192.168.50.101'),(62,2,1,'2017-10-08 18:13:53','2017-10-08 18:13:53','192.168.50.101'),(63,2,1,'2017-10-08 18:14:02','2017-10-08 18:14:02','192.168.50.101'),(64,2,1,'2017-10-08 18:14:11','2017-10-08 18:14:11','192.168.50.101'),(65,2,1,'2017-10-08 18:14:43','2017-10-08 18:14:43','192.168.50.101'),(66,2,1,'2017-10-08 18:24:49','2017-10-08 18:24:49','192.168.50.101'),(67,2,1,'2017-10-08 18:25:20','2017-10-08 18:25:20','192.168.50.101'),(68,2,1,'2017-10-08 18:34:02','2017-10-08 18:34:02','192.168.50.101'),(69,2,1,'2017-10-08 18:34:32','2017-10-08 18:34:32','192.168.50.101'),(70,2,1,'2017-10-08 18:35:35','2017-10-08 18:35:35','192.168.50.101'),(71,2,1,'2017-10-08 18:35:59','2017-10-08 18:35:59','192.168.50.101'),(72,2,1,'2017-10-10 04:12:28','2017-10-10 04:12:28','192.168.50.101'),(73,2,1,'2017-10-10 04:13:19','2017-10-10 04:13:19','192.168.50.101'),(74,2,1,'2017-10-10 06:08:05','2017-10-10 06:08:05','192.168.50.101'),(75,2,1,'2017-10-10 06:08:11','2017-10-10 06:08:11','192.168.50.101'),(76,2,1,'2017-10-10 06:08:59','2017-10-10 06:08:59','192.168.50.101'),(77,2,1,'2017-10-10 06:09:06','2017-10-10 06:09:06','192.168.50.101'),(78,2,1,'2017-10-10 06:14:57','2017-10-10 06:14:57','192.168.50.101'),(79,2,1,'2017-10-10 06:14:58','2017-10-10 06:14:58','192.168.50.101'),(80,2,1,'2017-10-10 06:15:00','2017-10-10 06:15:00','192.168.50.101'),(81,2,1,'2017-10-10 07:28:31','2017-10-10 07:28:31','192.168.50.101'),(82,2,1,'2017-10-10 07:28:33','2017-10-10 07:28:33','192.168.50.101'),(83,2,1,'2017-10-10 07:28:34','2017-10-10 07:28:34','192.168.50.101'),(84,2,1,'2017-10-10 07:47:53','2017-10-10 07:47:53','192.168.50.101'),(85,2,1,'2017-10-10 07:51:13','2017-10-10 07:51:13','192.168.50.101'),(86,2,1,'2017-10-10 07:51:44','2017-10-10 07:51:44','192.168.50.101'),(87,2,1,'2017-10-10 07:57:00','2017-10-10 07:57:00','192.168.50.101'),(88,2,1,'2017-10-10 07:57:35','2017-10-10 07:57:35','192.168.50.101'),(89,2,1,'2017-10-10 07:59:08','2017-10-10 07:59:08','192.168.50.101'),(90,2,1,'2017-10-10 07:59:52','2017-10-10 07:59:52','192.168.50.101'),(91,2,1,'2017-10-10 08:00:37','2017-10-10 08:00:37','192.168.50.101'),(92,2,1,'2017-10-10 08:01:21','2017-10-10 08:01:21','192.168.50.101'),(93,2,1,'2017-10-10 08:01:53','2017-10-10 08:01:53','192.168.50.101'),(94,2,1,'2017-10-10 08:02:13','2017-10-10 08:02:13','192.168.50.101'),(95,2,1,'2017-10-10 08:02:32','2017-10-10 08:02:32','192.168.50.101'),(96,2,1,'2017-10-10 08:03:02','2017-10-10 08:03:02','192.168.50.101'),(97,2,1,'2017-10-10 08:03:20','2017-10-10 08:03:20','192.168.50.101'),(98,2,1,'2017-10-10 08:03:59','2017-10-10 08:03:59','192.168.50.101'),(99,2,1,'2017-10-10 08:04:28','2017-10-10 08:04:28','192.168.50.101'),(100,2,1,'2017-10-10 08:04:34','2017-10-10 08:04:34','192.168.50.101'),(101,2,1,'2017-10-10 08:04:48','2017-10-10 08:04:48','192.168.50.101'),(102,2,1,'2017-10-10 08:05:52','2017-10-10 08:05:52','192.168.50.101'),(103,2,1,'2017-10-10 08:08:06','2017-10-10 08:08:06','192.168.50.101'),(104,2,1,'2017-10-10 08:08:08','2017-10-10 08:08:08','192.168.50.101'),(105,2,1,'2017-10-10 08:08:11','2017-10-10 08:08:11','192.168.50.101'),(106,2,1,'2017-10-10 08:08:24','2017-10-10 08:08:24','192.168.50.101'),(107,2,1,'2017-10-10 08:09:08','2017-10-10 08:09:08','192.168.50.101'),(108,2,1,'2017-10-10 08:09:47','2017-10-10 08:09:47','192.168.50.101'),(109,2,1,'2017-10-10 08:09:55','2017-10-10 08:09:55','192.168.50.101'),(110,2,1,'2017-10-10 08:11:21','2017-10-10 08:11:21','192.168.50.101'),(111,2,1,'2017-10-10 08:11:34','2017-10-10 08:11:34','192.168.50.101'),(112,2,1,'2017-10-10 08:11:40','2017-10-10 08:11:40','192.168.50.101'),(113,2,1,'2017-10-10 08:12:32','2017-10-10 08:12:32','192.168.50.101'),(114,2,1,'2017-10-10 08:14:09','2017-10-10 08:14:09','192.168.50.101'),(115,2,1,'2017-10-10 08:14:12','2017-10-10 08:14:12','192.168.50.101'),(116,2,1,'2017-10-10 08:16:30','2017-10-10 08:16:30','192.168.50.101'),(117,2,1,'2017-10-10 08:16:34','2017-10-10 08:16:34','192.168.50.101'),(118,2,1,'2017-10-10 08:16:35','2017-10-10 08:16:35','192.168.50.101'),(119,2,1,'2017-10-10 08:16:37','2017-10-10 08:16:37','192.168.50.101'),(120,2,1,'2017-10-10 08:16:39','2017-10-10 08:16:39','192.168.50.101'),(121,2,1,'2017-10-10 08:16:41','2017-10-10 08:16:41','192.168.50.101'),(122,2,1,'2017-10-10 08:16:43','2017-10-10 08:16:43','192.168.50.101'),(123,2,1,'2017-10-10 08:17:45','2017-10-10 08:17:45','192.168.50.101'),(124,2,1,'2017-10-10 08:18:09','2017-10-10 08:18:09','192.168.50.101'),(125,2,1,'2017-10-10 08:18:14','2017-10-10 08:18:14','192.168.50.101'),(126,2,1,'2017-10-10 08:18:50','2017-10-10 08:18:50','192.168.50.101'),(127,2,1,'2017-10-10 08:19:10','2017-10-10 08:19:10','192.168.50.101'),(128,2,1,'2017-10-10 08:19:19','2017-10-10 08:19:19','192.168.50.101'),(129,2,1,'2017-10-10 08:19:24','2017-10-10 08:19:24','192.168.50.101'),(130,2,1,'2017-10-10 08:19:35','2017-10-10 08:19:35','192.168.50.101'),(131,2,1,'2017-10-10 08:19:37','2017-10-10 08:19:37','192.168.50.101'),(132,2,1,'2017-10-10 08:19:39','2017-10-10 08:19:39','192.168.50.101'),(133,2,1,'2017-10-10 08:19:52','2017-10-10 08:19:52','192.168.50.101'),(134,2,1,'2017-10-10 08:19:54','2017-10-10 08:19:54','192.168.50.101'),(135,2,1,'2017-10-10 08:20:10','2017-10-10 08:20:10','192.168.50.101'),(136,2,1,'2017-10-10 08:20:18','2017-10-10 08:20:18','192.168.50.101'),(137,2,1,'2017-10-10 08:20:20','2017-10-10 08:20:20','192.168.50.101'),(138,2,1,'2017-10-10 08:20:37','2017-10-10 08:20:37','192.168.50.101'),(139,2,1,'2017-10-10 08:20:46','2017-10-10 08:20:46','192.168.50.101'),(140,2,1,'2017-10-10 08:20:49','2017-10-10 08:20:49','192.168.50.101'),(141,2,1,'2017-10-10 10:03:29','2017-10-10 10:03:29','192.168.50.101'),(142,2,1,'2017-10-10 10:03:55','2017-10-10 10:03:55','192.168.50.101'),(143,2,1,'2017-10-10 10:04:11','2017-10-10 10:04:11','192.168.50.101'),(144,2,1,'2017-10-10 10:04:14','2017-10-10 10:04:14','192.168.50.101'),(145,2,1,'2017-10-10 10:05:35','2017-10-10 10:05:35','192.168.50.101'),(146,2,1,'2017-10-10 10:06:16','2017-10-10 10:06:16','192.168.50.101'),(147,2,1,'2017-10-10 10:06:39','2017-10-10 10:06:39','192.168.50.101'),(148,2,1,'2017-10-10 10:07:06','2017-10-10 10:07:06','192.168.50.101'),(149,2,1,'2017-10-10 10:07:54','2017-10-10 10:07:54','192.168.50.101'),(150,2,1,'2017-10-10 10:08:14','2017-10-10 10:08:14','192.168.50.101'),(151,2,1,'2017-10-10 10:09:45','2017-10-10 10:09:45','192.168.50.101'),(152,2,1,'2017-10-10 10:10:39','2017-10-10 10:10:39','192.168.50.101'),(153,2,1,'2017-10-10 10:10:58','2017-10-10 10:10:58','192.168.50.101'),(154,2,1,'2017-10-10 10:11:01','2017-10-10 10:11:01','192.168.50.101'),(155,2,1,'2017-10-10 10:12:06','2017-10-10 10:12:06','192.168.50.101'),(156,2,1,'2017-10-10 10:13:05','2017-10-10 10:13:05','192.168.50.101'),(157,2,1,'2017-10-10 10:13:38','2017-10-10 10:13:38','192.168.50.101'),(158,2,1,'2017-10-10 10:14:04','2017-10-10 10:14:04','192.168.50.101'),(159,2,1,'2017-10-10 10:17:26','2017-10-10 10:17:26','192.168.50.101'),(160,2,1,'2017-10-10 10:17:42','2017-10-10 10:17:42','192.168.50.101'),(161,2,1,'2017-10-10 10:18:15','2017-10-10 10:18:15','192.168.50.101'),(162,2,1,'2017-10-10 10:18:26','2017-10-10 10:18:26','192.168.50.101'),(163,2,1,'2017-10-10 10:18:41','2017-10-10 10:18:41','192.168.50.101'),(164,2,1,'2017-10-10 10:19:09','2017-10-10 10:19:09','192.168.50.101'),(165,2,1,'2017-10-10 10:20:26','2017-10-10 10:20:26','192.168.50.101'),(166,2,1,'2017-10-10 10:20:28','2017-10-10 10:20:28','192.168.50.101'),(167,2,1,'2017-10-10 10:20:30','2017-10-10 10:20:30','192.168.50.101'),(168,2,1,'2017-10-10 10:20:32','2017-10-10 10:20:32','192.168.50.101'),(169,2,1,'2017-10-10 10:20:53','2017-10-10 10:20:53','192.168.50.101'),(170,2,1,'2017-10-10 10:22:23','2017-10-10 10:22:23','192.168.50.101'),(171,2,1,'2017-10-10 10:22:51','2017-10-10 10:22:51','192.168.50.101'),(172,2,1,'2017-10-10 10:23:18','2017-10-10 10:23:18','192.168.50.101'),(173,2,1,'2017-10-10 10:24:50','2017-10-10 10:24:50','192.168.50.101'),(174,2,1,'2017-10-10 10:25:03','2017-10-10 10:25:03','192.168.50.101'),(175,1,1,'2017-10-12 07:57:04','2017-10-12 07:57:04','192.168.50.101'),(176,1,1,'2017-10-12 07:57:41','2017-10-12 07:57:41','192.168.50.101'),(177,1,1,'2017-10-12 07:57:44','2017-10-12 07:57:44','192.168.50.101'),(178,1,1,'2017-10-12 08:00:06','2017-10-12 08:00:06','192.168.50.101'),(179,1,1,'2017-10-12 08:03:58','2017-10-12 08:03:58','192.168.50.101'),(180,1,1,'2017-10-12 08:05:40','2017-10-12 08:05:40','192.168.50.101'),(181,1,1,'2017-10-12 08:06:16','2017-10-12 08:06:16','192.168.50.101'),(182,1,1,'2017-10-12 08:06:28','2017-10-12 08:06:28','192.168.50.101'),(183,1,1,'2017-10-12 08:08:30','2017-10-12 08:08:30','192.168.50.101'),(184,1,1,'2017-10-12 08:10:40','2017-10-12 08:10:40','192.168.50.101'),(185,1,1,'2017-10-12 08:11:37','2017-10-12 08:11:37','192.168.50.101'),(186,1,1,'2017-10-12 08:12:21','2017-10-12 08:12:21','192.168.50.101'),(187,1,1,'2017-10-12 08:13:00','2017-10-12 08:13:00','192.168.50.101'),(188,1,1,'2017-10-12 08:13:27','2017-10-12 08:13:27','192.168.50.101'),(189,1,1,'2017-10-12 08:14:08','2017-10-12 08:14:08','192.168.50.101'),(190,1,1,'2017-10-12 08:14:16','2017-10-12 08:14:16','192.168.50.101'),(191,1,1,'2017-10-12 08:14:43','2017-10-12 08:14:43','192.168.50.101'),(192,1,1,'2017-10-12 08:14:54','2017-10-12 08:14:54','192.168.50.101'),(193,1,1,'2017-10-12 08:15:10','2017-10-12 08:15:10','192.168.50.101'),(194,1,1,'2017-10-12 08:15:16','2017-10-12 08:15:16','192.168.50.101'),(195,1,1,'2017-10-12 08:15:22','2017-10-12 08:15:22','192.168.50.101'),(196,1,1,'2017-10-12 08:15:28','2017-10-12 08:15:28','192.168.50.101'),(197,1,1,'2017-10-12 08:15:43','2017-10-12 08:15:43','192.168.50.101'),(198,1,1,'2017-10-12 08:15:49','2017-10-12 08:15:49','192.168.50.101'),(199,1,1,'2017-10-12 08:20:04','2017-10-12 08:20:04','192.168.50.101'),(200,1,1,'2017-10-12 08:20:57','2017-10-12 08:20:57','192.168.50.101'),(201,1,1,'2017-10-12 08:20:59','2017-10-12 08:20:59','192.168.50.101'),(202,1,1,'2017-10-12 08:32:22','2017-10-12 08:32:22','192.168.50.101'),(203,1,1,'2017-10-12 08:32:35','2017-10-12 08:32:35','192.168.50.101'),(204,1,1,'2017-10-12 08:32:57','2017-10-12 08:32:57','192.168.50.101'),(205,1,1,'2017-10-12 08:33:18','2017-10-12 08:33:18','192.168.50.101'),(206,1,1,'2017-10-12 08:33:23','2017-10-12 08:33:23','192.168.50.101'),(207,1,1,'2017-10-12 08:40:20','2017-10-12 08:40:20','192.168.50.101'),(208,1,1,'2017-10-12 08:40:26','2017-10-12 08:40:26','192.168.50.101'),(209,1,1,'2017-10-12 08:40:29','2017-10-12 08:40:29','192.168.50.101'),(210,1,1,'2017-10-12 08:42:28','2017-10-12 08:42:28','192.168.50.101'),(211,1,1,'2017-10-12 08:47:30','2017-10-12 08:47:30','192.168.50.101'),(212,1,1,'2017-10-12 08:58:43','2017-10-12 08:58:43','192.168.50.101'),(213,1,1,'2017-10-12 08:58:54','2017-10-12 08:58:54','192.168.50.101'),(214,1,1,'2017-10-12 09:00:00','2017-10-12 09:00:00','192.168.50.101'),(215,1,1,'2017-10-12 09:00:24','2017-10-12 09:00:24','192.168.50.101'),(216,1,1,'2017-10-12 09:00:53','2017-10-12 09:00:53','192.168.50.101'),(217,1,1,'2017-10-12 09:09:50','2017-10-12 09:09:50','192.168.50.101'),(218,1,1,'2017-10-12 09:09:52','2017-10-12 09:09:52','192.168.50.101'),(219,1,1,'2017-10-12 09:09:53','2017-10-12 09:09:53','192.168.50.101'),(220,1,1,'2017-10-12 09:14:10','2017-10-12 09:14:10','192.168.50.101'),(221,1,1,'2017-10-12 09:15:32','2017-10-12 09:15:32','192.168.50.101'),(222,2,1,'2017-10-12 09:15:36','2017-10-12 09:15:36','192.168.50.101'),(223,1,1,'2017-10-12 09:16:00','2017-10-12 09:16:00','192.168.50.101'),(224,1,1,'2017-10-12 09:16:03','2017-10-12 09:16:03','192.168.50.101'),(225,1,1,'2017-10-12 09:16:18','2017-10-12 09:16:18','192.168.50.101'),(226,1,1,'2017-10-12 09:16:46','2017-10-12 09:16:46','192.168.50.101'),(227,1,1,'2017-10-12 09:16:54','2017-10-12 09:16:54','192.168.50.101'),(228,1,1,'2017-10-12 09:17:14','2017-10-12 09:17:14','192.168.50.101'),(229,1,1,'2017-10-12 09:17:22','2017-10-12 09:17:22','192.168.50.101'),(230,1,1,'2017-10-12 09:17:46','2017-10-12 09:17:46','192.168.50.101'),(231,1,1,'2017-10-12 09:17:54','2017-10-12 09:17:54','192.168.50.101'),(232,1,1,'2017-10-12 09:17:56','2017-10-12 09:17:56','192.168.50.101'),(233,1,1,'2017-10-12 09:18:27','2017-10-12 09:18:27','192.168.50.101'),(234,1,1,'2017-10-12 09:18:29','2017-10-12 09:18:29','192.168.50.101'),(235,2,1,'2017-10-12 09:22:28','2017-10-12 09:22:28','192.168.50.101'),(236,2,1,'2017-10-12 09:22:29','2017-10-12 09:22:29','192.168.50.101'),(237,2,1,'2017-10-12 09:22:59','2017-10-12 09:22:59','192.168.50.101'),(238,2,1,'2017-10-12 09:23:20','2017-10-12 09:23:20','192.168.50.101'),(239,2,1,'2017-10-12 09:23:23','2017-10-12 09:23:23','192.168.50.101'),(240,2,1,'2017-10-12 09:24:44','2017-10-12 09:24:44','192.168.50.101'),(241,2,1,'2017-10-12 09:30:37','2017-10-12 09:30:37','192.168.50.101'),(242,1,1,'2017-10-12 10:13:41','2017-10-12 10:13:41','192.168.50.101'),(243,1,1,'2017-10-12 10:14:59','2017-10-12 10:14:59','192.168.50.101'),(244,1,1,'2017-10-12 10:15:01','2017-10-12 10:15:01','192.168.50.101'),(245,1,1,'2017-10-12 10:15:05','2017-10-12 10:15:05','192.168.50.101'),(246,1,1,'2017-10-12 10:15:15','2017-10-12 10:15:15','192.168.50.101'),(247,1,1,'2017-10-12 10:15:44','2017-10-12 10:15:44','192.168.50.101'),(248,1,1,'2017-10-12 10:17:42','2017-10-12 10:17:42','192.168.50.101'),(249,1,1,'2017-10-12 10:19:30','2017-10-12 10:19:30','192.168.50.101'),(250,1,1,'2017-10-12 10:19:32','2017-10-12 10:19:32','192.168.50.101'),(251,1,1,'2017-10-12 10:19:35','2017-10-12 10:19:35','192.168.50.101'),(252,1,1,'2017-10-12 10:19:38','2017-10-12 10:19:38','192.168.50.101'),(253,1,1,'2017-10-12 10:19:41','2017-10-12 10:19:41','192.168.50.101'),(254,1,1,'2017-10-13 05:33:44','2017-10-13 05:33:44','192.168.50.101'),(255,1,1,'2017-10-13 05:33:46','2017-10-13 05:33:46','192.168.50.101'),(256,1,1,'2017-10-13 06:57:43','2017-10-13 06:57:43','192.168.50.101'),(257,1,1,'2017-10-13 07:23:41','2017-10-13 07:23:41','192.168.50.101'),(258,1,1,'2017-10-13 07:34:18','2017-10-13 07:34:18','192.168.50.101'),(259,1,1,'2017-10-13 08:10:16','2017-10-13 08:10:16','192.168.50.101'),(260,1,1,'2017-10-13 08:10:51','2017-10-13 08:10:51','192.168.50.101'),(261,1,1,'2017-10-13 08:11:08','2017-10-13 08:11:08','192.168.50.101'),(262,1,1,'2017-10-13 08:12:16','2017-10-13 08:12:16','192.168.50.101'),(263,1,1,'2017-10-13 08:12:30','2017-10-13 08:12:30','192.168.50.101'),(264,1,1,'2017-10-13 08:14:02','2017-10-13 08:14:02','192.168.50.101'),(265,1,1,'2017-10-13 08:14:06','2017-10-13 08:14:06','192.168.50.101'),(266,1,1,'2017-10-13 08:14:10','2017-10-13 08:14:10','192.168.50.101'),(267,1,1,'2017-10-13 08:15:18','2017-10-13 08:15:18','192.168.50.101'),(268,1,1,'2017-10-13 08:17:28','2017-10-13 08:17:28','192.168.50.101'),(269,1,1,'2017-10-13 08:17:35','2017-10-13 08:17:35','192.168.50.101'),(270,1,1,'2017-10-13 08:17:53','2017-10-13 08:17:53','192.168.50.101'),(271,1,1,'2017-10-13 08:17:55','2017-10-13 08:17:55','192.168.50.101'),(272,1,1,'2017-10-13 08:17:59','2017-10-13 08:17:59','192.168.50.101'),(273,1,1,'2017-10-13 08:18:05','2017-10-13 08:18:05','192.168.50.101'),(274,1,1,'2017-10-13 08:18:09','2017-10-13 08:18:09','192.168.50.101'),(275,1,1,'2017-10-13 08:19:02','2017-10-13 08:19:02','192.168.50.101'),(276,1,1,'2017-10-13 08:19:58','2017-10-13 08:19:58','192.168.50.101'),(277,1,1,'2017-10-13 08:20:01','2017-10-13 08:20:01','192.168.50.101'),(278,1,1,'2017-10-13 08:20:16','2017-10-13 08:20:16','192.168.50.101'),(279,1,1,'2017-10-13 08:20:39','2017-10-13 08:20:39','192.168.50.101'),(280,1,1,'2017-10-13 08:21:26','2017-10-13 08:21:26','192.168.50.101'),(281,1,1,'2017-10-13 08:28:15','2017-10-13 08:28:15','192.168.50.101'),(282,1,1,'2017-10-13 08:28:24','2017-10-13 08:28:24','192.168.50.101'),(283,1,1,'2017-10-13 08:29:06','2017-10-13 08:29:06','192.168.50.101'),(284,1,1,'2017-10-13 08:33:30','2017-10-13 08:33:30','192.168.50.101'),(285,1,1,'2017-10-13 08:34:04','2017-10-13 08:34:04','192.168.50.101'),(286,1,1,'2017-10-13 08:34:21','2017-10-13 08:34:21','192.168.50.101'),(287,1,1,'2017-10-13 08:35:06','2017-10-13 08:35:06','192.168.50.101'),(288,1,1,'2017-10-13 08:35:17','2017-10-13 08:35:17','192.168.50.101'),(289,1,1,'2017-10-13 08:46:41','2017-10-13 08:46:41','192.168.50.101'),(290,1,1,'2017-10-13 08:47:21','2017-10-13 08:47:21','192.168.50.101'),(291,1,1,'2017-10-13 08:49:41','2017-10-13 08:49:41','192.168.50.101'),(292,1,1,'2017-10-13 08:50:52','2017-10-13 08:50:52','192.168.50.101'),(293,1,1,'2017-10-13 08:51:26','2017-10-13 08:51:26','192.168.50.101'),(294,1,1,'2017-10-13 08:51:54','2017-10-13 08:51:54','192.168.50.101'),(295,1,1,'2017-10-13 08:51:56','2017-10-13 08:51:56','192.168.50.101'),(296,1,1,'2017-10-13 08:52:26','2017-10-13 08:52:26','192.168.50.101'),(297,1,1,'2017-10-13 08:52:55','2017-10-13 08:52:55','192.168.50.101'),(298,1,1,'2017-10-13 08:55:04','2017-10-13 08:55:04','192.168.50.101'),(299,1,1,'2017-10-13 08:55:20','2017-10-13 08:55:20','192.168.50.101'),(300,1,1,'2017-10-13 08:55:25','2017-10-13 08:55:25','192.168.50.101'),(301,1,1,'2017-10-13 08:55:39','2017-10-13 08:55:39','192.168.50.101'),(302,1,1,'2017-10-13 08:55:56','2017-10-13 08:55:56','192.168.50.101'),(303,1,1,'2017-10-13 08:56:08','2017-10-13 08:56:08','192.168.50.101'),(304,1,1,'2017-10-13 08:56:46','2017-10-13 08:56:46','192.168.50.101'),(305,1,1,'2017-10-13 08:56:56','2017-10-13 08:56:56','192.168.50.101'),(306,1,1,'2017-10-13 08:59:40','2017-10-13 08:59:40','192.168.50.101'),(307,1,1,'2017-10-13 09:00:16','2017-10-13 09:00:16','192.168.50.101'),(308,1,1,'2017-10-13 09:00:52','2017-10-13 09:00:52','192.168.50.101'),(309,1,1,'2017-10-13 09:04:09','2017-10-13 09:04:09','192.168.50.101'),(310,1,1,'2017-10-13 09:04:25','2017-10-13 09:04:25','192.168.50.101'),(311,1,1,'2017-10-13 09:05:05','2017-10-13 09:05:05','192.168.50.101'),(312,1,1,'2017-10-13 09:05:43','2017-10-13 09:05:43','192.168.50.101'),(313,1,1,'2017-10-13 09:09:36','2017-10-13 09:09:36','192.168.50.101'),(314,1,1,'2017-10-13 09:09:59','2017-10-13 09:09:59','192.168.50.101'),(315,1,1,'2017-10-13 09:10:35','2017-10-13 09:10:35','192.168.50.101'),(316,1,1,'2017-10-13 09:11:30','2017-10-13 09:11:30','192.168.50.101'),(317,1,1,'2017-10-13 09:22:42','2017-10-13 09:22:42','192.168.50.101'),(318,1,1,'2017-10-13 09:22:46','2017-10-13 09:22:46','192.168.50.101'),(319,1,1,'2017-10-13 09:23:02','2017-10-13 09:23:02','192.168.50.101'),(320,1,1,'2017-10-13 09:26:25','2017-10-13 09:26:25','192.168.50.101'),(321,1,1,'2017-10-13 09:26:33','2017-10-13 09:26:33','192.168.50.101'),(322,1,1,'2017-10-13 09:26:36','2017-10-13 09:26:36','192.168.50.101'),(323,1,1,'2017-10-13 09:27:18','2017-10-13 09:27:18','192.168.50.101'),(324,1,1,'2017-10-13 09:27:31','2017-10-13 09:27:31','192.168.50.101'),(325,1,1,'2017-10-13 09:27:36','2017-10-13 09:27:36','192.168.50.101'),(326,1,1,'2017-10-13 09:28:27','2017-10-13 09:28:27','192.168.50.101'),(327,1,1,'2017-10-13 09:28:35','2017-10-13 09:28:35','192.168.50.101'),(328,1,1,'2017-10-13 09:35:30','2017-10-13 09:35:30','192.168.50.101'),(329,1,1,'2017-10-13 09:43:02','2017-10-13 09:43:02','192.168.50.101'),(330,1,1,'2017-10-13 09:43:20','2017-10-13 09:43:20','192.168.50.101'),(331,1,1,'2017-10-13 09:55:45','2017-10-13 09:55:45','192.168.50.101'),(332,1,1,'2017-10-13 09:57:13','2017-10-13 09:57:13','192.168.50.101'),(333,1,1,'2017-10-13 10:08:15','2017-10-13 10:08:15','192.168.50.101'),(334,1,1,'2017-10-13 10:25:42','2017-10-13 10:25:42','192.168.50.101'),(335,1,1,'2017-10-13 10:25:54','2017-10-13 10:25:54','192.168.50.101'),(336,1,1,'2017-10-13 10:25:56','2017-10-13 10:25:56','192.168.50.101'),(337,1,1,'2017-10-13 10:25:57','2017-10-13 10:25:57','192.168.50.101'),(338,1,1,'2017-10-13 10:26:41','2017-10-13 10:26:41','192.168.50.101'),(339,1,1,'2017-10-13 10:47:27','2017-10-13 10:47:27','192.168.50.101'),(340,1,1,'2017-10-13 10:47:30','2017-10-13 10:47:30','192.168.50.101'),(341,1,1,'2017-10-13 10:49:04','2017-10-13 10:49:04','192.168.50.101'),(342,1,1,'2017-10-13 10:51:28','2017-10-13 10:51:28','192.168.50.101'),(343,1,1,'2017-10-13 11:03:55','2017-10-13 11:03:55','192.168.50.101'),(344,1,1,'2017-10-13 11:04:19','2017-10-13 11:04:19','192.168.50.101'),(345,1,1,'2017-10-13 11:04:30','2017-10-13 11:04:30','192.168.50.101'),(346,1,1,'2017-10-13 11:06:07','2017-10-13 11:06:07','192.168.50.101'),(347,1,1,'2017-10-13 11:07:40','2017-10-13 11:07:40','192.168.50.101'),(348,1,1,'2017-10-13 11:08:09','2017-10-13 11:08:09','192.168.50.101'),(349,1,1,'2017-10-13 11:08:27','2017-10-13 11:08:27','192.168.50.101'),(350,1,1,'2017-10-13 11:08:37','2017-10-13 11:08:37','192.168.50.101'),(351,1,1,'2017-10-13 11:09:41','2017-10-13 11:09:41','192.168.50.101'),(352,1,1,'2017-10-13 11:10:42','2017-10-13 11:10:42','192.168.50.101'),(353,1,1,'2017-10-13 11:11:16','2017-10-13 11:11:16','192.168.50.101'),(354,1,1,'2017-10-13 11:13:32','2017-10-13 11:13:32','192.168.50.101'),(355,1,1,'2017-10-13 11:13:55','2017-10-13 11:13:55','192.168.50.101'),(356,1,1,'2017-10-13 11:28:33','2017-10-13 11:28:33','192.168.50.101'),(357,1,1,'2017-10-13 11:31:59','2017-10-13 11:31:59','192.168.50.101'),(358,1,1,'2017-10-13 11:32:18','2017-10-13 11:32:18','192.168.50.101'),(359,1,1,'2017-10-13 11:34:32','2017-10-13 11:34:32','192.168.50.101'),(360,1,1,'2017-10-13 11:39:42','2017-10-13 11:39:42','192.168.50.101'),(361,1,1,'2017-10-13 11:40:47','2017-10-13 11:40:47','192.168.50.101'),(362,1,1,'2017-10-13 11:42:37','2017-10-13 11:42:37','192.168.50.101'),(363,1,1,'2017-10-13 11:43:14','2017-10-13 11:43:14','192.168.50.101'),(364,1,1,'2017-10-13 16:15:37','2017-10-13 16:15:37','192.168.50.101'),(365,1,1,'2017-10-13 16:17:12','2017-10-13 16:17:12','192.168.50.101'),(366,1,1,'2017-10-13 16:17:20','2017-10-13 16:17:20','192.168.50.101'),(367,1,1,'2017-10-13 16:17:52','2017-10-13 16:17:52','192.168.50.101'),(368,1,1,'2017-10-13 16:18:52','2017-10-13 16:18:52','192.168.50.101'),(369,1,1,'2017-10-13 16:19:15','2017-10-13 16:19:15','192.168.50.101'),(370,1,1,'2017-10-13 16:20:05','2017-10-13 16:20:05','192.168.50.101'),(371,1,1,'2017-10-13 16:20:10','2017-10-13 16:20:10','192.168.50.101'),(372,1,1,'2017-10-13 16:20:52','2017-10-13 16:20:52','192.168.50.101'),(373,1,1,'2017-10-13 16:22:20','2017-10-13 16:22:20','192.168.50.101'),(374,1,1,'2017-10-13 16:22:31','2017-10-13 16:22:31','192.168.50.101'),(375,1,1,'2017-10-13 16:24:11','2017-10-13 16:24:11','192.168.50.101'),(376,1,1,'2017-10-13 16:24:20','2017-10-13 16:24:20','192.168.50.101'),(377,1,1,'2017-10-13 16:24:37','2017-10-13 16:24:37','192.168.50.101'),(378,1,1,'2017-10-13 16:26:17','2017-10-13 16:26:17','192.168.50.101'),(379,1,1,'2017-10-13 16:26:19','2017-10-13 16:26:19','192.168.50.101'),(380,1,1,'2017-10-13 16:26:22','2017-10-13 16:26:22','192.168.50.101'),(381,1,1,'2017-10-13 16:26:29','2017-10-13 16:26:29','192.168.50.101'),(382,1,1,'2017-10-13 16:26:34','2017-10-13 16:26:34','192.168.50.101'),(383,1,1,'2017-10-13 16:28:52','2017-10-13 16:28:52','192.168.50.101'),(384,1,1,'2017-10-13 16:28:55','2017-10-13 16:28:55','192.168.50.101'),(385,1,1,'2017-10-13 16:33:15','2017-10-13 16:33:15','192.168.50.101'),(386,1,1,'2017-10-13 16:35:30','2017-10-13 16:35:30','192.168.50.101'),(387,1,1,'2017-10-13 16:35:41','2017-10-13 16:35:41','192.168.50.101'),(388,1,1,'2017-10-13 16:35:45','2017-10-13 16:35:45','192.168.50.101'),(389,1,1,'2017-10-13 16:35:47','2017-10-13 16:35:47','192.168.50.101'),(390,1,1,'2017-10-13 16:36:01','2017-10-13 16:36:01','192.168.50.101'),(391,1,1,'2017-10-13 16:36:02','2017-10-13 16:36:02','192.168.50.101'),(392,1,1,'2017-10-13 16:46:50','2017-10-13 16:46:50','192.168.50.101'),(393,1,1,'2017-10-13 16:47:20','2017-10-13 16:47:20','192.168.50.101'),(394,1,1,'2017-10-13 16:47:40','2017-10-13 16:47:40','192.168.50.101'),(395,1,1,'2017-10-16 11:33:01','2017-10-16 11:33:01','192.168.50.101'),(396,1,1,'2017-10-16 11:38:57','2017-10-16 11:38:57','192.168.50.101'),(397,1,1,'2017-10-17 03:14:14','2017-10-17 03:14:14','192.168.50.101'),(398,1,1,'2017-10-17 03:14:20','2017-10-17 03:14:20','192.168.50.101'),(399,1,1,'2017-10-17 04:23:22','2017-10-17 04:23:22','192.168.50.101'),(400,1,1,'2017-10-17 04:24:22','2017-10-17 04:24:22','192.168.50.101'),(401,1,1,'2017-10-17 04:24:55','2017-10-17 04:24:55','192.168.50.101'),(402,1,1,'2017-10-17 04:25:16','2017-10-17 04:25:16','192.168.50.101'),(403,1,1,'2017-10-17 04:27:32','2017-10-17 04:27:32','192.168.50.101'),(404,1,1,'2017-10-17 04:28:03','2017-10-17 04:28:03','192.168.50.101'),(405,1,1,'2017-10-17 04:28:17','2017-10-17 04:28:17','192.168.50.101'),(406,1,1,'2017-10-17 04:30:59','2017-10-17 04:30:59','192.168.50.101'),(407,1,1,'2017-10-17 04:31:41','2017-10-17 04:31:41','192.168.50.101'),(408,1,1,'2017-10-17 04:33:05','2017-10-17 04:33:05','192.168.50.101'),(409,1,1,'2017-10-17 04:33:33','2017-10-17 04:33:33','192.168.50.101'),(410,1,1,'2017-10-17 04:33:43','2017-10-17 04:33:43','192.168.50.101'),(411,1,1,'2017-10-17 04:34:11','2017-10-17 04:34:11','192.168.50.101'),(412,1,1,'2017-10-17 04:35:52','2017-10-17 04:35:52','192.168.50.101'),(413,1,1,'2017-10-17 04:35:55','2017-10-17 04:35:55','192.168.50.101'),(414,1,1,'2017-10-17 04:58:29','2017-10-17 04:58:29','192.168.50.101'),(415,1,1,'2017-10-17 04:58:53','2017-10-17 04:58:53','192.168.50.101'),(416,1,1,'2017-10-17 04:59:07','2017-10-17 04:59:07','192.168.50.101'),(417,1,1,'2017-10-17 04:59:24','2017-10-17 04:59:24','192.168.50.101'),(418,1,1,'2017-10-17 05:03:06','2017-10-17 05:03:06','192.168.50.101'),(419,1,1,'2017-10-17 05:04:53','2017-10-17 05:04:53','192.168.50.101'),(420,1,1,'2017-10-17 05:05:20','2017-10-17 05:05:20','192.168.50.101'),(421,1,1,'2017-10-17 05:05:37','2017-10-17 05:05:37','192.168.50.101'),(422,1,1,'2017-10-17 05:05:46','2017-10-17 05:05:46','192.168.50.101'),(423,1,1,'2017-10-17 05:05:49','2017-10-17 05:05:49','192.168.50.101'),(424,1,1,'2017-10-17 05:05:52','2017-10-17 05:05:52','192.168.50.101'),(425,1,1,'2017-10-17 05:07:35','2017-10-17 05:07:35','192.168.50.101'),(426,1,1,'2017-10-17 05:09:36','2017-10-17 05:09:36','192.168.50.101'),(427,1,1,'2017-10-17 05:10:41','2017-10-17 05:10:41','192.168.50.101'),(428,1,1,'2017-10-17 05:11:18','2017-10-17 05:11:18','192.168.50.101'),(429,1,1,'2017-10-17 05:12:28','2017-10-17 05:12:28','192.168.50.101'),(430,1,1,'2017-10-17 05:18:39','2017-10-17 05:18:39','192.168.50.101'),(431,1,1,'2017-10-17 05:18:51','2017-10-17 05:18:51','192.168.50.101'),(432,1,1,'2017-10-17 05:18:57','2017-10-17 05:18:57','192.168.50.101'),(433,1,1,'2017-10-17 05:22:34','2017-10-17 05:22:34','192.168.50.101'),(434,1,1,'2017-10-17 05:22:38','2017-10-17 05:22:38','192.168.50.101'),(435,1,1,'2017-10-17 05:25:20','2017-10-17 05:25:20','192.168.50.101'),(436,1,1,'2017-10-17 05:25:53','2017-10-17 05:25:53','192.168.50.101'),(437,1,1,'2017-10-17 05:26:27','2017-10-17 05:26:27','192.168.50.101'),(438,1,1,'2017-10-17 05:27:00','2017-10-17 05:27:00','192.168.50.101'),(439,1,1,'2017-10-17 05:27:05','2017-10-17 05:27:05','192.168.50.101'),(440,1,1,'2017-10-17 05:28:19','2017-10-17 05:28:19','192.168.50.101'),(441,1,1,'2017-10-17 05:29:27','2017-10-17 05:29:27','192.168.50.101'),(442,1,1,'2017-10-17 05:29:57','2017-10-17 05:29:57','192.168.50.101'),(443,1,1,'2017-10-17 05:30:23','2017-10-17 05:30:23','192.168.50.101'),(444,1,1,'2017-10-17 05:31:16','2017-10-17 05:31:16','192.168.50.101'),(445,1,1,'2017-10-17 05:31:36','2017-10-17 05:31:36','192.168.50.101'),(446,1,1,'2017-10-17 05:32:04','2017-10-17 05:32:04','192.168.50.101'),(447,1,1,'2017-10-17 05:32:34','2017-10-17 05:32:34','192.168.50.101'),(448,1,1,'2017-10-17 05:32:46','2017-10-17 05:32:46','192.168.50.101'),(449,1,1,'2017-10-17 05:32:49','2017-10-17 05:32:49','192.168.50.101'),(450,1,1,'2017-10-17 05:33:32','2017-10-17 05:33:32','192.168.50.101'),(451,1,1,'2017-10-17 05:33:34','2017-10-17 05:33:34','192.168.50.101'),(452,1,1,'2017-10-17 05:50:02','2017-10-17 05:50:02','192.168.50.101'),(453,1,1,'2017-10-17 05:50:20','2017-10-17 05:50:20','192.168.50.101'),(454,1,1,'2017-10-17 05:50:23','2017-10-17 05:50:23','192.168.50.101'),(455,1,1,'2017-10-17 05:50:46','2017-10-17 05:50:46','192.168.50.101'),(456,1,1,'2017-10-17 05:50:53','2017-10-17 05:50:53','192.168.50.101'),(457,1,1,'2017-10-17 05:50:54','2017-10-17 05:50:54','192.168.50.101'),(458,1,1,'2017-10-17 05:50:56','2017-10-17 05:50:56','192.168.50.101'),(459,1,1,'2017-10-17 05:51:03','2017-10-17 05:51:03','192.168.50.101'),(460,1,1,'2017-10-17 06:04:01','2017-10-17 06:04:01','192.168.50.101'),(461,4,1,'2017-10-17 06:14:11','2017-10-17 06:14:11','192.168.50.101'),(462,1,1,'2017-10-17 06:15:46','2017-10-17 06:15:46','192.168.50.101'),(463,1,1,'2017-10-17 06:16:10','2017-10-17 06:16:10','192.168.50.101'),(464,5,1,'2017-10-17 06:18:41','2017-10-17 06:18:41','192.168.50.101'),(465,6,1,'2017-10-17 06:20:45','2017-10-17 06:20:45','192.168.50.101'),(466,6,1,'2017-10-17 06:21:22','2017-10-17 06:21:22','192.168.50.101'),(467,7,1,'2017-10-17 06:23:49','2017-10-17 06:23:49','192.168.50.101'),(468,7,1,'2017-10-17 06:24:56','2017-10-17 06:24:56','192.168.50.101'),(469,7,1,'2017-10-17 06:24:57','2017-10-17 06:24:57','192.168.50.101'),(470,8,1,'2017-10-17 06:27:14','2017-10-17 06:27:14','192.168.50.101'),(471,8,1,'2017-10-17 06:27:25','2017-10-17 06:27:25','192.168.50.101'),(472,3,1,'2017-10-17 06:34:16','2017-10-17 06:34:16','192.168.50.101'),(473,3,1,'2017-10-17 06:34:22','2017-10-17 06:34:22','192.168.50.101'),(474,9,1,'2017-10-17 07:10:09','2017-10-17 07:10:09','192.168.50.101'),(475,1,1,'2017-10-17 07:10:18','2017-10-17 07:10:18','192.168.50.101'),(476,1,1,'2017-10-17 07:10:20','2017-10-17 07:10:20','192.168.50.101'),(477,1,1,'2017-10-17 07:16:10','2017-10-17 07:16:10','192.168.50.101'),(478,1,1,'2017-10-17 07:16:38','2017-10-17 07:16:38','192.168.50.101'),(479,1,1,'2017-10-17 07:28:06','2017-10-17 07:28:06','192.168.50.101'),(480,1,1,'2017-10-17 07:49:48','2017-10-17 07:49:48','192.168.50.101'),(481,1,1,'2017-10-17 07:53:46','2017-10-17 07:53:46','192.168.50.101'),(482,1,1,'2017-10-17 07:58:02','2017-10-17 07:58:02','192.168.50.101'),(483,1,1,'2017-10-17 07:58:30','2017-10-17 07:58:30','192.168.50.101'),(484,1,1,'2017-10-17 07:58:41','2017-10-17 07:58:41','192.168.50.101'),(485,1,1,'2017-10-17 07:59:00','2017-10-17 07:59:00','192.168.50.101'),(486,1,1,'2017-10-17 07:59:18','2017-10-17 07:59:18','192.168.50.101'),(487,1,1,'2017-10-17 07:59:46','2017-10-17 07:59:46','192.168.50.101'),(488,1,1,'2017-10-17 08:00:17','2017-10-17 08:00:17','192.168.50.101'),(489,1,1,'2017-10-17 08:00:35','2017-10-17 08:00:35','192.168.50.101'),(490,1,1,'2017-10-17 08:01:46','2017-10-17 08:01:46','192.168.50.101'),(491,1,1,'2017-10-17 08:03:56','2017-10-17 08:03:56','192.168.50.101'),(492,1,1,'2017-10-17 08:04:27','2017-10-17 08:04:27','192.168.50.101'),(493,1,1,'2017-10-17 08:05:17','2017-10-17 08:05:17','192.168.50.101'),(494,1,1,'2017-10-17 08:05:48','2017-10-17 08:05:48','192.168.50.101'),(495,1,1,'2017-10-17 08:06:11','2017-10-17 08:06:11','192.168.50.101'),(496,1,1,'2017-10-17 08:06:34','2017-10-17 08:06:34','192.168.50.101'),(497,1,1,'2017-10-17 08:07:29','2017-10-17 08:07:29','192.168.50.101'),(498,1,1,'2017-10-17 08:09:06','2017-10-17 08:09:06','192.168.50.101'),(499,1,1,'2017-10-17 08:09:22','2017-10-17 08:09:22','192.168.50.101'),(500,10,1,'2017-10-17 08:12:08','2017-10-17 08:12:08','192.168.50.101'),(501,1,1,'2017-10-17 09:39:48','2017-10-17 09:39:48','192.168.50.101'),(502,1,1,'2017-10-17 12:13:17','2017-10-17 12:13:17','192.168.50.101'),(503,2,1,'2017-10-17 17:01:19','2017-10-17 17:01:19','192.168.50.101'),(504,2,1,'2017-10-17 17:10:22','2017-10-17 17:10:22','192.168.50.101'),(505,2,1,'2017-10-17 17:10:55','2017-10-17 17:10:55','192.168.50.101'),(506,11,1,'2017-10-17 17:19:53','2017-10-17 17:19:53','192.168.50.101'),(507,11,1,'2017-10-17 17:20:29','2017-10-17 17:20:29','192.168.50.101'),(508,11,1,'2017-10-17 17:20:31','2017-10-17 17:20:31','192.168.50.101'),(509,11,1,'2017-10-17 17:28:55','2017-10-17 17:28:55','192.168.50.101'),(510,11,1,'2017-10-17 17:34:07','2017-10-17 17:34:07','192.168.50.101'),(511,12,1,'2017-10-17 17:34:33','2017-10-17 17:34:33','192.168.50.101'),(512,12,1,'2017-10-17 17:34:40','2017-10-17 17:34:40','192.168.50.101'),(513,13,1,'2017-10-17 17:40:05','2017-10-17 17:40:05','192.168.50.101'),(514,14,1,'2017-10-17 17:44:37','2017-10-17 17:44:37','192.168.50.101'),(515,14,1,'2017-10-17 17:46:12','2017-10-17 17:46:12','192.168.50.101'),(516,15,1,'2017-10-17 17:46:59','2017-10-17 17:46:59','192.168.50.101'),(517,16,1,'2017-10-17 17:47:23','2017-10-17 17:47:23','192.168.50.101'),(518,16,1,'2017-10-17 17:47:34','2017-10-17 17:47:34','192.168.50.101'),(519,17,1,'2017-10-17 17:47:46','2017-10-17 17:47:46','192.168.50.101'),(520,18,1,'2017-10-17 17:48:27','2017-10-17 17:48:27','192.168.50.101'),(521,19,1,'2017-10-17 17:50:11','2017-10-17 17:50:11','192.168.50.101'),(522,19,1,'2017-10-17 17:51:11','2017-10-17 17:51:11','192.168.50.101'),(523,19,1,'2017-10-17 17:52:19','2017-10-17 17:52:19','192.168.50.101'),(524,19,1,'2017-10-17 17:53:18','2017-10-17 17:53:18','192.168.50.101'),(525,20,1,'2017-10-17 17:54:22','2017-10-17 17:54:22','192.168.50.101'),(526,21,1,'2017-10-17 17:55:12','2017-10-17 17:55:12','192.168.50.101'),(527,22,1,'2017-10-17 17:57:50','2017-10-17 17:57:50','192.168.50.101'),(528,23,1,'2017-10-17 17:59:55','2017-10-17 17:59:55','192.168.50.101'),(529,24,1,'2017-10-17 18:00:16','2017-10-17 18:00:16','192.168.50.101'),(530,25,1,'2017-10-17 18:01:38','2017-10-17 18:01:38','192.168.50.101'),(531,2,1,'2017-10-18 07:54:26','2017-10-18 07:54:26','192.168.50.101'),(532,2,1,'2017-10-18 07:54:30','2017-10-18 07:54:30','192.168.50.101'),(533,26,1,'2017-10-18 07:58:45','2017-10-18 07:58:45','192.168.50.101'),(534,2,1,'2017-10-18 08:03:27','2017-10-18 08:03:27','192.168.50.101'),(535,2,1,'2017-10-18 08:03:31','2017-10-18 08:03:31','192.168.50.101'),(536,2,1,'2017-10-18 08:53:03','2017-10-18 08:53:03','192.168.50.101'),(537,2,1,'2017-10-18 08:53:05','2017-10-18 08:53:05','192.168.50.101'),(538,26,1,'2017-10-18 08:53:08','2017-10-18 08:53:08','192.168.50.101'),(539,2,1,'2017-10-18 08:59:15','2017-10-18 08:59:15','192.168.50.101'),(540,2,1,'2017-10-18 08:59:19','2017-10-18 08:59:19','192.168.50.101'),(541,2,1,'2017-10-18 08:59:27','2017-10-18 08:59:27','192.168.50.101'),(542,2,1,'2017-10-18 09:30:56','2017-10-18 09:30:56','192.168.50.101'),(543,2,1,'2017-10-18 09:30:57','2017-10-18 09:30:57','192.168.50.101'),(544,2,1,'2017-10-18 09:30:59','2017-10-18 09:30:59','192.168.50.101'),(545,2,1,'2017-10-18 09:31:04','2017-10-18 09:31:04','192.168.50.101'),(546,2,1,'2017-10-18 09:31:08','2017-10-18 09:31:08','192.168.50.101'),(547,2,1,'2017-10-18 09:31:10','2017-10-18 09:31:10','192.168.50.101'),(548,2,1,'2017-10-18 09:31:11','2017-10-18 09:31:11','192.168.50.101'),(549,2,1,'2017-10-18 09:31:48','2017-10-18 09:31:48','192.168.50.101'),(550,26,1,'2017-10-18 09:32:05','2017-10-18 09:32:05','192.168.50.101'),(551,2,1,'2017-10-18 09:32:22','2017-10-18 09:32:22','192.168.50.101'),(552,2,1,'2017-10-18 09:32:36','2017-10-18 09:32:36','192.168.50.101'),(553,2,1,'2017-10-18 09:32:42','2017-10-18 09:32:42','192.168.50.101'),(554,2,1,'2017-10-18 09:32:57','2017-10-18 09:32:57','192.168.50.101'),(555,1,1,'2017-10-19 06:28:30','2017-10-19 06:28:30','192.168.50.101'),(556,1,1,'2017-10-19 06:34:34','2017-10-19 06:34:34','192.168.50.101'),(557,1,1,'2017-10-19 08:04:49','2017-10-19 08:04:49','192.168.50.101'),(558,1,1,'2017-10-19 08:09:58','2017-10-19 08:09:58','192.168.50.101'),(559,1,1,'2017-10-19 08:10:06','2017-10-19 08:10:06','192.168.50.101'),(560,1,1,'2017-10-19 08:10:15','2017-10-19 08:10:15','192.168.50.101'),(561,1,1,'2017-10-19 08:10:26','2017-10-19 08:10:26','192.168.50.101'),(562,1,1,'2017-10-19 08:10:38','2017-10-19 08:10:38','192.168.50.101'),(563,1,1,'2017-10-19 08:11:05','2017-10-19 08:11:05','192.168.50.101'),(564,1,1,'2017-10-19 08:11:12','2017-10-19 08:11:12','192.168.50.101'),(565,1,1,'2017-10-19 08:11:23','2017-10-19 08:11:23','192.168.50.101'),(566,1,1,'2017-10-19 08:12:40','2017-10-19 08:12:40','192.168.50.101'),(567,1,1,'2017-10-19 08:16:48','2017-10-19 08:16:48','192.168.50.101'),(568,1,1,'2017-10-19 08:17:24','2017-10-19 08:17:24','192.168.50.101'),(569,1,1,'2017-10-19 08:18:38','2017-10-19 08:18:38','192.168.50.101'),(570,1,1,'2017-10-19 08:19:58','2017-10-19 08:19:58','192.168.50.101'),(571,1,1,'2017-10-19 08:20:22','2017-10-19 08:20:22','192.168.50.101'),(572,1,1,'2017-10-19 08:20:34','2017-10-19 08:20:34','192.168.50.101'),(573,1,1,'2017-10-19 08:29:12','2017-10-19 08:29:12','192.168.50.101'),(574,2,1,'2017-10-19 08:41:03','2017-10-19 08:41:03','192.168.50.101'),(575,2,1,'2017-10-19 08:41:09','2017-10-19 08:41:09','192.168.50.101'),(576,2,1,'2017-10-19 08:41:23','2017-10-19 08:41:23','192.168.50.101'),(577,1,1,'2017-10-19 08:50:09','2017-10-19 08:50:09','192.168.50.101'),(578,2,1,'2017-10-19 09:00:07','2017-10-19 09:00:07','192.168.50.101'),(579,2,1,'2017-10-19 09:14:33','2017-10-19 09:14:33','192.168.50.101'),(580,1,1,'2017-10-21 12:15:40','2017-10-21 12:15:40','192.168.50.101'),(581,1,1,'2017-10-21 12:21:13','2017-10-21 12:21:13','192.168.50.101'),(582,1,1,'2017-10-21 12:21:16','2017-10-21 12:21:16','192.168.50.101'),(583,1,1,'2017-10-21 12:21:18','2017-10-21 12:21:18','192.168.50.101'),(584,1,1,'2017-10-21 12:21:20','2017-10-21 12:21:20','192.168.50.101'),(585,1,1,'2017-10-21 12:26:37','2017-10-21 12:26:37','192.168.50.101'),(586,1,1,'2017-10-21 12:27:18','2017-10-21 12:27:18','192.168.50.101'),(587,1,1,'2017-10-21 12:27:52','2017-10-21 12:27:52','192.168.50.101'),(588,1,1,'2017-10-21 12:27:53','2017-10-21 12:27:53','192.168.50.101'),(589,1,1,'2017-10-21 12:29:03','2017-10-21 12:29:03','192.168.50.101'),(590,1,1,'2017-10-21 12:29:51','2017-10-21 12:29:51','192.168.50.101'),(591,1,1,'2017-10-21 12:30:03','2017-10-21 12:30:03','192.168.50.101'),(592,1,1,'2017-10-21 12:30:40','2017-10-21 12:30:40','192.168.50.101'),(593,1,1,'2017-10-21 12:30:43','2017-10-21 12:30:43','192.168.50.101'),(594,1,1,'2017-10-21 12:30:46','2017-10-21 12:30:46','192.168.50.101'),(595,1,1,'2017-10-21 13:48:50','2017-10-21 13:48:50','192.168.50.101'),(596,1,1,'2017-10-21 13:53:20','2017-10-21 13:53:20','192.168.50.101'),(597,1,1,'2017-10-24 08:43:33','2017-10-24 08:43:33','192.168.50.101'),(598,1,1,'2017-10-24 08:56:21','2017-10-24 08:56:21','192.168.50.101'),(599,1,1,'2017-10-24 08:56:24','2017-10-24 08:56:24','192.168.50.101'),(600,1,1,'2017-10-24 08:56:29','2017-10-24 08:56:29','192.168.50.101'),(601,1,1,'2017-10-24 08:56:46','2017-10-24 08:56:46','192.168.50.101'),(602,1,1,'2017-10-24 08:56:49','2017-10-24 08:56:49','192.168.50.101'),(603,1,1,'2017-10-24 09:30:35','2017-10-24 09:30:35','192.168.50.101'),(604,1,1,'2017-10-24 09:30:36','2017-10-24 09:30:36','192.168.50.101'),(605,1,1,'2017-10-24 09:38:06','2017-10-24 09:38:06','192.168.50.101'),(606,1,1,'2017-10-24 09:47:41','2017-10-24 09:47:41','192.168.50.101'),(607,1,1,'2017-10-24 09:56:48','2017-10-24 09:56:48','192.168.50.101'),(608,1,1,'2017-10-24 09:56:51','2017-10-24 09:56:51','192.168.50.101'),(609,1,1,'2017-10-24 09:56:53','2017-10-24 09:56:53','192.168.50.101'),(610,1,1,'2017-10-24 10:04:37','2017-10-24 10:04:37','192.168.50.101'),(611,1,1,'2017-10-24 10:06:37','2017-10-24 10:06:37','192.168.50.101'),(612,1,1,'2017-10-24 10:07:01','2017-10-24 10:07:01','192.168.50.101'),(613,1,1,'2017-10-24 10:07:16','2017-10-24 10:07:16','192.168.50.101'),(614,1,1,'2017-10-24 10:07:19','2017-10-24 10:07:19','192.168.50.101'),(615,1,1,'2017-10-24 10:07:21','2017-10-24 10:07:21','192.168.50.101'),(616,1,1,'2017-10-24 15:35:39','2017-10-24 15:35:39','192.168.50.101'),(617,1,1,'2017-10-24 17:03:41','2017-10-24 17:03:41','192.168.50.101'),(618,1,1,'2017-10-24 17:05:29','2017-10-24 17:05:29','192.168.50.101'),(619,1,1,'2017-10-24 17:08:35','2017-10-24 17:08:35','192.168.50.101'),(620,1,1,'2017-10-24 17:08:51','2017-10-24 17:08:51','192.168.50.101'),(621,1,1,'2017-10-24 17:21:13','2017-10-24 17:21:13','192.168.50.101'),(622,1,1,'2017-10-24 17:40:06','2017-10-24 17:40:06','192.168.50.101'),(623,1,1,'2017-10-26 03:21:23','2017-10-26 03:21:23','192.168.50.101'),(624,1,1,'2017-10-26 04:28:08','2017-10-26 04:28:08','192.168.50.101'),(625,1,1,'2017-10-26 04:28:40','2017-10-26 04:28:40','192.168.50.101'),(626,1,1,'2017-10-26 06:03:30','2017-10-26 06:03:30','192.168.50.101'),(627,1,1,'2017-10-26 06:03:33','2017-10-26 06:03:33','192.168.50.101'),(628,1,1,'2017-10-26 06:03:35','2017-10-26 06:03:35','192.168.50.101'),(629,1,1,'2017-10-26 07:16:29','2017-10-26 07:16:29','192.168.50.101'),(630,1,1,'2017-10-26 07:26:35','2017-10-26 07:26:35','192.168.50.101'),(631,1,1,'2017-10-26 07:29:41','2017-10-26 07:29:41','192.168.50.101'),(632,1,1,'2017-10-26 07:30:43','2017-10-26 07:30:43','192.168.50.101'),(633,1,1,'2017-10-26 07:30:44','2017-10-26 07:30:44','192.168.50.101'),(634,1,1,'2017-10-26 07:30:47','2017-10-26 07:30:47','192.168.50.101'),(635,1,1,'2017-10-26 07:30:48','2017-10-26 07:30:48','192.168.50.101'),(636,1,1,'2017-10-26 07:31:34','2017-10-26 07:31:34','192.168.50.101'),(637,1,1,'2017-10-26 07:31:35','2017-10-26 07:31:35','192.168.50.101'),(638,1,1,'2017-10-26 07:32:24','2017-10-26 07:32:24','192.168.50.101'),(639,1,1,'2017-10-26 08:06:15','2017-10-26 08:06:15','192.168.50.101'),(640,1,1,'2017-10-26 08:08:31','2017-10-26 08:08:31','192.168.50.101'),(641,1,1,'2017-10-26 08:08:42','2017-10-26 08:08:42','192.168.50.101'),(642,1,1,'2017-10-26 08:10:10','2017-10-26 08:10:10','192.168.50.101'),(643,1,1,'2017-10-26 08:11:23','2017-10-26 08:11:23','192.168.50.101'),(644,1,1,'2017-10-26 08:11:48','2017-10-26 08:11:48','192.168.50.101'),(645,1,1,'2017-10-26 08:12:35','2017-10-26 08:12:35','192.168.50.101'),(646,1,1,'2017-10-26 08:32:40','2017-10-26 08:32:40','192.168.50.101'),(647,1,1,'2017-10-26 08:33:09','2017-10-26 08:33:09','192.168.50.101'),(648,1,1,'2017-10-26 08:33:29','2017-10-26 08:33:29','192.168.50.101'),(649,1,1,'2017-10-26 08:33:34','2017-10-26 08:33:34','192.168.50.101'),(650,1,1,'2017-10-26 08:34:47','2017-10-26 08:34:47','192.168.50.101'),(651,1,1,'2017-10-26 08:35:31','2017-10-26 08:35:31','192.168.50.101'),(652,1,1,'2017-10-26 08:35:33','2017-10-26 08:35:33','192.168.50.101'),(653,1,1,'2017-10-26 08:35:46','2017-10-26 08:35:46','192.168.50.101'),(654,1,1,'2017-10-26 08:36:05','2017-10-26 08:36:05','192.168.50.101'),(655,1,1,'2017-10-26 08:36:12','2017-10-26 08:36:12','192.168.50.101'),(656,1,1,'2017-10-26 08:36:21','2017-10-26 08:36:21','192.168.50.101'),(657,1,1,'2017-10-26 08:36:23','2017-10-26 08:36:23','192.168.50.101'),(658,1,1,'2017-10-26 08:36:54','2017-10-26 08:36:54','192.168.50.101'),(659,1,1,'2017-10-26 08:37:37','2017-10-26 08:37:37','192.168.50.101'),(660,1,1,'2017-10-26 08:38:45','2017-10-26 08:38:45','192.168.50.101'),(661,1,1,'2017-10-26 08:39:02','2017-10-26 08:39:02','192.168.50.101'),(662,1,1,'2017-10-26 08:39:15','2017-10-26 08:39:15','192.168.50.101'),(663,1,1,'2017-10-26 08:43:16','2017-10-26 08:43:16','192.168.50.101'),(664,1,1,'2017-10-26 08:44:44','2017-10-26 08:44:44','192.168.50.101'),(665,1,1,'2017-10-26 08:45:16','2017-10-26 08:45:16','192.168.50.101'),(666,1,1,'2017-10-26 08:46:29','2017-10-26 08:46:29','192.168.50.101'),(667,1,1,'2017-10-26 08:46:37','2017-10-26 08:46:37','192.168.50.101'),(668,1,1,'2017-10-26 08:47:27','2017-10-26 08:47:27','192.168.50.101'),(669,1,1,'2017-10-26 08:50:11','2017-10-26 08:50:11','192.168.50.101'),(670,1,1,'2017-10-26 08:50:28','2017-10-26 08:50:28','192.168.50.101'),(671,1,1,'2017-10-26 08:58:06','2017-10-26 08:58:06','192.168.50.101'),(672,1,1,'2017-10-26 08:58:18','2017-10-26 08:58:18','192.168.50.101'),(673,1,1,'2017-10-26 08:58:31','2017-10-26 08:58:31','192.168.50.101'),(674,1,1,'2017-10-26 09:05:55','2017-10-26 09:05:55','192.168.50.101'),(675,1,1,'2017-10-26 09:06:10','2017-10-26 09:06:10','192.168.50.101'),(676,1,1,'2017-10-26 09:06:30','2017-10-26 09:06:30','192.168.50.101'),(677,1,1,'2017-10-26 09:06:53','2017-10-26 09:06:53','192.168.50.101'),(678,1,1,'2017-10-26 09:08:38','2017-10-26 09:08:38','192.168.50.101'),(679,1,1,'2017-10-26 09:08:50','2017-10-26 09:08:50','192.168.50.101'),(680,1,1,'2017-10-26 09:09:39','2017-10-26 09:09:39','192.168.50.101'),(681,1,1,'2017-10-26 09:10:00','2017-10-26 09:10:00','192.168.50.101'),(682,1,1,'2017-10-26 09:10:16','2017-10-26 09:10:16','192.168.50.101'),(683,1,1,'2017-10-26 09:11:58','2017-10-26 09:11:58','192.168.50.101'),(684,1,1,'2017-10-26 09:12:04','2017-10-26 09:12:04','192.168.50.101'),(685,1,1,'2017-10-26 09:12:06','2017-10-26 09:12:06','192.168.50.101'),(686,1,1,'2017-10-26 09:14:25','2017-10-26 09:14:25','192.168.50.101'),(687,1,1,'2017-10-26 09:15:24','2017-10-26 09:15:24','192.168.50.101'),(688,1,1,'2017-10-27 02:50:01','2017-10-27 02:50:01','192.168.50.101'),(689,1,1,'2017-10-27 02:50:12','2017-10-27 02:50:12','192.168.50.101'),(690,1,1,'2017-10-27 02:50:15','2017-10-27 02:50:15','192.168.50.101'),(691,1,1,'2017-10-27 02:50:27','2017-10-27 02:50:27','192.168.50.101'),(692,1,1,'2017-10-27 02:50:33','2017-10-27 02:50:33','192.168.50.101'),(693,1,1,'2017-10-27 02:50:35','2017-10-27 02:50:35','192.168.50.101'),(694,1,1,'2017-10-27 02:50:41','2017-10-27 02:50:41','192.168.50.101'),(695,1,1,'2017-10-27 03:45:43','2017-10-27 03:45:43','192.168.50.101'),(696,1,1,'2017-10-27 03:49:44','2017-10-27 03:49:44','192.168.50.101'),(697,1,1,'2017-10-27 03:49:47','2017-10-27 03:49:47','192.168.50.101'),(698,1,1,'2017-10-27 03:50:13','2017-10-27 03:50:13','192.168.50.101'),(699,1,1,'2017-10-27 03:51:43','2017-10-27 03:51:43','192.168.50.101'),(700,1,1,'2017-10-27 03:51:45','2017-10-27 03:51:45','192.168.50.101'),(701,1,1,'2017-10-27 03:52:37','2017-10-27 03:52:37','192.168.50.101'),(702,1,1,'2017-10-27 03:52:42','2017-10-27 03:52:42','192.168.50.101'),(703,1,1,'2017-10-27 03:52:46','2017-10-27 03:52:46','192.168.50.101'),(704,1,1,'2017-10-27 03:53:14','2017-10-27 03:53:14','192.168.50.101'),(705,1,1,'2017-10-27 05:12:48','2017-10-27 05:12:48','192.168.50.101'),(706,1,1,'2017-10-27 06:34:11','2017-10-27 06:34:11','192.168.50.101'),(707,1,1,'2017-10-27 07:53:52','2017-10-27 07:53:52','192.168.50.101'),(708,1,1,'2017-10-27 08:11:55','2017-10-27 08:11:55','192.168.50.101'),(709,1,1,'2017-10-27 08:15:05','2017-10-27 08:15:05','192.168.50.101'),(710,1,1,'2017-10-27 08:16:28','2017-10-27 08:16:28','192.168.50.101'),(711,1,1,'2017-10-27 08:16:48','2017-10-27 08:16:48','192.168.50.101'),(712,1,1,'2017-10-27 08:16:54','2017-10-27 08:16:54','192.168.50.101'),(713,1,1,'2017-10-27 08:21:33','2017-10-27 08:21:33','192.168.50.101'),(714,1,1,'2017-10-27 08:22:42','2017-10-27 08:22:42','192.168.50.101'),(715,1,1,'2017-10-27 08:22:53','2017-10-27 08:22:53','192.168.50.101'),(716,1,1,'2017-10-27 08:28:36','2017-10-27 08:28:36','192.168.50.101'),(717,1,1,'2017-10-27 08:31:00','2017-10-27 08:31:00','192.168.50.101'),(718,1,1,'2017-10-27 08:31:21','2017-10-27 08:31:21','192.168.50.101'),(719,1,1,'2017-10-27 08:31:46','2017-10-27 08:31:46','192.168.50.101'),(720,1,1,'2017-10-27 08:32:25','2017-10-27 08:32:25','192.168.50.101'),(721,1,1,'2017-10-27 08:35:12','2017-10-27 08:35:12','192.168.50.101'),(722,1,1,'2017-10-27 08:35:14','2017-10-27 08:35:14','192.168.50.101'),(723,1,1,'2017-10-27 08:57:07','2017-10-27 08:57:07','192.168.50.101'),(724,1,1,'2017-10-27 08:57:34','2017-10-27 08:57:34','192.168.50.101'),(725,1,1,'2017-10-27 08:58:19','2017-10-27 08:58:19','192.168.50.101'),(726,1,1,'2017-10-27 08:58:30','2017-10-27 08:58:30','192.168.50.101'),(727,1,1,'2017-10-27 08:59:22','2017-10-27 08:59:22','192.168.50.101'),(728,1,1,'2017-10-27 08:59:33','2017-10-27 08:59:33','192.168.50.101'),(729,1,1,'2017-10-27 08:59:56','2017-10-27 08:59:56','192.168.50.101'),(730,1,1,'2017-10-27 08:59:59','2017-10-27 08:59:59','192.168.50.101'),(731,2,1,'2017-10-27 09:00:30','2017-10-27 09:00:30','192.168.50.101'),(732,1,1,'2017-10-27 09:17:04','2017-10-27 09:17:04','192.168.50.101'),(733,1,1,'2017-10-27 09:17:43','2017-10-27 09:17:43','192.168.50.101'),(734,2,1,'2017-10-27 09:18:01','2017-10-27 09:18:01','192.168.50.101'),(735,1,1,'2017-10-27 09:18:15','2017-10-27 09:18:15','192.168.50.101'),(736,1,1,'2017-10-27 09:18:24','2017-10-27 09:18:24','192.168.50.101'),(737,1,1,'2017-10-27 09:19:53','2017-10-27 09:19:53','192.168.50.101'),(738,1,1,'2017-10-27 09:20:29','2017-10-27 09:20:29','192.168.50.101'),(739,1,1,'2017-10-27 09:20:56','2017-10-27 09:20:56','192.168.50.101'),(740,1,1,'2017-10-27 09:24:36','2017-10-27 09:24:36','192.168.50.101'),(741,1,1,'2017-10-27 09:27:46','2017-10-27 09:27:46','192.168.50.101'),(742,1,1,'2017-10-27 09:28:04','2017-10-27 09:28:04','192.168.50.101'),(743,1,1,'2017-10-27 09:28:16','2017-10-27 09:28:16','192.168.50.101'),(744,1,1,'2017-10-27 09:28:32','2017-10-27 09:28:32','192.168.50.101'),(745,2,1,'2017-10-27 09:28:58','2017-10-27 09:28:58','192.168.50.101'),(746,2,1,'2017-10-27 09:29:36','2017-10-27 09:29:36','192.168.50.101'),(747,2,1,'2017-10-27 09:30:00','2017-10-27 09:30:00','192.168.50.101'),(748,2,1,'2017-10-27 09:30:36','2017-10-27 09:30:36','192.168.50.101'),(749,2,1,'2017-10-27 09:30:38','2017-10-27 09:30:38','192.168.50.101'),(750,1,1,'2017-10-27 09:40:21','2017-10-27 09:40:21','192.168.50.101'),(751,1,1,'2017-10-27 09:43:35','2017-10-27 09:43:35','192.168.50.101'),(752,1,1,'2017-10-27 09:43:58','2017-10-27 09:43:58','192.168.50.101'),(753,1,1,'2017-10-27 09:44:22','2017-10-27 09:44:22','192.168.50.101'),(754,1,1,'2017-10-27 09:44:24','2017-10-27 09:44:24','192.168.50.101'),(755,1,1,'2017-10-29 10:56:09','2017-10-29 10:56:09','192.168.50.101'),(756,1,1,'2017-10-29 10:57:30','2017-10-29 10:57:30','192.168.50.101'),(757,27,1,'2017-11-16 13:10:59','2017-11-16 13:10:59','192.168.50.101'),(758,27,1,'2017-11-16 13:34:03','2017-11-16 13:34:03','192.168.50.101'),(759,27,1,'2017-11-16 13:34:14','2017-11-16 13:34:14','192.168.50.101'),(760,27,1,'2017-11-16 13:35:58','2017-11-16 13:35:58','192.168.50.101'),(761,27,1,'2017-11-16 13:36:15','2017-11-16 13:36:15','192.168.50.101'),(762,27,1,'2017-11-16 13:36:17','2017-11-16 13:36:17','192.168.50.101'),(763,27,1,'2017-11-16 13:36:20','2017-11-16 13:36:20','192.168.50.101'),(764,27,1,'2017-11-16 13:37:15','2017-11-16 13:37:15','192.168.50.101'),(765,27,1,'2017-11-16 13:37:34','2017-11-16 13:37:34','192.168.50.101'),(766,27,1,'2017-11-16 13:38:19','2017-11-16 13:38:19','192.168.50.101'),(767,27,1,'2017-11-16 21:27:39','2017-11-16 21:27:39','192.168.50.101'),(768,27,1,'2017-11-16 21:27:43','2017-11-16 21:27:43','192.168.50.101'),(769,27,1,'2017-11-16 21:27:45','2017-11-16 21:27:45','192.168.50.101'),(770,27,1,'2017-11-16 21:34:38','2017-11-16 21:34:38','192.168.50.101'),(771,27,1,'2017-11-16 21:34:48','2017-11-16 21:34:48','192.168.50.101'),(772,27,1,'2017-11-16 21:37:34','2017-11-16 21:37:34','192.168.50.101'),(773,27,1,'2017-11-16 21:37:38','2017-11-16 21:37:38','192.168.50.101'),(774,27,1,'2017-11-16 21:38:42','2017-11-16 21:38:42','192.168.50.101'),(775,27,1,'2017-11-16 21:38:43','2017-11-16 21:38:43','192.168.50.101'),(776,27,1,'2017-11-16 21:39:41','2017-11-16 21:39:41','192.168.50.101'),(777,27,1,'2017-11-16 21:39:43','2017-11-16 21:39:43','192.168.50.101'),(778,27,1,'2017-11-16 21:40:44','2017-11-16 21:40:44','192.168.50.101'),(779,27,1,'2017-11-17 03:47:16','2017-11-17 03:47:16','192.168.50.101'),(780,27,1,'2017-11-17 04:37:08','2017-11-17 04:37:08','192.168.50.101'),(781,27,1,'2017-11-17 07:28:39','2017-11-17 07:28:39','192.168.50.101'),(782,27,1,'2017-11-17 07:28:41','2017-11-17 07:28:41','192.168.50.101'),(783,27,1,'2017-11-17 09:46:13','2017-11-17 09:46:13','192.168.50.101'),(784,27,1,'2017-11-17 09:46:27','2017-11-17 09:46:27','192.168.50.101'),(785,27,1,'2017-11-17 09:47:01','2017-11-17 09:47:01','192.168.50.101'),(786,27,1,'2017-11-17 09:47:05','2017-11-17 09:47:05','192.168.50.101'),(787,27,1,'2017-11-17 09:47:07','2017-11-17 09:47:07','192.168.50.101'),(788,27,1,'2017-11-17 09:47:13','2017-11-17 09:47:13','192.168.50.101'),(789,27,1,'2017-11-17 09:48:03','2017-11-17 09:48:03','192.168.50.101'),(790,27,1,'2017-11-17 09:48:50','2017-11-17 09:48:50','192.168.50.101'),(791,27,1,'2017-11-17 09:49:51','2017-11-17 09:49:51','192.168.50.101'),(792,27,1,'2017-11-17 09:50:34','2017-11-17 09:50:34','192.168.50.101'),(793,28,1,'2017-11-17 09:51:49','2017-11-17 09:51:49','192.168.50.101'),(794,28,1,'2017-11-17 09:51:51','2017-11-17 09:51:51','192.168.50.101'),(795,27,1,'2017-11-17 09:53:56','2017-11-17 09:53:56','192.168.50.101'),(796,27,1,'2017-11-17 09:53:59','2017-11-17 09:53:59','192.168.50.101'),(797,27,1,'2017-11-17 09:54:02','2017-11-17 09:54:02','192.168.50.101'),(798,27,1,'2017-11-17 09:54:06','2017-11-17 09:54:06','192.168.50.101'),(799,27,1,'2017-11-17 09:54:07','2017-11-17 09:54:07','192.168.50.101'),(800,27,1,'2017-11-17 09:54:11','2017-11-17 09:54:11','192.168.50.101'),(801,27,1,'2017-11-17 09:54:14','2017-11-17 09:54:14','192.168.50.101'),(802,27,1,'2017-11-17 09:54:20','2017-11-17 09:54:20','192.168.50.101'),(803,27,1,'2017-11-17 09:54:23','2017-11-17 09:54:23','192.168.50.101'),(804,27,1,'2017-11-17 09:54:29','2017-11-17 09:54:29','192.168.50.101'),(805,27,1,'2017-11-17 09:54:35','2017-11-17 09:54:35','192.168.50.101'),(806,27,1,'2017-11-17 09:55:20','2017-11-17 09:55:20','192.168.50.101'),(807,27,1,'2017-11-17 13:45:02','2017-11-17 13:45:02','192.168.50.101'),(808,29,1,'2017-11-19 08:11:49','2017-11-19 08:11:49','192.168.50.101'),(809,29,1,'2017-11-19 08:51:44','2017-11-19 08:51:44','192.168.50.101'),(810,29,1,'2017-11-19 08:52:02','2017-11-19 08:52:02','192.168.50.101'),(811,29,1,'2017-11-19 08:53:05','2017-11-19 08:53:05','192.168.50.101'),(812,29,1,'2017-11-19 08:53:06','2017-11-19 08:53:06','192.168.50.101'),(813,29,1,'2017-11-19 08:53:09','2017-11-19 08:53:09','192.168.50.101'),(814,29,1,'2017-11-19 08:53:20','2017-11-19 08:53:20','192.168.50.101'),(815,29,1,'2017-11-19 08:53:23','2017-11-19 08:53:23','192.168.50.101'),(816,29,1,'2017-11-19 08:57:48','2017-11-19 08:57:48','192.168.50.101'),(817,29,1,'2017-11-19 08:57:50','2017-11-19 08:57:50','192.168.50.101'),(818,29,1,'2017-11-19 08:58:00','2017-11-19 08:58:00','192.168.50.101'),(819,29,1,'2017-11-19 09:02:30','2017-11-19 09:02:30','192.168.50.101'),(820,29,1,'2017-11-19 09:02:33','2017-11-19 09:02:33','192.168.50.101'),(821,29,1,'2017-11-19 09:04:11','2017-11-19 09:04:11','192.168.50.101'),(822,29,1,'2017-11-19 09:05:04','2017-11-19 09:05:04','192.168.50.101'),(823,29,1,'2017-11-19 09:07:07','2017-11-19 09:07:07','192.168.50.101'),(824,29,1,'2017-11-19 09:07:09','2017-11-19 09:07:09','192.168.50.101'),(825,29,1,'2017-11-19 09:11:17','2017-11-19 09:11:17','192.168.50.101'),(826,29,1,'2017-11-19 09:12:23','2017-11-19 09:12:23','192.168.50.101'),(827,29,1,'2017-11-19 09:12:25','2017-11-19 09:12:25','192.168.50.101'),(828,29,1,'2017-11-19 09:12:25','2017-11-19 09:12:25','192.168.50.101'),(829,29,1,'2017-11-19 09:12:26','2017-11-19 09:12:26','192.168.50.101'),(830,29,1,'2017-11-19 09:12:27','2017-11-19 09:12:27','192.168.50.101'),(831,29,1,'2017-11-19 10:30:38','2017-11-19 10:30:38','192.168.50.101'),(832,29,1,'2017-11-19 13:46:52','2017-11-19 13:46:52','192.168.50.101'),(833,29,1,'2017-11-19 13:46:54','2017-11-19 13:46:54','192.168.50.101'),(834,29,1,'2017-11-19 13:47:00','2017-11-19 13:47:00','192.168.50.101'),(835,29,1,'2017-11-19 13:47:01','2017-11-19 13:47:01','192.168.50.101'),(836,29,1,'2017-11-19 13:47:31','2017-11-19 13:47:31','192.168.50.101'),(837,29,1,'2017-11-19 13:49:37','2017-11-19 13:49:37','192.168.50.101'),(838,29,1,'2017-11-19 13:57:11','2017-11-19 13:57:11','192.168.50.101'),(839,29,1,'2017-11-19 13:57:33','2017-11-19 13:57:33','192.168.50.101'),(840,30,1,'2017-11-20 09:46:58','2017-11-20 09:46:58','192.168.50.1'),(841,30,1,'2017-11-20 09:47:12','2017-11-20 09:47:12','192.168.50.1'),(842,30,1,'2017-11-20 09:47:21','2017-11-20 09:47:21','192.168.50.1'),(843,30,1,'2017-11-20 09:47:23','2017-11-20 09:47:23','192.168.50.1'),(844,30,1,'2017-11-20 09:47:32','2017-11-20 09:47:32','192.168.50.1'),(845,30,1,'2017-11-20 09:47:39','2017-11-20 09:47:39','192.168.50.1'),(846,30,1,'2017-11-20 09:47:41','2017-11-20 09:47:41','192.168.50.1'),(847,30,1,'2017-11-20 09:47:42','2017-11-20 09:47:42','192.168.50.1'),(848,30,1,'2017-11-20 09:47:44','2017-11-20 09:47:44','192.168.50.1'),(849,30,1,'2017-11-20 09:53:05','2017-11-20 09:53:05','192.168.50.1'),(850,30,1,'2017-11-20 09:53:10','2017-11-20 09:53:10','192.168.50.1'),(851,31,1,'2017-11-20 23:32:48','2017-11-20 23:32:48','192.168.50.1'),(852,31,1,'2017-11-20 23:33:24','2017-11-20 23:33:24','192.168.50.1'),(853,31,1,'2017-11-20 23:54:28','2017-11-20 23:54:28','192.168.50.1'),(854,32,1,'2017-11-21 07:44:29','2017-11-21 07:44:29','192.168.50.1'),(855,32,1,'2017-11-21 07:44:39','2017-11-21 07:44:39','192.168.50.1'),(856,32,1,'2017-11-21 07:44:42','2017-11-21 07:44:42','192.168.50.1'),(857,32,1,'2017-11-21 07:44:45','2017-11-21 07:44:45','192.168.50.1'),(858,32,1,'2017-11-21 07:44:49','2017-11-21 07:44:49','192.168.50.1'),(859,32,1,'2017-11-21 07:44:50','2017-11-21 07:44:50','192.168.50.1'),(860,32,1,'2017-11-21 07:45:04','2017-11-21 07:45:04','192.168.50.1'),(861,32,1,'2017-11-21 07:45:07','2017-11-21 07:45:07','192.168.50.1'),(862,32,1,'2017-11-21 07:45:10','2017-11-21 07:45:10','192.168.50.1'),(863,32,1,'2017-11-21 07:45:12','2017-11-21 07:45:12','192.168.50.1'),(864,32,1,'2017-11-21 07:45:14','2017-11-21 07:45:14','192.168.50.1'),(865,32,1,'2017-11-21 07:45:17','2017-11-21 07:45:17','192.168.50.1'),(866,32,1,'2017-11-21 07:47:40','2017-11-21 07:47:40','192.168.50.1'),(867,32,1,'2017-11-21 07:47:43','2017-11-21 07:47:43','192.168.50.1'),(868,32,1,'2017-11-21 07:47:46','2017-11-21 07:47:46','192.168.50.1'),(869,32,1,'2017-11-21 07:47:47','2017-11-21 07:47:47','192.168.50.1'),(870,32,1,'2017-11-21 07:47:50','2017-11-21 07:47:50','192.168.50.1'),(871,32,1,'2017-11-21 07:47:52','2017-11-21 07:47:52','192.168.50.1'),(872,32,1,'2017-11-21 07:47:54','2017-11-21 07:47:54','192.168.50.1'),(873,32,1,'2017-11-21 07:47:57','2017-11-21 07:47:57','192.168.50.1'),(874,32,1,'2017-11-21 07:48:12','2017-11-21 07:48:12','192.168.50.1'),(875,32,1,'2017-11-21 08:07:43','2017-11-21 08:07:43','192.168.50.1'),(876,32,1,'2017-11-21 08:07:45','2017-11-21 08:07:45','192.168.50.1'),(877,32,1,'2017-11-21 08:07:58','2017-11-21 08:07:58','192.168.50.1'),(878,32,1,'2017-11-21 08:08:00','2017-11-21 08:08:00','192.168.50.1'),(879,32,1,'2017-11-21 08:08:02','2017-11-21 08:08:02','192.168.50.1'),(880,32,1,'2017-11-21 08:08:03','2017-11-21 08:08:03','192.168.50.1'),(881,32,1,'2017-11-21 08:08:04','2017-11-21 08:08:04','192.168.50.1'),(882,32,1,'2017-11-21 08:13:20','2017-11-21 08:13:20','192.168.50.1'),(883,32,1,'2017-11-21 08:13:22','2017-11-21 08:13:22','192.168.50.1'),(884,32,1,'2017-11-21 08:13:25','2017-11-21 08:13:25','192.168.50.1'),(885,32,1,'2017-11-21 08:13:27','2017-11-21 08:13:27','192.168.50.1'),(886,32,1,'2017-11-21 08:16:53','2017-11-21 08:16:53','192.168.50.1');
/*!40000 ALTER TABLE `fingerprint_recordings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fingerprints`
--

DROP TABLE IF EXISTS `fingerprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fingerprints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fingerprints_hash_unique` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fingerprints`
--

LOCK TABLES `fingerprints` WRITE;
/*!40000 ALTER TABLE `fingerprints` DISABLE KEYS */;
INSERT INTO `fingerprints` VALUES (1,'a0997aa6b7aa429b97699709c381082e','2017-10-08 10:03:15','2017-10-08 10:03:15'),(2,'e6a30c96d64fe6d584375785d820f1bb','2017-10-08 12:05:56','2017-10-08 12:05:56'),(3,'44ef761a8e6a0e4e04f0a138f11ca136','2017-10-08 17:01:41','2017-10-08 17:01:41'),(4,'3e48ea846c5a6d53623772f564a38c32','2017-10-17 06:14:11','2017-10-17 06:14:11'),(5,'445e1f25eb82666ccc2813a7f51b0618','2017-10-17 06:18:41','2017-10-17 06:18:41'),(6,'92831c5d333db1adcf06efe2fccdc2ff','2017-10-17 06:20:45','2017-10-17 06:20:45'),(7,'4fc2c9955c01c397dd9652640472a83f','2017-10-17 06:23:49','2017-10-17 06:23:49'),(8,'21c6d40b4b8f0616fa826c19fd789f6c','2017-10-17 06:27:14','2017-10-17 06:27:14'),(9,'95c5279d2dac8da4f7610fc1e1c6701a','2017-10-17 07:10:09','2017-10-17 07:10:09'),(10,'4171e6f5a30dc846159eff6add89e518','2017-10-17 08:12:08','2017-10-17 08:12:08'),(11,'782d4158e36a7cbf343faa32f2196169','2017-10-17 17:19:53','2017-10-17 17:19:53'),(12,'abf7ffd46ee01ac86bdc1442ec1b1fc7','2017-10-17 17:34:33','2017-10-17 17:34:33'),(13,'e1cd158af8308465607656694bbd507f','2017-10-17 17:40:05','2017-10-17 17:40:05'),(14,'b46e996e4558d84834346b51457719cb','2017-10-17 17:44:37','2017-10-17 17:44:37'),(15,'ddc416da6e1011f1dbb4eba26ed00524','2017-10-17 17:46:59','2017-10-17 17:46:59'),(16,'75999612cb62e3b298330f8451581166','2017-10-17 17:47:23','2017-10-17 17:47:23'),(17,'08d88525864f7970eebd102c70ceece6','2017-10-17 17:47:46','2017-10-17 17:47:46'),(18,'efc9a2e3af0c5e058079c3dbea9d9f5f','2017-10-17 17:48:27','2017-10-17 17:48:27'),(19,'0ed648ac7bbc2af9050dc583ed110881','2017-10-17 17:50:11','2017-10-17 17:50:11'),(20,'3cb28535a502f14f3c24e155fe986b7a','2017-10-17 17:54:22','2017-10-17 17:54:22'),(21,'8b9820e2463280d4bd8050d051b339b7','2017-10-17 17:55:12','2017-10-17 17:55:12'),(22,'b55799f1773244b5d60a874cd718bb74','2017-10-17 17:57:50','2017-10-17 17:57:50'),(23,'f2e897a6664b8d0b0dffb34632b8e52c','2017-10-17 17:59:55','2017-10-17 17:59:55'),(24,'4c3e54d38bf69801d97e5310a1c426ab','2017-10-17 18:00:16','2017-10-17 18:00:16'),(25,'a56b5438f9ac6337c9931dadc07e78af','2017-10-17 18:01:38','2017-10-17 18:01:38'),(26,'9b3920aaaaef67a21f05ccd919d0936c','2017-10-18 07:58:45','2017-10-18 07:58:45'),(27,'ce91527a2c72b4c4de4b791b7ec2cd2a','2017-11-16 13:10:59','2017-11-16 13:10:59'),(28,'a2019f4447c122b2799d7ddb5a5f8fdd','2017-11-17 09:51:49','2017-11-17 09:51:49'),(29,'13df398d9bcf4aa05420b83e3ba65d25','2017-11-19 08:11:49','2017-11-19 08:11:49'),(30,'9bde217b644ec9baaee981cd5935225b','2017-11-20 09:46:58','2017-11-20 09:46:58'),(31,'6c06edfd96b0159e9aeab561e2c1f381','2017-11-20 23:32:48','2017-11-20 23:32:48'),(32,'6059d00eee0a7751c0c17c47d5ca55ff','2017-11-21 07:44:29','2017-11-21 07:44:29');
/*!40000 ALTER TABLE `fingerprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `helpdesk_conversations`
--

DROP TABLE IF EXISTS `helpdesk_conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk_conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `conv_id` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_reply_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `helpdesk_conversations_user_id_foreign` (`user_id`),
  CONSTRAINT `helpdesk_conversations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `helpdesk_conversations`
--

LOCK TABLES `helpdesk_conversations` WRITE;
/*!40000 ALTER TABLE `helpdesk_conversations` DISABLE KEYS */;
/*!40000 ALTER TABLE `helpdesk_conversations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invitations`
--

DROP TABLE IF EXISTS `invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invitations` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invitations_token_unique` (`token`),
  KEY `invitations_team_id_index` (`team_id`),
  KEY `invitations_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invitations`
--

LOCK TABLES `invitations` WRITE;
/*!40000 ALTER TABLE `invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_reminders`
--

DROP TABLE IF EXISTS `invoice_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` int(11) NOT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_reminders_subscription_id_index` (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_reminders`
--

LOCK TABLES `invoice_reminders` WRITE;
/*!40000 ALTER TABLE `invoice_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `card_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoices_created_at_index` (`created_at`),
  KEY `invoices_user_id_index` (`user_id`),
  KEY `invoices_team_id_index` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2017_02_11_173613_create_performance_indicators_table',1),(2,'2017_02_11_173614_create_announcements_table',1),(3,'2017_02_11_173616_create_users_table',1),(4,'2017_02_11_173619_create_password_resets_table',1),(5,'2017_02_11_173623_create_api_tokens_table',1),(6,'2017_02_11_173628_create_subscriptions_table',1),(7,'2017_02_11_173634_create_invoices_table',1),(8,'2017_02_11_173641_create_notifications_table',1),(9,'2017_02_11_173649_create_teams_table',1),(10,'2017_02_11_173658_create_team_users_table',1),(11,'2017_02_11_173708_create_invitations_table',1),(12,'2017_02_26_155459_add_is_subscribed_to_user_table',1),(13,'2017_02_17_162416_create_posts_table',2),(14,'2017_03_04_173635_add_paypal_fields_to_subscription',3),(15,'2017_03_04_175247_make_stripe_fields_nullable_in_subscriptions',3),(16,'2017_03_04_182545_create_paypal_plans_table',3),(17,'2017_03_09_100348_add_provider_plan_name_to_subscriptions',3),(18,'2017_03_09_160613_add_billing_provider_to_subscriptions',3),(19,'2017_03_12_173021_add_last_payment_at_to_subscriptions',3),(20,'2017_03_13_213904_create_user_paypal_plans_table',3),(21,'2017_03_18_190909_rename_user_paypal_plans_to_user_pending_subscriptions',3),(22,'2017_04_02_201515_add_has_used_trial_to_users',3),(23,'2017_04_09_205803_create_invoice_reminders_table',3),(24,'2017_04_22_191553_add_is_cancelled_to_subscriptions',3),(25,'2017_04_23_203337_make_paypal_plan_id_nullable_in_paypal_plans',4),(26,'2017_03_29_145006_create_tag_tables',5),(27,'2017_04_03_054651_create_media_table',5),(28,'2017_05_07_174545_create_coinbase_checkouts_table',6),(29,'2017_05_08_182042_create_coinbase_notifications_table',6),(30,'2017_05_08_183446_create_paypal_notifications_table',7),(31,'2017_05_20_093449_create_permission_tables',8),(32,'2017_05_20_103808_add_default_roles_permissions',8),(33,'2017_05_21_070520_add_verification_to_user_table',8),(34,'2017_06_08_165936_create_paypal_and_coinbase_permissions',9),(35,'2017_06_02_063006_fingerprints_table',10),(36,'2017_06_02_063727_fingerprint_recordings_table',10),(37,'2017_06_05_074303_permissions_to_view_fingerprints',10),(38,'2017_06_15_135541_add_frauder_role_and_capture_ip_address',11),(39,'2017_06_05_150636_create_partial_payments_table',12),(40,'2017_06_09_113326_add_pending_reason_to_user_pending_subscriptions',12),(41,'2017_06_09_113442_add_pending_reason_to_subscriptions',12),(42,'2017_06_12_040827_add_is_affiliate_to_users_table',12),(43,'2017_06_12_140550_create_affiliates_table',12),(44,'2017_06_12_140620_create_affiliate_transactions_table',12),(45,'2017_06_14_081938_add_affiliate_id_to_users_table',12),(46,'2017_06_16_200945_add_credits_to_users_table',12),(47,'2017_06_22_070851_create_soft_launch_invitations_table',12),(48,'2017_06_22_092753_soft_launch_shortlist_table',12),(49,'2017_06_18_091736_ad_campaign_bonus_types_table',13),(50,'2017_06_18_091816_ad_campaigns_table',13),(51,'2017_06_18_091955_ad_campaign_bonuses_table',13),(52,'2017_06_18_092449_add_ad_campaign_bonus_to_user_table',13),(53,'2017_07_04_175613_create_referral_logs_table',14),(54,'2017_07_05_180554_add_referrer_affiliate_id_to_users_table',14),(55,'2017_07_13_170745_remove_affiliate_id_from_users_table',14),(56,'2017_07_14_092804_add_ad_campaign_id_to_paypal_plans',14),(57,'2017_07_14_100509_add_ad_campaign_id_to_coinbase_checkouts',14),(58,'2017_07_14_142026_add_reward_type_to_ad_campaign_bonus_types_table',14),(59,'2017_07_14_225311_add_discount_type_to_coinbase_checkouts_table',14),(60,'2017_06_27_083423_helpdesk_permissions',15),(61,'2017_05_29_201724_add_banned_at_to_users_table',16),(62,'2017_08_30_103743_email_marketing',16),(63,'2017_08_26_143313_add_tfa_secret_to_users_table',17),(64,'2017_08_30_103227_add_tfa_enable_to_users_table',17),(65,'2017_08_31_174956_create_tfa_backup_codes_table',17),(66,'2017_08_31_203940_add_generated_backup_codes_at_to_users_table',17),(67,'2017_09_11_210041_add_verified_role',18),(68,'2017_08_22_132856_add_show_guide_to_users_table',19),(69,'2017_08_07_160456_create_sales_invoices_table',20),(70,'2017_08_07_165033_create_sales_invoice_items_table',20),(71,'2017_08_14_031818_create_payment_logs_table',20),(72,'2017_08_14_084206_add_ext_invoice_id_to_sales_invoices_table',20),(73,'2017_06_22_181013_add_paypal_payer_info_to_users_table',21),(74,'2017_06_25_062714_create_pending_orders_table',21),(75,'2017_06_26_093835_create_pending_order_actions_table',21),(76,'2017_07_07_131625_create_user_paypal_verified_emails_table',21),(77,'2017_07_07_173909_add_confirmation_key_to_pending_orders',21),(78,'2017_07_09_174809_add_user_id_to_pending_orders_table',21),(91,'2017_09_20_082328_create_rank_tracking_versions_table',22),(92,'2017_09_20_082953_create_rank_tracking_settings_table',22),(93,'2017_09_25_083149_create_rank_tracking_engines_table',22),(94,'2017_09_25_083411_create_rank_tracking_table',22),(95,'2017_09_25_083422_create_rank_tracking_ranks_table',22),(96,'2017_09_25_085125_create_rank_tracking_favorites_table',23),(97,'2017_09_27_021649_create_alexa_submissions_table',24),(98,'2017_09_27_024102_create_alexa_settings_table',24),(99,'2017_09_28_042106_create_c_t_r_settings_table',25),(100,'2017_09_28_042123_create_c_t_r_submissions_table',25),(101,'2017_09_28_191405_create_rank_tracking_statuses_table',26),(104,'2017_09_28_192603_create_alexa_statuses_table',27),(105,'2017_09_28_192721_add_id_status_to_rank_tracking_table',27),(107,'2017_09_28_193020_create_c_t_r_statuses_table',28),(110,'2017_09_28_195931_drop_version_foreign_key_from_rank_tracking_settings_table',29),(111,'2017_09_30_233109_create_traffic_submissions_table',30),(112,'2017_09_30_233118_create_traffic_settings_table',30),(113,'2017_10_02_012618_create_wayback_settings_table',31),(114,'2017_10_02_012625_create_wayback_submissions_table',31),(115,'2017_10_02_082628_create_traffic_statuses_table',32),(116,'2017_10_02_084738_create_wayback_statuses_table',33),(117,'2017_10_05_175351_add_indexes_to_rank_tracking_table',34),(118,'2017_10_05_175404_add_indexes_to_rank_tracking_ranks_table',34),(119,'2017_10_05_175417_add_indexes_to_rank_tracking_favorites_table',34),(120,'2017_10_05_175424_add_indexes_to_alexa_submissions_table',34),(121,'2017_10_05_175435_add_indexes_to_c_t_r_submissions_table',34),(122,'2017_10_05_175447_add_indexes_to_traffic_submissions_table',34),(123,'2017_10_05_175454_add_indexes_to_wayback_submissions_table',34),(124,'2017_10_08_181237_add_sent_verification_email_at_to_users_table',35),(125,'2017_10_11_202515_create_rank_tracking_domains_table',36),(126,'2017_10_11_234132_add_domain_id_to_rank_tracking',36),(127,'2017_10_15_132246_create_helpdesk_conversations_table',37),(129,'2017_10_17_071825_add_timezone_to_users_table',38),(139,'2017_11_17_041435_update_rank_tracking_ranks_schema_to_match_api',39);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,1,'App\\User'),(6,1,'App\\User'),(7,1,'App\\User'),(8,1,'App\\User'),(1,6,'App\\User'),(6,6,'App\\User'),(7,6,'App\\User'),(1,11,'App\\User'),(6,11,'App\\User'),(7,11,'App\\User'),(2,16,'App\\User'),(6,16,'App\\User'),(7,16,'App\\User'),(4,17,'App\\User'),(6,17,'App\\User'),(7,17,'App\\User'),(3,18,'App\\User'),(6,18,'App\\User'),(7,18,'App\\User'),(3,37,'App\\User'),(6,37,'App\\User'),(7,37,'App\\User'),(3,38,'App\\User'),(6,38,'App\\User'),(7,38,'App\\User'),(3,61,'App\\User'),(6,61,'App\\User'),(7,61,'App\\User'),(3,62,'App\\User'),(6,62,'App\\User'),(7,62,'App\\User'),(3,63,'App\\User'),(6,63,'App\\User'),(7,63,'App\\User'),(3,64,'App\\User'),(6,64,'App\\User'),(7,64,'App\\User'),(3,65,'App\\User'),(6,65,'App\\User'),(7,65,'App\\User'),(3,66,'App\\User'),(6,66,'App\\User'),(7,66,'App\\User'),(3,67,'App\\User'),(6,67,'App\\User'),(7,67,'App\\User'),(3,68,'App\\User'),(6,68,'App\\User'),(7,68,'App\\User'),(3,70,'App\\User'),(6,70,'App\\User'),(7,70,'App\\User'),(3,71,'App\\User'),(6,71,'App\\User'),(7,71,'App\\User'),(3,72,'App\\User'),(6,72,'App\\User'),(7,72,'App\\User');
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_url` text COLLATE utf8mb4_unicode_ci,
  `read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_user_id_created_at_index` (`user_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES ('140d5784-87e0-4e78-a1b3-5ebff7afc0d1',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-10 06:12:26','2017-10-17 03:03:29'),('385f0128-d1a3-4fd0-b7e2-c9200de109cc',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-08 18:36:02','2017-10-17 03:03:29'),('472be5d9-2e0d-49fa-9853-10c109162a1e',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-10 06:12:56','2017-10-17 03:03:29'),('4bb7a4ba-42f9-4e17-907d-9434e7e5a2f6',61,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,0,'2017-10-03 14:19:13','2017-10-03 14:19:13'),('5e00a1c9-2d5d-4054-ba36-21b0f198d3ed',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-08 18:34:35','2017-10-17 03:03:29'),('652532c8-f786-466d-9a41-d53538b85460',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-08 18:37:53','2017-10-17 03:03:29'),('a1761fbc-ad49-4826-b066-faf13a572b63',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-08 18:34:08','2017-10-17 03:03:29'),('ab5e0a7c-530f-42ee-a9b3-eae2a6884a2e',11,NULL,'fa-clock-o','Your trial period will expire on April 18th.','Subscribe','/settings#/subscription',0,'2017-04-08 17:08:43','2017-04-08 17:08:43'),('bc23fbed-aa70-420d-9548-a7e9cc846bf8',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-08 18:35:39','2017-10-17 03:03:29'),('cf88638f-2cf8-4e30-bc21-ffc858bb4a44',6,NULL,'fa-clock-o','Your trial period will expire on March 24th.','Subscribe','/settings#/subscription',1,'2017-03-14 14:31:02','2017-03-14 15:03:58'),('f9a93b42-f9dd-4203-b935-fe9f39e94baf',1,NULL,'fa-envelope-o','The verification message has been re-sent to your email address.',NULL,NULL,1,'2017-10-08 18:36:34','2017-10-17 03:03:29');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partial_payments`
--

DROP TABLE IF EXISTS `partial_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partial_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `subscription_id` int(11) DEFAULT NULL,
  `pending_subscription_id` int(11) DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_paid` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `total_amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `partial_payments_user_id_index` (`user_id`),
  KEY `partial_payments_subscription_id_index` (`subscription_id`),
  KEY `partial_payments_pending_subscription_id_index` (`pending_subscription_id`),
  KEY `partial_payments_order_id_index` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partial_payments`
--

LOCK TABLES `partial_payments` WRITE;
/*!40000 ALTER TABLE `partial_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `partial_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_logs`
--

DROP TABLE IF EXISTS `payment_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sales_invoice_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `payment_processor_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason_data` json NOT NULL,
  `notification_data` json NOT NULL,
  `dollar_amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_logs_sales_invoice_id_foreign` (`sales_invoice_id`),
  KEY `payment_logs_user_id_foreign` (`user_id`),
  CONSTRAINT `payment_logs_sales_invoice_id_foreign` FOREIGN KEY (`sales_invoice_id`) REFERENCES `sales_invoices` (`id`) ON DELETE SET NULL,
  CONSTRAINT `payment_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_logs`
--

LOCK TABLES `payment_logs` WRITE;
/*!40000 ALTER TABLE `payment_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_notifications`
--

DROP TABLE IF EXISTS `paypal_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `notification_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_details` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_notifications`
--

LOCK TABLES `paypal_notifications` WRITE;
/*!40000 ALTER TABLE `paypal_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paypal_plans`
--

DROP TABLE IF EXISTS `paypal_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paypal_plans` (
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_plan_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_campaign_id` int(10) unsigned DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `paypal_plans_ad_campaign_id_foreign` (`ad_campaign_id`),
  CONSTRAINT `paypal_plans_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paypal_plans`
--

LOCK TABLES `paypal_plans` WRITE;
/*!40000 ALTER TABLE `paypal_plans` DISABLE KEYS */;
INSERT INTO `paypal_plans` VALUES ('provider-id-1','P-4PE90897064052640HCRFFEQ',NULL,1);
/*!40000 ALTER TABLE `paypal_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pending_order_actions`
--

DROP TABLE IF EXISTS `pending_order_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_order_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pending_order_actions_user_id_foreign` (`user_id`),
  KEY `pending_order_actions_order_id_foreign` (`order_id`),
  CONSTRAINT `pending_order_actions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `pending_orders` (`id`),
  CONSTRAINT `pending_order_actions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pending_order_actions`
--

LOCK TABLES `pending_order_actions` WRITE;
/*!40000 ALTER TABLE `pending_order_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pending_order_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pending_orders`
--

DROP TABLE IF EXISTS `pending_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_type` int(11) NOT NULL,
  `billing_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_agreement_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pending_subscription_id` int(10) unsigned DEFAULT NULL,
  `request_details` json DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `confirmation_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmation_key_expires_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pending_orders_pending_subscription_id_foreign` (`pending_subscription_id`),
  KEY `pending_orders_user_id_foreign` (`user_id`),
  CONSTRAINT `pending_orders_pending_subscription_id_foreign` FOREIGN KEY (`pending_subscription_id`) REFERENCES `user_pending_subscriptions` (`id`) ON DELETE SET NULL,
  CONSTRAINT `pending_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pending_orders`
--

LOCK TABLES `pending_orders` WRITE;
/*!40000 ALTER TABLE `pending_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `pending_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `performance_indicators`
--

DROP TABLE IF EXISTS `performance_indicators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `performance_indicators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `monthly_recurring_revenue` decimal(8,2) NOT NULL,
  `yearly_recurring_revenue` decimal(8,2) NOT NULL,
  `daily_volume` decimal(8,2) NOT NULL,
  `new_users` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `performance_indicators_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `performance_indicators`
--

LOCK TABLES `performance_indicators` WRITE;
/*!40000 ALTER TABLE `performance_indicators` DISABLE KEYS */;
/*!40000 ALTER TABLE `performance_indicators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'access kiosk','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(2,'administer announcements','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(3,'administer posts','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(4,'administer users','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(5,'assign roles','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(6,'administer paypal','web','2017-06-08 17:08:43','2017-06-08 17:08:43'),(7,'administer coinbase','web','2017-06-08 17:08:43','2017-06-08 17:08:43'),(8,'view fingerprints','web','2017-06-13 16:53:17','2017-06-13 16:53:17'),(9,'invite users','web','2017-07-10 07:41:02','2017-07-10 07:41:02'),(10,'administer ad campaigns','web','2017-07-10 07:57:30','2017-07-10 07:57:30'),(11,'open new support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49'),(12,'reply to support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_title_unique` (`title`),
  UNIQUE KEY `posts_slug_unique` (`slug`),
  KEY `posts_user_id_foreign` (`user_id`),
  CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (8,6,'Try this','<p>Ahahah</p>','try-this',1,'2017-04-08 19:13:02','2017-04-08 19:10:35','2017-04-08 19:13:02'),(9,6,'one more','<p>Ahahah</p>','one-more',1,'2017-04-08 19:12:59','2017-04-08 19:12:47','2017-04-08 19:12:59');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking`
--

DROP TABLE IF EXISTS `rank_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `engine_id` int(10) unsigned NOT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `stopped_at` timestamp NULL DEFAULT NULL,
  `domain_id` int(10) unsigned DEFAULT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_user_id_foreign` (`user_id`),
  KEY `rank_tracking_engine_id_foreign` (`engine_id`),
  KEY `rank_tracking_version_foreign` (`version`),
  KEY `rank_tracking_user_id_index` (`user_id`),
  KEY `rank_tracking_keyword_index` (`keyword`),
  KEY `rank_tracking_domain_id_foreign` (`domain_id`),
  CONSTRAINT `rank_tracking_domain_id_foreign` FOREIGN KEY (`domain_id`) REFERENCES `rank_tracking_domains` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_engine_id_foreign` FOREIGN KEY (`engine_id`) REFERENCES `rank_tracking_engines` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_version_foreign` FOREIGN KEY (`version`) REFERENCES `rank_tracking_versions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking`
--

LOCK TABLES `rank_tracking` WRITE;
/*!40000 ALTER TABLE `rank_tracking` DISABLE KEYS */;
INSERT INTO `rank_tracking` VALUES (6,'2017-10-02 19:23:53','2017-11-17 13:31:47',1,'alexa seo',1,1,NULL,NULL,'1','',0),(7,'2017-10-02 19:23:54','2017-11-21 07:58:02',1,'awesome seo',1,1,NULL,NULL,'2','',0),(14,'2017-10-27 07:38:50','2017-11-17 13:31:47',1,'seo',1,1,NULL,2,'3','',0),(15,'2017-10-27 07:39:30','2017-11-17 13:31:47',1,'good seo',1,1,NULL,2,'4','',0),(16,'2017-10-27 07:42:23','2017-11-17 13:31:47',1,'cheap seo',1,1,NULL,2,'5','',0),(17,'2017-11-19 13:48:50','2017-11-21 08:19:01',1,'buy youtube views',1,1,NULL,3,'15','US',0);
/*!40000 ALTER TABLE `rank_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_domains`
--

DROP TABLE IF EXISTS `rank_tracking_domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_domains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_domains_user_id_foreign` (`user_id`),
  CONSTRAINT `rank_tracking_domains_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_domains`
--

LOCK TABLES `rank_tracking_domains` WRITE;
/*!40000 ALTER TABLE `rank_tracking_domains` DISABLE KEYS */;
INSERT INTO `rank_tracking_domains` VALUES (1,'2017-10-12 08:06:26','2017-10-12 08:06:26','www.gainrank.com',1),(2,'2017-10-21 08:11:19','2017-10-21 08:11:19','gainrank.com',1),(3,'2017-11-19 13:46:59','2017-11-19 13:46:59','qqtube.com',1);
/*!40000 ALTER TABLE `rank_tracking_domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_engines`
--

DROP TABLE IF EXISTS `rank_tracking_engines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_engines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_engines`
--

LOCK TABLES `rank_tracking_engines` WRITE;
/*!40000 ALTER TABLE `rank_tracking_engines` DISABLE KEYS */;
INSERT INTO `rank_tracking_engines` VALUES (1,'google.com',1,'Google','US');
/*!40000 ALTER TABLE `rank_tracking_engines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_favorites`
--

DROP TABLE IF EXISTS `rank_tracking_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `track_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_favorites_user_id_foreign` (`user_id`),
  KEY `rank_tracking_favorites_track_id_foreign` (`track_id`),
  KEY `rank_tracking_favorites_user_id_index` (`user_id`),
  CONSTRAINT `rank_tracking_favorites_track_id_foreign` FOREIGN KEY (`track_id`) REFERENCES `rank_tracking` (`id`) ON DELETE CASCADE,
  CONSTRAINT `rank_tracking_favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_favorites`
--

LOCK TABLES `rank_tracking_favorites` WRITE;
/*!40000 ALTER TABLE `rank_tracking_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `rank_tracking_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_ranks`
--

DROP TABLE IF EXISTS `rank_tracking_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_ranks` (
  `id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `track_id` int(10) unsigned NOT NULL,
  `rank` smallint(6) NOT NULL,
  `applied_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rank_tracking_ranks_track_id_foreign` (`track_id`),
  KEY `rank_tracking_ranks_rank_index` (`rank`),
  CONSTRAINT `rank_tracking_ranks_track_id_foreign` FOREIGN KEY (`track_id`) REFERENCES `rank_tracking` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_ranks`
--

LOCK TABLES `rank_tracking_ranks` WRITE;
/*!40000 ALTER TABLE `rank_tracking_ranks` DISABLE KEYS */;
INSERT INTO `rank_tracking_ranks` VALUES (391,'2017-11-21 07:43:36','2017-11-21 08:07:22',17,391,'2017-11-15'),(395,'2017-11-21 07:43:36','2017-11-21 08:07:22',17,395,'2017-11-18'),(396,'2017-11-21 07:43:36','2017-11-21 08:07:22',17,396,'2017-11-14'),(397,'2017-11-21 07:43:36','2017-11-21 08:06:36',17,397,'2017-11-13');
/*!40000 ALTER TABLE `rank_tracking_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_settings`
--

DROP TABLE IF EXISTS `rank_tracking_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_settings_name_version_unique` (`name`,`version`),
  KEY `rank_tracking_settings_version_foreign` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_settings`
--

LOCK TABLES `rank_tracking_settings` WRITE;
/*!40000 ALTER TABLE `rank_tracking_settings` DISABLE KEYS */;
INSERT INTO `rank_tracking_settings` VALUES (1,NULL,'2017-10-01 21:21:55',1,'update_interval','60'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,1,'current_version','1'),(6,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_alexa.php'),(7,NULL,NULL,1,'url_update','http://107.178.115.210:8080/domain/{domain}/bulk'),(8,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(9,NULL,NULL,1,'cost_per','1000'),(10,NULL,'2017-10-01 21:22:05',1,'cost_per_x','100');
/*!40000 ALTER TABLE `rank_tracking_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_statuses`
--

DROP TABLE IF EXISTS `rank_tracking_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_statuses`
--

LOCK TABLES `rank_tracking_statuses` WRITE;
/*!40000 ALTER TABLE `rank_tracking_statuses` DISABLE KEYS */;
INSERT INTO `rank_tracking_statuses` VALUES (111,'1233','4532','435','634','345'),(200,'Ok','Ok!!','Ok!sdfsdf','Ok Staff','http://url.com'),(345,'ij','iji','ih','hnb','ii');
/*!40000 ALTER TABLE `rank_tracking_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_tracking_versions`
--

DROP TABLE IF EXISTS `rank_tracking_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_tracking_versions` (
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_tracking_versions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_tracking_versions`
--

LOCK TABLES `rank_tracking_versions` WRITE;
/*!40000 ALTER TABLE `rank_tracking_versions` DISABLE KEYS */;
INSERT INTO `rank_tracking_versions` VALUES (NULL,NULL,1,'1.0');
/*!40000 ALTER TABLE `rank_tracking_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referral_logs`
--

DROP TABLE IF EXISTS `referral_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referral_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` json DEFAULT NULL,
  `fingerprint_id` int(10) unsigned DEFAULT NULL,
  `affiliate_id` int(10) unsigned DEFAULT NULL,
  `referred_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `referral_logs_fingerprint_id_foreign` (`fingerprint_id`),
  KEY `referral_logs_affiliate_id_foreign` (`affiliate_id`),
  KEY `referral_logs_referred_user_id_foreign` (`referred_user_id`),
  CONSTRAINT `referral_logs_affiliate_id_foreign` FOREIGN KEY (`affiliate_id`) REFERENCES `affiliates` (`id`) ON DELETE SET NULL,
  CONSTRAINT `referral_logs_fingerprint_id_foreign` FOREIGN KEY (`fingerprint_id`) REFERENCES `fingerprints` (`id`) ON DELETE SET NULL,
  CONSTRAINT `referral_logs_referred_user_id_foreign` FOREIGN KEY (`referred_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referral_logs`
--

LOCK TABLES `referral_logs` WRITE;
/*!40000 ALTER TABLE `referral_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `referral_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,2),(2,2),(3,2),(9,3),(11,6),(12,7);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(2,'editor','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(3,'regular user','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(4,'blocked','web','2017-05-31 04:38:35','2017-05-31 04:38:35'),(5,'frauder','web','2017-06-15 16:29:24','2017-06-15 16:29:24'),(6,'can open new support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49'),(7,'can reply to support ticket','web','2017-07-25 10:30:49','2017-07-25 10:30:49'),(8,'verified','web','2017-09-16 16:37:34','2017-09-16 16:37:34');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoice_items`
--

DROP TABLE IF EXISTS `sales_invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sales_invoice_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_invoice_items_sales_invoice_id_foreign` (`sales_invoice_id`),
  CONSTRAINT `sales_invoice_items_sales_invoice_id_foreign` FOREIGN KEY (`sales_invoice_id`) REFERENCES `sales_invoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoice_items`
--

LOCK TABLES `sales_invoice_items` WRITE;
/*!40000 ALTER TABLE `sales_invoice_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoice_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_invoices`
--

DROP TABLE IF EXISTS `sales_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `apply_at` timestamp NULL DEFAULT NULL,
  `due_at` timestamp NULL DEFAULT NULL,
  `total_amount` decimal(16,8) NOT NULL DEFAULT '0.00000000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `ext_invoice_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_invoices_user_id_foreign` (`user_id`),
  CONSTRAINT `sales_invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_invoices`
--

LOCK TABLES `sales_invoices` WRITE;
/*!40000 ALTER TABLE `sales_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soft_launch_invitations`
--

DROP TABLE IF EXISTS `soft_launch_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soft_launch_invitations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `invitee_user_id` int(10) unsigned DEFAULT NULL,
  `token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `soft_launch_invitations_token_unique` (`token`),
  KEY `soft_launch_invitations_user_id_foreign` (`user_id`),
  KEY `soft_launch_invitations_invitee_user_id_foreign` (`invitee_user_id`),
  CONSTRAINT `soft_launch_invitations_invitee_user_id_foreign` FOREIGN KEY (`invitee_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `soft_launch_invitations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soft_launch_invitations`
--

LOCK TABLES `soft_launch_invitations` WRITE;
/*!40000 ALTER TABLE `soft_launch_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `soft_launch_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soft_launch_shortlist`
--

DROP TABLE IF EXISTS `soft_launch_shortlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soft_launch_shortlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `soft_launch_shortlist_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soft_launch_shortlist`
--

LOCK TABLES `soft_launch_shortlist` WRITE;
/*!40000 ALTER TABLE `soft_launch_shortlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `soft_launch_shortlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_agreement_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_plan_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_payment_at` timestamp NULL DEFAULT NULL,
  `is_cancelled` tinyint(1) NOT NULL DEFAULT '0',
  `pending_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggables`
--

DROP TABLE IF EXISTS `taggables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggables` (
  `tag_id` int(10) unsigned NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `taggables_tag_id_foreign` (`tag_id`),
  CONSTRAINT `taggables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggables`
--

LOCK TABLES `taggables` WRITE;
/*!40000 ALTER TABLE `taggables` DISABLE KEYS */;
INSERT INTO `taggables` VALUES (1,9,'App\\Post');
/*!40000 ALTER TABLE `taggables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` json NOT NULL,
  `slug` json NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_column` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'{\"en\": \"dsds\"}','{\"en\": \"dsds\"}',NULL,1,'2017-04-08 19:12:47','2017-04-08 19:12:47');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_subscriptions`
--

DROP TABLE IF EXISTS `team_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_subscriptions`
--

LOCK TABLES `team_subscriptions` WRITE;
/*!40000 ALTER TABLE `team_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_users`
--

DROP TABLE IF EXISTS `team_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_users` (
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  UNIQUE KEY `team_users_team_id_user_id_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_users`
--

LOCK TABLES `team_users` WRITE;
/*!40000 ALTER TABLE `team_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `team_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_url` text COLLATE utf8mb4_unicode_ci,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_billing_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_billing_information` text COLLATE utf8mb4_unicode_ci,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teams_slug_unique` (`slug`),
  KEY `teams_owner_id_index` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tfa_backup_codes`
--

DROP TABLE IF EXISTS `tfa_backup_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tfa_backup_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tfa_backup_codes_user_id_foreign` (`user_id`),
  CONSTRAINT `tfa_backup_codes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tfa_backup_codes`
--

LOCK TABLES `tfa_backup_codes` WRITE;
/*!40000 ALTER TABLE `tfa_backup_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tfa_backup_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_settings`
--

DROP TABLE IF EXISTS `traffic_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `traffic_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_settings`
--

LOCK TABLES `traffic_settings` WRITE;
/*!40000 ALTER TABLE `traffic_settings` DISABLE KEYS */;
INSERT INTO `traffic_settings` VALUES (1,NULL,NULL,0,'current_version','1'),(2,NULL,NULL,1,'enabled_add','1'),(3,NULL,NULL,1,'enabled_update','1'),(4,NULL,NULL,1,'enabled_display','1'),(5,NULL,NULL,1,'url_add','http://control.publicdnsroute.com/api/seo_add_traffic.php'),(6,NULL,NULL,1,'url_update','http://control.publicdnsroute.com/api/seo_update_traffic.php'),(7,NULL,NULL,1,'api_key','9ADFE67882901EFCABF27377116'),(8,NULL,NULL,1,'cost_per','1000'),(9,NULL,NULL,1,'cost_per_x','100'),(10,NULL,NULL,1,'update_interval','60');
/*!40000 ALTER TABLE `traffic_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_statuses`
--

DROP TABLE IF EXISTS `traffic_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `traffic_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_statuses`
--

LOCK TABLES `traffic_statuses` WRITE;
/*!40000 ALTER TABLE `traffic_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `traffic_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traffic_submissions`
--

DROP TABLE IF EXISTS `traffic_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wanted_count` int(11) NOT NULL,
  `sent_count` int(11) NOT NULL,
  `cost_per_x` int(11) NOT NULL,
  `cost_per_amt` int(11) NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `traffic_submissions_user_id_foreign` (`user_id`),
  KEY `traffic_submissions_user_id_index` (`user_id`),
  CONSTRAINT `traffic_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_submissions`
--

LOCK TABLES `traffic_submissions` WRITE;
/*!40000 ALTER TABLE `traffic_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `traffic_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_paypal_verified_emails`
--

DROP TABLE IF EXISTS `user_paypal_verified_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_paypal_verified_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_paypal_verified_emails_user_id_foreign` (`user_id`),
  CONSTRAINT `user_paypal_verified_emails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_paypal_verified_emails`
--

LOCK TABLES `user_paypal_verified_emails` WRITE;
/*!40000 ALTER TABLE `user_paypal_verified_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_paypal_verified_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_pending_subscriptions`
--

DROP TABLE IF EXISTS `user_pending_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_pending_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `agreement_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_plan_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `billing_provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pending_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_paypal_plans_user_id_index` (`user_id`),
  KEY `user_paypal_plans_paypal_agreement_id_index` (`agreement_id`),
  KEY `user_paypal_plans_provider_plan_name_index` (`provider_plan_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_pending_subscriptions`
--

LOCK TABLES `user_pending_subscriptions` WRITE;
/*!40000 ALTER TABLE `user_pending_subscriptions` DISABLE KEYS */;
INSERT INTO `user_pending_subscriptions` VALUES (1,1,NULL,'provider-id-1','2017-07-10 14:49:15','2017-07-10 14:49:15','paypal_recurring',NULL);
/*!40000 ALTER TABLE `user_pending_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_url` text COLLATE utf8mb4_unicode_ci,
  `uses_two_factor_auth` tinyint(4) NOT NULL DEFAULT '0',
  `authy_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_reset_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` int(11) DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_billing_plan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_line_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_billing_information` text COLLATE utf8mb4_unicode_ci,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `last_read_announcements_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT '0',
  `has_used_trial` tinyint(1) NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `verification_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helpdesk_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_affiliate` tinyint(1) NOT NULL DEFAULT '0',
  `credits` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ad_campaign_id` int(10) unsigned DEFAULT NULL,
  `referrer_affiliate_id` int(10) unsigned DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  `email_user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tfa_secret` varchar(17) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tfa_enable` tinyint(1) NOT NULL DEFAULT '0',
  `generated_backup_codes_at` timestamp NULL DEFAULT NULL,
  `show_guide` tinyint(1) NOT NULL DEFAULT '1',
  `paypal_payer_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_payer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sent_verification_email_at` timestamp NULL DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_ad_campaign_id_foreign` (`ad_campaign_id`),
  KEY `users_referrer_affiliate_id_foreign` (`referrer_affiliate_id`),
  CONSTRAINT `users_ad_campaign_id_foreign` FOREIGN KEY (`ad_campaign_id`) REFERENCES `ad_campaigns` (`id`) ON DELETE SET NULL,
  CONSTRAINT `users_referrer_affiliate_id_foreign` FOREIGN KEY (`referrer_affiliate_id`) REFERENCES `affiliates` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Tarquin Biscuit-Barrel','kc@jankstudio.com','$2y$10$cJLbLUVbBUPQi8GV1YwzXeIs01nFOfpDBO4idamrgoBh.X5K4aXoG','6UiN4oZjYz7pBVNjNFzcjfZVdLpahCEsibfKJcpl6jSX1XlFYNcmCagsr6FW',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-11 15:26:23','2017-09-26 12:03:31','2017-03-01 15:26:23','2017-10-17 08:09:15',1,0,0,'79681a247a266eda0460ebc7eca58e0ae4942aab2e82ef6a811c2bfaefad40b5',NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,'2017-10-08 18:11:46',0,NULL,NULL,'2017-10-08 18:37:50','Asia/Manila'),(6,'Spyros K','spyrosk@mindaria.com','$2y$10$dw/GV1r..wBukKtpMMs/iOo3B01JOEbbNg7VGN.Fv0AjFhPjGBhk6','64qKOlqPJZONeuOjanWUR9aBIUUJFfXilllqR5g98cWpyyUOSDuRNXJOcUQS',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-24 14:31:02','2017-03-14 14:31:02','2017-03-14 14:31:02','2017-03-14 14:31:02',1,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(11,'Test Admin','spyrosktestadmin@mindaria.com','$2y$10$2uBy3P1dbT0JkV9VsZMUv.AC6Idao2cjeTvFGIy/3qDPv06VSTO2G',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-18 17:08:43','2017-04-08 17:08:43','2017-04-08 17:08:43','2017-04-08 17:08:43',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(16,'Test Editor','spyrosktesteditor@mindaria.com','$2y$10$ryjxI7wVFDPDgMQu9DuhLO0V1uMGq.XEKso2SEOjXYEPSNIv8iSae',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-31 07:51:03','2017-05-31 07:51:03','2017-05-31 07:51:03','2017-05-31 07:51:03',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(17,'Blocked','spyrosktestblocked@mindaria.com','$2y$10$DE0OV2d1g9pvdBDe70QDqeX5uWqNKF7t0E8IHqx3B5TVEN8fJJKfW',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-31 08:47:01','2017-05-31 08:47:01','2017-05-31 08:47:01','2017-05-31 08:47:01',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(18,'Regular User','spyroskregular@mindaria.com','$2y$10$d.W.4Tbb24u2mydiDpXTL.Vm.3EO3vJqwHRBsYpEh9Tae9iIvGe5a',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-06-13 16:54:00','2017-06-13 16:54:00','2017-06-13 16:54:00','2017-06-13 16:54:00',0,0,1,NULL,NULL,0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(37,'Test','test4@jankstudio.com','$2y$10$JjikR.MnPvS9VZnQn3oRhu8j9dJHQXp3330bltiscQoVo6P3D1DrK',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-14 04:59:16','2017-09-14 04:59:16','2017-09-14 04:59:16','2017-09-14 04:59:20',0,0,0,'cffcf75831cf9359892686201ca6b7b907c9029b64eef836f0b243d740fac3a6','920ab0a2',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(38,'Test Ticket Sender','test8@jankstudio.com','$2y$10$OdT8buMNoxERj9CH8E67EuMWosI5CJC8pw4iHnEZJ8euviRUqnX.6','14uf7ZVE8KyGVFAJ49TR2BYRrlkcYKaRmDIoAGbFO9aTeMUBsZ3imxtz0vmG',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-09-15 04:42:50','2017-09-15 04:42:50','2017-09-15 04:42:50','2017-09-15 04:42:56',0,0,1,NULL,'181e674e',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(61,'Test Person X','testpersonx@gmail.com','$2y$10$qRBCUP8TTCgKtyUbHGXAf.lEhTAd6RuDpbcpouUG.LLWZoSQE1Eoe','vBSG3ayyq9Tvffg6QPpZUiinQ5dPCyWy2NUYJTzGjvY8fyGLHaHnF4CITSU9',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:18:32','2017-10-03 14:18:32','2017-10-03 14:18:32','2017-10-03 14:19:10',0,0,0,'e43971e0bc9e73051b86ad4ea76b16c7cb33731bae343fa697ccf14114987016','9e4fd87b',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(62,'Test Person Z','testpersonz@gmail.com','$2y$10$6tW3jDJ8BiNOMi5ezQUUQ.A5ejZX5FS9nX4IVhcyMoarrGdxByv.S','kSCQivMlOBHjkgkkEUWx1QlgTnjdNpfLzbMZxLRd0V4TPgC90XpjzZb5jptP',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:22:17','2017-10-03 14:22:17','2017-10-03 14:22:17','2017-10-03 14:22:23',0,0,0,'e29bc6a3c88fae5338ce972914110487dc3cbf6ac8be4ebb98fe38ae3cd6d1da','c8db349e',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(63,'Test Person A','testpersona@gmail.com','$2y$10$xJksF9n2EOltpKNWLeXReOxxhX4YseErcAMoirTnspMiIns.qc6M2','jk0QU3uy86ZdyKyqsHOiXVw4fXjRbbRMMt9B2WLF4Q9amVU2jLhjb5sJm6ze',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:22:38','2017-10-03 14:22:38','2017-10-03 14:22:38','2017-10-03 14:22:44',0,0,0,'eabb595eb13bf4fd72992e5f8e201c2fe1b57feb5ec8ee58a8aa4a9ba46bfa64','501e0497',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(64,'Test Person AB','testpersonab@gmail.com','$2y$10$5TbSFEfKld0NAjeuLcrbWu.iPXOadqN5tBODz8FHf6oKJjopmpkeS','VKJt9Fm17OaVYRgdr4MLszA6XXafeihuK1aWkzeQ81u1fPSiYbpQJWv73sYI',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 14:59:07','2017-10-03 14:59:07','2017-10-03 14:59:07','2017-10-03 14:59:14',0,0,0,'72c8724ee41e01f7d7bd2a8308ad36a136d244c72af4bd861de44c05f06b6ae0','7ec2c30c',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(65,'Test Person BA','testpersonba@gmail.com','$2y$10$i4gQvaI71qHecqO3/0UuGepLkEiIaMpzvsxs7rB8S/u1vn7dJoOgi','uR34654GolJ48m6Y29YDdenU0BD4njXUTZWQHh40UFZFZgvWGmQlek0luyF9',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:00:53','2017-10-03 15:00:53','2017-10-03 15:00:53','2017-10-03 15:00:57',0,0,0,'9d7f64c5a8a40b084f9de3beb1c024de57f787f24f1666b6ac2e278b4cba048b','4b2e2ef8',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(66,'Test User ABA','testuseraba@gmail.com','$2y$10$qxPrLSZ7a9bUZRKAO2IDouQrsEYILDMazlo9LmD/TM1f79A0Pu6Q6','VvD2dOGJpAEnzFtAg93LwDGLnIz79fgMW2jcjY1CkPNIxjze9JrEZXeCJa6F',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:17:51','2017-10-03 15:17:51','2017-10-03 15:17:51','2017-10-03 15:17:56',0,0,0,'ff1b1220f16539eebe5ebd52be3a5cb30d355638d9d448a48d93b9944eae319f','79c8ea41',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(67,'Test User ABB','testuserabb@gmail.com','$2y$10$7xZVb9hP9eqviuxynflzFOW4Q0srNlyOG8Ubnh3RtEg.Jgg2Ujik2','Yv0d1uFQmyI3zFxCwu5AyY4avz9LrS5uRTpkkkIfTDeVdHwtEZTXAjjcXUxz',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:18:11','2017-10-03 15:18:11','2017-10-03 15:18:11','2017-10-03 15:18:14',0,0,0,'a38441c62a70c5c73c6afc111f0e3d3c9760a0f1ae25823ba8718a58a365a760','64cb916f',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(68,'Test Person Y','testpersony@gmail.com','$2y$10$GoZvkyWMY53KMhv/GHo4IOzxFxJ0hCaCCz0n.CSMqlBOJ1kuVcBDm','leFgZCLENxTE2WTXSh9lpUxF0aBvSGTteSVX5GqlN4fhoFRlV0i4qZ9QjCll',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:23:37','2017-10-03 15:23:37','2017-10-03 15:23:37','2017-10-03 15:23:43',0,0,0,'68ca44b67097e58b25f3c00120a55be575f5c70bb2ccb0da0a4da715771dfb9e','d9fa0f44',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(70,'Test Person AAA','testpersonaaa@gmail.com','$2y$10$lVUxQlCeHZzowMpeprxCgO/ULCSGXTKRyCr9SfXtaj90I.zLmzxTu',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-03 15:28:50','2017-10-03 15:28:50','2017-10-03 15:28:50','2017-10-03 15:28:56',0,0,0,'435a8dcaeb03ca6c493a040b4226d3fe674ba6078e9f00d75816890e08547251','1175b22c',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(71,'testx','testpersonax@gmail.com','$2y$10$5d0qejUQXJrFCV0a8d3w2OmocSloKNgRsnk7MyMvv2.Ky32HKrdD.','Ub90gGtEOBrf9Eq3eQgBrqQWa7wgqDS8o19cXcsaOXt21aiX4fsC7clX1q7I',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-08 10:03:04','2017-10-08 10:03:04','2017-10-08 10:03:04','2017-10-08 10:03:14',0,0,0,'7e13c6aa67bf3913660258b1dd0f165b81c8201d1ff934e32d23cc53a73c0675','a0f112be',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL),(72,'Testabc','testpersonabc@gmail.com','$2y$10$Ugd44cDNhszOMp1sIvu1CeOcQ190i75EGP.e2yqGhrsnIRjPwoaYi','6HpollugKImNwgKP8A6o5yQPjsM5oKky8ucZhufMD489VFANZq1AoF7GDT9Z',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-08 10:05:49','2017-10-08 10:05:49','2017-10-08 10:05:49','2017-10-08 10:05:53',0,0,0,'86a9e883f6600744101e51880f27522351a77fc50727ca38ae3ed65aec7004be','3d0e777c',0,0.00,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wayback_settings`
--

DROP TABLE IF EXISTS `wayback_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wayback_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wayback_settings_name_version_unique` (`name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wayback_settings`
--

LOCK TABLES `wayback_settings` WRITE;
/*!40000 ALTER TABLE `wayback_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `wayback_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wayback_statuses`
--

DROP TABLE IF EXISTS `wayback_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wayback_statuses` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cust_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wayback_statuses_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wayback_statuses`
--

LOCK TABLES `wayback_statuses` WRITE;
/*!40000 ALTER TABLE `wayback_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `wayback_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wayback_submissions`
--

DROP TABLE IF EXISTS `wayback_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wayback_submissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `submission_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipfile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_status` smallint(6) NOT NULL,
  `version` smallint(6) NOT NULL,
  `ip_addr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_api_add` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `wayback_submissions_user_id_foreign` (`user_id`),
  KEY `wayback_submissions_user_id_index` (`user_id`),
  CONSTRAINT `wayback_submissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wayback_submissions`
--

LOCK TABLES `wayback_submissions` WRITE;
/*!40000 ALTER TABLE `wayback_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wayback_submissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21  8:19:14
