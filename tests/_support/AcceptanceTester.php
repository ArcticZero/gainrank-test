<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */
    public function login($email, $password)
    {
        $I = $this;
        /*
        // NOTE: removing this due to error: Failed to set the 'cookie' property on 'Document': Cookies are disabled inside 'data:' URLs
        // if snapshot exists - skipping login
        if ($I->loadSessionSnapshot('login')) {
            return;
        }
        */
        $I->amOnPage('/login');
        $I->submitForm('#login', [
            'email' => $email,
            'password' => $password
        ]);
        $I->wait(5);
        $I->see('Dashboard');
        // saving snapshot
        $I->saveSessionSnapshot('login');
    }

    public function registerAccount($name, $email, $pass)
    {
        $I = $this;
        $I->amOnPage('/');
        $I->see('Get your free trial');
        $I->click('Get your free trial');
        $I->fillField('name', $name);
        $I->fillField('email', $email);
        $I->fillField('password', $pass);
        $I->fillField('password_confirmation', $pass);
        $I->checkOption('[name=terms]');
        $I->click('.btn-primary');
        $I->wait(5);
        $I->seeInCurrentUrl('thank-you-for-registering');
        $I->see('Thank you for creating an account on Gainrank.
        A message with further instructions has been sent to your email address.');
        $I->receiveAnEmailToEmail($email);
        $I->receiveAnEmailWithSubject('Gainrank Email Verification');
        $I->seeInEmailHtmlBody('Click here to verify your account');
        $matches = array();
        $body = $I->fetchLastMessage()['html_body'];
        preg_match_all('/<a[^>]+href=[\'"]http:\/\/.+?(\/.+?)[\'"][^>]*>/', $body, $matches);
        $I->amOnPage($matches[1][0]);
        $I->wait(3);
        $I->see('Account');
        $I->click('Account');
    }
}
