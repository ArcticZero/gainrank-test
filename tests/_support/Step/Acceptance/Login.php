<?php
namespace Step\Acceptance;

class Login extends \AcceptanceTester
{
    public function loginAsAdmin()
    {
        $I = $this;
        $I->login('spyrosktestadmin@mindaria.com', 'a(bln.321dlsAS#dnlLD_N');
        $I->amOnPage('/spark/kiosk');
        $I->see('Kiosk');
    }

    public function loginAsEditor()
    {
        $I = $this;
        $I->login('spyrosktesteditor@mindaria.com', 'a(bln.321dlsAS#dnlLD_N');
        $I->amOnPage('/spark/kiosk');
        $I->see('Kiosk');
    }

    public function loginAsRegular()
    {
        $I = $this;
        $I->login('spyroskregular@mindaria.com', 'a(bln.321dlsAS#dnlLD_N');
        $I->amOnPage('/home');
        $I->see('active users: last 24 hours');
    }

    public function loginAsBlocked()
    {
        $I = $this;
        $I->amOnPage('/login');
        $I->submitForm('#login', [
            'email' => 'spyrosktestblocked@mindaria.com',
            'password' => 'a(bln.321dlsAS#dnlLD_N'
        ]);
        $I->see('This account has been banned for a terms of service violation');
    }
}
