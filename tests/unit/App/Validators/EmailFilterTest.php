<?php
namespace App\Validators;

class EmailFilterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $filter;

    protected function _before()
    {
        $this->filter = new EmailFilter();
    }

    protected function _after()
    {
    }

    // tests
    public function testEmailDomainIsAllowed()
    {
        $email = "test@gmail.com";

        $this->assertTrue($this->filter->validate('email', $email, null, null));
    }

    public function testEmailDomainIsBlacklisted()
    {
        $email = "test@mailinator.com";

        $this->assertFalse($this->filter->validate('email', $email, null, null));
    }
}
