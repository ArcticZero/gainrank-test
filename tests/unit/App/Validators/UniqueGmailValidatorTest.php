<?php
namespace App\Validators;

class UniqueGmailValidatorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testValidateAGmailAccountIsUnique()
    {
        $validator = new UniqueGmailValidator();

        // create user with plain gmail
        $this->tester->haveRecord('users', [
            'name'      => 'Test Account',
            'email'     => 'uniquegmailvalidatortest@gmail.com',
            'password'  => 'tester123',
        ]);

        // test stylized gmail against existing
        $email = 'Unique.Gmail.Validator.Test+this.string.does.not+count@gmail.com';
        $this->assertFalse($validator->validate('email', $email, ['users'], null));
    }
}
