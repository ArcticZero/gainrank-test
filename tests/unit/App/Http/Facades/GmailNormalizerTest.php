<?php
namespace App\Http\Facades;

use GmailNormalizer;

class GmailNormalizerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testNormalizeAGmailAccount()
    {
        $email = 'T.es.t+thispartshouldntmatter+atall@gmail.com';
        $parsed = GmailNormalizer::parse($email);
        $this->assertEquals($parsed, "test@gmail.com");
    }
}
