<?php
namespace App\Services;

use RewardHandler;
use App\Reward;
use App\User;

class RewardHandlerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function setUp()
    {
        parent::setUp();

        // instatiate reward handler
        $this->reward_handler = new \App\Services\RewardHandler();
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testPayout()
    {
        $reward = new Reward(Reward::TYPE_PAYOUT, ['payout_amount' => 10]);
        $user = new User();
        $res = $this->reward_handler->implement($reward, $user);
        $this->assertEquals($res, true);
    }
}
