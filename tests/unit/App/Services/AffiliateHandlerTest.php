<?php
namespace App\Services;

use App\Affiliate;
use App\User;
use App\Reward;
use App\Contracts\AffiliateContract;

class AffiliateHandlerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $ref_id = 'test1234';

    protected $aff_handler;

    public function setUp()
    {
        parent::setUp();

        // instantiate affiliate handler
        $this->aff_handler = new \App\Services\AffiliateHandler();
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGetByReferralId()
    {
        $affiliate = new Affiliate();
        $affiliate->ref_id = $this->ref_id;
        $affiliate->save();

        $this->tester->seeRecord('affiliates', ['ref_id' => $this->ref_id]);
        $this->assertNotNull($this->aff_handler->getByReferralId($this->ref_id));
    }

    /*
    public function testAddTransaction()
    {
        $affiliate = $this->createTestAffiliate();
        $user = $this->createTestUser($affiliate);

        $this->tester->seeRecord('affiliates', ['id' => $affiliate->id]);
        $reward = new Reward(Reward::TYPE_PAYOUT, ['payout_amount' => 10]);
        $this->assertTrue($this->aff_handler->addTransaction($user, 1, $reward, []));
    }
    */

    // helpers
    private function createTestAffiliate()
    {
        $affiliate = new Affiliate();
        $affiliate->ref_id = $this->ref_id;
        $affiliate->save();

        return $affiliate;
    }

    private function createTestUser($affiliate)
    {
        $user = new User();
        $user->name = 'test';
        $user->email = 'test1234@test.com';
        $user->password = 'password';
        $user->save();

        $user->affiliate()->save($affiliate);

        return $user;
    }
}
