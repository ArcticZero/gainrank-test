<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPendingSubscription extends Model
{
    /**
     * Return the user associated with this pending subscription
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
