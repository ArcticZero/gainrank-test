<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesInvoice extends Model
{
    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'apply_at',
        'due_at'
    ];

    /**
     * Return the user associated with this sales invoice
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return the items included in this invoice
     *
     * @return mixed
     */
    public function items()
    {
        return $this->hasMany('App\SalesInvoiceItem');
    }

    /**
     * Return the payments recorded for this invoice
     *
     * @return mixed
     */
    public function payments()
    {
        return $this->hasMany('App\PaymentLog');
    }
}
