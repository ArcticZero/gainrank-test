<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingDomain extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tracks()
    {
    	return $this->hasMany('App\RankTracking', 'domain_id');
    }
}
