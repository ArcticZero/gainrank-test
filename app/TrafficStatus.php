<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrafficStatus extends Model
{
    public $timestamps = false;
    
    /**
     * Return submissions associated with the status
     *
     * @return mixed
     */
    public function submissions()
    {
        return $this->hasMany('App\TrafficSubmission', 'id_status');
    }
}
