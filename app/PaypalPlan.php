<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalPlan extends Model
{
    public $timestamps = false;
    protected $fillable = ['plan_id'];

    /**
     * Return the AdCampaign that this plan is tied to
     * @return mixed
     */
    public function campaign() {
        return $this->belongsTo('App\AdCampaign', 'ad_campaign_id');
    }
}
