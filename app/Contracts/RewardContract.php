<?php

namespace App\Contracts;

use App\Reward;
use App\User;

interface RewardContract
{
    // implement the reward for the user
    public function implement(Reward $reward, User $user);

    // generate Reward object based on affiliate type
    public function generateAffiliateReward($aff_type);
}
