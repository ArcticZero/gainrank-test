<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface PaymentProcessorContract
{
    /**
     * Handle the pre-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionCheckout();

    /**
     * Handle post-redirect phase for subscription checkout
     *
     * @return boolean
     */
    public function subscriptionPostCheckout();

    /**
     * Suspend a current subscription
     *
     * @return boolean
     */
    public function suspendSubscription();

    /**
     * Update an existing subscription
     *
     * @return boolean
     */
    public function updateSubscription();

    /**
     * Handle incoming notifications from the payment gateway
     *
     * @return mixed
     */
    public function handleNotification();

    /**
     * Get the initialized payment gateway client
     *
     * @return mixed
     */
    public static function getClient();

    /**
     * Get the unique ID of this payment processor
     *
     * @return string
     */
    public static function getId();
    
    /**
     * Get the user friendly name of this payment processor
     *
     * @return string
     */
    public static function getDisplayName();
}
