<?php

namespace App\Contracts\Repositories;

interface HelpdeskRepository
{

    /**
     * Registers a new User with the Helpdesk.
     * @param $user
     * @return mixed
     */
    public function registerUser($user);

    /**
     * Get User information.
     * @param $id
     * @return mixed
     */
    public function getUser($user);

    /**
     * Add a User to a Group.
     * @param $id
     * @param $group_name
     * @return mixed
     */
    public function addUserToGroup($user, $group_name);

    /**
     * Remove a User from a Group.
     * @param $id
     * @param $group_name
     * @return mixed
     */
    public function removeUserFromGroup($user, $group_name);

    /**
     * Get all of a User's groups.
     * @param $id
     * @return mixed
     */
    public function getUserGroups($user);

    /**
     * Upload a file to the Helpdesk.
     * @param $file
     * @return mixed
     */
    public function uploadFile($file);

    /**
     * Delete a file from the Helpdesk.
     * @param $file
     * @return mixed
     */
    public function deleteFile($file_id);

    /**
     * Create a new conversation.
     * @param $message
     * @return mixed
     */
    public function createConversation($conversation);

    /**
     * Return all the conversations that a user created.
     * @param $user
     * @return mixed
     */
    public function getUserConversations($user);

    /**
     * Returns information about a conversation.
     * @param $conversation_id
     * @return mixed
     */
    public function getConversation($conversation_id);

    /**
     * Retrieves all the messages posted in a conversation.
     * @param $conversation_id
     * @return mixed
     */
    public function getConversationMessages($conversation_id);

    /**
     * Add a message to a conversation
     * @param $conversation_id
     * @param $message
     * @return mixed
     */
    public function createNewMessage($conversation_id, $message);
}
