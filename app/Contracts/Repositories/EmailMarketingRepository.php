<?php

namespace App\Contracts\Repositories;

interface EmailMarketingRepository
{

    /**
     * Creates a new Contact.
     * @param $user
     * @return mixed
     */
    public function createContact($user);

    /**
     * Get Contact information.
     * @param $id
     * @return mixed
     */
    public function getContact($user);

    /**
     * Add a Contact to a Segment.
     * @param $id
     * @param $group_name
     * @return mixed
     */
    public function addContactToSegment($user, $segment);

    /**
     * Remove a Contact from a Segment.
     * @param $id
     * @param $group_name
     * @return mixed
     */
    public function removeContactFromSegment($user, $segment);

    /**
     * Get all of a Contact's segments.
     * @param $id
     * @return mixed
     */
    public function getSegments($user);
}
