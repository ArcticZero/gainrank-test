<?php

namespace App\Contracts;

use App\User;
use App\Reward;

interface AffiliateContract
{
    // get affiliate by referral id
    public function getByReferralID($referral_id);

    // add an affiliate transaction (not yet mature)
    public function addTransaction(User $user, $type_id, Reward $reward, $details = [], $transacted_at = false, $mature_at = false);
}
