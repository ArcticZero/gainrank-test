<?php

namespace App\Gainrank;

use GuzzleHttp\Client as GuzzleClient;

abstract class BaseAPI
{
    protected $g_client;
    protected static $required_settings = [];
    protected $settings;

    public function __construct($settings = [])
    {
        $this->g_client = null;
        $this->settings = $settings;
    }

    protected function initGuzzleClient()
    {
        // if we already have a client, don't re-initialize
        if ($this->g_client != null) {
            return;
        }

        $this->g_client = new GuzzleClient();
    }

    protected function verifySettings()
    {
        // verify that we have the required settings
        foreach (static::$required_settings as $key) {
            if (empty($this->settings[$key])) {
                throw new GainrankException("Missing required field [$key] in settings.");
            }
        }
    }
}
