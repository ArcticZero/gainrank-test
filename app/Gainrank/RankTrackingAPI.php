<?php

namespace App\Gainrank;

use GuzzleHttp\Client as GuzzleClient;
use DateTime;
use App\RankTracking;
use App\RankTrackingRank;
use App\RankTrackingEngine;
use App\Gainrank\GoogleDomain;
use App\RankTrackingSetting;
use App\RankTrackingDomain;
use Carbon\Carbon;
use Auth;

use Illuminate\Support\Facades\Log;

class RankTrackingAPI extends BaseAPI
{
    protected static $required_settings = [
        'api_key',
        'url_update',
        'url_add',
        'enabled_add',
        'enabled_update',
    ];

    public function add($domain_id, RankTrackingEngine $engine, $keyword)
    {
        // search for domain url
        $domain = RankTrackingDomain::find($domain_id);
        $url = $domain->url;

        // TODO: handle domain not found

        Log::info('ranktrack add - ' . $url);

        if (!$this->settings['enabled_add']) {
            return false;
        }

        // check user
        $user = Auth::user();
        if ($user == null) {
            return false;
        }

        $this->verifySettings();
        $this->initGuzzleClient();

        Log::info("sending request");

        // send add request
        $res = $this->g_client->request('POST', $this->settings['url_add'], [
            'form_params' => [
                'key' => $this->settings['api_key'],
                'url' => $url,
                'country' => $engine->url,
                'keyword' => $keyword,
            ]
        ]);

        Log::info("request sent");

        // check result
        $status_code = $res->getStatusCode();
        if ($status_code != 200) {
            throw new GainrankException('Invalid response from API server - (' . $status_code . ') ' . $res->getReasonPhrase());
        }

        $body = $res->getBody();
        Log::info($body);

        // DEBUG: show results
        // echo $body;

        // check if error
        $result = json_decode($body, true);
        if (isset($result['error']) && !empty($result['error'])) {
            Log::info($result['error']);
            return;
        }

        Log::info($result);

        $sub_id = $result['id_submission'];

        $country_a2 = $engine->country;

        // on success, save to db
        $rt = new RankTracking();
        $rt->user_id = Auth::user()->id;
        $rt->submission_id = $sub_id;
        $rt->domain_id = $domain_id;
        $rt->keyword = $keyword;
        $rt->engine_id = $engine->id;
        $rt->country = $country_a2;
        $rt->version = $this->settings['version'];
        $rt->id_status = 0;
        $rt->save();
    }

    public function update($domain, $keywords, $engine, $start = null, $end = null)
    {
        if (!$this->settings['enabled_update']) {
            return false;
        }

        $this->verifySettings();
        $this->initGuzzleClient();

        // initialize query string parameters
        $q_string_arr = [];
        foreach ($keywords as $kw => $track_id) {
            $q_string_arr[] = 'kw=' . urlencode($kw);
        }
        $q_string_arr[] = 'domain=' . urlencode($domain);
        $q_string_arr[] = 'engine=' . urlencode($engine);

        /*
        $params = [
            'kw' => implode(",", $keywords),
            'engine' => $engine
        ];
        */

        if (empty($start)) {
            $q_string_arr[] = 'start=' . urlencode(Carbon::now()->subDays(10)->startOfDay()->toDateString());
        } else {
            $q_string_arr[] = 'start=' . urlencode($start);
        }

        if (empty($end)) {
            $q_string_arr[] = 'end=' . urlencode(Carbon::now()->endOfDay()->toDateString());
        } else {
            $q_string_arr[] = 'end=' . urlencode($end);
        }

        $q_string = implode('&', $q_string_arr);
        Log::info("query string - " . $q_string);

        // send update request
        $res = $this->g_client->request('GET', str_replace('{domain}', $domain, $this->settings['url_update']), [
            'query' => $q_string
        ]);

        // check result
        $status_code = $res->getStatusCode();
        if ($status_code != 200) {
            throw new GainrankException('Invalid response from API server - (' . $status_code . ') ' . $res->getReasonPhrase());
        }

        $body = $res->getBody();

        // DEBUG: show results
        // echo $body;

        // check if error
        $result = json_decode($body, true);
        // DEBUG
        Log::info(print_r($result, true));
        if (isset($result['error'])) {
            Log::info("Error in ranktracking API: " . $result['error']);
            return;
        }

        // update each entry
        foreach ($result as $row) {
            if (count($row['ranks']) > 0) {
                // get track
                $track_id = $keywords[$row['keyword']];
                $track = RankTracking::find($track_id);

                foreach ($row['ranks'] as $rdate => $rank_arr) {
                    $rank = end($rank_arr);
                    // Log::info(print_r($rank, true));
                    // create or update existing rank
                    $rtr = RankTrackingRank::firstOrNew(['id' => $rank['id']]);
                    $rtr->rank = $rank['rank'];
                    $rtr->applied_at = $rdate;
                    $rtr->track_id = $track_id;
                    $rtr->save();

                    // update the parent record, so we know this doesn't need an update until next interval
                    $track->last_rank = $rank['rank'];
                    $track->save();
                }
            }
        }

        /*
        // do we need to save to db?
        if ($id != null) {
            // save to db if ranking is there
            if (count($result['rankings']) > 0) {
                // get first for now since we don't know what actual result looks like
                $rank = $result['rankings'][0]['position'];

                $rtr = new RankTrackingRank();
                $rtr->track_id = $id;
                $rtr->rank = $rank;
                $rtr->save();

                // update the parent record, so we know this doesn't need an update until next interval
                $track = $rtr->track();
                $track->touch();
            } else {
                // TODO: what to do with no results
                echo "no results";
            }
        }
        */
    }

    public static function initialize()
    {
        // return RankTrackingAPI instance with settings loaded from database

        // TODO: put this in a 'service' class to handle getting settings
        // get current version
        $version = RankTrackingSetting::where([
            ['version', '=', 1],
            ['name', 'current_version']
        ])
            ->select('value')
            ->pluck('value')
            ->first();
        ;

        // get our settings for that version
        $indexed_settings = [
            'version' => $version
        ];
        $settings = RankTrackingSetting::where('version', $version)
            ->select(['name', 'value'])
            ->get();
        foreach ($settings as $setting) {
            $indexed_settings[$setting->name] = $setting->value;
        }

        return new RankTrackingAPI($indexed_settings);
    }
}
