<?php

namespace App\Gainrank;

class GoogleDomain
{
    public static $domains = [
        'google.com',
        'google.ac',
        'google.ad',
        'google.ae',
        'google.al',
        'google.am',
        'google.as',
        'google.at',
        'google.az',
        'google.ba',
        'google.be',
        'google.bf',
        'google.bg',
        'google.bi',
        'google.bj',
        'google.bs',
        'google.by',
        'google.ca',
        'google.cat',
        'google.cd',
        'google.cf',
        'google.cg',
        'google.ch',
        'google.ci',
        'google.cl',
        'google.cm',
        'google.co.ao',
        'google.co.bw',
        'google.co.ck',
        'google.co.cr',
        'google.co.id',
        'google.co.il',
        'google.co.in',
        'google.co.jp',
        'google.co.ke',
        'google.co.kr',
        'google.co.ls',
        'google.co.ma',
        'google.co.mz',
        'google.co.nz',
        'google.co.th',
        'google.co.tz',
        'google.co.ug',
        'google.co.uk',
        'google.co.uz',
        'google.co.ve',
        'google.co.vi',
        'google.co.za',
        'google.co.zm',
        'google.co.zw',
        'google.com.af',
        'google.com.ag',
        'google.com.ai',
        'google.com.ar',
        'google.com.au',
        'google.com.bd',
        'google.com.bh',
        'google.com.bn',
        'google.com.bo',
        'google.com.br',
        'google.com.bz',
        'google.com.co',
        'google.com.cu',
        'google.com.cy',
        'google.com.do',
        'google.com.ec',
        'google.com.eg',
        'google.com.et',
        'google.com.fj',
        'google.com.gh',
        'google.com.gi',
        'google.com.gt',
        'google.com.hk',
        'google.com.jm',
        'google.com.kh',
        'google.com.kw',
        'google.com.lb',
        'google.com.ly',
        'google.com.mm',
        'google.com.mt',
        'google.com.mx',
        'google.com.my',
        'google.com.na',
        'google.com.nf',
        'google.com.ng',
        'google.com.ni',
        'google.com.np',
        'google.com.om',
        'google.com.pa',
        'google.com.pe',
        'google.com.pg',
        'google.com.ph',
        'google.com.pk',
        'google.com.pr',
        'google.com.py',
        'google.com.qa',
        'google.com.sa',
        'google.com.sb',
        'google.com.sg',
        'google.com.sl',
        'google.com.sv',
        'google.com.tr',
        'google.com.tw',
        'google.com.ua',
        'google.com.uy',
        'google.com.vc',
        'google.com.vn',
        'google.cv',
        'google.cz',
        'google.de',
        'google.dj',
        'google.dk',
        'google.dm',
        'google.dz',
        'google.ee',
        'google.es',
        'google.fi',
        'google.fm',
        'google.fr',
        'google.ga',
        'google.ge',
        'google.gg',
        'google.gl',
        'google.gm',
        'google.gp',
        'google.gr',
        'google.gy',
        'google.hn',
        'google.hr',
        'google.ht',
        'google.hu',
        'google.ie',
        'google.im',
        'google.iq',
        'google.is',
        'google.it',
        'google.je',
        'google.jo',
        'google.kg',
        'google.kz',
        'google.la',
        'google.li',
        'google.lk',
        'google.lt',
        'google.lu',
        'google.lv',
        'google.md',
        'google.me',
        'google.mk',
        'google.ml',
        'google.mn',
        'google.ms',
        'google.mu',
        'google.mv',
        'google.mw',
        'google.ne',
        'google.nl',
        'google.no',
        'google.nr',
        'google.nu',
        'google.pl',
        'google.pn',
        'google.ps',
        'google.pt',
        'google.ro',
        'google.rs',
        'google.ru',
        'google.rw',
        'google.sc',
        'google.se',
        'google.sh',
        'google.si',
        'google.sk',
        'google.sm',
        'google.sn',
        'google.so',
        'google.st',
        'google.td',
        'google.tg',
        'google.tk',
        'google.tl',
        'google.tm',
        'google.tn',
        'google.to',
        'google.tt',
        'google.vg',
        'google.vu',
        'google.ws',
    ];

    public static function getIDFromDomain($domain)
    {
        return array_search($domain, self::$domains);
    }

    public static function getDomainFromID($id)
    {
        if (isset(self::$domains[$id])) {
            return self::$domains[$id];
        }

        return false;
    }

    public static function getAlpha2FromID($id)
    {
        $domain = self::getDomainFromID($id);
        if ($domain === false) {
            return false;
        }

        return self::getAlpha2FromDomain($domain);
    }

    public static function getAlpha2FromDomain($domain)
    {
        $domain_split = explode('.', $domain);
        $domain_end = end($domain_split);
        
        switch ($domain_end) {
            // return US for google.com
            case 'com':
                return 'US';
            // catatonia is not a country, return null
            case 'cat':
                return null;
        }

        return strtoupper($domain_end);
    }

    public static function getRegions()
    {
        return self::$domains;
    }
}
