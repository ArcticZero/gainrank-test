<?php

namespace App\Gainrank;

use Auth;
use Request;
use App\AlexaSetting;
use App\AlexaSubmission;

class AlexaAPI extends BaseAPI
{
    protected static $required_settings = [
        'api_key',
        'url_add',
        'url_update',
        'enabled_add',
        'enabled_update',
        'cost_per',
        'cost_per_x',
    ];

    public function add($url, $total_visits)
    {
        // check user
        $user = Auth::user();
        if ($user == null) {
            return false;
        }

        $this->verifySettings();
        $this->initGuzzleClient();

        // send add request
        $res = $this->g_client->request('POST', $this->settings['url_add'], [
            'form_params' => [
                'key' => $this->settings['api_key'],
                'url' => $url,
                'total_visits' => $total_visits,
            ]
        ]);

        // check result
        $status_code = $res->getStatusCode();
        if ($status_code != 200) {
            throw new GainrankException('Invalid response from API server - (' . $status_code . ') ' . $res->getReasonPhrase());
        }

        $body = $res->getBody();

        // DEBUG: show results
        //echo $body;

        $result = json_decode($body, true);
        $sub_id = $result['id_submission'];

        // on success, save to db
        $obj = new AlexaSubmission();
        $obj->user_id = Auth::user()->id;
        $obj->submission_id = $sub_id;
        $obj->url = $url;
        $obj->wanted_count = $total_visits;
        $obj->sent_count = 0;
        $obj->cost_per_x = $this->settings['cost_per_x'];
        $obj->cost_per_amt = $this->settings['cost_per'];
        $obj->id_status = 0;
        $obj->version = $this->settings['version'];
        $obj->ip_addr = Request::ip();
        $obj->flag_api_add = 0;
        $obj->save();
    }

    public function update($submission_id, $id = null)
    {
        // do not save to db if $id == null
        $this->verifySettings();
        $this->initGuzzleClient();

        // send update request
        $res = $this->g_client->request('GET', $this->settings['url_update'], [
            'query' => [
                'k' => $this->settings['api_key'],
                'i' => $submission_id,
            ]
        ]);

        // check result
        $status_code = $res->getStatusCode();
        if ($status_code != 200) {
            throw new GainrankException('Invalid response from API server - (' . $status_code . ') ' . $res->getReasonPhrase());
        }

        $body = $res->getBody();

        // DEBUG: show results
        //echo $body;

        $result = json_decode($body, true);
        $sent_count = $result['sent_count'];

        // do we need to save to db?
        if ($id != null) {
            $obj = AlexaSubmission::find($id);
            $obj->sent_count = $sent_count;
            $obj->id_status = 0;
            $obj->save();

            // in case no change is made, touch to make sure
            $obj->touch();
        }
    }

    public static function initialize()
    {
        // return instance with settings loaded from database

        // TODO: put this in a 'service' class to handle getting settings
        // get current version
        $version = AlexaSetting::where([
            ['version', '=', 0],
            ['name', 'current_version']
        ])
            ->select('value')
            ->pluck('value')
            ->first();
        ;

        // get our settings for that version
        $indexed_settings = [
            'version' => $version
        ];
        $settings = AlexaSetting::where('version', $version)
            ->select(['name', 'value'])
            ->get();
        foreach ($settings as $setting) {
            $indexed_settings[$setting->name] = $setting->value;
        }

        return new AlexaAPI($indexed_settings);
    }
}
