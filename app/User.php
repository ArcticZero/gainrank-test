<?php

namespace App;

use Laravel\Spark\User as SparkUser;
use Carbon\Carbon;
use Spatie\Permission\Traits\HasRoles;
use Jrean\UserVerification\Traits\UserVerification;

class User extends SparkUser
{
    use HasRoles;
    use UserVerification;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'datetime',
        'uses_two_factor_auth' => 'boolean',
    ];

    /**
     * Return this User's Posts.
     * @return mixed
     */
    public function posts() {
        return $this->hasMany('App\Post');
    }

    public function isBanned() {
        return $this->banned_at && Carbon::now() >= $this->banned_at;
    }

    public function ban() {
        $this->assignRole('blocked');
        $this->banned_at = Carbon::now();
        $this->save();
    }

    public function affiliate() {
        return $this->hasOne('App\Affiliate');
    }

    public function affiliate_transactions() {
        return $this->hasMany('App\AffiliateTransaction');
    }

    /**
     * Return the Fingerprints associated with this User.
     * @return mixed
     */
    public function fingerprints() {
        return $this->belongsToMany('App\Fingerprint', 'fingerprint_recordings')->withTimestamps()->using('App\FingerprintRecording');
    }

    /**
     * Return this User's Verified PayPal Payer Emails.
     * @return mixed
     */
    public function paypalVerifiedEmails() {
        return $this->hasMany('App\UserPaypalVerifiedEmail');
    }

    /**
     * Return the AdCampaign that this User signed up for.
     * @return mixed
     */
    public function campaign() {
        return $this->belongsTo('App\AdCampaign', 'ad_campaign_id');
    }

    /**
     * Return the Referral Log for this user
     * @return mixed
     */
    public function referralLog() {
        return $this->hasOne('App\ReferralLog', 'referred_user_id');
    }

    /**
     * Return the Referrer Affiliate for this user
     * @return mixed
     */
    public function referrerAffiliate() {
        return $this->belongsTo('App\Affiliate', 'referrer_affiliate_id');
    }

    /**
     * Return this User's sent SoftLaunchInvitations.
     * @return mixed
     */
    public function sentInvitations() {
        return $this->hasMany('App\SoftLaunchInvitation');
    }

    /**
     * Return the SoftLaunchInvitations that this User signed up with.
     * @return mixed
     */
    public function registrationInvitation() {
        return $this->hasOne('App\SoftLaunchInvitation', 'invitee_user_id');
    }

    /**
     * Return if a user is an affiliate
     * @return boolean
     */
    public function isAffiliate() {
        return $this->is_affiliate;
    }

    /**
     * Return this user's actions on PendingOrders
     * @return mixed
     */
    public function pendingOrderActions()
    {
        return $this->hasMany('App\PendingOrderAction');
    }

    /**
     * Return the sales invoices recorded for this user
     *
     * @return mixed
     */
    public function salesInvoices()
    {
        return $this->hasMany('App\SalesInvoice');
    }

    /**
     * Return the payments recorded for this user
     *
     * @return mixed
     */
    public function payments()
    {
        return $this->hasMany('App\PaymentLog');
    }

    /*
     * Return this User's TfaBackupCodes.
     * @return mixed
     */
    public function backupCodes()
    {
        return $this->hasMany('App\TfaBackupCode');
    }

    /*
     * Return this User's RankTracking tracks
     * @return mixed
     */
    public function tracks() {
        return $this->hasMany('App\RankTracking');
    }

    /*
     * Return this User's RankTracking favorites
     * @return mixed
     */
    public function favorites() {
        return $this->hasMany('App\RankTrackingFavorite');
    }

    /*
     * Return this User's Alexa submissions
     * @return mixed
     */
    public function alexaSubmissions() {
        return $this->hasMany('App\AlexaSubmission');
    }

    /*
     * Return this User's CTR submissions
     * @return mixed
     */
    public function ctrSubmissions() {
        return $this->hasMany('App\CTRSubmission');
    }

    /*
     * Return this User's Traffic submissions
     * @return mixed
     */
    public function trafficSubmissions() {
        return $this->hasMany('App\TrafficSubmission');
    }

    /*
     * Return this User's Wayback Machine submissions
     * @return mixed
     */
    public function waybackSubmissions() {
        return $this->hasMany('App\WaybackSubmission');
    }

    public function ranktrackingDomains() {
        return $this->hasMany('App\RankTrackingDomain');
    }

    public function helpdeskConversations() {
        return $this->hasMany('App\HelpdeskConversation');
    }

    /**
     * Set a date to this user's timezone
     * @return mixed
     */
    public function applyTz($date) {
        $date = Carbon::parse($date);

        if ($this->timezone) {
            return $date->setTimezone($this->timezone);
        } else {
            return $date;
        }
    }
}
