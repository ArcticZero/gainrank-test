<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;

class Post extends Model implements HasMediaConversions
{
    use SoftDeletes;
    use \Spatie\Tags\HasTags;
    use HasMediaTrait;

    protected $fillable = ['title', 'body', 'published'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            // TODO maybe do it ONLY when people create a post and not updating?
            $model->user_id = Auth::id();
            $slug = str_slug($model->title);
            // Create slug (note: $slug is safe for SQL injection attacks as str_slug() allows only letters, numbers
            // and the separator (-).
            // Supposedly whereRaw supports named & plain parameter binding but I couldn't get it to work.
            $count = Post::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
            // If you are getting errors about duplicate slugs that means that a row has been deleted e.g.
            // 'a slug' => a-slug
            // 'a  slug' => a-slug-1
            // 'a   slug' => a-slug-2
            // Then if the 2nd row is deleted (a-slug-1) and we try to add a new one with the same slug
            // it will take the value 'a-slug-2' as the count is 2 but 'a-slug-2' already exists leading to the error.
            // This is why Posts must always be soft deleted (column deleted_at).
            $model->slug = $count ? "{$slug}-{$count}" : $slug;
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function registerMediaConversions()
    {
        /*
         * TODO: Add a version for retina/4k or higher screens.
         * Will have to figure out the best solution once we have caching set up.
         * Could possibly use http://imulus.github.io/retinajs/
         * But the problem is that high res clients will download the images twice (original & @2x or @3x).
         * (Remember to check issue queue)
         */

        $this->addMediaConversion('hero')
            ->fit(Manipulations::FIT_CROP, 1361, 400);

        $this->addMediaConversion('thumb-large')
            ->fit(Manipulations::FIT_CROP, 434, 226);

        $this->addMediaConversion('thumb-small')
            ->fit(Manipulations::FIT_CROP, 186, 97);
    }
}
