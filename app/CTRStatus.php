<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CTRStatus extends Model
{
    protected $table = 'ctr_statuses';
    public $timestamps = false;

    /**
     * Return submissions associated with the status
     *
     * @return mixed
     */
    public function submissions()
    {
        return $this->hasMany('App\CTRSubmission', 'id_status');
    }
}
