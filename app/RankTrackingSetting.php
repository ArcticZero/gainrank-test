<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingSetting extends Model
{
    // NOTE: a setting with a version of 0 is for permanent general settings that are independent of the versions

    /**
     * Return the version for the setting
     *
     * @return mixed
     */
    public function version()
    {
        return $this->belongsTo('App\RankTrackingVersion');
    }
}
