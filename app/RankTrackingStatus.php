<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingStatus extends Model
{
    public $timestamps = false;
    
    /**
     * Return tracks associated with the status
     *
     * @return mixed
     */
    public function tracks()
    {
        return $this->hasMany('App\RankTracking', 'id_status');
    }
}
