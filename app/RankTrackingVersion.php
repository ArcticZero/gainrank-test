<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingVersion extends Model
{
    /**
     * Returns the settings for the version number
     *
     * @return mixed
     */
    public function settings()
    {
        return $this->hasMany('App\RankTrackingSetting');
    }
}
