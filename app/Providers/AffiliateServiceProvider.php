<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AffiliateServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Contracts\AffiliateContract',
            'App\Services\AffiliateHandler'
        );
    }
}
