<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use App\User;
use App\Observers\UserObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extendImplicit('email_filter', 'App\Validators\EmailFilter@validate');
        Validator::extendImplicit('unique_gmail', 'App\Validators\UniqueGmailValidator@validate');
        User::observe(UserObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
