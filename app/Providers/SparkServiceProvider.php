<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use Carbon\Carbon;
use GmailNormalizer;
use App\Affiliate;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Your Company',
        'product' => 'Your Product',
        'street' => 'PO Box 111',
        'location' => 'Your Town, NY 12345',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'paymentspp@ymail.com',
        'shane@njbbrands.com',
        'spyrosk@mindaria.com',
        'kc@jankstudio.com',
        'spyrosktest@mindaria.com',
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        //Spark::useStripe()->noCardUpFront()->trialDays(10);

        Spark::freePlan()
            ->features([
                'First', 'Second', 'Third'
            ]);

        Spark::plan('Basic', 'provider-id-1')
            ->price(0.01)
            ->trialDays(5)
            ->features([
                'First', 'Second', 'Third'
            ]);

        Spark::validateUsersWith(function ($request) {
            return [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|email_filter|unique_gmail:users',
                'password' => 'required|confirmed|min:'.Spark::minimumPasswordLength(),
                'vat_id' => 'nullable|max:50|vat_id',
                'terms' => 'required|accepted',
            ];
        });

        Spark::createUsersWith(function ($request) {
            $user = Spark::user();

            $data = $request->all();

            $data['email'] = GmailNormalizer::parse($data['email']);

            $user->forceFill([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'is_subscribed' => $data['is_subscribed'] ? true : false,
                'last_read_announcements_at' => Carbon::now(),
                'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
            ]);

            // Save referrer if present
            if ($request->session()->has('affiliate_id')) {
                $affiliate = Affiliate::find($request->session()->get('affiliate_id'));
                $user->referrerAffiliate()->associate($affiliate);
            }

            $user->save();

            return $user;
        });

        // Register kiosk metrics service override
        $this->app->singleton('App\Contracts\Repositories\PerformanceIndicatorsRepository', 'App\Repositories\PerformanceIndicatorsRepository');
    }
}
