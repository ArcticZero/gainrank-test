<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RewardServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Contracts\RewardContract',
            'App\Services\RewardHandler'
        );
    }
}
