<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCampaignBonusType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type'];

    /**
     * Returns the AdCampaignsBonus's that have this type.
     * @return mixed
     */
    public function bonuses()
    {
        return $this->hasMany('App\AdCampaignBonus');
    }
}
