<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoftLaunchInvitation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email'];

    /**
     * Returns the User that sent the invitation.
     * @return mixed
     */
    public function sender()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Returns the User that was invited.
     * @return mixed
     */
    public function receiver()
    {
        return $this->belongsTo('App\User', 'invitee_user_id');
    }
}
