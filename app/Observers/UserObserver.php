<?php

namespace App\Observers;

use App\User;
use Spatie\Permission\Models\Role;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use App\Contracts\Repositories\HelpdeskRepository;
use App\Jobs\RegisterHelpdeskUser;
use App\Jobs\RegisterEmailMarketingUser;

class UserObserver
{
    protected $helpdesk;

    /**
     * UserObserver constructor.
     * @param HelpdeskRepository $helpdesk
     */
    public function __construct(HelpdeskRepository $helpdesk)
    {
        $this->helpdesk = $helpdesk;
    }

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user)
    {
        // By default all new users get the 'regular user' role.
        $user->assignRole('regular user');
        $user->assignRole('can open new support ticket');
        $user->assignRole('can reply to support ticket');

        UserVerification::generate($user);
        UserVerification::send($user, 'Gainrank Email Verification');

        // Register the User to the helpdesk in the background
        $job = (new RegisterHelpdeskUser($user))->onQueue('helpdesk-registration');
        dispatch($job);

        /*
        // Register the User to the email marketing automation in the background
        $job = (new RegisterEmailMarketingUser($user))->onQueue('email-marketing-registration');
        dispatch($job);
        */
    }
}
