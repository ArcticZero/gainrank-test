<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user',
        'payment_processor_id',
        'reason',
        'reason_data',
        'notification_data',
        'dollar_amount',
        'amount',
        'currency'
    ];

    /**
     * Return the user associated with this sales invoice
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return the sales invoice this item belongs to
     *
     * @return App\SalesInvoice
     */
    public function invoice()
    {
        return $this->belongsTo('App\SalesInvoice');
    }
}
