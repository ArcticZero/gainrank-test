<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingEngine extends Model
{
    protected $fillable = [
        'id',
        'url',
        'is_enabled',
        'name',
        'country'
    ];
    public $timestamps = false;

    /**
     * Return tracks associated with the engine
     *
     * @return mixed
     */
    public function tracks()
    {
        return $this->hasMany('App\RankTracking');
    }
}
