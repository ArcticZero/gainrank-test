<?php

namespace App\Validators;

class EmailFilter
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        // filter check
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $parts = explode("@", $value);
        $blacklist = $this->getBlacklist();

        if (!isset($parts[1]) || in_array($parts[1], $blacklist)) {
            return false;
        }

        return true;
    }

    protected function getBlacklist()
    {
        $blacklist = [
            'mailinator.com',
            'zippymail.info',
        ];

        return $blacklist;
    }
}
