<?php

namespace App\Validators;

use Illuminate\Support\Facades\DB;
use GmailNormalizer;

class UniqueGmailValidator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $email = GmailNormalizer::parse($value);

        $query = DB::table($parameters[0])
            ->where('email', $email)
            ->count();

        if ($query > 0) {
            return false;
        }

        return true;
    }
}
