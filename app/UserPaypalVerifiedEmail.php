<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaypalVerifiedEmail extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
