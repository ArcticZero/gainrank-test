<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CTRSubmission extends Model
{
    protected $table = 'ctr_submissions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return status associated with the submission
     *
     * @return mixed
     */
    public function status()
    {
        return $this->belongsTo('App\CTRStatus', 'id_status');
    }
}
