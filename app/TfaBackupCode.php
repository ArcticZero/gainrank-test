<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TfaBackupCode extends Model
{
    /**
     * Return the User that this TfaBackupCode signed up for.
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
