<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FingerprintRecording extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Returns the Fingerprint associated with this FingerprintRecording.
     * @return mixed
     */
    public function fingerprint()
    {
        return $this->belongsTo('App\Fingerprint');
    }

    /**
     * Returns the User associated with this FingerprintRecording.
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
