<?php

namespace App\Repositories;

use Exception;
use App\User;
use Illuminate\Support\Facades\Log;
use Mautic\MauticApi;
use Mautic\Auth\ApiAuth;
use App\Contracts\Repositories\EmailMarketingRepository as EmailMarketingRepositoryContract;

class MauticRepository implements EmailMarketingRepositoryContract
{
    protected $contactApi;

    /**
     * HelpdeskRepository constructor.
     */
    public function __construct()
    {
        $username =  env('MAUTIC_USERNAME', false);
        if (empty($username)) {
            Log::error("Mautic username env var is not set.");
        }
        $password =  env('MAUTIC_PASSWORD', false);
        if (empty($password)) {
            Log::error("Mautic password env var is not set.");
        }
        $base_uri =  env('MAUTIC_BASE_URI', false);
        if (empty($base_uri)) {
            Log::error("Mautic base uri env var is not set.");
        }
        $settings = array(
            'userName' => $username,
            'password' => $password
        );

        $initAuth = new ApiAuth();
        $auth = $initAuth->newAuth($settings, 'BasicAuth');

        $api = new MauticApi();
        $this->contactApi = $api->newApi('contacts', $auth, $base_uri);
    }

    /**
     * Checks the response from the Mautic API for any errors.
     * @param $response
     * @return mixed
     * @throws Exception
     */
    protected function checkResponse($response)
    {
        if (isset($response['error'])) {
            throw new Exception("Mautic API Error: " . $response['error']['code'] . $response['error']['message']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createContact($user)
    {
        if ($user->is_subscribed) {
            $response = $this->contactApi->create([
                'firstname' => $user->name,
                'email' => $user->email,
            ]);
            $this->checkResponse($response);
            $user->email_user_id = $response['contact']['id'];
            $user->save();
            return $user;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getContact($user)
    {
        if (!empty($user->email_user_id)) {
            $response = $this->contactApi->get($user->email_user_id);
            $this->checkResponse($response);
            return $response;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addContactToSegment($user, $group_name)
    {
        // TODO: Implement addContactToSegment() method.
    }

    /**
     * {@inheritdoc}
     */
    public function removeContactFromSegment($user, $group_name)
    {
        // TODO: Implement removeContactFromSegment() method.
    }

    /**
     * {@inheritdoc}
     */
    public function getSegments($user)
    {
        // TODO: Implement getSegments() method.
    }
}
