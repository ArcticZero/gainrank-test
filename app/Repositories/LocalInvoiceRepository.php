<?php

namespace App\Repositories;

use Laravel\Spark\Contracts\Repositories\LocalInvoiceRepository as Contract;

class LocalInvoiceRepository implements Contract
{
    /**
     * {@inheritdoc}
     */
    public function createForUser($user, $input)
    {
        return $this->createForBillable($user, $input);
    }

    /**
     * {@inheritdoc}
     */
    public function createForTeam($team, $input)
    {
        return $this->createForBillable($team, $input);
    }

    /**
     * Create a local invoice for the given billable entity.
     *
     * @param  mixed  $billable
     * @param  mixed  $input
     * @return \Laravel\Spark\LocalInvoice
     */
    protected function createForBillable($billable, $input)
    {
        if ($existing = $billable->localInvoices()->where('provider_id', $input['txn_id'])->first()) {
            return $existing;
        }

        return $billable->localInvoices()->create([
            'provider_id' => $input['txn_id'],
            'total' => $input['amount'],
            'tax' => $input['tax'],
            'card_country' => null,
            'billing_state' => null,
            'billing_zip' => null,
            'billing_country' => null,
        ]);
    }
}
