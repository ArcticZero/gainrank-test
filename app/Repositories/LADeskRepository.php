<?php

namespace App\Repositories;

use Exception;
use GuzzleHttp\Client;
use App\User;
use Illuminate\Support\Facades\Log;
use App\Contracts\Repositories\HelpdeskRepository as HelpdeskRepositoryContract;

class LADeskRepository implements HelpdeskRepositoryContract
{
    protected $apikey;
    protected $guzzle;

    /**
     * HelpdeskRepository constructor.
     */
    public function __construct()
    {
        $apikey =  env('LADESK_API_KEY', false);
        if (empty($apikey)) {
            Log::error("LADesk API key env var is not set.");
        } else {
            $this->apikey = $apikey;
        }
        $this->apikey = $apikey;
        $base_uri =  env('LADESK_BASE_URI', false);
        if (empty($base_uri)) {
            Log::error("LADesk base uri env var is not set.");
        } else {
            $this->guzzle = new Client([
                'base_uri' => $base_uri,
                'timeout' => 20,
            ]);
        }
    }

    /**
     * Parses the response from the LADesk API.
     * @param $response
     * @return mixed
     * @throws Exception
     */
    protected function getAPIResponse($response)
    {
        // check result, throw exception if something's wrong
        // if reply isn't from 2xx series
        $scode = $response->getStatusCode();
        if ($scode >= 300) {
            $reason = $response->getReasonPhrase();
            throw new Exception("Got $scode status from LADesk API server with error: $reason");
        }

        // if body cannot be deocded to json
        $body = $response->getBody();
        $body_data = json_decode($body, true);
        if ($body_data === null) {
            throw new Exception("Could not decode LADesk API response body.");
        }

        // check for response field
        if (!isset($body_data['response'])) {
            throw new Exception("No response field in LADesk API response body.");
        }

        // check for API errors
        if (!empty($body_data['response']['status'])) {
            if ($body_data['response']['status'] == 'ERROR') {
                throw new Exception("LADesk API Error {$body_data['response']['statuscode']}: {$body_data['response']['errormessage']}", $body_data['response']['statuscode']);
            }
        }

        // return response portion of API
        return $body_data['response'];
    }

    /**
     * {@inheritdoc}
     */
    public function registerUser($user)
    {
        $data = [
            'apikey' => $this->apikey,
            'email' => $user->email,
            'name' => $user->name,
            'role' => 'R',
            'send_registration_mail' => 'N',
        ];

        try {
            $res = $this->guzzle->post('customers', [
                'form_params' => $data
            ]);

            $response = $this->getAPIResponse($res);
            $user_id = $response['userid'];
        } catch(Exception $e) {
            // check for 404 (customer already exists)
            $code = $e->getCode();
            if ($code == 404) {
                // customer's already there, let's get their userid
                $res = $this->getUser($user);
                $user_id = $res['userid'];
            }
        }

        /*
         * Even though email is a valid identifier for users, we want to use contactid so that we can tell which users
         * are not yet registered with LADesk.
         */
        $user->helpdesk_user_id = $user_id;
        $user->save();
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($user)
    {
        $url = 'customers/' . $user->email;
        $res = $this->guzzle->get($url, [
            'query' => ['apikey' => $this->apikey]
        ]);
        $response = $this->getAPIResponse($res);
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function addUserToGroup($user, $group_name)
    {
        // TODO: Implement addUserToGroup() method.
    }

    /**
     * {@inheritdoc}
     */
    public function removeUserFromGroup($user, $group_name)
    {
        // TODO: Implement removeUserFromGroup() method.
    }

    /**
     * {@inheritdoc}
     */
    public function getUserGroups($user)
    {
        // TODO: Implement getUserGroups() method.
    }

    /**
     * {@inheritdoc}
     */
    public function uploadFile($file)
    {
        if (
            is_array($file) &&
            !empty($file['name']) &&
            is_string($file['name']) &&
            !empty($file['type']) &&
            is_string($file['type']) &&
            !empty($file['data']) &&
            is_string($file['data'])
        ) {
            $offset = strpos($file['data'], ',');
            // Only grab what we need
            $data = [
                'apikey' => $this->apikey,
                'name' => $file['name'],
                'type' => $file['type'],
                'data' => substr($file['data'], $offset+1)
            ];
        } else {
            throw new Exception("Tried to upload invalid ile array to LADesk");
        }
        $res = $this->guzzle->post('files', [
            'form_params' => $data
        ]);

        $response = $this->getAPIResponse($res);

        return $response['fileid'];
    }

    /**
     * {@inheritdoc}
     */
    public function deleteFile($file_id)
    {
        // TODO: Implement deleteFile() method.
    }

    /**
     * {@inheritdoc}
     */
    public function createConversation($conversation)
    {
        if (
            is_array($conversation) &&
            !empty($conversation['message']) &&
            is_string($conversation['message']) &&
            !empty($conversation['useridentifier']) &&
            is_string($conversation['useridentifier']) &&
            !empty($conversation['department']) &&
            is_string($conversation['department']) &&
            !empty($conversation['subject']) &&
            is_string($conversation['subject']) &&
            !empty($conversation['recipient']) &&
            is_string($conversation['recipient'])
        ) {
            // Only grab what we need
            $data = [
                'apikey' => $this->apikey,
                'message' => $conversation['message'],
                'useridentifier' => $conversation['useridentifier'],
                'department' => $conversation['department'],
                'subject' => $conversation['subject'],
                'recipient' => $conversation['recipient'],
            ];
            if (!empty($conversation['attachments']) && is_string($conversation['attachments'])) {
                $data['attachments'] = $conversation['attachments'];
            }
        } else {
            throw new Exception("Invalid conversation array.");
        }
        $res = $this->guzzle->post('conversations', [
            'form_params' => $data
        ]);

        $response = $this->getAPIResponse($res);

        return $response['conversationid'];
    }

    /**
     * {@inheritdoc}
     */
    public function getUserConversations($user)
    {
        if (empty($user))
            return [];

        $res = $this->guzzle->get('conversations', [
            'query' => ['apikey' => $this->apikey, 'owneridentifier' => $user]
        ]);
        $response = $this->getAPIResponse($res);
        return !empty($response['conversations']) ? $response['conversations'] : [];
    }

    /**
     * {@inheritdoc}
     */
    public function getConversation($conversation_id)
    {
        $res = $this->guzzle->get('conversations/' . $conversation_id, [
            'query' => ['apikey' => $this->apikey]
        ]);
        return $this->getAPIResponse($res);
    }

    /**
     * {@inheritdoc}
     */
    public function getConversationMessages($conversation_id)
    {
        $res = $this->guzzle->get('conversations/' . $conversation_id . '/messages', [
            'query' => ['apikey' => $this->apikey]
        ]);
        $groups = $this->getAPIResponse($res)['groups'];
        $messages = [];
        foreach ($groups as $group) {
            if (!empty($group['rtype']) && $group['rtype'] == 'M') {
                $message = [];
                foreach ($group['messages'] as $msg) {
                    if (!empty($msg['rtype'])) {
                        switch ($msg['rtype']) {
                            case 'M':
                                $message['message'] = str_replace("\n", '<br/>', $msg['message']);
                                break;
                            case 'H':
                                if (substr($msg['message'], 0, 6) == 'From: ') {
                                    $message['user'] = substr($msg['message'], 6);
                                }
                                break;
                        }
                    }
                }
                if (!empty($message)) {
                    $messages[] = $message;
                }
            }
        }
        return $messages;
    }

    /**
     * {@inheritdoc}
     */
    public function createNewMessage($conversation_id, $message)
    {
        if (
            is_array($message) &&
            !empty($message['message']) &&
            is_string($message['message']) &&
            !empty($message['useridentifier']) &&
            is_string($message['useridentifier'])
        ) {
            $data = [
                'apikey' => $this->apikey,
                'message' => $message['message'],
                'useridentifier' => $message['useridentifier'],
                'type' => 'M',
            ];
            if (!empty($message['attachments']) && is_string($message['attachments'])) {
                $data['attachments'] = $message['attachments'];
            }
        } else {
            throw new Exception("Invalid message array.");
        }
        $res = $this->guzzle->post('conversations/' . $conversation_id . '/messages', [
            'form_params' => $data
        ]);
        if ($this->getAPIResponse($res)['status'] == 'OK') {
            return true;
        } else {
            throw new Exception("API error, could not create message.");
        }
    }
}
