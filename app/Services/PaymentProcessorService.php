<?php

namespace App\Services;

use App\Services\Payment\PaypalRecurringProcessor;
use App\Services\Payment\PaypalManualProcessor;
use App\Services\Payment\CoinbaseProcessor;

use Illuminate\Http\Request;

class PaymentProcessorService
{
    /**
     * Create a new repository instance.
     *
     * @param  string  $id
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public static function getPaymentProcessor(string $id, Request $request)
    {
        switch ($id) {
            case 'paypal_recurring':
                return new PaypalRecurringProcessor($request);
                break;
            case 'paypal_manual':
                return new PaypalManualProcessor($request);
                break;
            case 'coinbase':
                return new CoinbaseProcessor($request);
                break;
        }
    }
}
