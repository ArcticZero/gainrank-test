<?php

namespace App\Services;

use App\User;
use App\SalesInvoice;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class SalesInvoiceHandler
{
    /**
     * The initialized client
     */
    private $invoice;

    /**
     * Find existing invoice by ID
     *
     * @param  int  $id
     * @return App\SalesInvoice
     */
    public function find($id)
    {
        // assign property
        $this->invoice = SalesInvoice::find($id);

        // return invoice
        return $this->invoice;
    }

    /**
     * Generate sales invoice for an order
     *
     * @param  App\user  $user
     * @param  array  $items
     * @param  Carbon\Carbon  $apply_at
     * @param  Carbon\Carbon  $due_at
     * @param  string  $currency
     * @return App\SalesInvoice
     */
    public function generate(User $user, $items, $apply_at = null, $due_at = null, $ext_invoice_id = null, $pay_url = null, $currency = null)
    {
        // get current time
        $now = Carbon::now();

        // default invoice apply date to now
        if (empty($apply_at)) {
            $apply_at = $now;
        }

        // default invoice due date to now
        if (empty($due_at)) {
            $due_at = $now;
        }

        // default currency to config setting
        if (empty($currency)) {
            $currency = config('billing.currency');
        }

        $total_amount = 0;
        $item_array = [];

        // get total amount
        foreach ($items as $item) {
            $total_amount = bcadd($total_amount, $item['amount']);
        }

        // create invoice
        $invoice = new SalesInvoice();
        $invoice->user_id = $user->id;
        $invoice->apply_at = $apply_at;
        $invoice->due_at = $due_at;
        $invoice->total_amount = $total_amount;
        $invoice->ext_invoice_id = $ext_invoice_id;
        $invoice->currency = $currency;
        $invoice->pay_url = $pay_url;
        $invoice->save();

        // add items to invoice
        $invoice->items()->createMany($items);

        // assign property
        $this->invoice = $invoice;

        // return invoice
        return $this->invoice;
    }

    /**
     * Send invoice to user's email
     *
     * @param  string  $company
     * @param  array  $data
     * @return void
     */
    public function sendInvoice($data = [])
    {
        $this->send('emails.invoice', $data);
    }

    /**
     * Send receipt to user's email
     *
     * @param  string  $company
     * @param  array  $data
     * @return void
     */
    public function sendReceipt($data = [])
    {
        $this->send('emails.receipt', $data);
    }

    /**
     * Send invoice or receipt to user's email
     *
     * @param  string  $template
     * @param  string  $company
     * @param  array  $data
     * @return void
     */
    public function send($template, $data = [])
    {
        // add invoice object to data array
        $data['invoice'] = $this->invoice;

        // get the user associated with this invoice
        $data['user'] = $this->invoice->user;

        // build and send
        Mail::send($template, $data, function ($message) use ($data) {
            $message->to($data['user']->email, $data['user']->name)
                ->subject(isset($data['company']) ? $data['company'] . ' Invoice' : 'Invoice');
        });
    }
}
