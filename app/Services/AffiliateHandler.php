<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Contracts\AffiliateContract;
use App\User;
use App\Reward;
use App\Affiliate;
use App\AffiliateTransaction;
use Carbon\Carbon;

class AffiliateHandler implements AffiliateContract
{
    public function getByReferralID($referral_id)
    {
        return Affiliate::where('ref_id', $referral_id)->first();
    }

    public function addTransaction(User $user, $type_id, Reward $reward, $details = [], $transacted_at = false, $mature_at = false)
    {
        $affiliate = $user->affiliate;

        // User is not an affiliate
        if (!$affiliate) {
            return false;
        }

        // Set default transacted_at date
        if (!$transacted_at) {
            $transacted_at = Carbon::now();
        }

        // Set default mature at date
        if (!$mature_at) {
            $payout_mature_days = env('PAYOUT_MATURE_DAYS', 30);
            $mature_at = (clone $transacted_at)->addDays($payout_mature_days);
        }

        // Do database transaction
        $reward_type = $reward->getType();
        $reward_details = $reward->getDetails();
        DB::transaction(function () use ($affiliate, $user, $transacted_at, $type_id, $mature_at, $details, $reward_type, $reward_details) {
            // Save affiliate transaction
            $transaction = new AffiliateTransaction();
            $transaction->affiliate_id = $affiliate->id;
            $transaction->user_id = $user->id;
            $transaction->transacted_at = $transacted_at;
            $transaction->type_id = $type_id;
            $transaction->mature_at = $mature_at;
            $transaction->details = json_encode($details);
            $transaction->reward_type = $reward_type;
            $transaction->reward_details = json_encode($reward_details);
            $transaction->save();

            /*
             * Update affiliate payout total
             * For now, assumes payout amount is at $reward_details->payout_amount
             */
            if ($reward_type == Reward::TYPE_PAYOUT) {
                $affiliate->payout_total = bcadd($affiliate->payout_total, $reward_details['payout_amount']);
                $affiliate->save();
            }
        });

        return true;
    }
}
