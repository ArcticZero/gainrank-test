<?php

namespace App\Services;

use App\User;
use App\SalesInvoice;
use App\PaymentLog;

use Illuminate\Http\Request;

class PaymentLogService
{
    /**
     * Log a payment.
     *
     * @param  App\SalesInvoice  $invoice
     * @param  string  $payment_processor_id
     * @param  string  $reason
     * @param  array  $reason_data
     * @param  array  $notification_data
     * @param  float  $dollar_amount
     * @param  float  $amount
     * @param  string  $currency
     * @return mixed
     */
    public static function log(SalesInvoice $invoice, $payment_processor_id, $dollar_amount, $reason = null, $reason_data = [], $notification_data = [], $amount = false, $currency = false)
    {
        // set amount to same as dollar amount if undefined
        if ($amount === false || $currency === false) {
            $amount = $dollar_amount;
            $currency = 'USD';
        }

        // save log
        $log = $invoice->payments()->create([
            'user' => $invoice->user->id,
            'payment_processor_id' => $payment_processor_id,
            'reason' => $reason,
            'reason_data' => $reason_data,
            'notification_data' => $notification_data,
            'dollar_amount' => $dollar_amount,
            'amount' => $amount
        ]);

        return $log;
    }
}
