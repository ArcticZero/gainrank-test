<?php

namespace App\Services\Payment;

use App\PaypalPlan;
use App\Reward;
use App\UserPendingSubscription;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Plan;
use PayPal\Api\Payer;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Currency;
use PayPal\Exception\PayPalConnectionException;

use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use App\Contracts\PaymentProcessorContract;
use App\Services\SalesInvoiceHandler;
use App\Services\PaymentLogService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use DateTime;

class PaypalRecurringProcessor implements PaymentProcessorContract
{
    /**
     * The initialized client
     */
    private $client;

    /**
     * Request object
     */
    private $request;

    /**
     * Connect to the API using provided credentials
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        // initialize API
        $this->client = new ApiContext(
            new OAuthTokenCredential(
                config('services.paypal.key'),
                config('services.paypal.secret')
            )
        );

        // are we live? switch to appropriate endpoints
        $live = config('services.paypal.live');

        if ($live) {
            $this->client->setConfig(array(
                'mode' => 'live'
            ));
        }

        // set request
        $this->request = $request;
    }

    /**
     * Handle the pre-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionCheckout()
    {
        $error = false;
        $result = [];
        $pp_plan = false;
        $now = time();

        // get spark plan details
        $spark_plan = Spark::plans()->where('id', $this->request->input('plan'))->first();

        // get pending subscription with attached agreement
        $pending = UserPendingSubscription::where('user_id', $this->request->user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        $pp_plan = false;

        if (!empty($spark_plan)) {
            // get initial payment price
            $initial_price = $spark_plan->price;

            // if user is linked to an ad campaign, check for corresponding plan
            if (!empty($this->request->user()->campaign)) {
                $pp_plan = PaypalPlan::where('plan_id', $this->request->input('plan'))
                    ->where('ad_campaign_id', $this->request->user()->campaign->id)
                    ->first();

                if (!empty($pp_plan)) {
                    // if there are bonuses to the initial payment, let's grab them
                    $discounts = [
                        'amount' => 0,
                        'percent' => 0
                    ];

                    $bonuses = $pp_plan->campaign->bonuses()
                        ->whereHas('type', function ($q) {
                            $q->whereIn('reward_type', [
                                Reward::TYPE_DISCOUNT_INITIAL_AMOUNT,
                                Reward::TYPE_DISCOUNT_INITIAL_PERCENT
                            ]);
                        })
                        ->get();

                    if (!$bonuses->isEmpty()) {
                        foreach ($bonuses as $bonus) {
                            if (!empty($bonus->bonus_amount && $bonus->bonus_amount > 0)) {
                                switch ($bonus->type->reward_type) {
                                    case Reward::TYPE_DISCOUNT_INITIAL_AMOUNT:
                                        $discounts['amount'] = $bonus->bonus_amount;
                                        break;
                                    case Reward::TYPE_DISCOUNT_INITIAL_PERCENT:
                                        $discounts['percent'] = bcdiv($bonus->bonus_amount, 100);
                                        break;
                                }
                            }
                        }
                    }

                    // apply flat amount discount to initial payment
                    if ($discounts['amount'] > 0) {
                        $initial_price = bcsub($initial_price, $discounts['amount']);
                    }

                    // apply percentage discount to initial payment
                    if ($discounts['percent'] > 0) {
                        $initial_price = bcmul($initial_price, bcsub(1, $discounts['percent']));
                    }
                }
            }

            // use default plan definition
            if (empty($pp_plan)) {
                $pp_plan = PaypalPlan::where('plan_id', $this->request->input('plan'))
                    ->whereNull('ad_campaign_id')
                    ->first();
            }
        }

        if (empty($pp_plan)) {
            $error = 'The selected plan was not found';
        } elseif ($pending) {
            $error = 'You already have a pending subscription';
        } else {
            // delete any existing pending subs
            UserPendingSubscription::where('user_id', $this->request->user()->id)->delete();

            // create new pending sub
            $user_plan = new UserPendingSubscription();
            $user_plan->user_id = $this->request->user()->id;
            $user_plan->provider_plan_name = $this->request->input('plan');
            $user_plan->billing_provider = $this->getId();
            $user_plan->save();

            // get billing period
            switch ($spark_plan->interval) {
                case 'monthly':
                    $billing_period = 'month';
                    break;
                case 'yearly':
                    $billing_period = 'year';
                    break;
            }

            // set payment plan
            $plan = new Plan();
            $plan->setId($pp_plan->paypal_plan_id);

            // create payer
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            // override merchant preferences
            $return_url = url('settings/subscription/checkout/paypal_recurring');
            $preferences = new MerchantPreferences();
            $preferences->setReturnUrl($return_url . "/success?p=" . $user_plan->id)
                ->setCancelUrl($return_url . "/failure?p=" . $user_plan->id)
                ->setAutoBillAmount("YES")
                ->setInitialFailAmountAction("CANCEL")
                ->setMaxFailAttempts("3")
                ->setSetupFee(new Currency(
                    [
                        'currency' => config('billing.currency'),
                        'value' => $initial_price
                    ]
                ));

            // create billing agreement
            $agreement = new Agreement();
            $agreement->setName('Billing Agreement for Subscription')
                ->setDescription('Gainrank Subscription (' . $spark_plan->name . ')')
                ->setStartDate(gmdate('Y-m-d\TH:i:s\Z', strtotime("+1 " . $billing_period, $now)))
                ->setPlan($plan)
                ->setPayer($payer)
                ->setOverrideMerchantPreferences($preferences);

            // process billing agreement and get approval link
            try {
                $agreement = $agreement->create($this->client);

                $result = [
                    'success' => true,
                    'approval_url' => $agreement->getApprovalLink()
                ];
            } catch (PayPalConnectionException $e) {
                Log::error("PayPal exception occurred creating billing agreement", [
                    'code' => $e->getCode(),
                    'data' => $e->getData()
                ]);
                $error = 'An error occurred while processing your billing agreement.';
            } catch (Exception $e) {
                Log::error("Exception occurred creating billing agreement", [
                    'message' => $e->getMessage()
                ]);
                $error = 'An error occurred while processing your billing agreement.';
            }
        }

        if ($error) {
            $result = [
                'success' => false,
                'error' => $error
            ];
        }

        return $result;
    }

    /**
     * Handle the post-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionPostCheckout()
    {
        // validate input
        $validator = Validator::make($this->request->all(), [
            'p' => 'required|exists:user_pending_subscriptions,id',
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return false;
        }

        $pending_id = $this->request->input('p');
        $token = $this->request->input('token');

        $agreement = new Agreement();

        // execute the agreement
        try {
            $agreement->execute($token, $this->client);
        } catch (PayPalConnectionException $e) {
            Log::error("PayPal exception occurred executing billing agreement", [
                'code' => $e->getCode(),
                'data' => $e->getData()
            ]);
            return false;
        } catch (Exception $e) {
            Log::error("Exception occurred executing billing agreement", [
                'message' => $e->getMessage()
            ]);
            return false;
        }

        // refresh agreement info
        try {
            $agreement = Agreement::get($agreement->getId(), $this->client);
        } catch (PayPalConnectionException $e) {
            Log::error("PayPal exception occurred retrieving billing agreement", [
                'code' => $e->getCode(),
                'data' => $e->getData(),
                'token' => $token
            ]);
            return false;
        } catch (Exception $e) {
            Log::error("Exception occurred retrieving billing agreement", [
                'message' => $e->getMessage(),
                'token' => $token
            ]);
            return false;
        }

        // update pending sub
        $user_plan = UserPendingSubscription::find($pending_id);
        $user_plan->agreement_id = $agreement->getId();
        $user_plan->save();

        return $user_plan;
    }

    /**
     * Suspend a current subscription
     *
     * @return boolean
     */
    public function suspendSubscription()
    {
        // get current subscription
        $subscription = Subscription::where('user_id', $this->request->user()->id)->first();

        if (empty($subscription)) {
            return false;
        } else {
            // get provider plan info
            $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

            if (empty($spark_plan) || empty($subscription->paypal_agreement_id)) {
                return false;
            } else {
                $descriptor = new AgreementStateDescriptor();
                $descriptor->setNote("Suspending the subscription");

                try {
                    // get billing agreement
                    $agreement = Agreement::get($subscription->paypal_agreement_id, $this->client);
                    $agreement->suspend($descriptor, $this->client);
                } catch (PayPalConnectionException $e) {
                    Log::error("PayPal exception occurred suspending agreement", [
                        'code' => $e->getCode(),
                        'data' => $e->getData()
                    ]);
                    return false;
                } catch (Exception $e) {
                    Log::error("Exception occurred suspending agreement", [
                        'message' => $e->getMessage()
                    ]);
                    return false;
                }

                // get billing period
                switch ($spark_plan->interval) {
                    case 'monthly':
                        $billing_period = 'month';
                        break;
                    case 'yearly':
                        $billing_period = 'year';
                        break;
                }

                // compute for end of grace period
                $end_time = strtotime($subscription->last_payment_at . " +1" . $billing_period);

                // update subscription row
                $subscription->ends_at = date("Y-m-d H:i:s", $end_time);
                $subscription->save();
            }
        }

        return true;
    }

    /**
     * Update an existing subscription
     *
     * @return boolean
     */
    public function updateSubscription()
    {
        // validate input
        $validator = Validator::make($this->request->all(), [
            'plan' => 'required|alpha_dash'
        ]);

        if ($validator->fails()) {
            return false;
        }

        $plan = $this->request->input('plan');

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $plan)->first();

        // get current subscription
        $subscription = Subscription::where('user_id', $this->request->user()->id)->first();

        if (empty($subscription) || empty($spark_plan)) {
            return false;
        } else {
            if ($plan == $subscription->provider_plan_name) {
                // this is the same plan and we are resuming the subscription
                $descriptor = new AgreementStateDescriptor();
                $descriptor->setNote("Resuming the subscription");

                try {
                    // get billing agreement
                    $agreement = Agreement::get($subscription->paypal_agreement_id, $this->client);
                    $agreement->reActivate($descriptor, $this->client);
                } catch (PayPalConnectionException $e) {
                    Log::error("PayPal exception occurred resuming agreement", [
                        'code' => $e->getCode(),
                        'data' => $e->getData()
                    ]);
                    return false;
                } catch (Exception $e) {
                    Log::error("Exception occurred resuming agreement", [
                        'message' => $e->getMessage()
                    ]);
                    return false;
                }

                // update subscription row
                $subscription->ends_at = null;
                $subscription->save();
            } else {
                // we are changing subscription plans and PayPal does not support this
                Log::error("Attempted to switch subscription plans. Not supported by PayPal.", [
                    'old_plan' => $subscription->provider_plan_name,
                    'new_plan' => $plan
                ]);
                return false;
            }
        }

        return true;
    }

    /**
     * Handle incoming notifications from the payment gateway
     *
     * @return mixed
     */
    public function handleNotification()
    {
        // grab the entire payload
        $input = $this->request->all();

        // get the provided transaction type
        $txn_type = $input['txn_type'] ?? null;

        $now = new DateTime();

        switch ($txn_type) {
            // handles the initial payment
            case 'recurring_payment_profile_created':
                $agreement_id = $input['recurring_payment_id'];
                $payment_status = $input['initial_payment_status'];
                $transaction_id = $input['initial_payment_txn_id'];

                // find user pending subscription
                $user_plan = UserPendingSubscription::where('agreement_id', $agreement_id)
                    ->where('billing_provider', 'paypal_recurring')
                    ->first();

                if (!$user_plan) {
                    Log::info("User plan not found while processing IPN (ID: " . $agreement_id . ")");
                    abort(422);
                }

                // find user
                $user = User::find($user_plan->user_id);

                switch ($payment_status) {
                    case 'Completed':
                        Log::info("Payment is completed!");

                        // get spark plan
                        $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

                        if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                            $date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                            $trial_end_time = clone $now;
                            $trial_end_time->modify($date_mod);
                        }

                        // update subscription
                        $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
                        $subscription->name = 'default';
                        $subscription->quantity = '1';
                        $subscription->trial_ends_at = isset($trial_end_time) ? $trial_end_time->format('Y-m-d H:i:s') : null;
                        $subscription->paypal_agreement_id = $agreement_id;
                        $subscription->paypal_payer_id = $input['payer_id'];
                        $subscription->provider_plan_name = $spark_plan->id;
                        $subscription->billing_provider = 'paypal_recurring';
                        $subscription->last_payment_at = $now->format('Y-m-d H:i:s');
                        
                        // if eligible for trial, add trial end time
                        if (isset($trial_end_time)) {
                            $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
                        }

                        // save subscription
                        $subscription->save();

                        // delete pending subscription as it is no longer necessary
                        $user_plan->delete();

                        // mark user as having used trial
                        $user->has_used_trial = 1;
                        $user->save();

                        // build data for invoice
                        $items = [
                            [
                                'description' => $spark_plan->name,
                                'amount' => $spark_plan->price,
                                'currency' => config('billing.currency'),
                                'quantity' => 1
                            ]
                        ];

                        // create invoice
                        $invoice_handler = new SalesInvoiceHandler();
                        $invoice = $invoice_handler->generate($subscription->user, $items);

                        // send receipt
                        $data = [
                            'company' => Spark::product()
                        ];

                        $invoice_handler->sendReceipt($data);

                        // log payment
                        PaymentLogService::log($invoice, 'paypal_recurring', $spark_plan->price, 'subscription', [], $input);

                        break;

                    case 'Failed':
                        // delete transaction row
                        $user_plan->delete();

                        break;

                    default:
                        abort(422);

                        break;
                }

                break;

            // handles succeeding payments
            case 'recurring_payment':
                $agreement_id = $input['recurring_payment_id'];
                $payment_status = $input['payment_status'];
                $transaction_id = $input['txn_id'];

                switch ($payment_status) {
                    case 'Completed':
                        // find subscription in database
                        $subscription = Subscription::where('paypal_agreement_id', $agreement_id)->first();

                        if (!$subscription) {
                            Log::info("Subscription not found while processing IPN (ID: " . $agreement_id . ")");
                            abort(422);
                        }

                        // update last payment
                        $subscription->last_payment_at = $now->format('Y-m-d H:i:s');
                        $Subscription->save();

                        // build data for invoice
                        $items = [
                            [
                                'description' => $spark_plan->name,
                                'amount' => $spark_plan->price,
                                'currency' => config('billing.currency'),
                                'quantity' => 1
                            ]
                        ];

                        // create invoice
                        $invoice_handler = new SalesInvoiceHandler();
                        $invoice = $invoice_handler->generate($subscription->user, $items);

                        // send receipt
                        $data = [
                            'company' => Spark::product()
                        ];
                        
                        $invoice_handler->sendReceipt($data);

                        // log payment
                        PaymentLogService::log($invoice, 'paypal_recurring', $spark_plan->price, 'subscription', [], $input);

                        break;

                    case 'Failed':
                    default:
                        abort(422);

                        break;
                }

                break;

            // handles suspension of billing profile
            case 'recurring_payment_profile_cancel':
                $agreement_id = $input['recurring_payment_id'];
                        
                // find pending subscription
                $user_plan = UserPendingSubscription::where('agreement_id', $agreement_id)->first();

                if (!$user_plan) {
                    Log::info("User plan not found while processing IPN (ID: " . $agreement_id . ")");
                    abort(422);
                }

                // delete pending sub as it is no longer necessary
                $user_plan->delete();

                break;

            // handles failure to complete payment
            case 'recurring_payment_suspended_due_to_max_failed_payment':
                $agreement_id = $input['recurring_payment_id'];

                // find in database
                $subscription = Subscription::where('paypal_agreement_id', $agreement_id)->first();

                if (!$subscription) {
                    Log::info("Subscription not found while processing IPN (ID: " . $agreement_id . ")");
                    abort(422);
                }

                // get provider plan info
                $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

                // get billing period
                switch ($spark_plan->interval) {
                    case 'monthly':
                        $billing_period = 'month';
                        break;
                    case 'yearly':
                        $billing_period = 'year';
                        break;
                }

                // compute for end of grace period
                $end_time = strtotime($subscription->last_payment_at . " +1 " . $billing_period);

                // update subscription row
                $subscription->ends_at = date("Y-m-d H:i:s", $end_time);
                $subscription->save();

                break;
        }
    }

    /**
     * Get the initialized payment gateway client
     *
     * @return mixed
     */
    public static function getClient()
    {
        return $this->client;
    }

    /**
     * Get the unique ID of this payment processor
     *
     * @return string
     */
    public static function getId()
    {
        return 'paypal_recurring';
    }

    /**
     * Get the user friendly name of this payment processor
     *
     * @return string
     */
    public static function getDisplayName()
    {
        return 'PayPal (Recurring Payments)';
    }
}
