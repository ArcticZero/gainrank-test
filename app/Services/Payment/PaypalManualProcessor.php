<?php

namespace App\Services\Payment;

use App\Reward;
use App\UserPendingSubscription;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\WebProfile;
use PayPal\Exception\PayPalConnectionException;

use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use App\Contracts\PaymentProcessorContract;
use App\Services\PaymentLogService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PaypalManualProcessor implements PaymentProcessorContract
{
    /**
     * The initialized client
     */
    private $client;

    /**
     * Request object
     */
    private $request;

    /**
     * Connect to the API using provided credentials
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        // initialize API
        $this->client = new ApiContext(
            new OAuthTokenCredential(
                config('services.paypal.key'),
                config('services.paypal.secret')
            )
        );

        // are we live? switch to appropriate endpoints
        $live = config('services.paypal.live');

        if ($live) {
            $this->client->setConfig(array(
                'mode' => 'live'
            ));
        }

        // set request
        $this->request = $request;
    }

    /**
     * Handle the pre-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionCheckout()
    {
        $error = false;
        $result = [];
        $pp_plan = false;

        // get spark plan details
        $spark_plan = Spark::plans()->where('id', $this->request->input('plan'))->first();

        // get pending subscription with attached agreement
        $pending = UserPendingSubscription::where('user_id', $this->request->user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        if ($pending) {
            $error = 'You already have a pending subscription';
        } else {
            // get initial payment price
            $initial_price = $spark_plan->price;

            // delete any existing pending subs
            UserPendingSubscription::where('user_id', $this->request->user()->id)->delete();

            // create new pending sub
            $user_plan = new UserPendingSubscription();
            $user_plan->user_id = $this->request->user()->id;
            $user_plan->provider_plan_name = $this->request->plan;
            $user_plan->billing_provider = $this->getId();
            $user_plan->save();

            // get billing period
            switch ($spark_plan->interval) {
                case 'monthly':
                    $billing_period = 'month';
                    break;
                case 'yearly':
                    $billing_period = 'year';
                    break;
            }

            // create payer
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            // if user is linked to an ad campaign, apply corresponding discounts to initial payment
            if (!empty($this->request->user()->campaign)) {
                $discounts = [
                    'amount' => 0,
                    'percent' => 0
                ];

                $bonuses = $this->request->user()->campaign->bonuses()
                    ->whereHas('type', function ($q) {
                        $q->whereIn('reward_type', [
                                Reward::TYPE_DISCOUNT_INITIAL_AMOUNT,
                                Reward::TYPE_DISCOUNT_INITIAL_PERCENT
                            ]);
                    })
                        ->get();

                if (!$bonuses->isEmpty()) {
                    foreach ($bonuses as $bonus) {
                        if (!empty($bonus->bonus_amount && $bonus->bonus_amount > 0)) {
                            switch ($bonus->type->reward_type) {
                                case Reward::TYPE_DISCOUNT_INITIAL_AMOUNT:
                                    $discounts['amount'] = $bonus->bonus_amount;
                                    break;
                                case Reward::TYPE_DISCOUNT_INITIAL_PERCENT:
                                    $discounts['percent'] = bcdiv($bonus->bonus_amount, 100);
                                    break;
                            }
                        }
                    }
                }

                // apply flat amount discount to initial payment
                if ($discounts['amount'] > 0) {
                    $initial_price = bcsub($initial_price, $discounts['amount']);
                }

                // apply percentage discount to initial payment
                if ($discounts['percent'] > 0) {
                    $initial_price = bcmul($initial_price, bcsub(100, $discounts['percent']));
                }
            }

            // get currency type
            $currency = config('billing.currency');

            // add subscription to item list
            $item = new Item();
            $item->setName('Gainrank Subscription')
                ->setCurrency($currency)
                ->setQuantity(1)
                ->setSku($spark_plan->id)
                ->setPrice($initial_price);

            $item_list = new ItemList();
            $item_list->setItems([$item]);

            // set redirect URLs
            $return_url = url('settings/subscription/checkout/paypal_manual');
            $redirects = new RedirectUrls();
            $redirects->setReturnUrl($return_url . "/success?p=" . $user_plan->id)
                ->setCancelUrl($return_url . "/failure?p=" . $user_plan->id);

            // set payment amount
            $amount = new Amount();
            $amount->setCurrency($currency)
                ->setTotal($initial_price);

            // create transaction
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setCustom($user_plan->id)
                ->setDescription('Gainrank Subscription (' . $spark_plan->name . ')');

            // remove shipping fields
            $input_fields = new InputFields();
            $input_fields->setNoShipping(1)
                ->setAddressOverride(0);

            $web_profile = new WebProfile();
            $web_profile->setName('Gainrank ' . uniqid())
                ->setInputFields($input_fields)
                ->setTemporary(true);

            // create web profile
            try {
                $web_profile_id = $web_profile->create($this->client)->getId();
            } catch (PayPalConnectionException $e) {
                Log::error("PayPal exception occurred creating web profile", [
                    'code' => $e->getCode(),
                    'data' => $e->getData()
                ]);
                $error = 'An error occurred while processing your transaction.';
            } catch (Exception $e) {
                Log::error("Exception occurred creating web profile", [
                    'message' => $e->getMessage()
                ]);
                $error = 'An error occurred while processing your transaction.';
            }

            if (!$error) {
                // create full payment object
                $payment = new Payment();
                $payment->setIntent('authorize')
                    ->setPayer($payer)
                    ->setExperienceProfileId($web_profile_id)
                    ->setRedirectUrls($redirects)
                    ->setTransactions([$transaction]);

                // process payment
                try {
                    $payment->create($this->client);

                    $result = [
                        'success' => true,
                        'approval_url' => $payment->getApprovalLink()
                    ];
                } catch (PayPalConnectionException $e) {
                    Log::error("PayPal exception occurred creating payment object", [
                        'code' => $e->getCode(),
                        'data' => $e->getData()
                    ]);
                    $error = 'An error occurred while processing your transaction.';
                } catch (Exception $e) {
                    Log::error("Exception occurred creating payment object", [
                        'message' => $e->getMessage()
                    ]);
                    $error = 'An error occurred while processing your transaction.';
                }
            }
        }

        if ($error) {
            $result = [
                'success' => false,
                'error' => $error
            ];
        }

        return $result;
    }

    /**
     * Handle the post-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionPostCheckout()
    {
        // validate input
        $validator = Validator::make($this->request->all(), [
            'p' => 'required|exists:user_pending_subscriptions,id',
            'paymentId' => 'required',
            'PayerID' => 'required'
        ]);

        if ($validator->fails()) {
            return false;
        }

        $pending_id = $this->request->input('p');
        $payment_id = $this->request->input('paymentId');
        $payer_id = $this->request->input('PayerID');

        // get payment
        $payment = Payment::get($payment_id, $this->client);

        $execution = new PaymentExecution();
        $execution->setPayerId($payer_id);

        // execute payment
        try {
            $payment->execute($execution, $this->client);
        } catch (PayPalConnectionException $e) {
            Log::error("PayPal exception occurred executing payment", [
                'code' => $e->getCode(),
                'data' => $e->getData()
            ]);
            return false;
        } catch (Exception $e) {
            Log::error("Exception occurred executing payment", [
                'message' => $e->getMessage()
            ]);
            return false;
        }

        // refresh payment info
        try {
            $payment = Payment::get($payment_id, $this->client);
        } catch (PayPalConnectionException $e) {
            Log::error("PayPal exception occurred retrieving payment", [
                'code' => $e->getCode(),
                'data' => $e->getData(),
                'payment_id' => $payment_id
            ]);
            return false;
        } catch (Exception $e) {
            Log::error("Exception occurred retrieving payment", [
                'message' => $e->getMessage(),
                'payment_id' => $payment_id
            ]);
            return false;
        }

        // update pending sub
        $user_plan = UserPendingSubscription::find($pending_id);
        $user_plan->agreement_id = $payment_id;
        $user_plan->save();

        return $user_plan;
    }

    /**
     * Suspend a current subscription
     *
     * @return boolean
     */
    public function suspendSubscription()
    {
        // get current subscription
        $subscription = Subscription::where('user_id', $this->request->user()->id)
            ->where('billing_provider', 'paypal_manual')
            ->where('is_cancelled', 0)
            ->first();

        if (empty($subscription)) {
            return false;
        } else {
            // update subscription row
            $subscription->is_cancelled = 1;
            $subscription->save();
        }

        return true;
    }

    /**
     * Update an existing subscription
     *
     * @return boolean
     */
    public function updateSubscription()
    {
        // validate input
        $validator = Validator::make($this->request->all(), [
            'plan' => 'required|alpha_dash'
        ]);

        if ($validator->fails()) {
            return false;
        }

        $plan = $this->request->input('plan');

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $plan)->first();

        // get current subscription
        $subscription = Subscription::where('user_id', $this->request->user()->id)
            ->where('billing_provider', $this->getId())
            ->where('is_cancelled', 1)
            ->first();

        if (empty($subscription) || empty($spark_plan)) {
            return false;
        } else {
            if ($plan == $subscription->provider_plan_name) {
                // this is the same plan and we are resuming the subscription
                $subscription->is_cancelled = 0;
                $subscription->save();
            } else {
                // we are changing subscription plans and PayPal does not support this
                Log::error("Attempted to switch subscription plans. Not supported by PayPal.", [
                    'old_plan' => $subscription->provider_plan_name,
                    'new_plan' => $plan
                ]);
                return false;
            }
        }

        return true;
    }

    /**
     * Handle incoming notifications from the payment gateway
     *
     * @return mixed
     */
    public function handleNotification()
    {
        // grab the entire payload
        $input = $this->request->all();

        // get the provided transaction type
        $txn_type = $input['txn_type'] ?? null;

        $now = new DateTime();

        switch ($txn_type) {
            // handles receiving a payment
            case 'cart':
                $user_plan_id = $input['custom'];
                $payment_status = $input['payment_status'];
                $transaction_id = $input['txn_id'];
                $auth_id = $input['auth_id'];

                // find user pending subscription unless capturing a payment
                if ($payment_status != 'Completed') {
                    $user_plan = UserPendingSubscription::find($user_plan_id);

                    if (!$user_plan) {
                        Log::info("User plan not found while processing IPN (ID: " . $auth_id . ")");
                        abort(422);
                    }

                    // find user
                    $user = User::find($user_plan->user_id);
                }

                switch ($payment_status) {
                    case 'Pending':
                        // get spark plan
                        $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

                        if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                            $trial_date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                            $trial_end_time = clone $now;
                            $trial_end_time->modify($trial_date_mod);
                            $end_time = clone $trial_end_time;
                        } else {
                            $end_time = clone $now;
                        }

                        // get billing period
                        switch ($spark_plan->interval) {
                            case 'monthly':
                                $billing_period = 'month';
                                break;
                            case 'yearly':
                                $billing_period = 'year';
                                break;
                        }

                        // compute for end of subscription
                        $end_date_mod = "+1 " . $billing_period;
                        $end_time->modify($end_date_mod);

                        // update subscription
                        $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
                        $subscription->name = 'default';
                        $subscription->quantity = '1';
                        $subscription->paypal_agreement_id = $auth_id;
                        $subscription->paypal_payer_id = $input['payer_id'];
                        $subscription->provider_plan_name = $spark_plan->id;
                        $subscription->billing_provider = 'paypal_manual';
                        $subscription->ends_at = $end_time->format('Y-m-d H:i:s');
                        $subscription->last_payment_at = null;

                        // if eligible for trial, add trial end time
                        if (isset($trial_end_time)) {
                            $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
                        }

                        // save subscription
                        $subscription->save();

                        // delete pending sub as it is no longer necessary
                        $user_plan->delete();

                        // mark user as having used trial
                        $user->has_used_trial = 1;
                        $user->save();

                        break;

                    case 'Completed':
                        // pre-authorized payment was captured via IPN
                        $subscription = Subscription::where('paypal_agreement_id', $auth_id)->first();

                        if (!$subscription) {
                            Log::info("Subscription not found while processing IPN (ID: " . $auth_id . ")");
                            abort(422);
                        }

                        $subscription->last_payment_at = $now->format('Y-m-d H:i:s');
                        $subscription->save();

                        // get spark plan
                        $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

                        // build data for invoice
                        $items = [
                            [
                                'description' => $spark_plan->name,
                                'amount' => $spark_plan->price,
                                'currency' => config('billing.currency'),
                                'quantity' => 1
                            ]
                        ];

                        // create invoice
                        $invoice_handler = new SalesInvoiceHandler();
                        $invoice = $invoice_handler->generate($subscription->user, $items);

                        // send receipt
                        $data = [
                            'company' => Spark::product()
                        ];
                        
                        $invoice_handler->sendReceipt($data);

                        // log payment
                        PaymentLogService::log($invoice, 'paypal_manual', $spark_plan->price, 'subscription', [], $input);

                        break;

                    case 'Failed':
                        // delete transaction row
                        $user_plan->delete();

                        break;

                    default:
                        abort(422);
                        break;
                }

                break;
        }
    }

    /**
     * Get the initialized payment gateway client
     *
     * @return mixed
     */
    public static function getClient()
    {
        return $this->client;
    }

    /**
     * Get the unique ID of this payment processor
     *
     * @return string
     */
    public static function getId()
    {
        return 'paypal_manual';
    }

    /**
     * Get the user friendly name of this payment processor
     *
     * @return string
     */
    public static function getDisplayName()
    {
        return 'PayPal (Manual Payments)';
    }
}
