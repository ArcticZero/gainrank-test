<?php

namespace App\Services\Payment;

use App\UserPendingSubscription;
use App\CoinbaseCheckout;
use App\PartialPayment;

use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Exception\AuthenticationException;

use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use App\Contracts\PaymentProcessorContract;
use App\Services\PaymentLogService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CoinbaseProcessor implements PaymentProcessorContract
{
    /**
     * The initialized client
     */
    private $client;

    /**
     * Request object
     */
    private $request;

    /**
     * Connect to the API using provided credentials
     *
     * @return void
     */
    public function __construct(Request $request = null)
    {
        try {
            $configuration = Configuration::apiKey(config('services.coinbase.key'), config('services.coinbase.secret'));
            $this->client = Client::create($configuration);
        } catch (AuthenticationException $e) {
            Log::error("Coinbase authentication exception occurred", [
                'message' => $e->getMessage()
            ]);
        } catch (Exception $e) {
            Log::error("Exception occurred", [
                'message' => $e->getMessage()
            ]);
        }

        // set request
        $this->request = $request;
    }

    /**
     * Handle the pre-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionCheckout()
    {
        $error = false;
        $result = [];

        // get spark plan details
        $spark_plan = Spark::plans()->where('id', $this->request->input('plan'))->first();

        // get pending subscription with attached agreement
        $pending = UserPendingSubscription::where('user_id', $this->request->user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        $cc = false;

        if ($pending) {
            $error = 'You already have a pending subscription';
        } else {
            // if user is linked to an ad campaign, check for corresponding initial checkout
            if (!empty($this->request->user()->campaign)) {
                $cc = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                    ->where('ad_campaign_id', $this->request->user()->campaign->id)
                    ->where('discount_type', CoinbaseCheckout::DISCOUNT_TYPE_INITIAL)
                    ->first();
            }

            // use default checkout definition
            if (empty($cc)) {
                $cc = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                    ->whereNull('ad_campaign_id')
                    ->first();
            }

            if (empty($cc)) {
                Log::error("Coinbase checkout pairing not found", [
                    'plan_id' => $spark_plan->id
                ]);
                $error = 'An error occurred while processing your billing agreement.';
            }
        }

        if (!$error) {
            // create new pending sub
            $user_plan = new UserPendingSubscription();
            $user_plan->user_id = $this->request->user()->id;
            $user_plan->provider_plan_name = $this->request->input('plan');
            $user_plan->billing_provider = $this->getId();
            $user_plan->save();
            
            // get approval URL
            $code = $cc->checkout_embed_code;
            $approval_url = str_replace('{CODE}', $code, config('services.coinbase.url'));

            $result = [
                'success' => true,
                'approval_url' => $approval_url
            ];
        } else {
            $result = [
                'success' => false,
                'error' => $error
            ];
        }

        return $result;
    }

    /**
     * Handle the post-redirect phase for subscription checkout
     *
     * @return mixed
     */
    public function subscriptionPostCheckout()
    {
        // validate input
        $validator = Validator::make($this->request->all(), [
            'order' => 'required'
        ]);

        if ($validator->fails()) {
            return false;
        }

        $order = $this->request->input('order');
        $user_id = $this->request->user()->id;

        // look for active sub
        $user_plan = Subscription::where('user_id', $user_id)
            ->where('billing_provider', 'coinbase')
            ->first();

        // if none, look for pending sub
        if (!empty($user_plan)) {
            $id_column = 'subscription_id';
        } else {
            $user_plan = UserPendingSubscription::where('user_id', $user_id)
                ->where('billing_provider', 'coinbase')
                ->first();
        }

        if (!empty($user_plan)) {
            $id_column = 'pending_subscription_id';

            // save order ID
            if (!$user_plan->agreement_id) {
                $user_plan->agreement_id = $order['id'];
                $user_plan->save();
            }
        } else {
            // no existing sub found
            return false;
        }

        // record this payment
        $payment = new PartialPayment();
        $payment->$id_column = $user_plan->id;
        $payment->user_id = $user_id;
        $payment->order_id = $order['id'];
        $payment->currency = 'BTC';
        $payment->save();

        return $user_plan;
    }

    /**
     * Suspend a current subscription
     *
     * @return boolean
     */
    public function suspendSubscription()
    {
        // get current subscription
        $subscription = Subscription::where('user_id', $this->request->user()->id)
            ->where('billing_provider', 'coinbase')
            ->where('is_cancelled', 0)
            ->first();

        if (empty($subscription)) {
            return false;
        } else {
            // update subscription row
            $subscription->is_cancelled = 1;
            $subscription->save();
        }

        return true;
    }

    /**
     * Update an existing subscription
     *
     * @return boolean
     */
    public function updateSubscription()
    {
        // validate input
        $validator = Validator::make($this->request->all(), [
            'plan' => 'required|alpha_dash'
        ]);

        if ($validator->fails()) {
            return false;
        }

        $plan = $this->request->input('plan');

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $plan)->first();

        // get current subscription
        $subscription = Subscription::where('user_id', $this->request->user()->id)
            ->where('billing_provider', $this->getId())
            ->where('is_cancelled', 1)
            ->first();

        if (empty($subscription) || empty($spark_plan)) {
            return false;
        } else {
            if ($plan == $subscription->provider_plan_name) {
                // this is the same plan and we are resuming the subscription
                $subscription->is_cancelled = 0;
                $subscription->save();
            } else {
                // we are changing subscription plans and Coinbase does not support this
                Log::error("Attempted to switch subscription plans. Not supported by Coinbase.", [
                    'old_plan' => $subscription->provider_plan_name,
                    'new_plan' => $plan
                ]);
                return false;
            }
        }

        return true;
    }

    /**
     * Handle incoming notifications from the payment gateway
     *
     * @return mixed
     */
    public function handleNotification()
    {
        // grab the entire payload
        $input = $this->request->all();
        
        // get the provided transaction type
        $txn_type = $input['type'] ?? null;

        $now = new DateTime();

        switch ($txn_type) {
            // handles a successful payment
            case 'wallet:orders:paid':
                $order_code = $input['data']['code'];
                $payer_id = $input['user']['id'];
                $current_amount_paid = $input['data']['total_amount_received']['amount'];

                Log::info('Retrieving partial payment record', ['order_code' => $order_code]);

                // get payment row that matches this order
                $payment = PartialPayment::where('order_id', $order_code)->first();

                // can't find payment row for this
                if (!$payment) {
                    Log::info("Pending payment not found while processing notification.", ['order_code' => $order_code]);
                    abort(422);
                }

                // look for subscription
                if ($payment->subscription_id) {
                    $user_plan = Subscription::find($payment->subscription_id);
                    $id_name = 'Subscription ID';
                } elseif ($payment->pending_subscription_id) {
                    $user_plan = UserPendingSubscription::find($payment->pending_subscription_id);
                    $id_name = 'Pending Subscription ID';
                } else {
                    // I did not find any sub
                    Log::info("Active or pending subscription not found while processing notification.", ['order_code' => $order_code]);
                    abort(422);
                }

                $id_column = str_replace(" ", "_", strtolower($id_name));

                // find user
                $user = User::find($user_plan->user_id);

                // get spark plan
                $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

                // check for any existing, paid partial payments
                $partial_payments = PartialPayment::where($id_column, $user_plan->id)
                    ->where('is_paid', true)
                    ->orderBy('created_at', 'asc')
                    ->get();

                $total_paid = 0;

                // get total amount paid so far
                if ($partial_payments->count()) {
                    Log::info('Previously paid partial payments found');

                    // retrieve the total amount to be paid
                    $total_amount = $partial_payments->first()->total_amount;

                    foreach ($partial_payments as $pp) {
                        $total_paid = bcadd($total_paid, $pp->amount_paid);
                    }
                } else {
                    Log::info('No paid partial payments found');

                    // this is the first payment, record the total amount to be paid from the notification
                    $total_amount = $input['data']['bitcoin_amount']['amount'];
                }

                // add new amount to total
                $total_paid = bcadd($total_paid, $current_amount_paid);

                // get comparison of total paid and total amount
                $comparison = bccomp($total_paid, $total_amount);

                Log::info('Total Paid: ' . $total_paid);
                Log::info('Total Amount: ' . $total_amount);
                Log::info('Comparison: ' . $comparison);

                // compare total paid vs total amount requested
                if ($comparison === -1) {
                    // user underpaid
                    Log::info('User has underpaid on Coinbase', [
                        'difference' => bcsub($total_amount, $total_paid),
                        'user_id' => $user->id
                    ]);

                    // update current partial payment row with info
                    $payment->amount_paid = $current_amount_paid;
                    $payment->total_amount = $total_amount;
                    $payment->is_paid = true;
                    $payment->save();

                    // send a new temporary checkout
                    $balance = bcsub($total_amount, $total_paid);

                    $return_url = url('settings/subscription/coinbase');

                    $params = array(
                        'name'          => 'Balance for Gainrank ' . $id_name . '# ' . $user_plan->id,
                        'amount'        => new Money($balance, 'BTC'),
                        'description'   => $spark_plan->name,
                        'successUrl'    => $return_url . "/success",
                        'cancelUrl'     => $return_url . "/failure"
                    );

                    try {
                        $checkout = new Checkout($params);
                        $this->client->createCheckout($checkout);
                    } catch (AuthenticationException $e) {
                        Log::error("Coinbase Authentication Exception occurred: " . $e->getMessage());
                        exit;
                    } catch (InvalidTokenException $e) {
                        Log::error("Coinbase Token Exception occurred: " . $e->getMessage());
                        exit;
                    } catch (Exception $e) {
                        Log::error("Exception occurred: " . $e->getMessage());
                        exit;
                    }

                    // build email data
                    $data = [
                        'pay_url'       => str_replace('{CODE}', $checkout->getEmbedCode(), config('services.coinbase.url')),
                        'total_amount'  => $total_amount,
                        'total_paid'    => $total_paid,
                        'balance'       => $balance,
                        'currency'      => 'BTC',
                    ];

                    $invoiceData = Spark::invoiceDataFor($user);

                    // send email
                    Mail::send('settings.invoices.emails.partial', compact('data', 'invoiceData', 'user'), function ($message) use ($user) {
                        $message->to($user->email, $user->name)
                            ->subject('Your payment is incomplete');
                    });
                } else {
                    if ($comparison === 1) {
                        $difference = bcsub($total_paid, $total_amount);
                        $rate = $this->client->getBuyPrice('BTC-USD');

                        $credits = round(bcmul($difference, $rate), 2, PHP_ROUND_HALF_DOWN);
                        $user->credits = bcadd($user->credits, $credits);

                        Log::info('User overpaid on Coinbase, credits added to user', [
                            'difference' => $difference,
                            'user_id' => $user->id,
                            'credits' => $credits
                        ]);
                    } else {
                        Log::info('User has paid the exact amount', [
                            'amount' => $total_paid,
                            'user_id' => $user->id
                        ]);
                    }

                    Log::info('Deleting all partial payments');

                    // user is fully paid, delete all partial payment rows
                    PartialPayment::where($id_column, $user_plan->id)->delete();

                    // check if this is an existing subscription
                    if ($id_column == 'subscription_id') {
                        // this is an existing subscription
                        Log::info('Extending subscription');

                        if ($now > $user_plan->ends_at) {
                            // subscription is expired, begin new billing period now
                            $end_time = clone $now;
                        } else {
                            // subscription is active, extend it
                            $end_time = $user_plan->ends_at;
                        }

                        // compute for end of subscription
                        $billing_period = $this->getBillingPeriod($spark_plan->interval);
                        $end_date_mod = "+1 " . $billing_period;
                        $end_time->modify($end_date_mod);

                        // update subscription
                        $user_plan->ends_at = $end_time->format('Y-m-d H:i:s');
                        $user_plan->last_payment_at = $now->format('Y-m-d H:i:s');
                        $user_plan->save();
                    } else {
                        // this is a pending subscription, let's activate it
                        Log::info('Activating pending subscription');

                        if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                            // TODO: how to do trial on coinbase? future payments not supported
                            $trial_end_time = clone $now;
                            $end_time = clone $now;
                        } else {
                            $end_time = clone $now;
                        }

                        // compute for end of subscription
                        $billing_period = $this->getBillingPeriod($spark_plan->interval);
                        $end_date_mod = "+1 " . $billing_period;
                        $end_time->modify($end_date_mod);

                        // update subscription
                        $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
                        $subscription->name = 'default';
                        $subscription->quantity = '1';
                        $subscription->provider_plan_name = $spark_plan->id;
                        $subscription->billing_provider = 'coinbase';
                        $subscription->ends_at = $end_time->format('Y-m-d H:i:s');
                        $subscription->last_payment_at = $now->format('Y-m-d H:i:s');

                        // since Spark expects stripe by default if not using braintree, we fill in the stripe_plan field with the plan ID
                        $subscription->stripe_plan = $spark_plan->id;

                        // if eligible for trial, add trial end time
                        if (isset($trial_end_time)) {
                            $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
                        }

                        // save subscription
                        $subscription->save();

                        // delete pending sub as it is no longer necessary
                        $user_plan->delete();
                    }

                    // mark user as having used trial
                    $user->has_used_trial = 1;
                    $user->save();

                    // build data for invoice
                    $items = [
                        [
                            'description' => $spark_plan->name,
                            'amount' => $input['data']['amount']['amount'],
                            'currency' => config('billing.currency'),
                            'quantity' => 1
                        ]
                    ];

                    // create invoice
                    $invoice_handler = new SalesInvoiceHandler();
                    $invoice = $invoice_handler->generate($subscription->user, $items);

                    // send receipt
                    $data = [
                        'company' => Spark::product()
                    ];
                    
                    $invoice_handler->sendReceipt($data);

                    // log payment
                    PaymentLogService::log($invoice, 'coinbase', $input['data']['amount']['amount'], 'subscription', [], $input, $input['data']['bitcoin_amount']['amount'], 'BTC');
                }

                break;

            case 'wallet:orders:mispaid':
                // TODO: handle mispaid orders
                break;
        }

        Log::info("Saving notification to database");

        // save notification data
        $notification = new CoinbaseNotification();
        $notification->notification_type = $input['txn_type'];
        $notification->request_details = json_encode($input);
        $notification->save();
    }

    /**
     * Get the initialized payment gateway client
     *
     * @return mixed
     */
    public static function getClient()
    {
        return $this->client;
    }

    /**
     * Get the unique ID of this payment processor
     *
     * @return string
     */
    public static function getId()
    {
        return 'coinbase';
    }

    /**
     * Get the user friendly name of this payment processor
     *
     * @return string
     */
    public static function getDisplayName()
    {
        return 'Coinbase';
    }
}
