<?php

namespace App\Services;

use App\Contracts\RewardContract;
use App\Reward;
use App\User;

class RewardHandler implements RewardContract
{
    public function implement(Reward $reward, User $user)
    {
        $type = $reward->getType();

        switch ($type) {
            case Reward::TYPE_PAYOUT:
                return $this->actionPayout($reward, $user);
            case Reward::TYPE_BONUS_KEYWORD:
                return $this->actionBonusKeyword($reward, $user);
            case Reward::TYPE_DISCOUNT_AMOUNT:
                return $this->actionDiscountAmount($reward, $user);
            case Reward::TYPE_DISCONT_PERCENT:
                return $this->actionDiscountPercent($reward, $user);
        }

        return false;
    }

    public function generateAffiliateReward($aff_type)
    {
        switch ($aff_type) {
            case AffiliateTransaction::TYPE_SUBSCRIBE:
                return new Reward(Reward::TYPE_PAYOUT, ['payout_amount' => 5]);
        }

        return null;
    }

    // START of actions for reward types
    protected function actionPayout(Reward $reward, User $user)
    {
        $details = $reward->getDetails();

        $this->addPayoutAmount($user, $details['payout_amount']);

        return true;
    }

    protected function actionBonusKeyword(Reward $reward, User $user)
    {
        // TODO: implement the bonus keyword functionality
        return true;
    }

    protected function actionDiscountAmount(Reward $reward, User $user)
    {
        // TODO: implement disocunting (related to advertising issue)
        return true;
    }
    
    protected function actionDiscountPercent(Reward $reward, User $user)
    {
        // TODO: implement disocunting (related to advertising issue)
        return true;
    }
    // END of actions for reward types

    protected function addPayoutAmount(User $user, $amount)
    {
        if (!$user->isAffiliate()) {
            return false;
        }

        $user->affiliate->payout_available = bcadd($affiliate->payout_available, $amount);
        $user->affiliate->save();

        return true;
    }
}
