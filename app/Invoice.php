<?php

namespace App;

use Carbon\Carbon;
use Dompdf\Dompdf;

use Laravel\Spark\Spark;
use Laravel\Spark\Team as SparkTeam;
use Laravel\Cashier\Invoice as CashierInvoice;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class Invoice extends CashierInvoice
{
    protected $owner;
    protected $invoice;
    protected $subscription;

    /**
     * Create a new invoice instance.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $owner
     * @param  \Laravel\Spark\LocalInvoice  $invoice
     * @param  \Laravel\Spark\Subscription  $subscription
     * @return void
     */
    public function __construct($owner, $invoice, $subscription)
    {
        $this->owner = $owner;
        $this->invoice = $invoice;
        $this->subscription = $subscription;

        // set total * 100 to conform to Stripe
        $this->invoice->total = $this->invoice->total * 100;
    }

    /**
     * Get a Carbon date for the invoice.
     *
     * @param  \DateTimeZone|string  $timezone
     * @return \Carbon\Carbon
     */
    public function date($timezone = null)
    {
        $timestamp = strtotime($this->invoice->created_at);
        $carbon = Carbon::createFromTimestampUTC($timestamp);

        return $timezone ? $carbon->setTimezone($timezone) : $carbon;
    }

    /**
     * Get local invoice data
     *
     * @return \Laravel\Spark\LocalInvoice
     */
    public function getLocal()
    {
        return $this->invoice;
    }

     /**
     * Get active subscription
     *
     * @return \Laravel\Spark\Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Get billing period start date
     *
     * @return \Carbon\Carbon
     */
    public function getBillingPeriodStart($timezone = null)
    {
        $subscription = $this->getSubscription();
        $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();
        $timestamp = strtotime($this->invoice->created_at . "+" . $spark_plan->trialDays . " days");
        $carbon = Carbon::createFromTimestampUTC($timestamp);

        return $timezone ? $carbon->setTimezone($timezone) : $carbon;
    }

    /**
     * Get billing period end date
     *
     * @return \Carbon\Carbon
     */
    public function getBillingPeriodEnd($timezone = null)
    {
        $subscription = $this->getSubscription();

        if (!$subscription->ends_at) {
            $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

            switch ($spark_plan->interval) {
                case 'monthly':
                    $billing_period = 'month';
                    break;
                case 'yearly':
                    $billing_period = 'year';
                    break;
            }

            // compute for end of grace period
            $timestamp = strtotime($subscription->last_payment_at . " +1" . $billing_period);
        } else {
            $timestamp = strtotime($subscription->ends_at);
        }

        $carbon = Carbon::createFromTimestampUTC($timestamp);

        return $timezone ? $carbon->setTimezone($timezone) : $carbon;
    }

    /**
     * Get the View instance for the invoice.
     *
     * @param  array  $data
     * @return \Illuminate\View\View
     */
    public function view(array $data)
    {
        return View::make('receipt', array_merge($data, [
            'invoice' => $this,
            'owner' => $this->owner,
            'user' => $this->owner,
        ]));
    }

    /**
     * Capture the invoice as a PDF and return the raw bytes.
     *
     * @param  array  $data
     * @return string
     */
    public function pdf(array $data)
    {
        if (! defined('DOMPDF_ENABLE_AUTOLOAD')) {
            define('DOMPDF_ENABLE_AUTOLOAD', false);
        }

        if (file_exists($configPath = base_path().'/vendor/dompdf/dompdf/dompdf_config.inc.php')) {
            require_once $configPath;
        }

        $dompdf = new Dompdf;

        $dompdf->loadHtml($this->view($data)->render());

        @$dompdf->render();

        return $dompdf->output();
    }
}
