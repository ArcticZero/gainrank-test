<?php

namespace App\Console\Commands;

use App\AffiliateTransaction;
use App\Reward;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Contracts\RewardContract;

use Illuminate\Console\Command;

class ProcessMatureAffiliateTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'affiliate:mature-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all mature transactions, process payouts and implement rewards';

    // reward handler service
    protected $reward_handler;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RewardContract $reward_handler)
    {
        parent::__construct();
        $this->reward_handler = $reward_handler;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now();

        $this->line('Retrieving all due affiliate transactions...');

        // Get all transactions that have hit or are past mature date
        $transactions = AffiliateTransaction::with('user')
            ->whereDate('mature_at', '<=', $today->toDateString())
            ->where('is_mature', false)
            ->where('status', AffiliateTransaction::STATUS_PENDING)
            ->get();

        // No transactions were found
        if (empty($transactions)) {
            $this->line('No transactions found.');
            exit;
        }

        $i = 0;

        foreach ($transactions as $transaction) {
            // Get user object
            $this->line('Found transaction ' . $transaction->id . '. Retrieving user...');
            $user = $transaction->user;

            // User not found
            if (empty($user)) {
                $this->error('Could not find user associated with this transaction. Logged and skipping...');
                Log::error('User not found while processing affiliate transaction', [
                    'transaction_id' => $transaction->id,
                    'user_id' => $transaction->user_id
                ]);

                continue;
            }

            // Implement reward
            $this->line('Implementing reward...');
            $reward = new Reward($transaction->reward_type, json_decode($transaction->reward_details, true));
            $implement = $this->reward_handler->implement($reward, $user);

            // Something happened while implementing reward
            if (!$implement) {
                $this->error('Could not implement reward. Logged and skipping...');
                Log::error('Could not implement reward for affiliate transaction', [
                    'transaction_id' => $transaction->id,
                    'reward_type' => $transaction->reward_type,
                    'reward_details' => json_encode($transaction->reward_details)
                ]);

                continue;
            }

            // Mark transaction as matured
            $this->line('Updating transaction row...');
            $transaction->is_mature = true;
            $transaction->status = AffiliateTransaction::STATUS_MATURE;
            $transaction->save();
            $i++;
        }

        if ($i > 0) {
            $this->info('Done. Successfully processed ' . $i . ' affiliate transactions.');
        } else {
            $this->info('Done. No pending affiliate transactions found.');
        }
    }
}
