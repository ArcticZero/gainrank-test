<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Helpdesk;

class RegisterHelpdeskUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpdesk:register-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register unregistered helpdesk users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        Helpdesk::registerUsers();
    }
}
