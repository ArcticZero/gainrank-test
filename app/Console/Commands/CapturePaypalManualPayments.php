<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\PaypalBootstrap;

use Laravel\Cashier\Cashier;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use PayPal\Api\Amount;
use PayPal\Api\Authorization;
use PayPal\Api\Capture;
use PayPal\Exception\PayPalConnectionException;

use \DateTime;

/**
 *
 * Capture all due authorized PayPal (manual) payments. This is due to how the
 * initial checkout flow works to respect a plan's trial period, wherein we
 * authorize a future payment at initial checkout then collect/capture it once
 * trial expires and a regular subscription period starts.
 *
 */
class CapturePaypalManualPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paypal:capture';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Capture all due PayPal manual payments';
    protected $apiContext;
    protected $currency;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->apiContext = PaypalBootstrap::initialize();
        $this->currency = strtoupper(Cashier::usesCurrency());
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Retrieving all PayPal manual billing subscriptions with due payments...');

        $currency = strtoupper(Cashier::usesCurrency());

        // get all subs that match criteria
        $subscriptions = Subscription::where('billing_provider', 'paypal_manual')
            ->where('is_cancelled', 0)
            ->whereNull('last_payment_at')
            ->where(function ($query) {
                $now = new DateTime();
                $query->whereNull('trial_ends_at')
                    ->orWhere('trial_ends_at', '<=', $now->format("Y-m-d H:i:s"));
            })
            ->get();

        // attempt to claim each payment
        if (!empty($subscriptions)) {
            foreach ($subscriptions as $subscription) {
                $this->line('Found subscription for user ' . $subscription->user_id . '. Checking authorization...');

                // get spark plan
                $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

                if (!$spark_plan) {
                    $this->error("Spark plan " . $subscription->provider_plan_name . " not found");
                    exit;
                }

                // retrieve authorization
                try {
                    $authorization = Authorization::get($subscription->paypal_agreement_id, $this->apiContext);
                } catch (PayPalConnectionException $e) {
                    $this->error("PayPal exception occurred while retrieving authorization: (" . $e->getCode() . ") " . $e->getData());
                    exit;
                } catch (Exception $e) {
                    $this->error("Exception occurred while retrieving authorization: " . $e);
                    exit;
                }

                $this->line('Authorization confirmed, capturing payment...');

                // capture payment
                if ($authorization->getState() != 'captured') {
                    try {
                        $auth_id = $authorization->getId();

                        // set amount to capture
                        $amount = new Amount();
                        $amount->setCurrency($this->currency)
                            ->setTotal($spark_plan->price);

                        // set capture object
                        $capture = new Capture();
                        $capture->setAmount($amount);

                        // make the call
                        $result = $authorization->capture($capture, $this->apiContext);
                    } catch (PayPalConnectionException $e) {
                        $this->error("PayPal exception occurred while capturing authorized payment: (" . $e->getCode() . ") " . $e->getData());
                        exit;
                    } catch (Exception $e) {
                        $this->error("Exception occurred while capturing authorized payment: " . $e);
                        exit;
                    }
                }

                $this->info('Request to capture payment has been sent.');
            }
        }

        $this->info('Done.');
    }
}
