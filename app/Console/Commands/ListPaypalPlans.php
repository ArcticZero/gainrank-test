<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\PaypalBootstrap;

use PayPal\Api\Plan;

/**
 *
 * Command for listing all PayPal recurring payment plans.
 * Note that this views the plans stored on PayPal's side which can be different
 * or out of syn with our own plans.
 *
 */
class ListPaypalPlans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paypal:plans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show definitions for all PayPal API plans';
    protected $apiContext;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->apiContext = PaypalBootstrap::initialize();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Retrieving all PayPal plan definitions from API...');

        // get all existing plans
        try {
            $params = ['status' => 'ACTIVE'];
            $planList = Plan::all($params, $this->apiContext);
        } catch (Exception $e) {
            $this->error('An error has occurred while retrieving plans: ' . print_r($e, true));
            exit;
        }

        $plans = $planList->getPlans();

        if (!empty($plans)) {
            // delete them all
            foreach ($plans as $plan) {
                $plan = Plan::get($plan->getId(), $this->apiContext);
                $prefs = $plan->getMerchantPreferences();

                $this->line('Found "' . $plan->getId() . '"');
                $this->line('Return URL: ' . $prefs->getReturnUrl());
            }
        } else {
            $this->error('No plans found.');
        }

        $this->info('Done.');
    }
}
