<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RankTracking;
use App\RankTrackingEngine;
use App\RankTrackingDomain;
use App\Gainrank\RankTrackingAPI;
use Carbon\Carbon;

class RankTrackingUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ranktracking:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Request updates on rank tracking keywords that have not been updated within interval.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rt_api = RankTrackingAPI::initialize();

        // format date
        $today = Carbon::now()->toDateString();
        $yesterday = Carbon::now()->subDay(10)->toDateString();

        $this->line('Getting all engines...');

        // gather all engines
        $engines = RankTrackingEngine::orderBy('id', 'asc')->get();

        foreach ($engines as $engine) {
            $this->info('Now using engine ' . $engine->url);
            $this->line('Getting all domains...');

            // gather all domains
            $domains = RankTrackingDomain::orderBy('id', 'asc')->get();

            foreach ($domains as $domain) {
                $this->info('Now using domain ' . $domain->url);
                $this->line('Getting keywords for this domain using the current engine that need updating (limit 100)...');

                // gather all non-updated rank tracking records for this domain using this engine
                $tracks = RankTracking::where('engine_id', $engine->id)
               //  where('updated_at', '<', $yesterday)
                    ->where('domain_id', $domain->id)
                    ->orderBy('id', 'asc')
                    ->limit(100)
                    ->get();
                
                // if tracks are found, update as necessary
                if (count($tracks) > 0) {
                    $this->info(count($tracks) . ' keywords found!');
                    $this->line("Updating found keywords...");

                    $keywords = [];

                    foreach ($tracks as $track) {
                        $keywords[$track->keyword] = $track->id;
                    }

                    // pass to API
                    $rt_api->update($domain->url, $keywords, $engine->url, $yesterday, $today);
                } else {
                    $this->info('No keywords found.');
                }
            }
        }

        $this->info('Done.');  
    }
}
