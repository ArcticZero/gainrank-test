<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AlexaSubmission;
use App\Gainrank\AlexaAPI;

class AlexaUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alexa:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = AlexaAPI::initialize();

        $need_update = AlexaSubmission::orderBy('updated_at', 'desc')->limit(100)->get();

        foreach ($need_update as $to_update) {
            $api->update($to_update->submission_id, $to_update->id);
        }
    }
}
