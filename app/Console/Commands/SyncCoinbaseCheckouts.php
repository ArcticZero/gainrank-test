<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\CoinbaseBootstrap;
use App\CoinbaseCheckout;

use Coinbase\Wallet\Resource\Checkout;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Exception\AuthenticationException;
use Coinbase\Wallet\Exception\InvalidTokenException;

use Laravel\Spark\Spark;

class SyncCoinbaseCheckouts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinbase:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Coinbase checkout URLs with Spark plan definitions';
    protected $client;
    protected $currency;
    protected $whitelist;
    protected $error;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = CoinbaseBootstrap::initialize();
        $this->currency = config('billing.currency');
        $this->whitelist = [];
    }

    protected function createCoinbaseCheckout($spark_plan)
    {
        $this->line('Creating Checkout on Coinbase...');

        // build checkout object
        $return_url = url('settings/subscription/checkout/coinbase');

        $params = array(
            'name'          => 'Gainrank Subscription',
            'amount'        => new Money($spark_plan->price, $this->currency),
            'description'   => $spark_plan->name,
            'successUrl'    => $return_url . "/success",
            'cancelUrl'     => $return_url . "/failure"
        );

        try {
            $checkout = new Checkout($params);
            $this->client->createCheckout($checkout);
        } catch (AuthenticationException $e) {
            $this->error("Coinbase Authentication Exception occurred: " . $e);
            exit;
        } catch (InvalidTokenException $e) {
            $this->error("Coinbase Token Exception occurred: " . $e);
            exit;
        } catch (Exception $e) {
            $this->error("Exception occurred: " . $e);
            exit;
        }

        $checkout_id = $checkout->getId();

        $this->whitelist[] = $checkout_id;

        $this->info('Created Coinbase checkout ' . $checkout_id);

        return $checkout;
    }

    protected function updatePlanPairing($spark_plan_id, $checkout_id, $checkout_embed_code)
    {
        $checkout = CoinbaseCheckout::where('plan_id', $spark_plan_id)
            ->whereNull('ad_campaign_id')
            ->first();

        if (empty($checkout)) {
            // create new checkout pairing
            $checkout = new CoinbaseCheckout();
            $checkout->plan_id = $spark_plan_id;
            $checkout->ad_campaign_id = null;
        }

        $checkout->checkout_id = $checkout_id;
        $checkout->checkout_embed_code = $checkout_embed_code;
        $checkout->save();

        $this->info('Plan "' . $spark_plan_id . '" has been paired to Coinbase checkout "' . $checkout_id . '".');

        return true;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all defined plans in Spark
        $spark_plans = Spark::allPlans()->filter(function ($plan) {
            return $plan->price > 0;
        });

        $this->line('Retrieving all Spark plans...');

        foreach ($spark_plans as $spark_plan) {
            $this->line('Looking up "' . $spark_plan->id . '" pairing in DB...');

            // check if pairing exists
            $pair = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                ->where('ad_campaign_id', null)
                ->first();

            if (!empty($pair)) {
                $this->line('Pairing found. Checking if exists on Coinbase API...');

                $checkout = false;

                // check if we have a Coinbase checkout for this
                try {
                    $checkout = $this->client->getCheckout($pair->checkout_id);
                } catch (AuthenticationException $e) {
                    $this->error("Coinbase authentication exception occurred while looking up plan: " . $e);
                } catch (InvalidTokenException $e) {
                    $this->error("Coinbase token exception occurred while looking up plan: " . $e);
                } catch (Exception $e) {
                    $this->error("Exception occurred while looking up plan: " . $e);
                }

                if ($checkout) {
                    // this is an existing plan
                    $this->info('Existing Coinbase checkout found! Nothing to do here.');
                    $this->whitelist[] = $checkout->getEmbedCode();
                    continue;
                } else {
                    // this is a new plan, create it
                    $this->info("This is a new Coinbase checkout. Let's create it.");
                    $checkout = $this->createCoinbaseCheckout($spark_plan);
                    $this->updatePlanPairing($spark_plan->id, $checkout->getId(), $checkout->getEmbedCode());
                }
            } else {
                // no pair found, create
                $this->info("This is a new pairing. Let's create both a Coinbase checkout and local plan.");
                $checkout = $this->createCoinbaseCheckout($spark_plan);
                $this->line("Creating pairing for Spark Plan " . $spark_plan->id . ", Coinbase checkout " . $checkout->getId());
                $this->updatePlanPairing($spark_plan->id, $checkout->getId(), $checkout->getEmbedCode());
            }
        }

        // Note: Skipping deletion of obsolete checkouts since there is no way to do this programmatically as of now

        $this->info('Done.');
    }
}
