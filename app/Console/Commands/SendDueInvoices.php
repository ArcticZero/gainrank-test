<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Settings\Billing\SendsInvoiceNotifications;
use App\Models\CoinbaseBootstrap;
use App\Models\PaypalBootstrap;
use App\Repositories\LocalInvoiceRepository;
use App\Services\PaymentProcessorService;
use App\Services\SalesInvoiceHandler;
use App\InvoiceReminder;
use App\Invoice;
use App\CoinbaseCheckout;
use App\Reward;

use Illuminate\Support\Facades\Log;

use Laravel\Cashier\Cashier;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use PayPal\Api\Address;
use PayPal\Api\BillingInfo;
use PayPal\Api\Cost;
use PayPal\Api\Currency;
use PayPal\Api\Invoice as PayPalInvoice;
use PayPal\Api\InvoiceAddress;
use PayPal\Api\InvoiceItem;
use PayPal\Api\MerchantInfo;
use PayPal\Api\PaymentTerm;
use PayPal\Api\Phone;
use PayPal\Api\ShippingInfo;

use PayPal\Exception\PayPalConnectionException;

use \DateTime;

/**
 *
 * Send all due invoices for manual billing subscriptions. Once a user's subscription is
 * about to expire (configurable threshold), an invoice is sent in advance containing a
 * payment link where they can renew their subscription and avoid downtime.
 *
 */
class SendDueInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send all due invoices for manual billing subscriptions';
    protected $currency;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // set default currency
        $this->currency = config('billing.currency');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = config('subscriptions.invoice.days_before_expiry');
        $period = date("Y-m-d H:i:s", strtotime("+" . $days . " days"));
        $now = new DateTime();

        $this->line('Retrieving all subscriptions expiring in ' . $days . ' days or less...');

        // get all subs that will expire in set number of days
        $subscriptions = Subscription::whereIn('billing_provider', ['paypal_manual', 'coinbase'])
            ->where('ends_at', '<=', $period)
            ->where('is_cancelled', 0)
            ->get();

        $total_rows = count($subscriptions);

        if ($total_rows) {
            $this->line($total_rows . ' subscriptions found!');

            // initialize payment processor
            $processor = PaymentProcessorService::getPaymentProcessor('paypal_manual');
            $paypal_client = $processor->getClient();

            foreach ($subscriptions as $subscription) {
                $this->line('Found ' . $subscription->billing_provider . ' subscription for user ' . $subscription->user_id . '. Checking if reminder already sent...');

                // get invoice reminder for this subscription
                $reminder = InvoiceReminder::where('subscription_id', $subscription->id)
                    ->where('created_at', '<=', $period)
                    ->count();

                if (!$reminder) {
                    $this->line('No invoice found. Generating one now...');

                    $end_date = new DateTime($subscription->ends_at);
                    $days_left = $now->diff($end_date)->days;

                    // get spark plan
                    $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

                    if (!$spark_plan) {
                        $this->error("Spark plan " . $subscription->provider_plan_name . " not found");
                        exit;
                    }

                    $period_start = date("F j, Y", strtotime($subscription->ends_at));

                    // get recurring payment price
                    $recurring_price = $spark_plan->price;

                    // if user is linked to an ad campaign, apply corresponding discounts to recurring payments
                    if (!empty($subscription->user->campaign)) {
                        $recurring_price = $this->getDiscountedPrice($subscription->user->campaign, $recurring_price);
                    }      

                    // no invoice yet, let's create one and send
                    switch ($subscription->billing_provider) {
                        case 'paypal_manual':
                            // build data for local invoice
                            $invoice_items = [
                                [
                                    'description' => $spark_plan->name,
                                    'amount' => $spark_plan->price,
                                    'currency' => config('billing.currency'),
                                    'quantity' => 1
                                ]
                            ];

                            // create local invoice
                            $invoice_handler = new SalesInvoiceHandler();
                            $local_invoice = $invoice_handler->generate($subscription->user, $invoice_items);

                            // create paypal invoice
                            $invoice = new PayPalInvoice();
                            $invoice->setMerchantInfo(new MerchantInfo())
                                ->setBillingInfo(array(new BillingInfo()))
                                ->setNote("Invoice for active subscription period starting on " . $period_start . ".")
                                ->setReference($local_invoice->id);

                            $invoice->getMerchantInfo()
                                ->setEmail(config('services.paypal.email'))
                                ->setbusinessName("Gainrank");

                            $billing = $invoice->getBillingInfo();
                            $billing[0]->setEmail($subscription->owner->email);

                            $items = [];
                            $items[0] = new InvoiceItem();
                            $items[0]->setName("Gainrank Subscription")
                                ->setQuantity(1)
                                ->setUnitPrice(new Currency());

                            $items[0]->getUnitPrice()
                                ->setCurrency($this->currency)
                                ->setValue($recurring_price);

                            #$invoice->setLogoUrl("LOGO URL HERE");
                            $invoice->setItems($items);

                            // create the invoice
                            try {
                                $invoice->create($paypal_client);
                                $this->info("Invoice has been created for this subscription");
                            } catch (PayPalConnectionException $e) {
                                $this->error("PayPal exception occurred while creating invoice: (" . $e->getCode() . ") " . $e->getData());
                                exit;
                            } catch (Exception $e) {
                                $this->error("Exception occurred while creating invoice: " . $e);
                                exit;
                            }

                            // send the invoice
                            try {
                                $status = $invoice->send($paypal_client);
                                $invoice = PayPalInvoice::get($invoice->getId(), $paypal_client);

                                $url = $invoice->getMetaData()->getPayerViewUrl();
                                $this->info("Invoice has been sent with the URL: " . $url);
                            } catch (PayPalConnectionException $e) {
                                $this->error("PayPal exception occurred while sending invoice: (" . $e->getCode() . ") " . $e->getData());
                                exit;
                            } catch (Exception $e) {
                                $this->error("Exception occurred while sending invoice: " . $e);
                                exit;
                            }

                            // send local invoice
                            $data = [
                                'days' => $days_left,
                                'company' => Spark::product(),
                                'pay_url' -> $url
                            ];

                            $invoice_handler->sendInvoice($data);

                            break;

                        case 'coinbase':
                            // if user is linked to an ad campaign, check for corresponding recurring checkout
                            if (!empty($subscription->user->campaign)) {
                                $cc = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                                    ->where('ad_campaign_id', $subscription->user->campaign->id)
                                    ->where('discount_type', CoinbaseCheckout::DISCOUNT_TYPE_RECURRING)
                                    ->first();
                            }

                            // use default checkout definition
                            if (!$cc) {
                                $cc = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                                    ->whereNull('ad_campaign_id')
                                    ->first();
                            }

                            if (!$cc) {
                                $this->error("Coinbase checkout pairing not found for plan " . $spark_plan->id . ".");
                                exit;
                            }

                            // build data for local invoice
                            $invoice_items = [
                                [
                                    'description' => $spark_plan->name,
                                    'amount' => $spark_plan->price,
                                    'currency' => config('billing.currency'),
                                    'quantity' => 1
                                ]
                            ];

                            // create local invoice
                            $invoice_handler = new SalesInvoiceHandler();
                            $local_invoice = $invoice_handler->generate($subscription->user, $invoice_items);

                            // get approval URL
                            $code = $cc->checkout_embed_code;
                            $url = str_replace('{CODE}', $code, config('services.coinbase.url')) . '?c=' . $local_invoice->id;

                            // send invoice
                            $data = [
                                'days' => $days_left,
                                'company' => Spark::product(),
                                'pay_url' -> $url
                            ];

                            $invoice_handler->sendInvoice($data);

                            break;
                    }

                    // create record of this reminder
                    $new_reminder = new InvoiceReminder();
                    $new_reminder->subscription_id = $subscription->id;
                    $new_reminder->provider_id = $subscription->billing_provider;
                    $new_reminder->save();
                } else {
                    $this->line('Invoice reminder already sent. Skipping...');
                }
            }
        } else {
            $this->line('No matching subscriptions found.');
        }

        $this->info('Done.');
    }

    /**
     * Compute for discounted price
     *
     * @return mixed
     */
    public function getDiscountedPrice($campaign, $price)
    {
        $discounts = [
            'amount' => 0,
            'percent' => 0
        ];

        $bonuses = $campaign->bonuses()
            ->whereHas('type', function ($q) {
                    $q->whereIn('reward_type', [
                        Reward::TYPE_DISCOUNT_RECURRING_AMOUNT,
                        Reward::TYPE_DISCOUNT_RECURRING_PERCENT
                    ]);
                })
                ->get();

        if (!$bonuses->isEmpty()) {
            foreach ($bonuses as $bonus) {
                if ($bonus->bonus_amount) {
                    switch ($bonus->type->reward_type) {
                        case Reward::TYPE_DISCOUNT_RECURRING_AMOUNT:
                            $discounts['amount'] = $bonus->bonus_amount;
                            break;
                        case Reward::TYPE_DISCOUNT_RECURRING_PERCENT:
                            $discounts['percent'] = bcdiv($bonus->bonus_amount, 100);
                            break;
                    }
                }
            }
        }

        // apply flat amount discount to initial payment
        if ($discounts['amount']) {
            $price = bcsub($price, $discounts['amount']);
        }

        // apply percentage discount to initial payment
        if ($discounts['percent']) {
            $price = bcmul($price, bcsub(1, $discounts['percent']));
        }

        return $price;
    }
}
