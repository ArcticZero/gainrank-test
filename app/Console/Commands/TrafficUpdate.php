<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TrafficSubmission;
use App\Gainrank\TrafficAPI;

class TrafficUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'traffic:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = TrafficAPI::initialize();

        $need_update = TrafficSubmission::orderBy('updated_at', 'desc')->limit(100)->get();

        foreach ($need_update as $to_update) {
            $api->update($to_update->submission_id, $to_update->id);
        }
    }
}
