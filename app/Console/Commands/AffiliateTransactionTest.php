<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AffiliateTransaction;
use App\User;
use App\Affiliate;
use App\Reward;
// use RewardHandler;
// use AffiliateHandler;
use App\Contracts\AffiliateContract;
use App\Contracts\RewardContract;

class AffiliateTransactionTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'affiliate:build-test-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test user, make him an affiliate and insert test transactions.';

    // affiliate handler
    protected $aff_handler;

    // reward handler
    protected $reward_handler;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AffiliateContract $aff_handler, RewardContract $reward_handler)
    {
        parent::__construct();

        // inject affiliate and reward handlers in
        $this->aff_handler = $aff_handler;
        $this->reward_handler = $reward_handler;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Create user and make affiliate
        $aff = $this->createTestAffiliate();
        $user = $this->createTestUser($aff);

        // make fake transactions for the test affiliate
        $reward = $this->reward_handler->generateAffiliateReward(AffiliateTransaction::TYPE_SUBSCRIBE);

        // randomize the payout, so it's going to be different for every entry
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
        $this->aff_handler->addTransaction($user, AffiliateTransaction::TYPE_SUBSCRIBE, new Reward($reward->getType(), ['payout_amount' => rand(5, 20)]));
    }

    protected function createTestAffiliate()
    {
        $affiliate = new Affiliate();
        $affiliate->ref_id = uniqid();
        $affiliate->save();

        return $affiliate;
    }

    protected function createTestUser($aff)
    {
        $user = new User();
        $user->name = 'test';
        $user->email = 'aff_' . $aff->id . '@test.com';
        $user->password = 'password';
        $user->save();

        $user->affiliate()->save($aff);

        return $user;
    }
}
