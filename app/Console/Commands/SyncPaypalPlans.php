<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\PaypalBootstrap;
use App\PaypalPlan;

use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Patch;
use PayPal\Common\PayPalModel;
use PayPal\Api\PatchRequest;
use PayPal\Api\Currency;
use PayPal\Exception\PayPalConnectionException;

use Laravel\Spark\Spark;

/**
 *
 * Sync Spark plan definitions with PayPal recurring payment plans. This only needs
 * to be run once whenever changes are done on the SparkServiceProvider subscription
 * plan definitions.
 *
 */
class SyncPaypalPlans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paypal:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync PayPal recurring payment plans with Spark plan definitions';
    protected $apiContext;
    protected $currency;
    protected $whitelist;
    protected $error;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->apiContext = PaypalBootstrap::initialize();
        $this->currency = config('billing.currency');
        $this->whitelist = [];
    }

    protected function createPaypalPlan($spark_plan)
    {
        $this->line('Creating plan on PayPal...');

        switch ($spark_plan->interval) {
            case 'monthly':
                $billing_period = 'Month';
                break;
            case 'yearly':
                $billing_period = 'Year';
                break;
        }

        $return_url = url('settings/subscription/checkout/paypal_recurring');

        $this->line('Return URL base is ' . $return_url);

        // add trial definition if trial days is set
        if ($spark_plan->trialDays > 0) {
            $this->line('Plan includes a trial of ' . $spark_plan->trialDays . ' days');

            $trial_definition = new PaymentDefinition();
            $trial_definition->setName('Trial Subscription')
                ->setType('TRIAL')
                ->setFrequency('Day')
                ->setFrequencyInterval($spark_plan->trialDays)
                ->setCycles("1")
                ->setAmount(new Currency(
                    array(
                        'currency' => $this->currency,
                        'value' => 0
                    )
                ));
        }

        // set payment definition
        $definition = new PaymentDefinition();
        $definition->setName('Subscription Payments')
            ->setType('REGULAR')
            ->setFrequency($billing_period)
            ->setFrequencyInterval("1")
            ->setCycles("0")
            ->setAmount(new Currency(
                array(
                    'currency' => $this->currency,
                    'value' => $spark_plan->price
                )
            ));

        // set merchant preferences
        $preferences = new MerchantPreferences();
        $preferences->setReturnUrl($return_url . "/success")
            ->setCancelUrl($return_url . "/failure")
            ->setAutoBillAmount("YES")
            ->setInitialFailAmountAction("CANCEL")
            ->setMaxFailAttempts("3");
            /*
            ->setSetupFee(new Currency(
                array(
                    'currency' => $this->currency,
                    'value' => $spark_plan->price
                )
            ));
            */

        // set up payment plan
        $plan = new Plan();
        $plan->setName(ucfirst($spark_plan->interval) . ' Plan')
            ->setDescription('Regular Subscription')
            ->setType('INFINITE')
            ->setPaymentDefinitions(array($definition))
            ->setMerchantPreferences($preferences);

        // create plan
        try {
            $output = $plan->create($this->apiContext);
        } catch (PayPalConnectionException $e) {
            $this->error("PayPal exception occurred while creating plan: (" . $e->getCode() . ") " . $e->getData());
            exit;
        } catch (Exception $e) {
            $this->error('Exception occurred while creating plan: ' . $e);
            exit;
        }

        // activate plan
        try {
            $this->line('Activating plan...');

            $patch = new Patch();

            $pvalue = new PayPalModel('{
                "state": "ACTIVE"
            }');

            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($pvalue);

            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);

            $plan->update($patchRequest, $this->apiContext);
        } catch (PayPalConnectionException $e) {
            $this->error("PayPal exception occurred while activating plan: (" . $e->getCode() . ") " . $e->getData());
            exit;
        } catch (Exception $e) {
            $this->error("Exception occurred while activating plan: " . $e);
            exit;
        }

        $plan_id = $plan->getId();

        $this->whitelist[] = $plan_id;

        $this->info('Created PayPal plan ' . $plan_id);

        return $plan;
    }

    protected function updatePlanPairing($spark_plan_id, $paypal_plan_id)
    {
        $pp_plan = PaypalPlan::where('plan_id', $spark_plan_id)
            ->whereNull('ad_campaign_id')
            ->first();

        if (empty($pp_plan)) {
            // create new plan pairing
            $pp_plan = new PaypalPlan();
            $pp_plan->plan_id = $spark_plan_id;
            $pp_plan->ad_campaign_id = null;
        }

        $pp_plan->paypal_plan_id = $paypal_plan_id;
        $pp_plan->save();

        $this->info('Plan "' . $spark_plan_id . '" has been paired to PayPal plan "' . $paypal_plan_id . '".');

        return true;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all defined plans in Spark
        $spark_plans = Spark::allPlans()->filter(function ($plan) {
            return $plan->price > 0;
        });

        $this->line('Retrieving all Spark plans...');

        foreach ($spark_plans as $spark_plan) {
            $this->line('Looking up "' . $spark_plan->id . '" pairing in DB...');

            // check if pairing exists
            $pair = PaypalPlan::where('plan_id', $spark_plan->id)
                ->where('ad_campaign_id', null)
                ->first();

            if (!empty($pair)) {
                $this->line('Pairing found. Checking if exists on PayPal API...');

                $pp_plan = false;

                // check if we have a PayPal plan for this
                try {
                    $pp_plan = Plan::get($pair->paypal_plan_id, $this->apiContext);
                } catch (PayPalConnectionException $e) {
                    $this->error("PayPal exception occurred while looking up plan: (" . $e->getCode() . ") " . $e->getData());
                } catch (Exception $e) {
                    $this->error("Exception occurred while looking up plan: " . $e);
                }

                if ($pp_plan) {
                    // this is an existing plan
                    $this->info('Existing PayPal plan found! Nothing to do here.');
                    $this->whitelist[] = $pp_plan->getId();
                    continue;
                } else {
                    // this is a new plan, create it
                    $this->info("This is a new PayPal plan. Let's create it.");
                    $pp_plan = $this->createPaypalPlan($spark_plan);
                    $this->updatePlanPairing($spark_plan->id, $pp_plan->getId());
                }
            } else {
                // no pair found, create
                $this->info("This is a new pairing. Let's create both a PayPal and local plan.");
                $pp_plan = $this->createPaypalPlan($spark_plan);
                $this->line("Creating pairing for Spark Plan " . $spark_plan->id . ", PayPal plan " . $pp_plan->getId());
                $this->updatePlanPairing($spark_plan->id, $pp_plan->getId());
            }
        }

        $this->line('Cleaning up all obsolete plan records...');

        // delete any obsolete plans
        try {
            $params = ['status' => 'ACTIVE'];
            $planList = Plan::all($params, $this->apiContext);
        } catch (PayPalConnectionException $e) {
            $this->error('A PayPal error has occurred while retrieving plans: ' . print_r($e, true));
            exit;
        } catch (Exception $e) {
            $this->error('An error has occurred while retrieving plans: ' . print_r($e, true));
            exit;
        }

        $plans = $planList->getPlans();

        if (!empty($plans)) {
            foreach ($plans as $plan) {
                $planId = $plan->getId();
                $plan = Plan::get($planId, $this->apiContext);

                if (!in_array($planId, $this->whitelist)) {
                    $plan_row = PaypalPlan::where('paypal_plan_id', $planId)->first();

                    if (!empty($plan_row) && empty($plan_row->ad_campaign_id)) {
                        // delete if not in whitelist and not part of ad campaign
                        $this->line('Deleting unused plan ' . $planId);
                        $plan->delete($this->apiContext);

                        // delete from database
                        $plan_row->delete();
                    }
                }
            }
        }

        $this->info('Done.');
    }
}
