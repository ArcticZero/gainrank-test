<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RankTracking;
use App\RankTrackingDomain;
use App\RankTrackingEngine;
use App\RankTrackingRank;

class SeedRankTrackingRanks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ranktracking:seed {domain} {keyword} {--user=1} {--engine=1} {--days=60} {--interval=300} {--api_version=1} {--min=1} {--max=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sample data for ranktracking ranks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get command parameters
        $url = $this->argument('domain');
        $keyword = $this->argument('keyword');
        $user_id = $this->option('user');
        $engine_id = $this->option('engine');

        // first check if engine exists
        $this->info("Checking if rank tracking engine exists...");

        $engine = RankTrackingEngine::find($engine_id);

        // engine not found
        if (empty($engine)) {
            $this->error("Engine does not exist!");
            exit;
        }

        // delete all existing rank tracking data for this domain/keyword combo
        $this->info("Deleting all existing tracking info for the keyword '" . $keyword . "' on the domain '" . $url . "'...");
        $domain = RankTrackingDomain::where('user_id', $user_id)
            ->where('url', $url)
            ->first();

        if (!empty($domain)) {
            $tracks = $domain->tracks()
                ->where('keyword', $keyword)
                ->delete();
        }

        // generate new domain data
        if (empty($domain)) {
            $this->info("Adding domain '" . $url . "' to tracking list...");

            $domain = new RankTrackingDomain;
            $domain->url = $url;
            $domain->user_id = $user_id;
            $domain->save();
        }
        
        // generate new keyword data
        $this->info("Adding keyword '" . $keyword . "' to tracking list...");

        $track = new RankTracking;
        $track->engine_id = $engine_id;
        $track->user_id = $user_id;
        $track->submission_id = time(); // just a unique value
        $track->country = "US";
        $track->version = $this->option('api_version');
        $track->keyword = $keyword;
        $track->id_status = 1;

        $domain->tracks()->save($track);

        // generate rank data for the past X days, at specified intervals
        $this->info('Generating random rank data for the past ' . $this->option('days') . ' days, at ' . $this->option('interval') . ' second intervals...');

        $end_date = time();
        $start_date = strtotime('-' . $this->option('days') . ' days');
        $ctr = 0;

        for ($i = $start_date; $i < $end_date; $i += $this->option('interval')) {
            $rank = new RankTrackingRank;
            $rank->rank = rand($this->option('min'), $this->option('max'));
            $rank->created_at = date("Y-m-d H:i:s", $i);

            $track->ranks()->save($rank);
            $ctr++;
        }

        // finished
        $this->info('Done! Generated ' . $ctr . ' rows.');
    }
}
