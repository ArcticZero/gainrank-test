<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\UserPendingSubscription;
use App\User;
use App\Repositories\LocalInvoiceRepository;

use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use \DateTime;

class ActivateLatestPendingSub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:test:activate {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate the latest pending subscription (by type)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        $this->line('Finding the latest pending subscription (' . $type . ') ...');

        // get latest pending sub
        $user_plan = UserPendingSubscription::where('billing_provider', $type)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($user_plan) {
            $this->info('Found pending subscription, looking for user and Spark plan details...');

            $now = new DateTime();

            // find user
            $user = User::find($user_plan->user_id);

            // get spark plan
            $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

            // compute for trial and sub end date depending on billing provider
            switch ($user_plan->billing_provider) {
                case 'paypal_recurring':
                    if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                        $date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                        $trial_end_time = clone $now;
                        $trial_end_time->modify($date_mod);
                    }

                    break;

                case 'paypal_manual':
                    if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                        $trial_date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                        $trial_end_time = clone $now;
                        $trial_end_time->modify($trial_date_mod);
                        $end_time = clone $trial_end_time;
                    } else {
                        $end_time = clone $now;
                    }

                    break;

                case 'coinbase':
                    if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                        // TODO: how to do trial on coinbase? future payments not supported
                        $trial_end_time = clone $now;
                        $end_time = clone $now;
                    } else {
                        $end_time = clone $now;
                    }

                    break;
            }

            $this->info('Activating the subscription...');

            // update subscription
            $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
            $subscription->name = 'default';
            $subscription->quantity = '1';
            $subscription->trial_ends_at = isset($trial_end_time) ? $trial_end_time->format('Y-m-d H:i:s') : null;
            $subscription->provider_plan_name = $spark_plan->id;
            $subscription->billing_provider = $user_plan->billing_provider;
            $subscription->last_payment_at = $now->format('Y-m-d H:i:s');

            if ($user_plan->billing_provider != 'coinbase') {
                $subscription->paypal_agreement_id = $user_plan->agreement_id;
            }

            // since Spark expects stripe by default if not using braintree, we fill in the stripe_plan field with the plan ID
            $subscription->stripe_plan = $spark_plan->id;

            // if non recurring, add end time
            if (isset($end_time)) {
                $subscription->ends_at = $end_time->format('Y-m-d H:i:s');
            }
            
            // if eligible for trial, add trial end time
            if (isset($trial_end_time)) {
                $trial_end_time_string = $trial_end_time->format('Y-m-d H:i:s');
                $subscription->trial_ends_at = $trial_end_time_string;
            }

            // save subscription
            $subscription->save();

            $this->info('Adding invoice row for metrics...');

            // add a row to invoice table for metrics
            $input = [
                'amount'    => $spark_plan->price,
                'tax'       => 0,
                'txn_id'    => $user_plan->agreement_id
            ];

            $local_invoice = app(LocalInvoiceRepository::class)->createForUser($subscription->user, $input);

            $this->info('Removing pending subscription...');

            // delete pending subscription as it is no longer necessary
            $user_plan->delete();

            // mark user as having used trial
            $user->has_used_trial = 1;

            // ignore generic trial
            $user->trial_ends_at = null;

            $user->save();
        } else {
            $this->info('No pending sub found for this type.');
        }

        $this->info('Done.');
    }
}
