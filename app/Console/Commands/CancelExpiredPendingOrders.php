<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\PendingOrder;
use PendingOrderHandler;

use Illuminate\Support\Facades\Mail;

use Laravel\Spark\Spark;

use Carbon\Carbon;

class CancelExpiredPendingOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:cancel-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel all unverified pending orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();

        $this->line('Retrieving all expired pending orders...');

        // get all orders that have passed the expiry period
        $orders = PendingOrder::where('confirmation_key_expires_at', '>=', $now->format('Y-m-d H:i:s'))
            ->where('status', PendingOrder::ORDER_STATUS['pending'])
            ->get();

        $total_rows = count($orders);

        if ($total_rows) {
            $this->line($total_rows . ' orders found!');

            foreach ($orders as $order) {
                $this->line('Found order ID ' . $order->id . '. Cancelling...');

                // cancel this order and process refund
                $result = PendingOrderHandler::deny($order);

                if ($result) {
                    $this->line('Sending confirmation e-mail...');

                    // build and send notification email
                    $user = $order->user;
                    $expiry_days = config('orders.pending_via_email.days_before_expiry');
                    $invoice_data = Spark::invoiceDataFor($user);

                    $data = [
                        'user'          => $user,
                        'product'       => $invoice_data['product'],
                        'expiry_days'   => $expiry_days
                    ];

                    Mail::send('emails.orders.verification', $data, function ($m) use ($order) {
                        $m->to($order->paypal_payer_email)
                          ->subject('Order has been cancelled');
                    });
                }
            }
        } else {
            $this->line('No matching pending orders found.');
        }

        $this->info('Done.');
    }
}
