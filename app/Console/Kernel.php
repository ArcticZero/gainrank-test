<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SyncPaypalPlans::class,
        Commands\ListPaypalPlans::class,
        Commands\CapturePaypalManualPayments::class,
        Commands\SendDueInvoices::class,
        Commands\ActivateLatestPendingSub::class,
        Commands\SyncCoinbaseCheckouts::class,
        Commands\UserMakeAdmin::class,
        Commands\CancelExpiredPendingOrders::class,
        Commands\RegisterHelpdeskUsers::class,
        Commands\DropTables::class,
        Commands\ProcessMatureAffiliateTransactions::class,
        Commands\AffiliateTransactionTest::class,
        Commands\RankTrackingUpdate::class,
        Commands\AlexaUpdate::class,
        Commands\CTRUpdate::class,
        Commands\TrafficUpdate::class,
        Commands\WaybackUpdate::class,
        Commands\SeedRankTrackingRanks::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // gainrank services
        /*
        $schedule->command('ranktracking:update')
            ->withoutOverlapping()
            ->appendOutputTo('/tmp/rank_tracking.log');
        */

        $schedule->command('alexa:update')
            ->withoutOverlapping()
            ->appendOutputTo('/tmp/alexa.log');

        $schedule->command('ctr:update')
            ->withoutOverlapping()
            ->appendOutputTo('/tmp/ctr.log');

        $schedule->command('traffic:update')
            ->withoutOverlapping()
            ->appendOutputTo('/tmp/traffic.log');

        $schedule->command('wayback:update')
            ->withoutOverlapping()
            ->appendOutputTo('/tmp/wayback.log');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
