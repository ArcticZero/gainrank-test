<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaybackStatus extends Model
{
    public $timestamps = false;
    
    /**
     * Return submissions associated with the status
     *
     * @return mixed
     */
    public function submissions()
    {
        return $this->hasMany('App\WaybackSubmission', 'id_status');
    }
}
