<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingFavorite extends Model
{
    public $timestamps = false;

    /**
     * Return the customer/user for the favorite
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return the track for the favorite
     *
     * @return mixed
     */
    public function track()
    {
        return $this->belongsTo('App\RankTracking', 'track_id');
    }
}
