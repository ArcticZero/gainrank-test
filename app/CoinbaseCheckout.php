<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinbaseCheckout extends Model
{
    /*
     * discount types
     *
     * initial              - discounted checkout for first billing period
     * recurring            - discounted checkout for recurring payments
     *
     */
    const DISCOUNT_TYPE_INITIAL             = 1;
    const DISCOUNT_TYPE_RECURRING           = 2;

    public $timestamps = false;
    protected $fillable = ['plan_id'];

    /**
     * Return the AdCampaign that this plan is tied to
     * @return mixed
     */
    public function campaign() {
        return $this->belongsTo('App\AdCampaign', 'ad_campaign_id');
    }
}
