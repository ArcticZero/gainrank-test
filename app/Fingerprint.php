<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fingerprint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['hash'];

    /**
     * Returns the Users associated with this Fingerprint.
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'fingerprint_recordings')->withTimestamps()->using('App\FingerprintRecording');
    }
}
