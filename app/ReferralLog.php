<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferralLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id'];

    /**
     * Returns the Fingerprint associated with this ReferralLog.
     * @return mixed
     */
    public function fingerprint()
    {
        return $this->belongsTo('App\Fingerprint');
    }

    /**
     * Returns the Affiliate associated with this ReferralLog.
     * @return mixed
     */
    public function affiliate()
    {
        return $this->belongsTo('App\Affiliate');
    }

    /**
     * Returns the User associated with this ReferralLog.
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'referred_user_id');
    }
}
