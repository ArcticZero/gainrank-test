<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingOrderAction extends Model
{
    const ORDER_ACTION = [
        'approve'    => 1,
        'deny'        => 2
    ];

    /**
     * Return the User associated with this action
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return the PendingOrder associated with this action
     * @return mixed
     */
    public function order()
    {
        return $this->belongsTo('App\PendingOrder', 'order_id');
    }
}
