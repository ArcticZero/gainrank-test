<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCampaignBonus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['bonus_amount'];

    /**
     * Returns the parent AdCampaignBonusType.
     * @return mixed
     */
    public function type()
    {
        return $this->belongsTo('App\AdCampaignBonusType', 'ad_campaign_bonus_type_id');
    }

    /**
     * Returns the parent AdCampaign.
     * @return mixed
     */
    public function campaign()
    {
        return $this->belongsTo('App\AdCampaign', 'ad_campaign_id');
    }
}
