<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTrackingRank extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'applied_at'
    ];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id'];

    /**
     * Return the track for the rank
     *
     * @return mixed
     */
    public function track()
    {
        return $this->belongsTo('App\RankTracking', 'track_id');
    }
}
