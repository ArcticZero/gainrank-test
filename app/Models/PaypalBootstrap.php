<?php

namespace App\Models;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class PaypalBootstrap
{
    public static function initialize()
    {
        $live = config('services.paypal.live');

        // initialize API
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                config('services.paypal.key'),
                config('services.paypal.secret')
            )
        );

        // are we live?
        if ($live) {
            $apiContext->setConfig(array(
                'mode' => 'live'
            ));
        }

        return $apiContext;
    }
}
