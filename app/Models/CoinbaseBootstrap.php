<?php

namespace App\Models;

use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Exception\AuthenticationException;

use Illuminate\Support\Facades\Log;

class CoinbaseBootstrap
{
    public static function initialize()
    {
        try {
            $configuration = Configuration::apiKey(config('services.coinbase.key'), config('services.coinbase.secret'));
            $client = Client::create($configuration);
        } catch (AuthenticationException $e) {
            Log::error("Coinbase authentication exception occurred: " . $e->getMessage());
            exit;
        } catch (Exception $e) {
            Log::error("Exception occurred: " . $e->getMessage());
            exit;
        }

        // Log::info("Successfully connected to Coinbase API!");
        return $client;
    }
}
