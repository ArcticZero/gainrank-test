<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrafficSubmission extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return status associated with the submission
     *
     * @return mixed
     */
    public function status()
    {
        return $this->belongsTo('App\TrafficStatus', 'id_status');
    }
}
