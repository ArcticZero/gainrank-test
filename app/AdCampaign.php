<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCampaign extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'start_date',
        'end_date',
        'max_uses',
        'path',
        'active',
        'amount',
    ];

    /**
     * Returns the AdCampaignBonus's that this AdCampaign has.
     * @return mixed
     */
    public function bonuses()
    {
        return $this->hasMany('App\AdCampaignBonus');
    }

    /**
     * Returns the Users that signed up via this AdCampaign.
     * @return mixed
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Returns discounted PaypalPlans associated with this AdCampaign.
     * @return mixed
     */
    public function paypalPlans()
    {
        return $this->hasMany('App\PaypalPlan');
    }

    /**
     * Returns discounted CoinbaseCheckouts associated with this AdCampaign.
     * @return mixed
     */
    public function coinbaseCheckouts()
    {
        return $this->hasMany('App\CoinbaseCheckout');
    }
}
