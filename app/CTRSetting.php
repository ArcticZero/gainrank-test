<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CTRSetting extends Model
{
    protected $table = 'ctr_settings';
}
