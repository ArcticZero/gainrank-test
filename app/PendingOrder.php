<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingOrder extends Model
{
    const ORDER_STATUS = [
        'pending' => 1,
        'approved' => 2,
        'denied' => 3
    ];

    const ORDER_TYPES = [
        'subscription' => 1
    ];

    /**
     * Return this PendingOrder's Actions
     * @return mixed
     */
    public function actions()
    {
        return $this->hasMany('App\PendingOrderAction');
    }

    /**
     * Return the User associated with this pending order
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
