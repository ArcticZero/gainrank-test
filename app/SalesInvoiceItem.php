<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesInvoiceItem extends Model
{
    /**
     * Disable the usual timestamps
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'amount',
        'currency',
        'quantity'
    ];

    /**
     * Return the sales invoice this item belongs to
     *
     * @return App\SalesInvoice
     */
    public function invoice()
    {
        return $this->belongsTo('App\SalesInvoice');
    }
}
