<?php

namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserFingerprintHandler extends Facade
{
    /**
     * Show related users to a fingerprint.
     *
     * @return Array
     */
    protected function relatedUsers(string $email, $include_userdata = false)
    {
        $users_by_hash = [$email => 0];
        $users_by_ip = [$email => 0];
        $all_users = [];
        $hashes = [];
        $ips = [];
        $res = true;
        while (!empty($res)) {
            $res = DB::table('users')
                ->join('fingerprint_recordings', 'users.id', '=', 'user_id')
                ->join('fingerprints', 'fingerprint_id', '=', 'fingerprints.id')
                ->whereIn('users.email', array_keys($users_by_hash))
                ->whereNotIn('hash', array_keys($hashes))
                ->pluck('hash')
                ->flip()
                ->toArray();
            $hashes = $hashes + $res;
            $res = DB::table('fingerprints')
                ->join('fingerprint_recordings', 'fingerprints.id', '=', 'fingerprint_id')
                ->join('users', 'user_id', '=', 'users.id')
                ->whereIn('hash', array_keys($hashes))
                ->whereNotIn('users.email', array_keys($users_by_hash))
                ->select('users.email')
                ->pluck('email')
                ->flip()
                ->toArray();
            $users_by_hash = $users_by_hash + $res;
        }
        $res = true;
        while (!empty($res)) {
            $res = DB::table('users')
                ->join('fingerprint_recordings', 'users.id', '=', 'user_id')
                ->whereIn('users.email', array_keys($users_by_ip))
                ->whereNotIn('ip_address', array_keys($ips))
                ->pluck('ip_address')
                ->flip()
                ->toArray();
            $ips = $ips + $res;
            $res = DB::table('users')
                ->join('fingerprint_recordings', 'users.id', '=', 'user_id')
                ->whereIn('ip_address', array_keys($ips))
                ->whereNotIn('users.email', array_keys($users_by_ip))
                ->pluck('email')
                ->flip()
                ->toArray();
            $users_by_ip = $users_by_ip + $res;
        }
        foreach ($users_by_hash as $email => $unused) {
            $res = DB::table('users')
                ->join('fingerprint_recordings', 'users.id', '=', 'user_id')
                ->join('fingerprints', 'fingerprint_id', '=', 'fingerprints.id')
                ->where('users.email', $email)
                ->select('hash', 'fingerprint_recordings.created_at')
                ->get()
                ->toArray();
            $users_by_hash[$email] = [ 'hashes' => $res ];
            $all_users[] = $email;
        }
        foreach ($users_by_ip as $email => $unused) {
            $res = DB::table('users')
                ->join('fingerprint_recordings', 'users.id', '=', 'user_id')
                ->where('users.email', $email)
                ->select('ip_address', 'fingerprint_recordings.created_at')
                ->get()
                ->toArray();
            $users_by_ip[$email] = [ 'ip_addresses' => $res ];

            if (!in_array($email, $all_users)) {
                $all_users[] = $email;
            }
        }
        // TODO add transactions

        $response = [
            'users_by_hash' => $users_by_hash,
            'users_by_ip' => $users_by_ip,
        ];

        if ($include_userdata) {
            $response['users'] = $all_users;
        }

        return $response;
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'userfingerprinthandler';
    }
}
