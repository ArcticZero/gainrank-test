<?php

namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;

class GmailNormalizer extends Facade
{
    protected function parse(string $email)
    {
        $parts = explode("@", $email);
        $domains = $this->domains();

        if (in_array($parts[1], $domains)) {
            $plus_index = strpos($parts[0], '+');

            if ($plus_index !== false) {
                $parts[0] = substr($parts[0], 0, $plus_index);
            }

            $parts[0] = str_replace('.', '', $parts[0]);
            $cleaned = implode("@", $parts);
        } else {
            $cleaned = $email;
        }
        
        return strtolower($cleaned);
    }

    protected function domains()
    {
        $domains = [
            'gmail.com',
            'googlemail.com',
        ];

        return $domains;
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'gmailnormalizer';
    }
}
