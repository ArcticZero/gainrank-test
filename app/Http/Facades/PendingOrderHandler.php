<?php

namespace App\Http\Facades;

use App\PendingOrder;
use App\UserPendingSubscription;
use App\Invoice;
use App\User;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;

use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use App\Models\PaypalBootstrap;
use App\Repositories\LocalInvoiceRepository;
use App\Http\Controllers\Settings\Billing\SendsInvoiceNotifications;

use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;
use PayPal\Exception\PayPalConnectionException;

use Carbon\Carbon;

class PendingOrderHandler extends Facade
{
    use SendsInvoiceNotifications;

    /**
     * Approve a pending order
     *
     * @return boolean
     */
    protected function approve(PendingOrder $order)
    {
        if (!$order || !$order->pending_subscription_id) {
            return false;
        }

        // get pending sub
        $user_plan = UserPendingSubscription::find($order->pending_subscription_id);

        $payload = json_decode($order->request_details, true);
        $now = Carbon::now();

        // get user
        $user = User::find($user_plan->user_id);

        // get spark plan
        $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

        if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
            $date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
            $trial_end_time = clone $now;
            $trial_end_time->modify($date_mod);
        }

        // update subscription
        $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
        $subscription->name = 'default';
        $subscription->quantity = '1';
        $subscription->trial_ends_at = isset($trial_end_time) ? $trial_end_time->format('Y-m-d H:i:s') : null;
        $subscription->paypal_agreement_id = $order['paypal_agreement_id'];
        $subscription->paypal_payer_id = $order['paypal_payer_id'];
        $subscription->provider_plan_name = $spark_plan->id;
        $subscription->billing_provider = $order['billing_provider'];
        $subscription->last_payment_at = $now->format('Y-m-d H:i:s');
        
        // if eligible for trial, add trial end time
        if (isset($trial_end_time)) {
            $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
        }

        // save subscription
        $subscription->save();

        // delete pending subscription as it is no longer necessary
        $user_plan->delete();

        // mark user as having used trial
        $user->has_used_trial = 1;
        $user->paypal_payer_id = $order['paypal_payer_id'];
        $user->paypal_payer_email = $order['paypal_payer_email'];
        $user->save();

        // update order status
        $order->status = PendingOrder::ORDER_STATUS['approved'];
        $order->save();

        Log::error('THIS IS THE PAYLOAD:');
        Log::error($payload);

        // create invoice
        $local_invoice = app(LocalInvoiceRepository::class)->createForUser($subscription->user, $payload);
        $invoice = new Invoice($subscription->user, $local_invoice, $subscription);

        // send notification
        $this->sendInvoiceNotification($subscription->user, $invoice, $payload['txn_id']);

        return true;
    }

    /**
     * Deny a pending order
     *
     * @return boolean
     */
    protected function deny(PendingOrder $order)
    {
        if (!$order || !$order->pending_subscription_id) {
            return false;
        }
        
        // load paypal API
        $api_context = PaypalBootstrap::initialize();

        // get pending sub
        $user_plan = UserPendingSubscription::find($order->pending_subscription_id);

        $payload = json_decode($order->request_details, true);

        // get user
        $user = User::find($user_plan->user_id);

        // get spark plan
        $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

        // update order status
        $order->status = PendingOrder::ORDER_STATUS['denied'];
        $order->save();

        // ban the user
        $user->ban();

        // refund payment
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($spark_plan->price);

        $refund_request = new RefundRequest();
        $refund_request->setAmount($amount);

        $sale = new Sale();
        $sale->setId($payload['txn_id']);

        try {
            $refunded = $sale->refundSale($refund_request, $api_context);
        } catch (PayPalConnectionException $e) {
            Log::error("PayPal exception occurred while processing refund", [
                'txn_id'    => $payload['txn_id'],
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'data'      => $e->getData()
            ]);

            return false;
        } catch (Exception $e) {
            Log::error("Exception occurred while processing refund", [
                'txn_id'    => $payload['txn_id'],
                'message'   => $e->getMessage()
            ]);
            
            return false;
        }

        return true;
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pendingorderhandler';
    }
}
