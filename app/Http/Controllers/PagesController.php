<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    public function homepage()
    {
        return view('pages.homepage')->withPosts(Post::where('published', true)->orderBy('created_at', 'desc')->paginate(5));
    }

    public function features()
    {
        return view('pages.features');
    }
}
