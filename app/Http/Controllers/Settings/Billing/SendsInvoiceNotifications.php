<?php

namespace App\Http\Controllers\Settings\Billing;

use Laravel\Spark\Spark;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

trait SendsInvoiceNotifications
{
    /**
     * The invoice notification e-mail view.
     *
     * @var string
     */
    protected $receiptView = 'spark::settings.invoices.emails.invoice';
    protected $invoiceView = 'settings.invoices.emails.invoice';

    /**
     * Send an invoice notification e-mail.
     *
     * @param  mixed  $billable
     * @param  \App\Invoice
     * @return void
     */
    protected function sendInvoiceNotification($billable, $invoice, $transactionId, $pay_url = false)
    {
        $invoiceData = Spark::invoiceDataFor($billable);

        $data = compact('billable', 'invoice', 'invoiceData');

        // check if this is an invoice or receipt
        if ($pay_url) {
            $data['days'] = config('subscriptions.invoice.days_before_expiry');
            $data['pay_url'] = $pay_url;
            $view = $this->invoiceView;
        } else {
            $view = $this->receiptView;
        }

        Mail::send($view, $data, function ($message) use ($billable, $invoice, $invoiceData, $transactionId) {
            $this->buildInvoiceMessage($message, $billable, $invoice, $invoiceData, $transactionId);
        });
    }

    /**
     * Build the invoice notification message.
     *
     * @param  \Illuminate\Mail\Message  $message
     * @param  mixed  $billable
     * @param  \App\Invoice
     * @param  array  $invoiceData
     * @return void
     */
    protected function buildInvoiceMessage($message, $billable, $invoice, array $invoiceData, $transactionId)
    {
        $message->to($billable->email, $billable->name)
                ->subject($invoiceData['product'].' Invoice')
                ->attachData($invoice->pdf($invoiceData), 'invoice.pdf');
    }
}
