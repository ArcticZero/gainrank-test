<?php

namespace App\Http\Controllers\Settings;

use Laravel\Spark\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the settings dashboard.
     *
     * @return Response
     */
    public function show()
    {
        // get list of timezones
        $timezones = array();
        $timestamp = time();

        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $timezones[$key]['zone'] = $zone;
            $timezones[$key]['diff_from_gmt'] = 'UTC/GMT ' . date('P', $timestamp);
        }

        return view('spark::settings', ['timezones' => $timezones]);
    }
}
