<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileTimezoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the user's profile details.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $request->user()->forceFill([
            'timezone' => $request->timezone
        ])->save();
    }
}
