<?php

namespace App\Http\Controllers;

use App\FingerprintRecording;
use App\Fingerprint;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use UserFingerprintHandler;

class FingerprintRecordingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:view fingerprints', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request, $hash)
    {
        if ($hash == '*') {
            return Fingerprint::orderBy('created_at', 'desc')->paginate(5);
        } else {
            if (preg_match('/^[a-f0-9]{32}$/', $hash)) {
                return Fingerprint::where('hash', $hash)->first()->users()->get();
            }
        }
        abort(400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::guest()) {
            abort(403);
        } else {
            $this->validate($request, [
                'hash' => 'required|string|size:32',
            ]);

            $hash = $request->input('hash');
            $fingerprint = Fingerprint::firstOrCreate(['hash' => $hash]);
            $fingerprint->users()->attach($request->user()->id, ['ip_address' => $request->ip()]);
            $fingerprint->save();
            return response('Ok', 200);
        }
    }

    /**
     * Show related users to a fingerprint.
     *
     * @return \Illuminate\Http\Response
     */
    public function relatedUsers(Request $request, String $email)
    {
        return UserFingerprintHandler::relatedUsers($email);
    }
}
