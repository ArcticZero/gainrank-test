<?php

namespace App\Http\Controllers;

use App\PendingOrder;
use App\UserPaypalVerifiedEmail;
use PendingOrderHandler;

use Illuminate\Http\Request;

class PendingOrderConfirmationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Confirm a pending order via email
     *
     * @return Response
     */
    public function confirm($key)
    {
        // get order
        $order = PendingOrder::where('confirmation_key', $key)->first();

        // approve this order
        $approved = $order ? PendingOrderHandler::approve($order) : false;

        // save this confirmed email
        $verified = new UserPaypalVerifiedEmail();
        $verified->user()->associate($order->user);
        $verified->email = $order->paypal_payer_email;
        $verified->save();

        // load view
        return view($approved ? 'orders.pending.success' : 'orders.pending.failure');
    }
}
