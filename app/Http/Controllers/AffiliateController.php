<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\LengthAwarePaginator;

use App\FingerprintRecording;
use App\Fingerprint;
use App\ReferralLog;
use App\Affiliate;
use App\Reward;
use App\AffiliateTransaction;
use App\Contracts\AffiliateContract;

class AffiliateController extends Controller
{
    protected $per_page = 20;
    protected $aff_handler;

    public function __construct(AffiliateContract $aff_handler)
    {
        $this->aff_handler = $aff_handler;
        $this->middleware('auth');
    }

    public function view()
    {
        $user = Auth::user();
        $aff = $user->affiliate;

        // if user is an affiliate, show dashboard
        if ($user->isAffiliate()) {
            $total = $aff->transactions()->count();

            // gather the data for the status boxes on affiliate dashboard
            $data = [
                'payout_total' => $aff->payout_total,
                'payout_available' => $aff->payout_available,
                'payout_pending' => bcsub($aff->payout_total, $aff->payout_available),
                'total_referals' => $total,
            ];
            return view('affiliate.dashboard', $data);
        }

        // if not, show the affiliate landing page
        return view('affiliate.view');
    }

    public function apply(Request $req)
    {
        // user applies to become an affiliate

        // check if user has accepted terms of service
        $is_checked = $req->input('accept_terms', false);
        if (!$is_checked) {
            return back()->withInput();
        }

        // register as affiliate
        $user = $req->user();
        $user->is_affiliate = true;
        $user->save();

        // add affiliate row
        $affiliate = new Affiliate();

        // generate unique referral ID for affiliate
        do {
            $ref_id = uniqid();
        } while ($this->aff_handler->getByReferralId($ref_id) instanceof Affiliate);

        // save affiliate
        $affiliate->ref_id = $ref_id;
        $user->affiliate()->save($affiliate);

        return back();
    }

    public function trackReferral(Request $request)
    {
        if (Auth::guest()) {
            abort(403);
        } else {
            // Save fingerprint
            $this->validate($request, [
                'hash' => 'required|string|size:32',
            ]);

            $hash = $request->input('hash');
            $components = $request->input('components');
            $log_details = [];

            foreach ($components as $component) {
                $log_details[$component['key']] = $component['value'];
            }

            $fingerprint = Fingerprint::firstOrCreate(['hash' => $hash]);
            $fingerprint->users()->attach($request->user()->id);
            $fingerprint->save();

            // Get affiliate
            $affiliate = Affiliate::find($request->session()->get('affiliate_id'));

            // Build referral log
            $log = ReferralLog::firstOrCreate(['referred_user_id' => $request->user()->id]);
            $log->ip_address = $request->ip();
            $log->referer = $request->session()->get('referer_url');
            $log->user_agent = $log_details['user_agent'];
            $log->details = json_encode($log_details);

            // Set associations
            $log->affiliate()->associate($affiliate);
            $log->user()->associate($request->user());
            $log->fingerprint()->associate($fingerprint);

            // Save referral log
            $log->save();

            return response('Ok', 200);
        }
    }

    public function transactions(Request $request)
    {
        $page = abs(intval($request->get('page')));

        $items = [];

        // retrieve
        $user = $request->user();
        $transactions = AffiliateTransaction::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate($this->per_page);

        // get total
        $total = $user->affiliate_transactions()->count();

        // format results
        foreach ($transactions as $trans) {
            $reward = new Reward($trans->reward_type, json_decode($trans->reward_details, true));
            $items[] = [
                'transacted_at' => $trans->transacted_at->format('d M Y'),
                'mature_at' => $trans->mature_at->format('d M Y'),
                'maturity' => $trans->is_mature ? 'Mature' : 'Pending',
                'action_text' => AffiliateTransaction::getTypeText($trans->type_id),
                'reward' => $reward->getFullText(),
            ];
        }

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->per_page, $page);

        return $paginator;
    }
}
