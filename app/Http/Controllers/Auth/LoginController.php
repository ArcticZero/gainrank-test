<?php

namespace App\Http\Controllers\Auth;

use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use TwoFactorAuth;

use Laravel\Spark\Http\Controllers\Auth\LoginController as SparkLoginController;

class LoginController extends SparkLoginController
{
    /**
     * Override Spark login attempts.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {
        if ($request->has('remember')) {
            $request->session()->put('spark:auth-remember', $request->remember);
        }

        $user = Spark::user()->where('email', $request->email)->first();

        if ($user && $user->tfa_enable) {
            $request->merge(['remember' => '']);
        }

        return $this->traitLogin($request);
    }

    /**
     * Handle a successful authentication attempt.
     *
     * @param  Request  $request
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return Response
     */
    public function authenticated(Request $request, $user)
    {
        if ($user->tfa_enable) {
            return $this->redirectForTwoFactorAuth($request, $user);
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Redirect the user for two-factor authentication.
     *
     * @param  Request  $request
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return Response
     */
    protected function redirectForTwoFactorAuth(Request $request, $user)
    {
        Auth::logout();

        // Before we redirect the user to the two-factor token verification screen we will
        // store this user's ID and "remember me" choice in the session so that we will
        // be able to get it back out and log in the correct user after verification.
        $request->session()->put([
            'spark:auth:id' => $user->id,
            'spark:auth:remember' => $request->remember,
        ]);

        return redirect('login/token');
    }

    /**
     * Show the two-factor authentication token form.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showTokenForm(Request $request)
    {
        return $request->session()->has('spark:auth:id')
                        ? view('auth.token') : redirect('login');
    }

    /**
     * Verify the given authentication token.
     *
     * @param  Request  $request
     * @return Response
     */
    public function verifyToken(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        // If there is no authentication ID stored in the session, it means that the user
        // hasn't made it through the login screen so we'll just redirect them back to
        // the login view. They must have hit the route manually via a specific URL.
        if (! $request->session()->has('spark:auth:id')) {
            return redirect('login');
        }

        $user = Spark::user()->findOrFail(
            $request->session()->pull('spark:auth:id')
        );

        // initialize the TFA library
        $tfa = new TwoFactorAuth('Gainrank');

        // get token
        $token = str_replace(' ', '', $request->token);

        // get secret from session
        $secret = $user->tfa_secret;

        // verify it
        $result = $tfa->verifyCode($secret, $token);

        if ($result === true) {
            Auth::login($user, $request->session()->pull(
                'spark:auth:remember', false
            ));

            return redirect()->intended($this->redirectPath());
        } else {
            return back();
        }
    }

    /**
     * Show the form to login via backup code.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showBackupCodeForm(Request $request)
    {
        return $request->session()->has('spark:auth:id')
                        ? view('auth.login-via-backup-code')
                        : redirect('login');
    }

    /**
     * Login via backup code.
     *
     * @param  Request  $request
     * @return Response
     */
    public function loginByBackupCode(Request $request)
    {
        // get current time
        $now = Carbon::now();

        $this->validate($request, [
            'token' => 'required',
        ]);

        // If there is no authentication ID stored in the session, it means that the user
        // hasn't made it through the login screen so we'll just redirect them back to
        // the login view. They must have hit the route manually via a specific URL.
        if (! $request->session()->has('spark:auth:id')) {
            return redirect('login');
        }

        $user = Spark::user()->findOrFail(
            $request->session()->pull('spark:auth:id')
        );

        // get token
        $token = str_replace(' ', '', $request->token);

        $validated = false;

        // verify if backup code exists
        foreach($user->backupCodes as $code) {
            if (empty($code->used_at)) {
                $decrypted = decrypt($code->code);

                // compare with token
                if ($token == $decrypted) {
                    $validated = $code->id;
                    break;
                }
            }
        }

        if ($validated) {
            // mark code as used
            $used_code = $user->backupCodes()
                ->where('id', $validated)
                ->first();

            $used_code->used_at = $now;
            $used_code->save();

            // login user
            Auth::login($user, $request->session()->pull(
                'spark:auth:remember', false
            ));

            return redirect()->intended($this->redirectPath());
        } else {
            return redirect('login')->withErrors([
                'token' => 'The provided backup code was invalid.'
            ]);
        }
    }
}