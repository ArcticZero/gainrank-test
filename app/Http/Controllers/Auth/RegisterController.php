<?php

namespace App\Http\Controllers\Auth;

use Laravel\Spark\Spark;
use App\User;
use App\AdCampaign;
use App\SoftLaunchInvitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Spark\Events\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Log;
use Laravel\Spark\Contracts\Interactions\Auth\Register;
use Laravel\Spark\Contracts\Http\Requests\Auth\RegisterRequest;
use Laravel\Spark\Contracts\Repositories\NotificationRepository;
use Laravel\Spark\Http\Controllers\Auth\RegisterController as SparkRegisterController;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Jrean\UserVerification\Exceptions\UserNotFoundException;
use Jrean\UserVerification\Exceptions\UserIsVerifiedException;
use Jrean\UserVerification\Exceptions\TokenMismatchException;
use Carbon\Carbon;

class RegisterController extends SparkRegisterController
{
    use RedirectsUsers;
    use VerifiesUsers;

    /**
     * Number of seconds in between resending email verification messages.
     */
    const EMAIL_RESEND_SECS_LIMIT = 30;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(NotificationRepository $notifications)
    {
        $this->middleware('guest', ['except' => ['getVerification', 'resendVerificationForCurrentUser', 'getVerificationError']]);

        $this->middleware('auth')->only('resendVerificationForCurrentUser');

        $this->middleware('throttle:3,1', ['only' => ['resendVerification', 'resendVerificationForCurrentUser']]);

        $this->redirectTo = '/home';

        $this->redirectAfterVerification = '/home';

        $this->notifications = $notifications;
    }

    /**
     * Override user verification to add the appropriate role.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getVerification(Request $request, $token)
    {
        if (! $this->validateRequest($request)) {
            return redirect($this->redirectIfVerificationFails());
        }

        try {
            $user = UserVerification::process($request->input('email'), $token, $this->userTable());
        } catch (UserNotFoundException $e) {
            return redirect($this->redirectIfVerificationFails());
        } catch (UserIsVerifiedException $e) {
            return redirect($this->redirectIfVerified());
        } catch (TokenMismatchException $e) {
            return redirect($this->redirectIfVerificationFails());
        }

        if (config('user-verification.auto-login') === true) {
            auth()->loginUsingId($user->id);
        }

        // get proper User model
        $user = User::find($user->id);

        // add verified role
        $user->assignRole('verified');

        // log in the user
        Auth::login($user);

        return redirect($this->redirectAfterVerification());
    }

    /**
     * Override the register() method.
     * @param Request $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {
        $user = Spark::interact(
            Register::class, [$request], true
        );

        if (env('SOFT_LAUNCH', false)) {
            $invitation = SoftLaunchInvitation::where([
                ['token', '=', $request->input('invite')],
                ['used', '!=', true]
            ])->first();

            if (!empty($invitation)) {
                $user->registrationInvitation()->save($invitation);
                $invitation->used = true;
                $invitation->save();
            } else {
                $email = $user->email;
                if (empty(DB::table('soft_launch_shortlist')->where(['email' => $email])->count())) {
                    DB::table('soft_launch_shortlist')->insert([
                        'email' => $email,
                        "created_at" => \Carbon\Carbon::now(),
                        "updated_at" => \Carbon\Carbon::now(),
                    ]);
                }
                $user->delete();
                return response()->json([
                    'redirect' => '/',
                ]);
            }
        }

        event(new UserRegistered($user));

        $path = $request->input('campaign');
        $campaign = AdCampaign::where([
            ['path', '=', $path],
            ['active', '=', true]
        ])->first();
        if (!empty($campaign)) {
            $user->campaign()->associate($campaign);
            $user->save();
        }

        // log in the user
        Auth::login($user);

        return response()->json([
            'redirect' => $this->redirectPath(),
        ]);
    }

    /**
     * Show the page for resending the verification email.
     *
     * @return Response
     */
    public function emailNotVerified()
    {
        return view('spark::auth.email-not-verified');
    }

    /**
     * Show the Thank You page for registrations.
     *
     * @return Response
     */
    public function showRegistrationThankYou()
    {
        return view('spark::auth.thank-you-for-registering');
    }

    /**
     * Resend the verification email.
     *
     * @param  Request  $request
     * @return Response
     */
    public function resendVerification(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|email_filter',
        ]);
        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        // Silently skip wrong email addresses so that malicious users can't try and guess emails.
        if (!empty($user)) {
            UserVerification::generate($user);
            UserVerification::send($user, 'Gainrank Email Verification');
        }
        return redirect('/email-not-verified')->with('status', 'Email sent, please check your inbox and/or spam folder.');
    }

    /**
     * Resend the verification email (current user).
     *
     * @param  Request  $request
     * @return Response
     */
    public function resendVerificationForCurrentUser(Request $request)
    {
        // get user
        $user = $request->user();

        // get current time
        $now = Carbon::now();

        // only work for unverified users
        if (!$user->hasRole('verified')) {
            // prevent users from spamming code generation, assuming this is not the first run
            if (!empty($user->sent_verification_email_at)) {
                $sent_verification_email_at = Carbon::parse($user->sent_verification_email_at);

                if ($sent_verification_email_at->diffInSeconds($now) < self::EMAIL_RESEND_SECS_LIMIT) {
                    abort(422);
                }
            }

            // Silently skip wrong email addresses so that malicious users can't try and guess emails.
            if (!empty($user)) {
                // update timestamp
                $user->sent_verification_email_at = $now;
                $user->save();

                // send the email
                UserVerification::generate($user);
                UserVerification::send($user, 'Gainrank Email Verification');
            }

            // create notification
            $this->notifications->create($user, [
                'icon' => 'fa-envelope-o',
                'body' => 'The verification message has been re-sent to your email address.'
            ]);
        }

        return response('Ok', 200);
    }
}
