<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use App\SoftLaunchInvitation;
use App\Mail\SoftLaunchInvitation as SoftLaunchInvitationMail;
use App\User;

class SoftLaunchInvitationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:invite users', ['except' => ['addToShortlist', 'register']]);
    }

    /**
     * Return all the SoftLaunchInvitations a User has sent.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        if ($user->id == $request->user()->id || $request->user()->hasRole('admin')) {
            return $user->sentInvitations;
        }
        abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:255|email',
        ]);

        $validator->after(function ($validator) use (&$request) {
            if ($request->user()->sentInvitations->count() > env('SOFT_LAUNCH_INVITATIONS_LIMIT', 3)) {
                $validator->errors()->add(
                    'email',
                    'You have reached your invitation limit. Please delete any unused invitations if you\'d like to send more.');
            }
        });

        if ($validator->fails()) {
            return Response::json($validator->getMessageBag()->toArray(), 422);
        }

        $invitation = new SoftLaunchInvitation;
        $invitation->email = $request->input('email');
        do {
            $token = str_random(40);
        } while (
            SoftLaunchInvitation::where('token', '=', $token)->first() instanceof SoftLaunchInvitation
        );
        $invitation->token = $token;
        $invitation->sender()->associate($request->user());
        $invitation->save();

        Mail::to($invitation->email)->send(new SoftLaunchInvitationMail($invitation));

        return response('Ok', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addToShortlist(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:255|email',
        ]);
        $email = $request->input('email');
        if (empty(DB::table('soft_launch_shortlist')->where(['email' => $email])->count())) {
            DB::table('soft_launch_shortlist')->insert([
                'email' => $email,
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SoftLaunchInvitation $invitation
     */
    public function destroy(Request $request, User $user, SoftLaunchInvitation $invitation)
    {
        if (empty($invitation->used) && ($invitation->sender->id == $request->user()->id || $request->user()->hasRole('admin'))) {
            $invitation->delete();
            return response('Ok', 200);
        }
        abort(403);
    }

    public function register(String $token)
    {
        $invitation = SoftLaunchInvitation::where([
            ['token', '=', $token],
        ])->first();
        if (empty($invitation)) {
            abort(404);
        } else {
            // Don't use 'invitation' as that's used by Spark and causes errors
            return redirect()->route('register', ['invite' => $token]);
        }
    }
}
