<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Validation\Rule;
use App\User;

class ProfileRoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:grant roles');
    }

    /**
     * Return the current user's roles.
     *
     * @return Response
     */
    public function get(User $user)
    {
        return ['roles' => Role::all(), 'current' => $user->roles()->pluck('name')];
    }

    /**
     * Update the user's role.
     *
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        $roles = Role::all()->pluck('name')->toArray();
        $this->validate($request, [
            'roles.*' => [
                'string',
                'required',
                Rule::in($roles)
            ]
        ]);
        // Spark doesn't allow you to modify your own user account via Kiosk so this should never happen, nevertheless..
        if ($request->user()->id == $user->id) {
            response('Permission Denied.', 403);
        }
        $user->syncRoles($request->input('roles'));
    }

    /**
     * Redirect to a User's profile based on the supplied email.
     *
     * @return Response
     */
    public function loadByEmail(Request $request, String $email)
    {
        return redirect('/spark/kiosk#/users/' . User::where('email', $email)->pluck('id')->first());
    }
}
