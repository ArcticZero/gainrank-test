<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;
use App\RankTracking;
use App\RankTrackingDomain;
use App\RankTrackingRank;
use Carbon\Carbon;
use App\User;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('subscribed');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $past_x_days = 14;

        $data = $this->getStatCounters($request->user(), $past_x_days);

        $data['past_x_days'] = $past_x_days;

        return view('home', $data);
    }

    private function getStatCounters(User $user, $past_x_days)
    {
        $rank_data_dates = 30;

        $data = $this->initData();

        $user_id = $user->id;
        $user_tz = $user->timezone;
        // Log::info('user - ' . $user_id);

        // dates we need
        $date_now = Carbon::now();
        $dn_date = $date_now->format('Y-m-d');

        $start_date = Carbon::now();
        $start_date = $start_date->subDays($past_x_days - 1);
        $dw_from = $start_date->format('Y-m-d 00:00:00');
        $dw_date = $start_date->format('Y-m-d');
        $date_labels = [];

        // total keywords
        $data['stats']['keyword_total'] = RankTracking::where('user_id', $user_id)->count();

        // total websites
        $data['stats']['website_total'] = RankTrackingDomain::where('user_id', $user_id)->count();

        // keyword movement

        // rank 1
        $data['stats']['top1']['total'] = RankTracking::where('last_rank', 1)
            ->where('user_id', $user_id)
            ->count();

        // top 3
        $data['stats']['top3']['total'] = RankTracking::where('last_rank', '<=', 3)
            ->where('user_id', $user_id)
            ->count();

        // top 10
        $data['stats']['top10']['total'] = RankTracking::where('last_rank', '<=', 10)
            ->where('user_id', $user_id)
            ->count();

        // get rt ids first
        $ids = [];
        $rts = RankTracking::where('user_id', $user_id)
            //->whereNotNull('last_rank')
            ->whereNotNull('domain_id')
            ->orderBy('last_rank', 'asc')
            ->limit(5)
            ->get();
        foreach ($rts as $rt)
            $ids[] = $rt->id;

        print_r($ids); exit;

        // get everything after last week's date
        $ranks = RankTrackingRank::whereIn('track_id', $ids)
            ->whereDate('applied_at', '>=', $dw_date)
            ->orderBy('applied_at', 'asc')
            ->get();

        $data['dates'] = [];
        $ranks_aggregate = [];
        $ranks_aggregate_data = [];

        // get all rank data for dates needed 
        for ($i = $rank_data_dates; $i >= 0; $i--) {
            $date = Carbon::now()->subDays($i);
            $date_index = $date->format('Ymd');
            $ranks_aggregate[$date_index] = [];
            $ranks_aggregate_data[$date_index] = [];

            if ($i <= $past_x_days) {
                $data['dates'][] = $date->format('M j');
            }
        }

        $last_track_pointer = false;
        $last_track_data = false;

        // go through everything and consolidate by day
        $row_collection = [];
        foreach ($ranks as $rank) {
            $date = $rank->applied_at;
            if (!empty($user_timezone))
                $date->setTimezone($user_timezone);

            $date_index = $date->format('Ymd');
            // NOTE: assumes everyday, there's an entry
            //       if not, we'll have to initialize
            $date_label = $date->format('M j');
            $date_labels[$date_label] = $date->format('M j');
            $track_id = $rank->track_id;
            $ranks_aggregate[$date_index][$track_id] = $rank->rank;
            $ranks_aggregate_data[$date_index][$track_id] = $rank;

            $row_collection[$track_id][$date_label] = $rank->rank;

            $last_track_pointer = $ranks_aggregate[$date_index];
            $last_track_data = $ranks_aggregate_data[$date_index];
        }

        // Log::info($ranks_aggregate);

        //echo '<pre>'; print_r($last_track_pointer); exit;

        // get top 5
        //$last_track_pointer = end($ranks_aggregate);
        if ($last_track_pointer === false)
            $top_track_ids = [];
        else
        {
            // copy array
            $last_track = $last_track_pointer;
            // have to make the key string to retain keys after sorting
            $sorted_ranks = [];
            foreach ($last_track as $key => $value)
                $sorted_ranks['r' . $key] = $value;

            // Log::info($sorted_ranks);
            asort($sorted_ranks);
            $top_track_ids = array_slice($sorted_ranks, 0, 5);
            // Log::info($top_track_ids);
        }
        // Log::info($top_track_ids);

        // rows population
        $rows = [];
        //echo '<pre>'; print_r($ranks_aggregate_data); exit;
        $one_day_data = $ranks_aggregate_data[Carbon::now()->subDays(1)->format('Ymd')];
        $seven_day_data = $ranks_aggregate_data[Carbon::now()->subDays(7)->format('Ymd')];
        $thirty_day_data = $ranks_aggregate_data[Carbon::now()->subDays(30)->format('Ymd')];
        // Log::info($one_day_data);
        // Log::info('one day delta - ' . ($one_day_data[0][$track_id]->rank - $rank->rank));
        foreach ($top_track_ids as $pad_track_id => $rank_rating)
        {
            $track_id = substr($pad_track_id, 1);
            $rank = $last_track_data[$track_id];
            $track = $rank->track;
            $domain = $track->domain;
            $rows[] = [
                'id' => $track->id,
                'domain' => $domain->url,
                'keyword' => $track->keyword,
                'in_graph' => true,
                'favorite' => false,
                'last_rank' => $rank->rank,
                'engine' => $track->engine->name,
                'deltas' => [
                    '1' => isset($one_day_data[$track_id]) && !empty($one_day_data[$track_id]) ? $one_day_data[$track_id]->rank - $rank->rank : null,
                    '7' => isset($seven_day_data[$track_id]) && !empty($seven_day_data[$track_id]) ?  $seven_day_data[$track_id]->rank - $rank->rank : null,
                    '30' => isset($thirty_day_data[$track_id]) && !empty($thirty_day_data[$track_id]) ?  $thirty_day_data[$track_id]->rank - $rank->rank : null,
                ],
            ];
        }

        //echo '<pre>'; print_r($rows); exit;

        $data['ranks'] = [];

        if (!empty($rows)) {
            foreach ($rows as $row) {
                $data['ranks'][$row['id']] = [];

                // build ranks
                foreach ($data['dates'] as $date) {
                    $rank = RankTrackingRank::where('track_id', $row['id'])
                        ->where('applied_at', date("Y-m-d", strtotime($date)))
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if (!empty($rank)) {
                        $data['ranks'][$row['id']][$date] = $rank->rank;
                    } else {
                        $data['ranks'][$row['id']][$date] = null;
                    }
                }
            }
        }

        // Log::info($rows);
        $data['rows'] = $rows;

        //echo '<pre>'; print_r($data); exit;

        return $data;
    }


    protected function initData()
    {
        $data = [
            'stats' => [
                'keyword_total' => 0,
                'website_total' => 0,
                'total_rows' => 0,
                'top3' => [
                    'total' => 0,
                    'delta' => 0,
                ],
                'keyword_movement' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top1' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top3' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top10' => [
                    'total' => 0,
                    'delta' => 0
                ],
            ],
            'dates' => [],
            'rows' => []
            
        ];

        return $data;
    }

    protected function buildSampleData()
    {
        $past_x_days = 14;
        $rows = [];

        $data = $this->initData();

        // build result
        // get last X days
        for ($i = $total_days; $i >= 1; $i--) {
            $data['dates'][] = date('M j', strtotime("-$i day"));
        }

        // get historical dates
        $date_1day = Carbon::now()->subDays(1)->format('Y-m-d');
        $date_7days = Carbon::now()->subDays(7)->format('Y-m-d');
        $date_30days = Carbon::now()->subDays(30)->format('Y-m-d');

        // initialize ranks
        $data['ranks'] = [];

        if (!empty($rows)) {
            foreach ($rows as $row) {
                // handle deltas
                $rdata = $row->toArray();
                $rdata['domain'] = $row->domain->url;
                $rdata['engine'] = $row->engine->name;
                $rdata['ranks'] = $row->ranks;
                $rdata['in_graph'] = true;
                $rdata['deltas'] = [];

                $past_1day = RankTrackingRank::where('track_id', $row->id)
                    ->where('applied_at', $date_1day)
                    ->orderBy('created_at', 'desc')
                    ->first();

                $rdata['deltas']['1'] = !empty($past_1day) ? $past_1day->rank : null;

                $past_7days = RankTrackingRank::where('track_id', $row->id)
                    ->where('applied_at', $date_7days)
                    ->orderBy('created_at', 'desc')
                    ->first();

                $rdata['deltas']['7'] = !empty($past_1day) ? $past_1day->rank : null;

                $past_30days = RankTrackingRank::where('track_id', $row->id)
                    ->where('applied_at', $date_30days)
                    ->orderBy('created_at', 'desc')
                    ->first();

                $rdata['deltas']['30'] = !empty($past_1day) ? $past_1day->rank : null;

                $data['rows'][] = $rdata;

                // handle ranks
                $data['ranks'][$row['id']] = [];

                // build ranks
                foreach ($data['dates'] as $date) {
                    $rank = RankTrackingRank::where('track_id', $row->id)
                        ->where('applied_at', date("Y-m-d", strtotime($date)))
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if (!empty($rank)) {
                        $data['ranks'][$row->id][$date] = $rank->rank;
                    } else {
                        $data['ranks'][$row->id][$date] = null;
                    }
                }
            }
        }

        //echo '<pre>'; print_r($data['rows']); exit;

        // debug keywords
        /*
        $keywords = [
            'good seo',
            'cheap seo',
            'seo',
            'gainrank',
            'buy good seo',
            'buy cheap seo',
            'search engine optimization',
            'page rank',
            'google ranking',
            'seo service'
        ];
        */

        // debug rows
        /*
        for ($i = 0; $i < 5; $i++) {
            // generate ranks
            $ranks = [];
            $dranks = [];

            foreach ($data['dates'] as $index => $date) {
                $rank = rand(1, 100);
                $dranks[] = $rank;

                if ($index < $past_x_days) {
                    $ranks[$date] = $rank;
                }
            }

            $current_rank = end($ranks);

            // generate deltas
            $deltas = [
                1 => $current_rank - $dranks[5],
                7 => $current_rank - $dranks[0],
                30 => $current_rank - $dranks[28]
            ];

            $data['rows'][] = [
                'id' => $i + 1,
                'domain' => 'gainrank.com',
                'keyword' => $keywords[$i],
                'engine' => 'google.com',
                'favorite' => false,
                'in_graph' => true,
                'ranks' => $ranks,
                'deltas' => $deltas
            ];
        }
        */

        // unset extra dates
        $x = 0;

        foreach ($data['dates'] as $index => $date) {
            if ($x < $total_days - $past_x_days - 1) {
                unset($data['dates'][$index]);
            }

            $x++;
        }

        $data['dates'] = array_values($data['dates']);

        //echo '<pre>'; print_r($data['dates']); exit;

        return view('home', $data);
    }
}
