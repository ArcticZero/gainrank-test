<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Repositories\HelpdeskRepository;
use App\User;
use App\HelpdeskConversation;
use Log;
use Auth;

class HelpdeskController extends Controller
{
    protected $helpdesk;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(HelpdeskRepository $helpdesk)
    {
        $this->helpdesk = $helpdesk;
        $this->middleware('auth');
        $this->middleware('permission:open new support ticket', ['only' => ['store']]);
        $this->middleware('permission:reply to support ticket', ['only' => ['update']]);
    }

    private function syncUserHelpdesk()
    {
        // make sure that the user's helpdesk id is not null
        // if it's null, register the user's email
        $user = Auth::user();
        if ($user->helpdesk_user_id != null)
            return;

        $this->helpdesk->registerUser($user);
        return;
    }

    public function show()
    {
        $this->syncUserHelpdesk();

        return view('helpdesk');
    }

    public function index(Request $request)
    {
        $this->syncUserHelpdesk();

        $user = $request->user();
        $conversations = $this->helpdesk->getUserConversations($user->helpdesk_user_id);
        // NOTE: this is not working for some reason
        //       workaround for now is to manually search
        // $db_convs = $user->helpdeskConversations();

        $db_convs = HelpdeskConversation::where('user_id', $user->id)
            ->get();

        $index_conv = [];
        foreach($db_convs as $db_conv) {
            $index_conv[$db_conv->conv_id] = $db_conv->read_reply_count;
        }

        foreach ($conversations as $key => $conv) {
            $read_reply_count = 0;
            if (isset($index_conv[$conv['conversationid']]))
                $read_reply_count = $index_conv[$conv['conversationid']];

            $conversations[$key]['readgroupsout'] = $read_reply_count;
        }

        return response()->json([
            'conversations' => !empty($conversations) ? $conversations : [],
        ]);
    }

    /**
     * Load a specific ticket from the Helpdesk.
     * @param Request $request
     * @param String $conversation_id
     */
    public function view(Request $request, String $conversation_id)
    {
        $this->syncUserHelpdesk();

        $conversation = $this->helpdesk->getConversation($conversation_id);
        // Ensure that the owner of the ticket is the current user
        if (!empty($conversation['owneremail']) && $conversation['owneremail'] == $request->user()->email) {

            // db update
            $db_conv = HelpdeskConversation::firstOrNew([
                'user_id' => $request->user()->id,
                'conv_id' => $conversation['conversationid']
            ]);
            $db_conv->read_reply_count = $conversation['messagegroupsout'];
            $db_conv->save();

            return response()->json([
                'conversation' => $conversation,
                'messages' => $this->helpdesk->getConversationMessages($conversation_id),
            ]);
        }
        abort(403);
    }

    /**
     * Update a conversation.
     * @param Request $request
     * @param String $conversation_id
     * @return mixed
     */
    public function update(Request $request, String $conversation_id)
    {
        $this->syncUserHelpdesk();

        $this->validate($request, [
            'body' => 'required|string',
            'attachments' => 'array',
            'attachments.*.name' => 'string',
            'attachments.*.data' => 'string',
        ]);

        $conversation = $this->helpdesk->getConversation($conversation_id);
        // Ensure that the owner of the ticket is the current user
        if (!empty($conversation['owneremail']) && $conversation['owneremail'] == $request->user()->email) {
            $message = [
                'message' => $request->input('body'),
                'useridentifier' => $request->user()->helpdesk_user_id,
            ];
            if ($request->has('attachments')) {
                $file_ids = $this->uploadFiles($request->input('attachments'));
                $message['attachments'] = implode(',', $file_ids);
            }
            $this->helpdesk->createNewMessage($conversation_id, $message);

            return response()->json([
                'conversation' => $this->helpdesk->getConversation($conversation_id),
                'messages' => $this->helpdesk->getConversationMessages($conversation_id),
            ]);
        }
        abort(403);
    }

    /**
     * Create a new conversation (ticket)
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $this->syncUserHelpdesk();

        $this->validate($request, [
            'subject' => 'required|string|max:255',
            'body' => 'required|string',
            'attachments' => 'array',
            'attachments.*.name' => 'string',
            'attachments.*.data' => 'string',
        ]);
        $conversation = [
            'subject' => $request->input('subject'),
            'message' => $request->input('body'),
            'useridentifier' => $request->user()->helpdesk_user_id,
            'department' => 'default',
            'recipient' => env('LADESK_DEFAULT_RECIPIENT_EMAIL'),
        ];
        if ($request->has('attachments')) {
            $file_ids = $this->uploadFiles($request->input('attachments'));
            $conversation['attachments'] = implode(',', $file_ids);
        }
        $conversation_id = $this->helpdesk->createConversation($conversation);

        return response()->json([
            'redirect' => '/settings#/tickets/'.$conversation_id,
        ]);
    }

    /**
     * Uploads multiple files to the Helpdesk and returns an array of file id's.
     * @param $files
     * @return array
     */
    private function uploadFiles($files)
    {
        $file_ids = [];
        foreach ($files as $file) {
            $file['type'] = pathinfo($file['name'], PATHINFO_EXTENSION);
            $fileid = $this->helpdesk->uploadFile($file);
            if (!empty($fileid)) {
                $file_ids[] = $fileid;
            }
        }
        return $file_ids;
    }
}
