<?php

namespace App\Http\Controllers;

use App\RankTracking;
use App\RankTrackingRank;
use App\RankTrackingSetting;
use App\RankTrackingStatus;
use App\RankTrackingDomain;
use App\User;
use App\Gainrank\RankTrackingAPI;
use App\Gainrank\GoogleDomain;
use Carbon\Carbon;
use DB;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class RankTrackingController extends Controller
{
    protected $kiosk_per_page = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function test()
    {
        /*
        // test api calls
        $rt_api = new RankTrackingAPI($indexed_settings);
        */

        $rt_api = RankTrackingAPI::initialize();

        /*
        // add
        $country_id = GoogleDomain::getIDFromDomain('www.google.com');
        echo "country id - $country_id\n";
        $rt_api->add('www.bbc.com', $country_id, 'news');
        */

        // update
        $rt_api->update(23, 1);
    }

    /**
     * Get current version of API
     *
     * @return string
     */
    private function getCurrentVersion()
    {
        $version = RankTrackingSetting::where([
            ['version', '=', 0],
            ['name', 'current_version']
        ])
            ->select('value')
            ->pluck('value')
            ->first();
        ;

        return $version;
    }

    public function showDomainPage(Request $request, $limit = 100)
    {
        $data = [
            'rows' => []
        ];

        // get submissions from db
        $rows = $request->user()
            ->ranktrackingDomains()
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();

        foreach ($rows as $row) {
            $data['rows'][] = $row;
        }

        return view('services.ranktracker.domain', $data);
    }

    public function addDomain(Request $request)
    {
        $this->validate($request, [
            'url' => 'required',
        ]);

        $domain = new RankTrackingDomain();
        $domain->url = $request->input('url');
        $domain->user_id = $request->user()->id;
        $domain->save();

        return response('Ok', 200);
    }


    /**
     * Display the client view.
     *
     * @return Response
     */
    public function showKeywordPage(Request $request, $domain_id, $limit = 100)
    {
        $past_x_days = 30;

        // get submissions from db
        /*
        $rows = $request->user()
            ->tracks()
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();
        */

        /*
        $data = $this->initData();

        $rows = Ranktracking::where('user_id', $request->user()->id)
            ->where('domain_id', $domain_id)
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();

        $data['stats']['total_rows'] = count($rows);

        if (!empty($rows)) {
            foreach ($rows as $row) {
                $row['engine'] = $row->engine->name;
                $row['ranks'] = [];
                $row['domain'] = $row->domain->url;
                $data['rows'][] = $row;
            }
        }
        */

        $data = $this->getStatCounters($request->user(), $domain_id, $past_x_days);

        // get last X days
        /*
        for ($i = $past_x_days - 1; $i >= 0; $i--) {
            $data['dates'][] = date('M j', strtotime("-$i day"));
        }
        */

        $data['past_x_days'] = $past_x_days;

        //echo '<pre>';
        //print_r($data);
        //exit;

        return view('services.ranktracker.ranktracker', $data);
    }

    private function getStatCounters(User $user, $domain_id, $past_x_days)
    {
        $data = $this->initData();

        $user_id = $user->id;
        $user_tz = $user->timezone;
        // Log::info('user - ' . $user_id);

        // dates we need
        $date_now = Carbon::now();
        $dn_date = $date_now->format('Y-m-d');

        $start_date = Carbon::now();
        $start_date = $start_date->subDays($past_x_days - 1);
        $dw_from = $start_date->format('Y-m-d 00:00:00');
        $dw_date = $start_date->format('Y-m-d');
        $date_labels = [];

        // get rt ids first
        $ids = [];
        $rts = RankTracking::where('user_id', $user_id)
            ->where('domain_id', $domain_id)
            ->get();
        foreach ($rts as $rt)
            $ids[] = $rt->id;

        // get everything after last week's date
        $ranks = RankTrackingRank::whereIn('track_id', $ids)
            ->whereDate('applied_at', '>=', $dw_date)
            ->orderBy('applied_at', 'asc')
            ->get();

        $data['dates'] = [];
        $ranks_aggregate = [];
        $ranks_aggregate_data = [];

        // get last X days
        for ($i = $past_x_days; $i >= 0; $i--) {
            $date = Carbon::now()->subDays($i);
            $date_index = $date->format('Ymd');
            $data['dates'][] = $date->format('M j');
            $ranks_aggregate[$date_index] = [];
            $ranks_aggregate_data[$date_index] = [];
        }

        $last_track_pointer = false;
        $last_track_data = false;

        // go through everything and consolidate by day
        $row_collection = [];
        foreach ($ranks as $rank) {
            $date = $rank->applied_at;
            if (!empty($user_timezone))
                $date->setTimezone($user_timezone);

            $date_index = $date->format('Ymd');
            // NOTE: assumes everyday, there's an entry
            //       if not, we'll have to initialize
            $date_label = $date->format('M j');
            $date_labels[$date_label] = $date->format('M j');
            $track_id = $rank->track_id;
            $ranks_aggregate[$date_index][$track_id] = $rank->rank;
            $ranks_aggregate_data[$date_index][$track_id] = $rank;

            $row_collection[$track_id][$date_label] = $rank->rank;

            $last_track_pointer = $ranks_aggregate[$date_index];
            $last_track_data = $ranks_aggregate_data[$date_index];
        }

        // Log::info($ranks_aggregate);

        //echo '<pre>'; print_r($last_track_pointer); exit;

        // get top 5
        //$last_track_pointer = end($ranks_aggregate);
        if ($last_track_pointer === false)
            $top_track_ids = [];
        else
        {
            // copy array
            $last_track = $last_track_pointer;
            // have to make the key string to retain keys after sorting
            $sorted_ranks = [];
            foreach ($last_track as $key => $value)
                $sorted_ranks['r' . $key] = $value;

            // Log::info($sorted_ranks);
            asort($sorted_ranks);
            $top_track_ids = array_slice($sorted_ranks, 0, 5);
            // Log::info($top_track_ids);
        }
        // Log::info($top_track_ids);

        // rows population
        $rows = [];
        //echo '<pre>'; print_r($ranks_aggregate_data); exit;
        $one_day_data = $ranks_aggregate_data[Carbon::now()->subDays(1)->format('Ymd')];
        $seven_day_data = $ranks_aggregate_data[Carbon::now()->subDays(7)->format('Ymd')];
        $thirty_day_data = $ranks_aggregate_data[Carbon::now()->subDays(30)->format('Ymd')];
        // Log::info($one_day_data);
        // Log::info('one day delta - ' . ($one_day_data[0][$track_id]->rank - $rank->rank));
        foreach ($top_track_ids as $pad_track_id => $rank_rating)
        {
            $track_id = substr($pad_track_id, 1);
            $rank = $last_track_data[$track_id];
            $track = $rank->track;
            $domain = $track->domain;
            $rows[] = [
                'id' => $track->id,
                'domain' => $domain->url,
                'keyword' => $track->keyword,
                'in_graph' => true,
                'favorite' => false,
                'last_rank' => $rank->rank,
                'engine' => $track->engine->name,
                'deltas' => [
                    '1' => isset($one_day_data[$track_id]) && !empty($one_day_data[$track_id]) ? $one_day_data[$track_id]->rank - $rank->rank : null,
                    '7' => isset($seven_day_data[$track_id]) && !empty($seven_day_data[$track_id]) ?  $seven_day_data[$track_id]->rank - $rank->rank : null,
                    '30' => isset($thirty_day_data[$track_id]) && !empty($thirty_day_data[$track_id]) ?  $thirty_day_data[$track_id]->rank - $rank->rank : null,
                ],
            ];
        }

        //echo '<pre>'; print_r($rows); exit;

        $data['ranks'] = [];

        if (!empty($rows)) {
            foreach ($rows as $row) {
                $data['ranks'][$row['id']] = [];

                // build ranks
                foreach ($data['dates'] as $date) {
                    $rank = RankTrackingRank::where('track_id', $row['id'])
                        ->where('applied_at', date("Y-m-d", strtotime($date)))
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if (!empty($rank)) {
                        $data['ranks'][$row['id']][$date] = $rank->rank;
                    } else {
                        $data['ranks'][$row['id']][$date] = null;
                    }
                }
            }
        }

        // Log::info($rows);
        $data['rows'] = $rows;

        // get last period's day's data, group by track_id
        $rts_last_period = RankTrackingRank::select('track_id')
            ->whereIn('track_id', $ids)
            ->whereDate('created_at', $dw_date)
            ->groupBy('track_id')
            ->get();

        $lp_ids = [];
        $lp_ranks = [];
        $lp_initial_ranks = [];
        $lp_rank_ups = [];
        $lp_top3 = [];
        $lp_top10 = [];
        $lp_top20 = [];
        $lp_top30 = [];
        $lp_top100 = [];
        // $lp_movement = [];

        foreach ($rts_last_period as $rt) {
            // get rank last period
            $lp_rank = RankTrackingRank::where('track_id', $rt->track_id)
                ->whereDate('created_at', $dw_date)
                ->orderBy('created_at', 'desc')
                ->first();

            if (!empty($lp_rank)) {
                $lp_ids[] = $rt->track_id;
                $lp_ranks[$rt->track_id] = $lp_rank->rank;

                if ($lp_rank->rank <= 3) {
                    $lp_top3[] = $rt->track_id;
                }

                if ($lp_rank->rank <= 10) {
                    $lp_top10[] = $rt->track_id;
                }

                if ($lp_rank->rank <= 20) {
                    $lp_top20[] = $rt->track_id;
                }

                if ($lp_rank->rank <= 30) {
                    $lp_top30[] = $rt->track_id;
                }

                if ($lp_rank->rank <= 100) {
                    $lp_top100[] = $rt->track_id;
                }

                // get previous rank
                $lp_initial_rank = RankTrackingRank::where('track_id', $rt->track_id)
                    ->whereDate('created_at', (clone $start_date)->subDay()->format('Y-m-d'))
                    ->orderBy('created_at', 'desc')
                    ->first();

                if (!empty($lp_initial_rank)) {
                    $lp_initial_ranks[$rt->track_id] = $lp_initial_rank->rank;
                    //$lp_movement[$rt->track_id] = $lp_rank->rank - $lp_initial_rank->rank;
                } else {
                    $lp_initial_ranks[$rt->track_id] = 0;
                }

                // check if the keyword ranked up
                if ($lp_ranks[$rt->track_id] > $lp_initial_ranks[$rt->track_id]) {
                    $lp_rank_ups[] = $rt->track_id;
                }
            }
        }

        // get today's data, group by track_id
        $rts_now = RankTrackingRank::select('track_id')
            ->whereIn('track_id', $ids)
            ->whereDate('created_at', $dn_date)
            ->groupBy('track_id')
            ->get();

        $now_ids = [];
        $now_ranks = [];
        $now_initial_ranks = [];
        $now_rank_ups = [];
        $now_top3 = [];
        $now_top10 = [];
        $now_top20 = [];
        $now_top30 = [];
        $now_top100 = [];
        // $now_movement = [];

        foreach ($rts_now as $rt) {
            // get rank today
            $now_rank = RankTrackingRank::where('track_id', $rt->track_id)
                ->whereDate('created_at', $dn_date)
                ->orderBy('created_at', 'desc')
                ->first();

            if (!empty($now_rank)) {
                $now_ids[] = $rt->track_id;
                $now_ranks[$rt->track_id] = $now_rank->rank;

                if ($now_rank->rank <= 3) {
                    $now_top3[] = $rt->track_id;
                }

                if ($now_rank->rank <= 10) {
                    $now_top10[] = $rt->track_id;
                }

                if ($now_rank->rank <= 20) {
                    $now_top20[] = $rt->track_id;
                }

                if ($now_rank->rank <= 30) {
                    $now_top30[] = $rt->track_id;
                }

                if ($now_rank->rank <= 100) {
                    $now_top100[] = $rt->track_id;
                }

                // get previous rank
                $now_initial_rank = RankTrackingRank::where('track_id', $rt->track_id)
                    ->whereDate('created_at', (clone $date_now)->subDay()->format('Y-m-d'))
                    ->orderBy('created_at', 'desc')
                    ->first();

                if (!empty($now_initial_rank)) {
                    $now_initial_ranks[$rt->track_id] = $now_initial_rank->rank;
                    //$now_movement[$rt->track_id] = $now_rank->rank - $now_initial_rank->rank;
                } else {
                    $now_initial_ranks[$rt->track_id] = 0;
                }

                // check if the keyword ranked up
                if ($now_ranks[$rt->track_id] > $now_initial_ranks[$rt->track_id]) {
                    $now_rank_ups[] = $rt->track_id;
                }
            }
        }

        // build top totals
        $total_rank_up = count($now_rank_ups);
        $total_top3 = count($now_top3);
        $total_top10 = count($now_top10);
        $total_top20 = count($now_top20);
        $total_top30 = count($now_top30);
        $total_top100 = count($now_top100);

        $rank_up_delta = $total_rank_up - count($lp_rank_ups);
        $top3_delta = $total_top3 - count($lp_top3);
        $top10_delta = $total_top10 - count($lp_top10);
        $top20_delta = $total_top20 - count($lp_top20);
        $top30_delta = $total_top30 - count($lp_top30);
        $top100_delta = $total_top100 - count($lp_top100);

        if ($rank_up_delta != 0) {
            $rank_up_delta_percent = floor(($rank_up_delta / $total_rank_up) * 100);
        } else {
            $rank_up_delta_percent = 0;
        }

        if ($top3_delta != 0) {
            $top3_delta_percent = floor(($top3_delta / $total_top3) * 100);
        } else {
            $top3_delta_percent = 0;
        }

        if ($top10_delta != 0) {
            $top10_delta_percent = floor(($top10_delta / $total_top10) * 100);
        } else {
            $top10_delta_percent = 0;
        }

        if ($top20_delta != 0) {
            $top20_delta_percent = floor(($top20_delta / $total_top20) * 100);
        } else {
            $top20_delta_percent = 0;
        }

        if ($top30_delta != 0) {
            $top30_delta_percent = floor(($top30_delta / $total_top30) * 100);
        } else {
            $top30_delta_percent = 0;
        }

        if ($top100_delta != 0) {
            $top100_delta_percent = floor(($top100_delta / $total_top100) * 100);
        } else {
            $top100_delta_percent = 0;
        }

        $data['stats']['keywords_up'] = [
            'total' => $total_rank_up,
            'delta' => $rank_up_delta_percent
        ];

        $data['stats']['top_3'] = [
            'total' => $total_top3,
            'delta' => $top3_delta_percent
        ];

        $data['stats']['top_10'] = [
            'total' => $total_top10,
            'delta' => $top10_delta_percent
        ];

        $data['stats']['top_20'] = [
            'total' => $total_top20,
            'delta' => $top20_delta_percent
        ];

        $data['stats']['top_30'] = [
            'total' => $total_top30,
            'delta' => $top30_delta_percent
        ];

        $data['stats']['top_100'] = [
            'total' => $total_top100,
            'delta' => $top100_delta_percent
        ];

        //echo '<pre>'; print_r($data['ranks']); exit;

        return $data;
    }

    protected function initData()
    {
        $data = [
            // statistics boxes
            'stats' => [
                'total_rows' => 0,
                'keywords_up' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top3' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top10' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top20' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top30' => [
                    'total' => 0,
                    'delta' => 0
                ],
                'top100' => [
                    'total' => 0,
                    'delta' => 0
                ]
            ],
            'dates' => [],
            'rows' => [],
            'past_x_days' => 0
        ];

        return $data;
    }

    /**
     * Display the client view with test data.
     *
     * @return Response
     */
    public function showRankTrackerTestData()
    {
        // this is dummy data
        $data = $this->initData();

        // dates of last 30 days.. doing manually first so dummy data works
        $dates = [
            'Aug 27',
            'Aug 28',
            'Aug 29',
            'Aug 30',
            'Sep 1',
            'Sep 2',
            'Sep 3',
            'Sep 4',
            'Sep 5',
            'Sep 6',
            'Sep 7',
            'Sep 8',
            'Sep 9',
            'Sep 10',
            'Sep 11',
            'Sep 12',
            'Sep 13',
            'Sep 14',
            'Sep 15',
            'Sep 16',
            'Sep 17',
            'Sep 18',
            'Sep 19',
            'Sep 20',
            'Sep 21',
            'Sep 22',
            'Sep 23',
            'Sep 24',
            'Sep 25',
            'Sep 26'
        ];

        // keyword rows
        $rows = [
            [
                'id' => 1,
                'domain' => 'gainrank.com',
                'keyword' => 'gainrank',
                'engine' => 'google.com',
                'favorite' => true,
                'ranks' => [
                    '2017-08-27' => 18,
                    '2017-08-28' => 10,
                    '2017-08-29' => 25,
                    '2017-08-30' => 32,
                    '2017-09-01' => 18,
                    '2017-09-02' => 3,
                    '2017-09-03' => 6,
                    '2017-09-04' => 17,
                    '2017-09-05' => 38,
                    '2017-09-06' => 45,
                    '2017-09-07' => 4,
                    '2017-09-08' => 10,
                    '2017-09-09' => 18,
                    '2017-09-10' => 21,
                    '2017-09-11' => 15,
                    '2017-09-12' => 46,
                    '2017-09-13' => 20,
                    '2017-09-14' => 4,
                    '2017-09-15' => 8,
                    '2017-09-16' => 16,
                    '2017-09-17' => 18,
                    '2017-09-18' => 13,
                    '2017-09-19' => 20,
                    '2017-09-20' => 11,
                    '2017-09-21' => 5,
                    '2017-09-22' => 27,
                    '2017-09-23' => 38,
                    '2017-09-24' => 50,
                    '2017-09-25' => 85,
                    '2017-09-26' => 57
                ],
                'deltas' => [
                    '1' => 28,
                    '7' => -37,
                    '30' => -39
                ]
            ],
            [
                'id' => 2,
                'domain' => 'gainrank.com',
                'keyword' => 'buy seo',
                'engine' => 'google.com',
                'favorite' => true,
                'ranks' => [
                    '2017-08-27' => 99,
                    '2017-08-28' => 151,
                    '2017-08-29' => 189,
                    '2017-08-30' => 126,
                    '2017-09-01' => 53,
                    '2017-09-02' => 57,
                    '2017-09-03' => 50,
                    '2017-09-04' => 1,
                    '2017-09-05' => 195,
                    '2017-09-06' => 69,
                    '2017-09-07' => 58,
                    '2017-09-08' => 189,
                    '2017-09-09' => 189,
                    '2017-09-10' => 157,
                    '2017-09-11' => 27,
                    '2017-09-12' => 150,
                    '2017-09-13' => 172,
                    '2017-09-14' => 188,
                    '2017-09-15' => 102,
                    '2017-09-16' => 142,
                    '2017-09-17' => 37,
                    '2017-09-18' => 14,
                    '2017-09-19' => 40,
                    '2017-09-20' => 49,
                    '2017-09-21' => 82,
                    '2017-09-22' => 118,
                    '2017-09-23' => 25,
                    '2017-09-24' => 55,
                    '2017-09-25' => 177,
                    '2017-09-26' => 94
                ],
                'deltas' => [
                    '1' => 83,
                    '7' => -54,
                    '30' => 5
                ]
            ],
            [
                'id' => 3,
                'domain' => 'gainrank.com',
                'keyword' => 'purchase seo',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 111,
                    '2017-08-28' => 183,
                    '2017-08-29' => 125,
                    '2017-08-30' => 38,
                    '2017-09-01' => 185,
                    '2017-09-02' => 13,
                    '2017-09-03' => 47,
                    '2017-09-04' => 10,
                    '2017-09-05' => 138,
                    '2017-09-06' => 52,
                    '2017-09-07' => 4,
                    '2017-09-08' => 121,
                    '2017-09-09' => 97,
                    '2017-09-10' => 133,
                    '2017-09-11' => 53,
                    '2017-09-12' => 8,
                    '2017-09-13' => 62,
                    '2017-09-14' => 133,
                    '2017-09-15' => 192,
                    '2017-09-16' => 101,
                    '2017-09-17' => 74,
                    '2017-09-18' => 91,
                    '2017-09-19' => 2,
                    '2017-09-20' => 194,
                    '2017-09-21' => 93,
                    '2017-09-22' => 96,
                    '2017-09-23' => 63,
                    '2017-09-24' => 22,
                    '2017-09-25' => 187,
                    '2017-09-26' => 13
                ],
                'deltas' => [
                    '1' => 174,
                    '7' => -11,
                    '30' => 98
                ]
            ],
            [
                'id' => 4,
                'domain' => 'gainrank.com',
                'keyword' => 'cheap seo',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 79,
                    '2017-08-28' => 157,
                    '2017-08-29' => 85,
                    '2017-08-30' => 46,
                    '2017-09-01' => 198,
                    '2017-09-02' => 196,
                    '2017-09-03' => 182,
                    '2017-09-04' => 110,
                    '2017-09-05' => 46,
                    '2017-09-06' => 65,
                    '2017-09-07' => 48,
                    '2017-09-08' => 179,
                    '2017-09-09' => 90,
                    '2017-09-10' => 34,
                    '2017-09-11' => 147,
                    '2017-09-12' => 14,
                    '2017-09-13' => 184,
                    '2017-09-14' => 16,
                    '2017-09-15' => 122,
                    '2017-09-16' => 49,
                    '2017-09-17' => 61,
                    '2017-09-18' => 197,
                    '2017-09-19' => 36,
                    '2017-09-20' => 190,
                    '2017-09-21' => 103,
                    '2017-09-22' => 38,
                    '2017-09-23' => 97,
                    '2017-09-24' => 198,
                    '2017-09-25' => 16,
                    '2017-09-26' => 16
                ],
                'deltas' => [
                    '1' => 0,
                    '7' => 20,
                    '30' => 63
                ]
            ],
            [
                'id' => 5,
                'domain' => 'gainrank.com',
                'keyword' => 'awesome seo',
                'engine' => 'google.com',
                'favorite' => true,
                'ranks' => [
                    '2017-08-27' => 75,
                    '2017-08-28' => 146,
                    '2017-08-29' => 70,
                    '2017-08-30' => 77,
                    '2017-09-01' => 28,
                    '2017-09-02' => 150,
                    '2017-09-03' => 101,
                    '2017-09-04' => 74,
                    '2017-09-05' => 131,
                    '2017-09-06' => 39,
                    '2017-09-07' => 13,
                    '2017-09-08' => 21,
                    '2017-09-09' => 80,
                    '2017-09-10' => 119,
                    '2017-09-11' => 66,
                    '2017-09-12' => 111,
                    '2017-09-13' => 145,
                    '2017-09-14' => 10,
                    '2017-09-15' => 65,
                    '2017-09-16' => 177,
                    '2017-09-17' => 61,
                    '2017-09-18' => 17,
                    '2017-09-19' => 147,
                    '2017-09-20' => 160,
                    '2017-09-21' => 138,
                    '2017-09-22' => 86,
                    '2017-09-23' => 187,
                    '2017-09-24' => 81,
                    '2017-09-25' => 31,
                    '2017-09-26' => 31
                ],
                'deltas' => [
                    '1' => 0,
                    '7' => 116,
                    '30' => 44
                ]
            ],
            [
                'id' => 6,
                'domain' => 'gainrank.com',
                'keyword' => 'search engine optimization',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 146,
                    '2017-08-28' => 195,
                    '2017-08-29' => 17,
                    '2017-08-30' => 171,
                    '2017-09-01' => 104,
                    '2017-09-02' => 128,
                    '2017-09-03' => 189,
                    '2017-09-04' => 36,
                    '2017-09-05' => 25,
                    '2017-09-06' => 73,
                    '2017-09-07' => 146,
                    '2017-09-08' => 64,
                    '2017-09-09' => 163,
                    '2017-09-10' => 29,
                    '2017-09-11' => 152,
                    '2017-09-12' => 134,
                    '2017-09-13' => 193,
                    '2017-09-14' => 37,
                    '2017-09-15' => 181,
                    '2017-09-16' => 156,
                    '2017-09-17' => 83,
                    '2017-09-18' => 112,
                    '2017-09-19' => 45,
                    '2017-09-20' => 33,
                    '2017-09-21' => 189,
                    '2017-09-22' => 124,
                    '2017-09-23' => 134,
                    '2017-09-24' => 53,
                    '2017-09-25' => 85,
                    '2017-09-26' => 146
                ],
                'deltas' => [
                    '1' => -61,
                    '7' => -101,
                    '30' => 0
                ]
            ],
            [
                'id' => 7,
                'domain' => 'gainrank.com',
                'keyword' => 'good search engine optimization',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 178,
                    '2017-08-28' => 119,
                    '2017-08-29' => 77,
                    '2017-08-30' => 113,
                    '2017-09-01' => 198,
                    '2017-09-02' => 102,
                    '2017-09-03' => 57,
                    '2017-09-04' => 47,
                    '2017-09-05' => 44,
                    '2017-09-06' => 72,
                    '2017-09-07' => 61,
                    '2017-09-08' => 21,
                    '2017-09-09' => 85,
                    '2017-09-10' => 85,
                    '2017-09-11' => 63,
                    '2017-09-12' => 189,
                    '2017-09-13' => 122,
                    '2017-09-14' => 118,
                    '2017-09-15' => 63,
                    '2017-09-16' => 59,
                    '2017-09-17' => 84,
                    '2017-09-18' => 2,
                    '2017-09-19' => 91,
                    '2017-09-20' => 186,
                    '2017-09-21' => 66,
                    '2017-09-22' => 7,
                    '2017-09-23' => 176,
                    '2017-09-24' => 114,
                    '2017-09-25' => 91,
                    '2017-09-26' => 91
                ],
                'deltas' => [
                    '1' => 0,
                    '7' => 0,
                    '30' => 87
                ]
            ],
            [
                'id' => 8,
                'domain' => 'gainrank.com',
                'keyword' => 'google search optimization',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 199,
                    '2017-08-28' => 111,
                    '2017-08-29' => 6,
                    '2017-08-30' => 120,
                    '2017-09-01' => 135,
                    '2017-09-02' => 142,
                    '2017-09-03' => 177,
                    '2017-09-04' => 134,
                    '2017-09-05' => 131,
                    '2017-09-06' => 95,
                    '2017-09-07' => 87,
                    '2017-09-08' => 79,
                    '2017-09-09' => 72,
                    '2017-09-10' => 136,
                    '2017-09-11' => 30,
                    '2017-09-12' => 15,
                    '2017-09-13' => 38,
                    '2017-09-14' => 162,
                    '2017-09-15' => 89,
                    '2017-09-16' => 67,
                    '2017-09-17' => 187,
                    '2017-09-18' => 200,
                    '2017-09-19' => 118,
                    '2017-09-20' => 130,
                    '2017-09-21' => 185,
                    '2017-09-22' => 134,
                    '2017-09-23' => 15,
                    '2017-09-24' => 149,
                    '2017-09-25' => 89,
                    '2017-09-26' => 162
                ],
                'deltas' => [
                    '1' => -73,
                    '7' => -44,
                    '30' => 37
                ]
            ],
            [
                'id' => 9,
                'domain' => 'gainrank.com',
                'keyword' => 'buy cheap seo',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 66,
                    '2017-08-28' => 62,
                    '2017-08-29' => 145,
                    '2017-08-30' => 62,
                    '2017-09-01' => 190,
                    '2017-09-02' => 59,
                    '2017-09-03' => 63,
                    '2017-09-04' => 112,
                    '2017-09-05' => 61,
                    '2017-09-06' => 173,
                    '2017-09-07' => 110,
                    '2017-09-08' => 42,
                    '2017-09-09' => 107,
                    '2017-09-10' => 161,
                    '2017-09-11' => 171,
                    '2017-09-12' => 1,
                    '2017-09-13' => 104,
                    '2017-09-14' => 176,
                    '2017-09-15' => 14,
                    '2017-09-16' => 116,
                    '2017-09-17' => 80,
                    '2017-09-18' => 79,
                    '2017-09-19' => 175,
                    '2017-09-20' => 109,
                    '2017-09-21' => 175,
                    '2017-09-22' => 27,
                    '2017-09-23' => 94,
                    '2017-09-24' => 14,
                    '2017-09-25' => 59,
                    '2017-09-26' => 59
                ],
                'deltas' => [
                    '1' => 0,
                    '7' => 116,
                    '30' => 7
                ]
            ],
            [
                'id' => 10,
                'domain' => 'gainrank.com',
                'keyword' => 'buy good seo',
                'engine' => 'google.com',
                'favorite' => false,
                'ranks' => [
                    '2017-08-27' => 116,
                    '2017-08-28' => 179,
                    '2017-08-29' => 189,
                    '2017-08-30' => 71,
                    '2017-09-01' => 41,
                    '2017-09-02' => 6,
                    '2017-09-03' => 91,
                    '2017-09-04' => 29,
                    '2017-09-05' => 58,
                    '2017-09-06' => 174,
                    '2017-09-07' => 90,
                    '2017-09-08' => 95,
                    '2017-09-09' => 32,
                    '2017-09-10' => 88,
                    '2017-09-11' => 178,
                    '2017-09-12' => 192,
                    '2017-09-13' => 141,
                    '2017-09-14' => 129,
                    '2017-09-15' => 128,
                    '2017-09-16' => 112,
                    '2017-09-17' => 83,
                    '2017-09-18' => 140,
                    '2017-09-19' => 30,
                    '2017-09-20' => 42,
                    '2017-09-21' => 158,
                    '2017-09-22' => 122,
                    '2017-09-23' => 168,
                    '2017-09-24' => 80,
                    '2017-09-25' => 101,
                    '2017-09-26' => 151
                ],
                'deltas' => [
                    '1' => -50,
                    '7' => -121,
                    '30' => -35
                ]
            ],
        ];

        $data['dates'] = $dates;
        $data['rows'] = $rows;

        return view('services.ranktracker.ranktracker', $data);
    }


    /**
     * Return a list of all Google regions.
     *
     * @return Response
     */
    public function getGoogleRegions()
    {
        $regions = GoogleDomain::getRegions();
        $result = [];

        foreach ($regions as $key => $region) {
            $result[] = [
                'text' => $region,
                'value' => $key
            ];
        }

        return response()->json(['regions' => $result]);
    }

    /**
     * Add a new keyword to be tracked.
     *
     * @return Response
     */
    public function addKeyword(Request $request, $domain_id)
    {
        // build list of google domains
        $regions = GoogleDomain::getRegions();
        $domains = [];

        foreach ($regions as $key => $region) {
            $domains[] = $key;
        }

        // validate request
        $this->validate($request, [
            'google_region' => 'required|in:' . implode(',', $domains),
            'keywords' => 'required'
        ]);

        // initialize API
        $api = RankTrackingAPI::initialize();

        $keywords = explode("\n", trim($request->input('keywords')));

        // submit each keyword
        foreach ($keywords as $keyword) {
            if (!empty(trim($keyword))) {
                $api->add($domain_id, $request->input('google_region'), $keyword);
            }
        }

        // return
        return response('Ok', 200);
    }

    /**
     * Untrack a keyword.
     *
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Get a list of all submissions.
     *
     * @return Response
     */
    public function getSubmissions(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve rows
        $query = RankTracking::orderBy('created_at', 'desc');

        if ($filter) {
            // get total rows (filtered)
            $total = RankTracking::where('domain', 'like', '%' . $filter . '%')
                ->orWhere('keyword', 'like', '%' . $filter . '%')
                ->orWhereHas('user', function ($query) use ($filter) {
                    $query->where('name', 'like', '%' . $filter . '%');
                })
                ->count();

            // apply filter to query
            $query->where('domain', 'like', '%' . $filter . '%')
                ->orWhere('keyword', 'like', '%' . $filter . '%')
                ->orWhereHas('user', function ($query) use ($filter) {
                    $query->where('name', 'like', '%' . $filter . '%');
                });
        } else {
            // get total rows (all)
            $total = RankTracking::count();
        }

        // limit and offset
        $rows = $query->offset(($page - 1) * $this->kiosk_per_page)
            ->limit($this->kiosk_per_page)
            ->get();

        // format results
        foreach ($rows as $row) {
            $rowdata = $row->toArray();
            $rowdata['user_name'] = $row->user->name;
            $rowdata['engine_name'] = $row->engine->name;
            $items[] = $rowdata;
        }

        // DEBUG: this is dummy data
        $items = [];
        $total = 25;

        for ($i = 1; $i <= 25; $i++) {
            $items[] = [
                'id' => $i,
                'created_at' => "2017-09-24 01:12:23",
                'user_id' => 1,
                'user_name' => "Kendrick Chan", // $row->user->name
                'submission_id' => 123,
                'domain' => 'www.gainrank.com',
                'keyword' => 'great seo',
                'engine_id' => 1,
                'engine_name' => 'Google', // $row->engine->name
                'country' => 'US',
                'region' => 'www.google.com',
                'stopped_at' => "2017-09-28 22:23:01",
                'status_text' => "Ok" // $row->status->staff_message (?)
            ];
        }

        $items = array_slice($items, ($this->kiosk_per_page * ($page - 1)), $this->kiosk_per_page);
        // END dummy data

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->kiosk_per_page, $page);

        return $paginator;
    }

    /**
     * Get a list of all statuses.
     *
     * @return Response
     */
    public function getStatuses(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve rows
        $query = RankTrackingStatus::orderBy('id', 'asc');

        if ($filter) {
            // get total rows (filtered)
            $total = RankTrackingStatus::where('id', $filter)
                ->orWhere('name', 'like', '%' . $filter . '%')
                ->orWhere('code', 'like', '%' . $filter . '%')
                ->orWhere('cust_message', 'like', '%' . $filter . '%')
                ->orWhere('staff_message', 'like', '%' . $filter . '%')
                ->orWhere('cust_url', 'like', '%' . $filter . '%')
                ->count();

            // apply filter to query
            $query->where('id', $filter)
                ->orWhere('name', 'like', '%' . $filter . '%')
                ->orWhere('code', 'like', '%' . $filter . '%')
                ->orWhere('cust_message', 'like', '%' . $filter . '%')
                ->orWhere('staff_message', 'like', '%' . $filter . '%')
                ->orWhere('cust_url', 'like', '%' . $filter . '%');
        } else {
            // get total rows (all)
            $total = RankTrackingStatus::count();
        }

        // limit and offset
        $items = $query->offset(($page - 1) * $this->kiosk_per_page)
            ->limit($this->kiosk_per_page)
            ->get()
            ->toArray();

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->kiosk_per_page, $page);

        return $paginator;
    }

    /**
     * Create a new status.
     *
     * @return Response
     */
    public function addStatus(Request $request)
    {
        // validate input
        $this->validate($request, [
            'id' => 'required|unique:rank_tracking_statuses,id|integer|digits_between:1,6',
            'name' => 'required|unique:rank_tracking_statuses,name',
            'code' => 'required|unique:rank_tracking_statuses,code',
            'cust_message' => 'required',
            'staff_message' => 'required',
            'cust_url' => 'required'
        ]);

        // save to db
        $status = new RankTrackingStatus;
        $status->id = $request->input('id');
        $status->name = $request->input('name');
        $status->code = $request->input('code');
        $status->cust_message = $request->input('cust_message');
        $status->staff_message = $request->input('staff_message');
        $status->cust_url = $request->input('cust_url');
        $status->save();

        // return
        return response('Ok', 200);
    }

    /**
     * Save an existing status.
     *
     * @return Response
     */
    public function updateStatus(Request $request, $id)
    {
        // validate input
        $this->validate($request, [
            'id' => 'exists:rank_tracking_statuses|in:' . $id,
            'name' => 'required|unique:rank_tracking_statuses,name, ' . $id,
            'code' => 'required|unique:rank_tracking_statuses,code, ' . $id,
            'cust_message' => 'required',
            'staff_message' => 'required',
            'cust_url' => 'required'
        ]);

        // save to db
        $status = RankTrackingStatus::find($id);
        $status->name = $request->input('name');
        $status->code = $request->input('code');
        $status->cust_message = $request->input('cust_message');
        $status->staff_message = $request->input('staff_message');
        $status->cust_url = $request->input('cust_url');
        $status->save();

        // return
        return response('Ok', 200);
    }

    /**
     * Delete an existing status.
     *
     * @return Response
     */
    public function destroyStatus(Request $request, $id)
    {
        // delete row if it exists
        RankTrackingStatus::destroy($id);

        // return
        return response('Ok', 200);
    }

    /**
     * Get a list of all settings.
     *
     * @return Response
     */
    public function getSettings(Request $request)
    {
        // get current version
        $version = $this->getCurrentVersion();

        // get our settings for that version
        $indexed_settings = [
            'version' => $version
        ];

        $settings = [];

        $sdata = RankTrackingSetting::where('version', $version)
            ->select(['name', 'value'])
            ->get();

        foreach ($sdata as $srow) {
            $settings[$srow->name] = $srow->value;
        }

        return $settings;
    }

    /**
     * Save settings.
     *
     * @return Response
     */
    public function updateSettings(Request $request)
    {
        // get current version
        $version = $this->getCurrentVersion();

        // update settings if they exist
        foreach ($request->all() as $name => $value) {
            $setting = RankTrackingSetting::where('version', $version)
                ->where('name', $name)
                ->first();

            if ($setting) {
                $setting->value = $value;
                $setting->save();
            }
        }

        // return
        return response('Ok', 200);
    }
}
