<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileNotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the user's profile details.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $request->user()->forceFill([
            'is_subscribed' => $request->is_subscribed ? true : false,
            'show_guide' => $request->show_guide ? true : false
        ])->save();
    }

    /**
     * Disable user's guided tour.
     *
     * @return Response
     */
    public function disableTour(Request $request)
    {
        $request->user()->forceFill([
            'show_guide' => false
        ])->save();
    }
}
