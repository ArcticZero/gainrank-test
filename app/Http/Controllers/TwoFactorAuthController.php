<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use TwoFactorAuth;
use App\TfaBackupCode;
use RobThree\Auth\TwoFactorAuthException;
use RandomLib\Factory;
use Carbon\Carbon;

class TwoFactorAuthController extends Controller
{
    /**
     * Number of backup codes to generate.
     */
    const NUMBER_OF_CODES = 10;

    /**
     * Number of seconds in between backup code generation attempts.
     */
    const CODE_GENERATION_SECS_LIMIT = 30;

    /**
     * The TwoFactorAuth instance.
     */
    protected $tfa;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->tfa = new TwoFactorAuth('Gainrank');
    }

    /**
     * Generate secret and QR codes.
     *
     * @param Request $request
     * @return Response
     */
    public function generateSecret(Request $request)
    {
        // generate key and QR code
        $secret = $this->tfa->createSecret();
        $secret_img = $this->tfa->getQRCodeImageAsDataUri($request->user()->email, $secret);

        // check if server time is correct
        try {
            $this->tfa->ensureCorrectTime();
        } catch (TwoFactorAuthException $e) {
            Log::error('Your server time seems to be off', [
                'message' => $e->getMessage()
            ]);
        }

        // store secret in session
        $request->session()->put('tfa_secret', $secret);

        // return
        return response([
            'tfa_secret' => $secret,
            'tfa_secret_img' => $secret_img
        ]);
    }

    /**
     * Get existing backup codes.
     *
     * @param Request $request
     * @return Response
     */
    public function getBackupCodes(Request $request)
    {
        // get user
        $user = $request->user();

        $codes = [];

        // decrypt backup codes
        foreach ($user->backupCodes as $code) {
            $codes[] = empty($code->used_at) ? decrypt($code->code) : 'USED';
        }

        // return
        return response([
            'generation_date' => date('M j, Y', strtotime($user->generated_backup_codes_at)),
            'backup_codes' => $codes
        ]);
    }

    /**
     * Generate backup codes.
     *
     * @param Request $request
     * @return Response
     */
    public function generateBackupCodes(Request $request)
    {
        // get user
        $user = $request->user();

        // get current time
        $now = Carbon::now();

        // prevent users from spamming code generation, assuming this is not the first run
        if (!empty($user->generated_backup_codes_at)) {
            $last_generated_at = Carbon::parse($user->generated_backup_codes_at);

            if ($last_generated_at->diffInSeconds($now) < self::CODE_GENERATION_SECS_LIMIT) {
                return response()->json([
                    'generate_codes' => [
                        'You may only generate codes once every ' . self::CODE_GENERATION_SECS_LIMIT . ' seconds.'
                    ]
                ], 422);
            }
        }
            

        // initialize random string library
        $factory = new Factory();
        $generator = $factory->getLowStrengthGenerator();

        // delete all existing codes
        TfaBackupCode::where('user_id', $user->id)->delete();

        $codes = [];

        // generate backup codes
        for ($i = 0; $i < self::NUMBER_OF_CODES; $i++) {
            // make sure we are generating unique numbers
            do {
                $code = $generator->generateString(8, '234679ACDEFGHJKMNOPQRTUVWXYZ');
            } while (in_array($code, $codes));

            // add to array
            $codes[] = $code;

            // save to database
            $tfa_backup_code = new TfaBackupCode();
            $tfa_backup_code->code = encrypt($code);

            $user->backupCodes()->save($tfa_backup_code);
        }

        // save generation date
        $user->generated_backup_codes_at = $now;
        $user->save();

        // return
        return response([
            'generation_date' => date('M j, Y', strtotime($user->generated_backup_codes_at)),
            'backup_codes' => $codes
        ]);
    }

    /**
     * Enable Two-Factor Authentication for user.
     *
     * @return Response
     */
    public function enable(Request $request)
    {
        // get verification code
        $code = str_replace(' ', '', $request->verification_code);

        // get secret from session
        $secret = $request->session()->get('tfa_secret');

        // verify it
        $result = $this->tfa->verifyCode($secret, $code);

        // store secret and enable tfa if verified
        if ($result === true) {
            $request->user()->forceFill([
                'tfa_enable' => true,
                'tfa_secret' => $secret
            ])->save();

            // generate backup codes
            $this->generateBackupCodes($request);
        } else {
            return response()->json([
                'verification_code' => [
                    'The validation code you have entered is invalid.'
                ]
            ], 422);
        }
    }

    /**
     * Disable Two-Factor Authentication for user.
     *
     * @return Response
     */
    public function disable(Request $request)
    {
        // delete all existing codes
        TfaBackupCode::where('user_id', $request->user()->id)->delete();

        $request->user()->forceFill([
            'tfa_enable' => false,
            'tfa_secret' => null
        ])->save();
    }

    /**
     * Show the two-factor authentication token form if needed.
     *
     * @param  Request  $request
     * @return Response
     */
    public function redirectForTfaEnabled(Request $request)
    {
        if (Auth::user()->tfa_enable) {
            $user_id = Auth::user()->id;

            Auth::logout();

            $request->session()->put([
                'spark:auth:id' => $user_id,
                'spark:auth:remember' => $request->remember,
            ]);

            return $request->session()->has('spark:auth:id') ? view('auth.token') : redirect('login');
        }
         
        return redirect()->intended($this->redirectPath());
    }
}
