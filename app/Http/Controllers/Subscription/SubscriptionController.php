<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserPendingSubscription;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;
use Auth;
use DateTime;

class SubscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retrieve any pending subscriptions
     *
     * @return string
     */
    protected function getBillingPeriod($interval)
    {
        switch ($interval) {
            case 'monthly':
                $period = 'month';
                break;
            case 'yearly':
                $period = 'year';
                break;
        }

        return $period;
    }

    /**
     * Retrieve any pending subscriptions
     *
     * @return Response
     */
    public function getPending(Request $request)
    {
        $subscription = UserPendingSubscription::where('user_id', Auth::user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        if ($subscription) {
            return response()->json(['subscription' => $subscription]);
        } else {
            return response()->json(['subscription' => null]);
        }
    }


    /**
     * Activate any pending subscriptions
     * This is only for acceptance tests. Should not be available for normal use
     *
     * @return Response
     */
    public function activatePending($billing_provider)
    {
        // check if we will run
        $display_flag = env('ACTIVATE_URL_DISPLAY', false);
        if (!$display_flag) {
            abort(404);
        }


        $user_plan = UserPendingSubscription::where('billing_provider', $billing_provider)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($user_plan) {
            $now = new DateTime();

            // find user
            $user = User::find($user_plan->user_id);

            // get spark plan
            $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

            if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                $date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                $trial_end_time = clone $now;
                $trial_end_time = $trial_end_time->modify($date_mod);
            }

            // update subscription
            $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
            $subscription->name = 'default';
            $subscription->quantity = '1';
            $subscription->trial_ends_at = isset($trial_end_time) ? $trial_end_time->format('Y-m-d H:i:s') : null;
            $subscription->paypal_agreement_id = $user_plan->agreement_id;
            $subscription->provider_plan_name = $spark_plan->id;
            $subscription->billing_provider = $user_plan->billing_provider;
            $subscription->last_payment_at = $now->format('Y-m-d H:i:s');

            // if eligible for trial, add trial end time
            if (isset($trial_end_time)) {
                $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
            }

            // save subscription
            $subscription->save();

            // delete pending subscription as it is no longer necessary
            $user_plan->delete();

            // mark user as having used trial
            $user->has_used_trial = 1;
            $user->save();
        }
    }
}
