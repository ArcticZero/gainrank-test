<?php

namespace App\Http\Controllers\Subscription;

use App\Http\Controllers\Subscription\SubscriptionController;
use App\UserPendingSubscription;
use App\CoinbaseCheckout;
use App\PartialPayment;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use Laravel\Cashier\Cashier;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use Auth;

class SubscriptionCoinbaseController extends SubscriptionController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Start the checkout process
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $error = false;
        $response = [];
        $now = time();

        // get spark plan details
        $spark_plan = Spark::plans()->where('id', $request->plan)->first();

        // get pending subscription with attached agreement
        $pending = UserPendingSubscription::where('user_id', Auth::user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        $cc = false;

        if ($pending) {
            $error = 'You already have a pending subscription';
        } else {
            // if user is linked to an ad campaign, check for corresponding initial checkout
            if (!empty(Auth::user()->campaign)) {
                $cc = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                    ->where('ad_campaign_id', Auth::user()->campaign->id)
                    ->where('discount_type', CoinbaseCheckout::DISCOUNT_TYPE_INITIAL)
                    ->first();
            }

            // use default checkout definition
            if (empty($cc)) {
                $cc = CoinbaseCheckout::where('plan_id', $spark_plan->id)
                    ->whereNull('ad_campaign_id')
                    ->first();
            }

            if (empty($cc)) {
                Log::error("Coinbase checkout pairing not found.", ['plan_id' => $spark_plan->id]);
                $error = 'An error occurred while processing your billing agreement.';
            }
        }

        if (!$error) {
            Log::info('No pending subscription, creating...');

            // create new pending sub
            $user_plan = new UserPendingSubscription();
            $user_plan->user_id = Auth::user()->id;
            $user_plan->provider_plan_name = $request->plan;
            $user_plan->billing_provider = 'coinbase';
            $user_plan->save();

            Log::info('Getting embed code...');
            
            // get approval URL
            $code = $cc->checkout_embed_code;
            $approval_url = str_replace('{CODE}', $code, config('services.coinbase.url'));

            $response = [
                'success' => true,
                'approval_url' => $approval_url
            ];
        }
        else {
            $response = [
                'success' => false,
                'error' => $error,
            ];
        }

        return response()->json($response);
    }

    /**
     * Update subscription
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $error = false;
        $plan = $request->input('plan');

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $plan)->first();

        // get current subscription
        $subscription = Subscription::where('user_id', Auth::user()->id)
            ->where('billing_provider', 'coinbase')
            ->where('is_cancelled', 1)
            ->first();

        if (empty($subscription) || empty($spark_plan) || empty($plan)) {
            $error = true;
        } else {
            if ($plan == $subscription->provider_plan_name) {
                // this is the same plan and we are resuming the subscription
                $subscription->is_cancelled = 0;
                $subscription->save();
            } else {
                // we are changing subscription plans. paypal does not support this
                Log::error("Attempted to switch plan from " . $subscription->provider_plan_name . " to " . $plan);
                $error = true;
            }
        }

        if ($error) {
            return response('Error', 422);
        } else {
            return response('OK', 200);
        }
    }

    /**
     * Cancel (suspend) subscription
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $error = false;
        
        // get current subscription
        $subscription = Subscription::where('user_id', Auth::user()->id)
            ->where('billing_provider', 'coinbase')
            ->where('is_cancelled', 0)
            ->first();

        if (empty($subscription)) {
            $error = true;
        } else {
            // update subscription row
            $subscription->is_cancelled = 1;
            $subscription->save();
        }

        if ($error) {
            return response('Error', 422);
        } else {
            return response('OK', 200);
        }
    }

    /**
     * Successful transaction
     *
     * @return Response
     */
    public function success(Request $request)
    {
        $order = $request->input('order');

        $user_id = Auth::user()->id;

        // look for active sub
        $user_plan = Subscription::where('user_id', $user_id)
            ->where('billing_provider', 'coinbase')
            ->first();

        // if none, look for pending sub
        if (!empty($user_plan)) {
            $id_column = 'subscription_id';
        } else {
            $user_plan = UserPendingSubscription::where('user_id', $user_id)
                ->where('billing_provider', 'coinbase')
                ->first();
        }

        if (!empty($user_plan)) {
            $id_column = 'pending_subscription_id';

            // save order ID
            if (!$user_plan->agreement_id) {
                $user_plan->agreement_id = $order['id'];
                $user_plan->save();
            }
        } else {
            // no existing sub found
            return response('Error', 422);
        }

        // record this payment
        $payment = new PartialPayment();
        $payment->$id_column = $user_plan->id;
        $payment->user_id = $user_id;
        $payment->order_id = $order['id'];
        $payment->currency = 'BTC';
        $payment->save();

        return redirect('settings#/subscription')->withInput([
            'subscription_success' => true,
            'provider' => 'coinbase'
        ]);
    }

    /**
     * Failed transaction
     *
     * @return Response
     */
    public function failure(Request $request)
    {
        // update pending sub
        $user_plan = UserPendingSubscription::where('user_id', Auth::user()->id)
            ->where('billing_provider', 'coinbase')
            ->first();

        $user_plan->delete();

        return redirect('settings#/subscription')->withInput([
            'subscription_success' => false,
            'provider' => 'coinbase'
        ]);
    }
}
