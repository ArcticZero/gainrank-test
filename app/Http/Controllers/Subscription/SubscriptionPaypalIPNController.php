<?php

namespace App\Http\Controllers\Subscription;

use App\Http\Controllers\Subscription\SubscriptionController;
use App\Models\PaypalBootstrap;
use App\Models\PaypalIPN;
use App\PaypalNotification;
use App\Repositories\LocalInvoiceRepository;
use App\UserPendingSubscription;
use App\Invoice;
use App\User;
use App\PendingOrder;
use App\UserPaypalVerifiedEmail;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use App\Http\Controllers\Settings\Billing\SendsInvoiceNotifications;

use Carbon\Carbon;
use UserFingerprintHandler;

class SubscriptionPaypalIPNController extends SubscriptionController
{
    use SendsInvoiceNotifications;

    protected $apiContext;
    protected $input;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->apiContext = PaypalBootstrap::initialize();
        $this->now = Carbon::now();
    }

    /**
     * Listen for IPNs
     *
     * @return Response
     */
    public function listen(Request $request)
    {
        $ipn = new PaypalIPN($request);
        
        // set sandbox mode?
        if (!config('services.paypal.live')) {
            $ipn->useSandbox();
        }

        $verified = $ipn->verifyIPN();
        $this->input = $request->all();

        Log::info("IPN Listener Started");

        if ($verified) {
            $txn_type = $this->input['txn_type'] ?? null;

            // log IPN
            Log::info("IPN Received", $this->input);

            // detect if this is a chargeback
            if (isset($this->input['reason_code'])) {
                $chargeback_codes = [
                    'chargeback',
                    'unauthorized_claim',
                    'unauthorized_spoof',
                    'admin_fraud_reversal',
                    'unauthorized',
                    'chargeback_reimbursement',
                    'buyer_complaint'
                ];

                if (in_array($this->input['reason_code'], $chargeback_codes)) {
                    // find the user based on payer ID
                    $user_id = Subscription::where('paypal_payer_id', $this->input['payer_id'])->value('user_id');

                    if (!empty($user_id)) {
                        // get and the linked user
                        $user = User::find($user_id);
                        $user->ban();

                        // mark user as frauder
                        $user->assignRole('frauder');
                    }
                }
            } else {
                // otherwise, check IPN type and handle accordingly
                switch ($txn_type) {
                    case 'recurring_payment_profile_created':
                        $this->doRecurringPaymentProfileCreated();
                        break;

                    case 'recurring_payment':
                        $this->doRecurringPayment();
                        break;

                    case 'recurring_payment_profile_cancel':
                        $this->doRecurringPaymentProfileCancel();
                        break;

                    case 'recurring_payment_suspended_due_to_max_failed_payment':
                        $this->doRecurringPaymentProfileSuspendedDueToMaxFailedPayment();
                        break;

                    case 'cart':
                        $this->doCart();
                        break;
                }
            }

            // save this IPN
            $notification = new PaypalNotification;
            $notification->notification_type = $txn_type;
            $notification->request_details = json_encode($this->input);
            $notification->save();
                
        } else {
            Log::info("IPN could not be verified", $this->input);
        }

        return response('OK', 200);
    }

    /**
     * Handle IPN for the initial payment in a recurring subscription
     */
    private function doRecurringPaymentProfileCreated()
    {
        $agreement_id = $this->input['recurring_payment_id'];
        $payment_status = $this->input['initial_payment_status'];
        $transaction_id = $this->input['initial_payment_txn_id'];
        $require_verification = false;

        // find user pending subscription
        $user_plan = UserPendingSubscription::where('agreement_id', $agreement_id)
            ->where('billing_provider', 'paypal_recurring')
            ->first();

        if (!$user_plan) {
            Log::info("User plan not found while processing IPN", ['agreement_id' => $agreement_id]);
            return response('Error', 422);
        }

        // find user
        $user = User::find($user_plan->user_id);

        switch ($payment_status) {
            case 'Completed':
                Log::info("Payment is completed!");

                // check if this user is or is related to a frauder via payer_id or payer_email
                $frauder = User::whereHas('roles', function($q) {
                        $q->where('name', 'frauder');
                    })
                    ->where(function($q) {
                        $q->where('paypal_payer_id', $this->input['payer_id'])
                          ->orWhere('paypal_payer_email', $this->input['payer_email']);
                    })
                    ->first();

                // user email does not match payer_email
                if ($user->email != $this->input['payer_email'] && empty($frauder)) {
                    // check first if this email has already been previously verified
                    $is_verified = $user->paypalVerifiedEmails()->where('email', $this->input['payer_email'])->first();

                    if (empty($is_verified)) {
                        // check if user is related to frauder via hash or IP
                        $related_users = UserFingerprintHandler::relatedUsers($user->email, true);

                        foreach ($related_users['users'] as $related_user_email) {
                            $related_user = User::where('email', $related_user_email)
                                ->whereHas('roles', function($q) {
                                    $q->where('name', 'frauder');
                                })->first();

                            if ($related_user) {
                                $require_verification = true;
                            }
                        }
                    }
                }
                    
                if (!empty($frauder) || $require_verification) {
                    // check if a pending order already exists
                    $has_order = PendingOrder::where('pending_subscription_id', $user_plan->id)->first();

                    if (empty($has_order)) {
                        // add proper txn_id to payload array
                        $this->input['txn_id'] = $this->input['initial_payment_txn_id'];

                        // create pending order
                        $order = new PendingOrder();
                        $order->order_type = PendingOrder::ORDER_TYPES['subscription'];
                        $order->billing_provider = 'paypal_recurring';
                        $order->paypal_payer_id = $this->input['payer_id'];
                        $order->paypal_payer_email = $this->input['payer_email'];
                        $order->paypal_agreement_id = $agreement_id;
                        $order->pending_subscription_id = $user_plan->id;
                        $order->request_details = json_encode($this->input);
                        $order->user()->associate($user);
                        $order->save();

                        // if requires email verification, generate unique hash and send email
                        if ($require_verification) {
                            $this->send_verification_email($this->input['payer_email'], $order);
                        }

                        Log::error('Potential frauder detected, order withheld', [
                            'user_id'                   => $user->id,
                            'paypal_payer_id'           => $this->input['payer_id'],
                            'paypal_payer_email'        => $this->input['payer_email'],
                            'pending_order_id'          => $order->id,
                            'allow_email_verification'  => $require_verification
                        ]);
                    }
                } else {
                    // get spark plan
                    $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

                    if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                        $date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                        $trial_end_time = clone $this->now;
                        $trial_end_time->modify($date_mod);
                    }

                    // update subscription
                    $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
                    $subscription->name = 'default';
                    $subscription->quantity = '1';
                    $subscription->trial_ends_at = isset($trial_end_time) ? $trial_end_time->format('Y-m-d H:i:s') : null;
                    $subscription->paypal_agreement_id = $agreement_id;
                    $subscription->paypal_payer_id = $this->input['payer_id'];
                    $subscription->provider_plan_name = $spark_plan->id;
                    $subscription->billing_provider = 'paypal_recurring';
                    $subscription->last_payment_at = $this->now->format('Y-m-d H:i:s');
                    
                    // if eligible for trial, add trial end time
                    if (isset($trial_end_time)) {
                        $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
                    }

                    // save subscription
                    $subscription->save();

                    // delete pending subscription as it is no longer necessary
                    $user_plan->delete();

                    // mark user as having used trial
                    $user->has_used_trial = 1;
                    $user->paypal_payer_id = $this->input['payer_id'];
                    $user->paypal_payer_email = $this->input['payer_email'];
                    $user->save();

                    // create invoice
                    $this->input['txn_id'] = $this->input['initial_payment_txn_id'];
                    $local_invoice = app(LocalInvoiceRepository::class)->createForUser($subscription->user, $this->input);
                    $invoice = new Invoice($subscription->user, $local_invoice, $subscription);

                    // send notification
                    $this->sendInvoiceNotification($subscription->user, $invoice, $transaction_id);
                }

                break;

            case 'Failed':
                // delete transaction row
                $user_plan->delete();

                break;

            default:
                abort(422);
                break;
        }
    }

    /**
     * Handle IPN for succeeding payments in a recurring subscription
     */
    private function doRecurringPayment()
    {
        $agreement_id = $this->input['recurring_payment_id'];
        $payment_status = $this->input['payment_status'];
        $transaction_id = $this->input['txn_id'];

        switch ($payment_status) {
            case 'Completed':
                // find subscription in database
                $subscription = Subscription::where('paypal_agreement_id', $agreement_id)->first();

                if (empty($subscription)) {
                    Log::info("Subscription not found while processing IPN (ID: " . $agreement_id . ")");
                    abort(422);
                }

                // update last payment
                $subscription->last_payment_at = $this->now->format('Y-m-d H:i:s');
                $Subscription->save();

                // create invoice
                $local_invoice = app(PaypalLocalInvoiceRepository::class)->createForUser($subscription->user, $this->input);
                $invoice = new Invoice($subscription->user, $local_invoice, $subscription);

                // send notification
                $this->sendInvoiceNotification($subscription->user, $invoice, $transaction_id);

                break;

            case 'Failed':
            default:
                return response('Error', 422);
                break;
        }
    }

    /**
     * Handle IPN for when a recurring payment profile is suspended
     */
    private function doRecurringPaymentProfileCancel()
    {
        $agreement_id = $this->input['recurring_payment_id'];
                        
        // find pending subscription
        $user_plan = UserPendingSubscription::where('agreement_id', $agreement_id)->first();

        if (empty($user_plan)) {
            Log::info("User plan not found while processing IPN", ['agreement_id' => $agreement_id]);
            return response('Error', 422);
        }

        // delete pending sub as it is no longer necessary
        $user_plan->delete();
    }

    /**
     * Handle IPN for when a payment in a recurring subscription fails the maximum number of allowed times
     */
    private function doRecurringPaymentProfileSuspendedDueToMaxFailedPayment()
    {
        $agreement_id = $this->input['recurring_payment_id'];

        // find in database
        $subscription = Subscription::where('paypal_agreement_id', $agreement_id)->first();

        if (empty($subscription)) {
            Log::info("Subscription not found while processing IPN", ['agreement_id' => $agreement_id]);
            return response('Error', 422);
        }

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

        // get billing period
        $billing_period = $this->getBillingPeriod($spark_plan->interval);

        // compute for end of grace period
        $end_time = strtotime($subscription->last_payment_at . " +1 " . $billing_period);

        // update subscription row
        $subscription->ends_at = date("Y-m-d H:i:s", $end_time);
        $subscription->save();
    }

    /**
     * Handle IPN for payments in manual subscriptions
     */
    private function doCart()
    {
        $user_plan_id = $this->input['custom'];
        $payment_status = $this->input['payment_status'];
        $transaction_id = $this->input['txn_id'];
        $auth_id = $this->input['auth_id'];
        $require_verification = false;

        // find user pending subscription unless capturing a payment
        if ($payment_status != 'Completed') {
            $user_plan = UserPendingSubscription::find($user_plan_id);

            if (empty($user_plan)) {
                Log::info("User plan not found while processing IPN", ['auth_id' => $auth_id]);
                abort(422);
            }

            // find user
            $user = User::find($user_plan->user_id);
        }

        switch ($payment_status) {
            case 'Pending':
                // check if this user is or is related to a frauder
                $frauder = User::whereHas('roles', function($q) {
                        $q->where('name', 'frauder');
                    })
                    ->where(function($q) {
                        $q->where('paypal_payer_id', $this->input['payer_id'])
                          ->orWhere('paypal_payer_email', $this->input['payer_email']);
                    })
                    ->first();

                // user email does not match payer_email
                if ($user->email != $this->input['payer_email'] && empty($frauder)) {
                    // check first if this email has already been previously verified
                    $is_verified = $user->paypalVerifiedEmails()->where('email', $this->input['payer_email'])->first();

                    if (empty($is_verified)) {
                        // check if user is related to frauder via hash or IP
                        $related_users = UserFingerprintHandler::relatedUsers($user->email, true);

                        foreach ($related_users['users'] as $related_user_email) {
                            $related_user = User::where('email', $related_user_email)
                                ->whereHas('roles', function($q) {
                                    $q->where('name', 'frauder');
                                })->first();

                            if ($related_user) {
                                $require_verification = true;
                            }
                        }
                    }
                }

                if ($frauder || $require_verification) {
                    // check if a pending order already exists
                    $has_order = PendingOrder::where('pending_subscription_id', $user_plan->id)->first();

                    if (empty($has_order)) {
                        // create pending order
                        $order = new PendingOrder();
                        $order->order_type = PendingOrder::ORDER_TYPES['subscription'];
                        $order->billing_provider = 'paypal_manual';
                        $order->paypal_payer_id = $this->input['payer_id'];
                        $order->paypal_payer_email = $this->input['payer_email'];
                        $order->paypal_agreement_id = $agreement_id;
                        $order->pending_subscription_id = $user_plan->id;
                        $order->request_details = json_encode($this->input);
                        $order->user()->associate($user);
                        $order->save();

                        // if requires email verification, generate unique hash and send email
                        if ($require_verification) {
                            $this->send_verification_email($this->input['payer_email'], $order);
                        }

                        Log::error('Potential frauder detected, order withheld', [
                            'user_id'                   => $user->id,
                            'paypal_payer_id'           => $this->input['payer_id'],
                            'paypal_payer_email'        => $this->input['payer_email'],
                            'pending_order_id'          => $order->id,
                            'allow_email_verification'  => $require_verification
                        ]);
                    } 
                } else {
                    // get spark plan
                    $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

                    if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                        $trial_date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                        $trial_end_time = clone $this->now;
                        $trial_end_time->modify($trial_date_mod);
                        $end_time = clone $trial_end_time;
                    } else {
                        $end_time = clone $this->now;
                    }

                    // compute for end of subscription
                    $billing_period = $this->getBillingPeriod($spark_plan->interval);
                    $end_date_mod = "+1 " . $billing_period;
                    $end_time->modify($end_date_mod);

                    // update subscription
                    $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
                    $subscription->name = 'default';
                    $subscription->quantity = '1';
                    $subscription->paypal_agreement_id = $auth_id;
                    $subscription->paypal_payer_id = $this->input['payer_id'];
                    $subscription->provider_plan_name = $spark_plan->id;
                    $subscription->billing_provider = 'paypal_manual';
                    $subscription->ends_at = $end_time->format('Y-m-d H:i:s');
                    $subscription->last_payment_at = null;

                    // if eligible for trial, add trial end time
                    if (isset($trial_end_time)) {
                        $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
                    }

                    // save subscription
                    $subscription->save();

                    // delete pending sub as it is no longer necessary
                    $user_plan->delete();

                    // mark user as having used trial
                    $user->has_used_trial = 1;
                    $user->save();
                }

                break;

            case 'Completed':
                // pre-authorized payment was captured via IPN
                $subscription = Subscription::where('paypal_agreement_id', $auth_id)->first();

                if (empty($subscription)) {
                    Log::info("Subscription not found while processing IPN (ID: " . $auth_id . ")");
                    abort(422);
                }

                $subscription->last_payment_at = $this->now->format('Y-m-d H:i:s');
                $subscription->save();

                // create invoice
                $this->input['amount'] = $this->input['mc_gross'];
                $local_invoice = app(PaypalLocalInvoiceRepository::class)->createForUser($subscription->user, $this->input);
                $invoice = new Invoice($subscription->user, $local_invoice, $subscription);

                // send notification
                $this->sendInvoiceNotification($subscription->user, $invoice, $transaction_id);

                break;

            case 'Failed':
                // delete transaction row
                $user_plan->delete();

                break;

            default:
                abort(422);
                break;
        }
    }

    private function send_verification_email($email, $order)
    {
        // generate unique confirmation key for approving this transaction
        do {
            $confirmation_key = str_random(8);
            $duplicate = PendingOrder::where('confirmation_key', $confirmation_key)->first();
        } while (!empty($duplicate));

        // determine expiry date
        $expiry_days = config('orders.pending_via_email.days_before_expiry');
        $expires_at = clone($this->now);
        $expires_at->addDays($expiry_days);

        // save the key
        $order->confirmation_key = $confirmation_key;
        $order->confirmation_key_expires_at = $expires_at->format('Y-m-d H:i:s');
        $order->save();

        // build and send verification email
        $user = $order->user;
        $invoice_data = Spark::invoiceDataFor($user);

        $data = [
            'user'          => $user,
            'verify_url'    => url('orders/confirm/' . $confirmation_key),
            'product'       => $invoice_data['product'],
            'expiry_days'   => $expiry_days
        ];

        Mail::send('emails.orders.verification', $data, function($m) use ($email) {
            $m->to($email)
              ->subject('Verify your order');
        });
    }
}
