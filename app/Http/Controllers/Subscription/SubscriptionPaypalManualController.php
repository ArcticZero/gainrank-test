<?php

namespace App\Http\Controllers\Subscription;

use App\Http\Controllers\Subscription\SubscriptionController;
use App\Models\PaypalBootstrap;
use App\UserPendingSubscription;
use App\Invoice;
use App\Reward;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\WebProfile;
use PayPal\Exception\PayPalConnectionException;

use Laravel\Cashier\Cashier;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use Auth;

class SubscriptionPaypalManualController extends SubscriptionController
{
    protected $apiContext;
    protected $currency;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->apiContext = PaypalBootstrap::initialize();
        $this->currency = strtoupper(Cashier::usesCurrency());
    }

    /**
     * Start the checkout process
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $error = false;
        $response = [];
        $pp_plan = false;
        $now = time();

        // get spark plan details
        $spark_plan = Spark::plans()->where('id', $request->plan)->first();

        // get pending subscription with attached agreement
        $pending = UserPendingSubscription::where('user_id', Auth::user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        if ($pending) {
            $error = 'You already have a pending subscription';
        } else {
            // get initial payment price
            $initial_price = $spark_plan->price;

            // delete any existing pending subs
            UserPendingSubscription::where('user_id', Auth::user()->id)->delete();

            // create new pending sub
            $user_plan = new UserPendingSubscription();
            $user_plan->user_id = Auth::user()->id;
            $user_plan->provider_plan_name = $request->plan;
            $user_plan->billing_provider = 'paypal_manual';
            $user_plan->save();

            // get billing period
            $billing_period = $this->getBillingPeriod($spark_plan->interval);

            // create payer
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            // if user is linked to an ad campaign, apply corresponding discounts to initial payment
            if (!empty(Auth::user()->campaign)) {
                $discounts = [
                    'amount' => 0,
                    'percent' => 0
                ];

                $bonuses = Auth::user()->campaign->bonuses()
                    ->whereHas('type', function ($q) {
                            $q->whereIn('reward_type', [
                                Reward::TYPE_DISCOUNT_INITIAL_AMOUNT,
                                Reward::TYPE_DISCOUNT_INITIAL_PERCENT
                            ]);
                        })
                        ->get();

                if (!$bonuses->isEmpty()) {
                    foreach ($bonuses as $bonus) {
                        if (!empty($bonus->bonus_amount && $bonus->bonus_amount > 0)) {
                            switch ($bonus->type->reward_type) {
                                case Reward::TYPE_DISCOUNT_INITIAL_AMOUNT:
                                    $discounts['amount'] = $bonus->bonus_amount;
                                    break;
                                case Reward::TYPE_DISCOUNT_INITIAL_PERCENT:
                                    $discounts['percent'] = bcdiv($bonus->bonus_amount, 100);
                                    break;
                            }
                        }
                    }
                }

                // apply flat amount discount to initial payment
                if ($discounts['amount'] > 0) {
                    $initial_price = bcsub($initial_price, $discounts['amount']);
                }

                // apply percentage discount to initial payment
                if ($discounts['percent'] > 0) {
                    $initial_price = bcmul($initial_price, bcsub(100, $discounts['percent']));
                }
            }

            // add subscription to item list
            $item = new Item();
            $item->setName('Gainrank Subscription')
                ->setCurrency($this->currency)
                ->setQuantity(1)
                ->setSku($spark_plan->id)
                ->setPrice($initial_price);

            $item_list = new ItemList();
            $item_list->setItems([$item]);

            // set redirect URLs
            $return_url = url('settings/subscription/paypal/manual');
            $redirects = new RedirectUrls();
            $redirects->setReturnUrl($return_url . "/success?p=" . $user_plan->id)
                ->setCancelUrl($return_url . "/failure?p=" . $user_plan->id);

            // set payment amount
            $amount = new Amount();
            $amount->setCurrency($this->currency)
                ->setTotal($initial_price);

            // create transaction
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setCustom($user_plan->id)
                ->setDescription('Gainrank Subscription (' . $spark_plan->name . ')');

            // remove shipping fields
            $input_fields = new InputFields();
            $input_fields->setNoShipping(1)
                ->setAddressOverride(0);

            $web_profile = new WebProfile();
            $web_profile->setName('Gainrank ' . uniqid())
                ->setInputFields($input_fields)
                ->setTemporary(true);

            // create web profile
            try {
                $web_profile_id = $web_profile->create($this->apiContext)->getId();
            } catch (PayPalConnectionException $e) {
                Log::error("PayPal exception occurred: (" . $e->getCode() . ") " . $e->getData());
                $error = 'An error occurred while processing your billing agreement.';
            } catch (Exception $e) {
                Log::error("Exception occurred: " . $e);
                $error = 'An error occurred while processing your billing agreement.';
            }

            if (!$error) {
                // create full payment object
                $payment = new Payment();
                $payment->setIntent('authorize')
                    ->setPayer($payer)
                    ->setExperienceProfileId($web_profile_id)
                    ->setRedirectUrls($redirects)
                    ->setTransactions([$transaction]);

                // process payment
                try {
                    $payment->create($this->apiContext);

                    $response = [
                        'success' => true,
                        'approval_url' => $payment->getApprovalLink()
                    ];
                } catch (PayPalConnectionException $e) {
                    Log::error("PayPal exception occurred: (" . $e->getCode() . ") " . $e->getData());
                    $error = 'An error occurred while processing your billing agreement.';
                } catch (Exception $e) {
                    Log::error("Exception occurred: " . $e);
                    $error = 'An error occurred while processing your billing agreement.';
                }
            }
        }

        if ($error) {
            $response = [
                'success' => false,
                'error' => $error,
            ];
        }

        return response()->json($response);
    }

    /**
     * Update subscription
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $error = false;
        $plan = $request->input('plan');

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $plan)->first();

        // get current subscription
        $subscription = Subscription::where('user_id', Auth::user()->id)
            ->where('billing_provider', 'paypal_manual')
            ->where('is_cancelled', 1)
            ->first();

        if (empty($subscription) || empty($spark_plan) || empty($plan)) {
            $error = true;
        } else {
            if ($plan == $subscription->provider_plan_name) {
                // this is the same plan and we are resuming the subscription
                $subscription->is_cancelled = 0;
                $subscription->save();
            } else {
                // we are changing subscription plans. paypal does not support this
                Log::error("Attempted to switch plan from " . $subscription->provider_plan_name . " to " . $plan);
                $error = true;
            }
        }

        if ($error) {
            return response('Error', 422);
        } else {
            return response('OK', 200);
        }
    }

    /**
     * Cancel (suspend) subscription
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $error = false;
        
        // get current subscription
        $subscription = Subscription::where('user_id', Auth::user()->id)
            ->where('billing_provider', 'paypal_manual')
            ->where('is_cancelled', 0)
            ->first();

        if (empty($subscription)) {
            $error = true;
        } else {
            // update subscription row
            $subscription->is_cancelled = 1;
            $subscription->save();
        }

        if ($error) {
            return response('Error', 422);
        } else {
            return response('OK', 200);
        }
    }

    /**
     * Successful transaction
     *
     * @return Response
     */
    public function success(Request $request)
    {
        $pending_id = $request->input('p');
        $payment_id = $request->input('paymentId');
        $payer_id = $request->input('PayerID');

        if (!$payment_id) {
            Log::info("No payment ID provided");
            return response('Error', 422);
        }

        if (!$payer_id) {
            Log::info("No payer ID provided");
            return response('Error', 422);
        }

        // get payment
        $payment = Payment::get($payment_id, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payer_id);

        // execute payment
        try {
            $payment->execute($execution, $this->apiContext);
        } catch (PayPalConnectionException $e) {
            Log::info("Could not execute payment " . $payment_id . ": " . " (" . $e->getCode() . ") " . $e->getData());
            return response('Error', 422);
        } catch (Exception $e) {
            Log::info("Could not execute payment " . $payment_id);
            return response('Error', 422);
        }

        // refresh payment info
        try {
            $payment = Payment::get($payment_id, $this->apiContext);
        } catch (PayPalConnectionException $e) {
            Log::info("Could not retrieve payment " . $payment_id . ": " . " (" . $e->getCode() . ") " . $e->getData());
            return response('Error', 422);
        } catch (Exception $e) {
            Log::info("Could not retrieve payment " . $payment_id);
            return response('Error', 422);
        }

        // update pending sub
        $user_plan = UserPendingSubscription::find($pending_id);
        $user_plan->agreement_id = $payment_id;
        $user_plan->save();

        return redirect('settings#/subscription')->withInput([
            'subscription_success' => true,
            'provider' => 'paypal_manual'
        ]);
    }

    /**
     * Failed transaction
     *
     * @return Response
     */
    public function failure(Request $request)
    {
        $pending_id = $request->input('p');

        UserPendingSubscription::destroy($pending_id);

        return redirect('settings#/subscription')->withInput([
            'subscription_success' => false,
            'provider' => 'paypal_manual'
        ]);
    }
}
