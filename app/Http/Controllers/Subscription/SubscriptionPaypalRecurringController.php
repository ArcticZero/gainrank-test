<?php

namespace App\Http\Controllers\Subscription;

use App\Http\Controllers\Subscription\SubscriptionController;
use App\Models\PaypalBootstrap;
use App\PaypalPlan;
use App\UserPendingSubscription;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use PayPal\Api\Plan;
use PayPal\Api\Payer;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Currency;
use PayPal\Exception\PayPalConnectionException;

use Laravel\Cashier\Cashier;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;

use Auth;

class SubscriptionPaypalRecurringController extends SubscriptionController
{
    protected $apiContext;
    protected $currency;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->apiContext = PaypalBootstrap::initialize();
        $this->currency = strtoupper(Cashier::usesCurrency());
    }

    /**
     * Start the checkout process
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $error = false;
        $response = [];
        $pp_plan = false;
        $now = time();

        // get spark plan details
        $spark_plan = Spark::plans()->where('id', $request->plan)->first();

        // get pending subscription with attached agreement
        $pending = UserPendingSubscription::where('user_id', Auth::user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        $pp_plan = false;

        if (!empty($spark_plan)) {
            // get initial payment price
            $initial_price = $spark_plan->price;

            // if user is linked to an ad campaign, check for corresponding plan
            if (!empty(Auth::user()->campaign)) {
                $pp_plan = PaypalPlan::where('plan_id', $request->plan)
                    ->where('ad_campaign_id', Auth::user()->campaign->id)
                    ->first();

                if (!empty($pp_plan)) {
                    // if there are bonuses to the initial payment, let's grab them
                    $discounts = [
                        'amount' => 0,
                        'percent' => 0
                    ];

                    $bonuses = $pp_plan->campaign->bonuses()
                        ->whereHas('type', function ($q) {
                            $q->whereIn('reward_type', [
                                Reward::TYPE_DISCOUNT_INITIAL_AMOUNT,
                                Reward::TYPE_DISCOUNT_INITIAL_PERCENT
                            ]);
                        })
                        ->get();

                    if (!$bonuses->isEmpty()) {
                        foreach ($bonuses as $bonus) {
                            if (!empty($bonus->bonus_amount && $bonus->bonus_amount > 0)) {
                                switch ($bonus->type->reward_type) {
                                    case Reward::TYPE_DISCOUNT_INITIAL_AMOUNT:
                                        $discounts['amount'] = $bonus->bonus_amount;
                                        break;
                                    case Reward::TYPE_DISCOUNT_INITIAL_PERCENT:
                                        $discounts['percent'] = bcdiv($bonus->bonus_amount, 100);
                                        break;
                                }
                            }
                        }
                    }

                    // apply flat amount discount to initial payment
                    if ($discounts['amount'] > 0) {
                        $initial_price = bcsub($initial_price, $discounts['amount']);
                    }

                    // apply percentage discount to initial payment
                    if ($discounts['percent'] > 0) {
                        $initial_price = bcmul($initial_price, bcsub(1, $discounts['percent']));
                    }
                }
            }

            // use default plan definition
            if (empty($pp_plan)) {
                $pp_plan = PaypalPlan::where('plan_id', $request->plan)
                    ->whereNull('ad_campaign_id')
                    ->first();
            }
        }

        if (empty($pp_plan)) {
            $error = 'The selected plan was not found';
        } elseif ($pending) {
            $error = 'You already have a pending subscription';
        } else {
            // delete any existing pending subs
            UserPendingSubscription::where('user_id', Auth::user()->id)->delete();

            // create new pending sub
            $user_plan = new UserPendingSubscription();
            $user_plan->user_id = Auth::user()->id;
            $user_plan->provider_plan_name = $request->plan;
            $user_plan->billing_provider = 'paypal_recurring';
            $user_plan->save();

            // get billing period
            $billing_period = $this->getBillingPeriod($spark_plan->interval);

            // set payment plan
            $plan = new Plan();
            $plan->setId($pp_plan->paypal_plan_id);

            // create payer
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            // override merchant preferences
            $return_url = url('settings/subscription/paypal/recurring');
            $preferences = new MerchantPreferences();
            $preferences->setReturnUrl($return_url . "/success?p=" . $user_plan->id)
                ->setCancelUrl($return_url . "/failure?p=" . $user_plan->id)
                ->setAutoBillAmount("YES")
                ->setInitialFailAmountAction("CANCEL")
                ->setMaxFailAttempts("3")
                ->setSetupFee(new Currency(
                    array(
                        'currency' => $this->currency,
                        'value' => $initial_price
                    )
                ));

            // create billing agreement
            $agreement = new Agreement();
            $agreement->setName('Billing Agreement for Subscription')
                ->setDescription('Gainrank Subscription (' . $spark_plan->name . ')')
                ->setStartDate(gmdate('Y-m-d\TH:i:s\Z', strtotime("+1 " . $billing_period, $now)))
                ->setPlan($plan)
                ->setPayer($payer)
                ->setOverrideMerchantPreferences($preferences);

            // process billing agreement and get approval link
            try {
                $agreement = $agreement->create($this->apiContext);

                $response = [
                    'success' => true,
                    'approval_url' => $agreement->getApprovalLink()
                ];
            } catch (PayPalConnectionException $e) {
                Log::error("PayPal exception occurred: (" . $e->getCode() . ") " . $e->getData());
                $error = 'An error occurred while processing your billing agreement.';
            } catch (Exception $e) {
                Log::error("Exception occurred: " . $e);
                $error = 'An error occurred while processing your billing agreement.';
            }
        }

        if ($error) {
            $response = [
                'success' => false,
                'error' => $error,
            ];
        }

        return response()->json($response);
    }

    /**
     * Update subscription
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $error = false;
        $plan = $request->input('plan');

        // get provider plan info
        $spark_plan = Spark::plans()->where('id', $plan)->first();

        // get current subscription
        $subscription = Subscription::where('user_id', Auth::user()->id)->first();

        if (empty($subscription) || empty($spark_plan) || empty($plan)) {
            $error = true;
        } else {
            if ($plan == $subscription->provider_plan_name) {
                // this is the same plan and we are resuming the subscription
                $descriptor = new AgreementStateDescriptor();
                $descriptor->setNote("Resuming the subscription");

                try {
                    // get billing agreement
                    $agreement = Agreement::get($subscription->paypal_agreement_id, $this->apiContext);
                    $agreement->reActivate($descriptor, $this->apiContext);
                } catch (Exception $e) {
                    Log::error("Error resuming agreement: " . $e);
                    $error = true;
                }

                // update subscription row
                $subscription->ends_at = null;
                $subscription->save();
            } else {
                // we are changing subscription plans. paypal does not support this
                Log::error("Attempted to switch plan from " . $subscription->provider_plan_name . " to " . $plan);
                $error = true;
            }
        }

        if ($error) {
            return response('Error', 422);
        } else {
            return response('OK', 200);
        }
    }

    /**
     * Cancel (suspend) subscription
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $error = false;
        
        // get current subscription
        $subscription = Subscription::where('user_id', Auth::user()->id)->first();

        if (empty($subscription)) {
            $error = true;
        } else {
            // get provider plan info
            $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

            if (empty($spark_plan) || empty($subscription->paypal_agreement_id)) {
                $error = true;
            } else {
                $descriptor = new AgreementStateDescriptor();
                $descriptor->setNote("Suspending the subscription");

                try {
                    // get billing agreement
                    $agreement = Agreement::get($subscription->paypal_agreement_id, $this->apiContext);
                    $agreement->suspend($descriptor, $this->apiContext);
                } catch (Exception $e) {
                    Log::error("Error suspending agreement: " . $e);
                    $error = true;
                } catch (PayPalConnectionException $e) {
                    Log::error("PayPal error suspending agreement: " . " (" . $e->getCode() . ") " . $e->getData());
                    $error = true;
                }

                // get billing period
                $billing_period = $this->getBillingPeriod($spark_plan->interval);

                // compute for end of grace period
                $end_time = strtotime($subscription->last_payment_at . " +1" . $billing_period);

                // update subscription row
                $subscription->ends_at = date("Y-m-d H:i:s", $end_time);
                $subscription->save();
            }
        }

        if ($error) {
            return response('Error', 422);
        } else {
            return response('OK', 200);
        }
    }

    /**
     * Successful transaction
     *
     * @return Response
     */
    public function success(Request $request)
    {
        $pending_id = $request->input('p');
        $token = $request->input('token');

        if (!$token) {
            Log::info("No token provided");
            return response('Error', 422);
        }

        $agreement = new Agreement();

        // execute the agreement
        try {
            $agreement->execute($token, $this->apiContext);
        } catch (PayPalConnectionException $e) {
            Log::error("Could not execute agreement for token " . $token . ": " . " (" . $e->getCode() . ") " . $e->getData());
            return response('Error', 422);
        } catch (Exception $e) {
            Log::error("Could not execute agreement for token " . $token . ": " . $e);
            return response('Error', 422);
        }

        // refresh agreement info
        try {
            $agreement = Agreement::get($agreement->getId(), $this->apiContext);
        } catch (PayPalConnectionException $e) {
            Log::error("Could not retrieve agreement for token " . $token . ": " . " (" . $e->getCode() . ") " . $e->getData());
            return response('Error', 422);
        } catch (Exception $e) {
            Log::error("Could not retrieve agreement for token " . $token);
            return response('Error', 422);
        }

        // update pending sub
        $user_plan = UserPendingSubscription::find($pending_id);
        $user_plan->agreement_id = $agreement->getId();
        $user_plan->save();

        return redirect('settings#/subscription')->withInput([
            'subscription_success' => true,
            'provider' => 'paypal_recurring'
        ]);
    }

    /**
     * Failed transaction
     *
     * @return Response
     */
    public function failure(Request $request)
    {
        $pending_id = $request->input('p');

        UserPendingSubscription::destroy($pending_id);

        return redirect('settings#/subscription')->withInput([
            'subscription_success' => false,
            'provider' => 'paypal_recurring'
        ]);
    }
}
