<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PaymentProcessorService;

use App\Models\PaypalIPN;
use App\PaypalNotification;
use App\CoinbaseNotification;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Laravel\Spark\Subscription;

class PaymentNotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    
    /**
     * Handle PayPal Instant Payment Notifications (IPNs)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function listenPaypal(Request $request)
    {
        $ipn = new PaypalIPN($request);

        // set sandbox mode?
        if (!config('services.paypal.live')) {
            $ipn->useSandbox();
        }

        // verify this notification
        $verified = $ipn->verifyIPN();

        if ($verified) {
            $txn_type = $request->input('txn_type') ?? null;

            // save notification
            $notification = new PaypalNotification;
            $notification->notification_type = $txn_type;
            $notification->request_details = json_encode($request->all());
            $notification->save();

            Log::info("PayPal notification received", $request->all());

            // detect if this is a chargeback
            if (!empty($request->input('reason_code'))) {
                $chargeback_codes = [
                    'chargeback',
                    'unauthorized_claim',
                    'unauthorized_spoof',
                    'admin_fraud_reversal',
                    'unauthorized',
                    'chargeback_reimbursement',
                    'buyer_complaint'
                ];

                if (in_array($request->input('reason_code'), $chargeback_codes)) {
                    // find the user based on payer ID
                    $user_id = Subscription::where('paypal_payer_id', $request->input('payer_id'))->value('user_id');

                    if ($user_id) {
                        // get and the linked user
                        $user = User::find($user_id);
                        $user->ban();

                        // mark user as frauder
                        $user->assignRole('frauder');
                    }
                }
            } else {
                // get payment processor depending on IPN type
                switch ($txn_type) {
                    case 'recurring_payment_profile_created':
                    case 'recurring_payment':
                    case 'recurring_payment_profile_cancel':
                    case 'recurring_payment_suspended_due_to_max_failed_payment':
                        $processor = PaymentProcessorService::getPaymentProcessor('paypal_recurring', $request);
                        break;
                    case 'cart':
                        $processor = PaymentProcessorService::getPaymentProcessor('paypal_manual', $request);
                        break;
                }

                // process notification
                return $processor->handleNotification();
            }
        } else {
            // this notification cannot be verified
            Log::error('Unverified PayPal notification received', $request->all());
            abort(422);
        }
    }

    /**
     * Handle Coinbase Notifications
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function listenCoinbase(Request $request)
    {
        // get payment processor
        $processor = PaymentProcessorService::getPaymentProcessor('coinbase', $request);

        // verify this notification
        $signature = $request->server('HTTP_CB_SIGNATURE');
        $verified = $processor->getClient()->verifyCallback($request->getContent(), $signature);

        if ($verified) {
            // save notification
            $notification = new CoinbaseNotification();
            $notification->notification_type = $input['txn_type'];
            $notification->request_details = json_encode($input);
            $notification->save();

            Log::info("Coinbase notification received", $request->all());

            // process notification
            return $processor->handleNotification();
        } else {
            // this notification cannot be verified
            Log::error('Unverified Coinbase notification received', $request->all());
            abort(422);
        }
    }
}
