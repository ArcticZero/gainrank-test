<?php

namespace App\Http\Controllers;

use App\AlexaSubmission;
use App\AlexaSetting;
use App\AlexaStatus;
use App\Gainrank\AlexaAPI;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class AlexaController extends Controller
{
    protected $kiosk_per_page = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function test()
    {
        /*
        $indexed_settings = [
            'version' => 1,
            'enabled_add' => 1,
            'enabled_update' => 1,
            'enabled_display' => 1,
            'url_update' => 'http://control.publicdnsroute.com/api/seo_update_alexa.php',
            'url_add' => 'http://control.publicdnsroute.com/api/seo_add_alexa.php',
            'api_key' => '9ADFE67882901EFCABF27377116',
        ];
        // test api calls
        $api = new AlexaAPI($indexed_settings);
        */
        $api = AlexaAPI::initialize();


        // add
        // $api->add('www.munchpunch.com', 5);

        $api->update(5, 1);
    }

    /**
     * Get current version of API
     *
     * @return string
     */
    private function getCurrentVersion()
    {
        $version = AlexaSetting::where([
            ['version', '=', 0],
            ['name', 'current_version']
        ])
            ->select('value')
            ->pluck('value')
            ->first();
        ;

        return $version;
    }

    /**
     * Display the client view.
     *
     * @return Response
     */
    public function showAlexa(Request $request, $limit = 100)
    {
        $data = [
            'rows' => []
        ];

        // get submissions from db
        $rows = $request->user()
            ->alexaSubmissions()
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();

        foreach ($rows as $row) {
            $row['status'] = !empty($row->status) ? $row->status->cust_message : $row->id_status;
            $data['rows'][] = $row;
        }

        return view('services.alexa.alexa', $data);
    }

    /**
     * Display the client view with test data.
     *
     * @return Response
     */
    public function showAlexaTestData()
    {
        // this is dummy data
        $data = [
            // keyword rows
            'rows' => [
                [
                    'id' => 1,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 2,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 3,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 4,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 5,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 6,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 7,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 8,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 9,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 10,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 11,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 12,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 13,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 14,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 15,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 16,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 17,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 18,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 19,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 20,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'wanted_count' => 1000,
                    'sent_count' => 500,
                    'status' => 'Ok'
                ]
            ]
        ];

        return view('services.alexa.alexa', $data);
    }

    /**
     * Add a new submission.
     *
     * @return Response
     */
    public function add(Request $request)
    {
        // validate request
        $this->validate($request, [
            'url' => 'required',
            'amt' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/'
        ]);

        // initialize API
        $api = AlexaAPI::initialize();

        // add submission
        $api->add($request->input('url'), $request->input('amt'));

        // return
        return response('Ok', 200);
    }

    /**
     * Delete a submission.
     *
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Get a list of all submissions.
     *
     * @return Response
     */
    public function getSubmissions(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve rows
        $query = AlexaSubmission::orderBy('created_at', 'desc');

        if ($filter) {
            // get total rows (filtered)
            $total = AlexaSubmission::where('url', 'like', '%' . $filter . '%')
                ->orWhere('ip_addr', 'like', '%' . $filter . '%')
                ->orWhereHas('user', function ($query) use ($filter) {
                    $query->where('name', 'like', '%' . $filter . '%');
                })
                ->count();

            // apply filter to query
            $query->where('url', 'like', '%' . $filter . '%')
                ->orWhere('ip_addr', 'like', '%' . $filter . '%')
                ->orWhereHas('user', function ($query) use ($filter) {
                    $query->where('name', 'like', '%' . $filter . '%');
                });
        } else {
            // get total rows (all)
            $total = AlexaSubmission::count();
        }

        // limit and offset
        $rows = $query->offset(($page - 1) * $this->kiosk_per_page)
            ->limit($this->kiosk_per_page)
            ->get();

        // format results
        foreach ($rows as $row) {
            $rowdata = $row->toArray();
            $rowdata['user_name'] = $row->user->name;
            $rowdata['engine_name'] = $row->engine->name;
            $items[] = $rowdata;
        }

        // DEBUG: this is dummy data
        $items = [];
        $total = 25;

        for ($i = 1; $i <= 25; $i++) {
            $items[] = [
                'id' => $i,
                'created_at' => "2017-09-24 01:12:23",
                'user_id' => 1,
                'user_name' => "Kendrick Chan", // $row->user->name
                'submission_id' => 123,
                'url' => 'www.gainrank.com',
                'wanted_count' => 10,
                'sent_count' => 20,
                'cost_per_x' => 100,
                'cost_per_amt' => 1000,
                'ip_addr' => '208.67.222.222',
                'status_text' => "Ok" // $row->status->staff_message (?)
            ];
        }

        $items = array_slice($items, ($this->kiosk_per_page * ($page - 1)), $this->kiosk_per_page);
        // END dummy data

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->kiosk_per_page, $page);

        return $paginator;
    }

    /**
     * Get a list of all statuses.
     *
     * @return Response
     */
    public function getStatuses(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve rows
        $query = AlexaStatus::orderBy('id', 'asc');

        if ($filter) {
            // get total rows (filtered)
            $total = AlexaStatus::where('id', $filter)
                ->orWhere('name', 'like', '%' . $filter . '%')
                ->orWhere('code', 'like', '%' . $filter . '%')
                ->orWhere('cust_message', 'like', '%' . $filter . '%')
                ->orWhere('staff_message', 'like', '%' . $filter . '%')
                ->orWhere('cust_url', 'like', '%' . $filter . '%')
                ->count();

            // apply filter to query
            $query->where('id', $filter)
                ->orWhere('name', 'like', '%' . $filter . '%')
                ->orWhere('code', 'like', '%' . $filter . '%')
                ->orWhere('cust_message', 'like', '%' . $filter . '%')
                ->orWhere('staff_message', 'like', '%' . $filter . '%')
                ->orWhere('cust_url', 'like', '%' . $filter . '%');
        } else {
            // get total rows (all)
            $total = AlexaStatus::count();
        }

        // limit and offset
        $items = $query->offset(($page - 1) * $this->kiosk_per_page)
            ->limit($this->kiosk_per_page)
            ->get()
            ->toArray();

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->kiosk_per_page, $page);

        return $paginator;
    }

    /**
     * Create a new status.
     *
     * @return Response
     */
    public function addStatus(Request $request)
    {
        // validate input
        $this->validate($request, [
            'id' => 'required|unique:alexa_statuses,id|integer|digits_between:1,6',
            'name' => 'required|unique:alexa_statuses,name',
            'code' => 'required|unique:alexa_statuses,code',
            'cust_message' => 'required',
            'staff_message' => 'required',
            'cust_url' => 'required'
        ]);

        // save to db
        $status = new AlexaStatus;
        $status->id = $request->input('id');
        $status->name = $request->input('name');
        $status->code = $request->input('code');
        $status->cust_message = $request->input('cust_message');
        $status->staff_message = $request->input('staff_message');
        $status->cust_url = $request->input('cust_url');
        $status->save();

        // return
        return response('Ok', 200);
    }

    /**
     * Save an existing status.
     *
     * @return Response
     */
    public function updateStatus(Request $request, $id)
    {
        // validate input
        $this->validate($request, [
            'id' => 'exists:alexa_statuses|in:' . $id,
            'name' => 'required|unique:alexa_statuses,name, ' . $id,
            'code' => 'required|unique:alexa_statuses,code, ' . $id,
            'cust_message' => 'required',
            'staff_message' => 'required',
            'cust_url' => 'required'
        ]);

        // save to db
        $status = AlexaStatus::find($id);
        $status->name = $request->input('name');
        $status->code = $request->input('code');
        $status->cust_message = $request->input('cust_message');
        $status->staff_message = $request->input('staff_message');
        $status->cust_url = $request->input('cust_url');
        $status->save();

        // return
        return response('Ok', 200);
    }

    /**
     * Delete an existing status.
     *
     * @return Response
     */
    public function destroyStatus(Request $request, $id)
    {
        // delete row if it exists
        AlexaStatus::destroy($id);

        // return
        return response('Ok', 200);
    }

    /**
     * Get a list of all settings.
     *
     * @return Response
     */
    public function getSettings(Request $request)
    {
        // get current version
        $version = $this->getCurrentVersion();

        // get our settings for that version
        $indexed_settings = [
            'version' => $version
        ];

        $settings = [];

        $sdata = AlexaSetting::where('version', $version)
            ->select(['name', 'value'])
            ->get();

        foreach ($sdata as $srow) {
            $settings[$srow->name] = $srow->value;
        }

        return $settings;
    }

    /**
     * Save settings.
     *
     * @return Response
     */
    public function updateSettings(Request $request)
    {
        // get current version
        $version = $this->getCurrentVersion();

        // update settings if they exist
        foreach ($request->all() as $name => $value) {
            $setting = AlexaSetting::where('version', $version)
                ->where('name', $name)
                ->first();

            if ($setting) {
                $setting->value = $value;
                $setting->save();
            }
        }

        // return
        return response('Ok', 200);
    }
}
