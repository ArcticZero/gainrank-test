<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PaymentProcessorService;
use App\Services\SalesInvoiceHandler;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

use Laravel\Spark\Spark;

class PostCheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    /**
     * Handle a successful checkout
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string   $billing_provider
     * @return \Illuminate\Http\Response
     */
    public function doCheckoutSuccess(Request $request, string $billing_provider)
    {
        return $this->doPostCheckout($request, $billing_provider, true);
    }

    /**
     * Handle a failed checkout
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string   $billing_provider
     * @return \Illuminate\Http\Response
     */
    public function doCheckoutFailure(Request $request, string $billing_provider)
    {
        return $this->doPostCheckout($request, $billing_provider, false);
    }

    /**
     * Complete the checkout process
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string   $billing_provider
     * @param  boolean  $success
     * @return \Illuminate\Http\Response
     */
    private function doPostCheckout(Request $request, string $billing_provider, bool $success)
    {
        // get all valid payment methods
        $providers = config('billing.providers');

        // invalid billing provider given
        if (!in_array($billing_provider, $providers)) {
            abort(404);
        }

        if ($success) {
            // handle post-checkout based on billing provider
            switch ($billing_provider) {
                case 'paypal_recurring':
                    return $this->doPaypalRecurring($request);
                case 'paypal_manual':
                    return $this->doPaypalManual($request);
                case 'coinbase':
                    return $this->doCoinbase($request);
            }
        } else {
            // checkout has failed or was cancelled, delete pending sub
            $user_plan = UserPendingSubscription::where('user_id', $request->user()->id)
                ->where('billing_provider', $billing_provider)
                ->delete();

            return redirect('settings#/subscription')->withInput([
                'subscription_success' => false,
                'provider' => $billing_provider
            ]);
        }
    }

    /**
     * Handle post-checkout for PayPal (Recurring Transactions)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function doPaypalRecurring(Request $request)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('paypal_recurring', $request);

        // do post-checkout
        $subscription = $processor->subscriptionPostCheckout();

        // generate invoice
        $invoice = $this->generateInvoice($subscription);

        // return response
        return redirect('settings#/subscription')->withInput([
            'subscription_success' => $subscription === false ? false : true,
            'provider' => $processor->getId()
        ]);
    }

    /**
     * Handle post-checkout for PayPal (Manual Transactions)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function doPaypalManual(Request $request)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('paypal_manual', $request);

        // do post-checkout
        $subscription = $processor->subscriptionPostCheckout();

        // generate invoice
        $invoice = $this->generateInvoice($subscription);

        // return response
        return redirect('settings#/subscription')->withInput([
            'subscription_success' => $subscription === false ? false : true,
            'provider' => $processor->getId()
        ]);
    }

    /**
     * Handle post-checkout for Coinbase
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function doCoinbase(Request $request)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('coinbase', $request);

        // do post-checkout
        $subscription = $processor->subscriptionPostCheckout();

        // generate invoice
        $invoice = $this->generateInvoice($subscription);

        // return response
        return redirect('settings#/subscription')->withInput([
            'subscription_success' => $subscription === false ? false : true,
            'provider' => $processor->getId()
        ]);
    }

    /**
     * Create sales invoice for the order
     *
     * @param  App\UserPendingSubscription  $subscription
     * @return App\SalesInvoice
     */
    private function generateInvoice($subscription)
    {
        // instantiate service
        $invoice = new SalesInvoiceHandler();

        // get spark plan
        $spark_plan = Spark::plans()->where('id', $subscription->provider_plan_name)->first();

        // build data for invoice
        $items = [
            [
                'description' => $spark_plan->name,
                'amount' => $spark_plan->price,
                'currency' => config('billing.currency'),
                'quantity' => 1
            ]
        ];

        // generate invoice
        return $invoice->generate($subscription->user, $items);
    }
}
