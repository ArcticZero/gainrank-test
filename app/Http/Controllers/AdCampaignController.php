<?php

namespace App\Http\Controllers;

use App\Models\PaypalBootstrap;
use App\Models\CoinbaseBootstrap;

use App\AdCampaign;
use App\AdCampaignBonus;
use App\AdCampaignBonusType;
use App\Reward;
use App\PaypalPlan;
use App\CoinbaseCheckout;

use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Patch;
use PayPal\Common\PayPalModel;
use PayPal\Api\PatchRequest;
use PayPal\Api\Currency;
use PayPal\Exception\PayPalConnectionException;

use Coinbase\Wallet\Resource\Checkout;
use Coinbase\Wallet\Value\Money;
use Coinbase\Wallet\Exception\AuthenticationException;
use Coinbase\Wallet\Exception\InvalidTokenException;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Laravel\Cashier\Cashier;
use Laravel\Spark\Spark;

use \DateTime;

class AdCampaignController extends Controller
{
    protected $currency;
    protected $paypal;
    protected $coinbase;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:administer ad campaigns', ['except' => ['view']]);

        $this->currency = strtoupper(Cashier::usesCurrency());
        $this->paypal = PaypalBootstrap::initialize();
        $this->coinbase = CoinbaseBootstrap::initialize();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AdCampaign::with('bonuses')->orderBy('created_at', 'desc')->paginate(20);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'bonuses.*' => 'numeric',
            'description' => 'required',
            'start_date' => 'date_format:"Y-m-d"',
            'end_date' => 'date_format:"Y-m-d"',
            'max_uses' => 'integer',
            'path' => 'required|max:255|alpha_dash|unique:ad_campaigns,path',
            'active' => 'boolean',
        ]);
        $campaign = new AdCampaign;
        $campaign->name = $request->input('name');
        $campaign->description = $request->input('description');
        $campaign->start_date = $request->input('start_date') . ' 00:00:00';
        $campaign->end_date = $request->input('end_date') . ' 00:00:00';
        $campaign->max_uses = $request->input('max_uses');
        $campaign->path = $request->input('path');
        $campaign->active = $request->input('active');
        $campaign->save();
        foreach ($request->input('bonuses') as $type_id => $value) {
            if (!empty($value)) {
                $type = AdCampaignBonusType::find($type_id);
                $bonus = new AdCampaignBonus;
                $bonus->bonus_amount = $value;
                $bonus->type()->associate($type);
                $bonus->campaign()->associate($campaign);
                $bonus->save();
            }
        }

        // create discounted plans
        $recurring_discounts = $this->getDiscounts($campaign, Reward::TYPE_DISCOUNT_RECURRING_AMOUNT, Reward::TYPE_DISCOUNT_RECURRING_PERCENT);
        $initial_discounts = $this->getDiscounts($campaign, Reward::TYPE_DISCOUNT_INITIAL_AMOUNT, Reward::TYPE_DISCOUNT_INITIAL_PERCENT);
        $spark_plans = $this->getSparkPlans();

        // begin db transaction
        DB::beginTransaction();

        foreach ($spark_plans as $spark_plan) {
            // check if discounted rates are valid
            $sp_data = $spark_plan['plan'];
            $discounted_recurring = $this->applyDiscounts($sp_data->price, $recurring_discounts);
            $discounted_initial = $this->applyDiscounts($sp_data->price, $initial_discounts);

            if (!$discounted_recurring) {
                // recurring price is below 0
                $error = 'Discounted recurring price for Plan "' . $sp_data->name . '" is less than 0. The base price is ' . $this->currency . ' ' . $sp_data->price . '.';

                Log::error($error, [
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id,
                    'recurring_discounts' => $recurring_discounts,
                    'original_price' => $sp_data->price
                ]);

                DB::rollBack();

                return response()->json([
                    'status' => $error
                ], 422);
            } elseif (!$discounted_initial) {
                // initial month price is below 0
                $error = 'Discounted inital month price for Plan "' . $sp_data->name . '" is less than 0. The base price is ' . $this->currency . ' ' . $sp_data->price . '.';

                Log::error($error, [
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id,
                    'initial_discounts' => $initial_discounts,
                    'original_price' => $sp_data->price
                ]);

                DB::rollBack();

                return response()->json([
                    'status' => $error
                ], 422);
            } else {
                // create paypal plan
                $pp = $this->createDiscountedPaypalPlan($campaign, $spark_plan, $discounted_recurring);

                if (!$pp['success']) {
                    DB::rollBack();

                    return response()->json([
                        'status' => $pp['error']
                    ], 422);
                }

                // create coinbase checkout
                $cb = $this->createDiscountedCoinbaseCheckouts($campaign, $spark_plan, $discounted_initial, $discounted_recurring);

                if (!$cb['success']) {
                    DB::rollBack();

                    return response()->json([
                        'status' => $cb['error']
                    ], 422);
                }
            }
        }

        // no more errors, commit changes to db
        DB::commit();

        // TODO: % bonus on first deposit

        return response('Ok', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdCampaign $campaign)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'bonuses.*' => 'numeric',
            'description' => 'required',
            'start_date' => 'date_format:"Y-m-d"',
            'end_date' => 'date_format:"Y-m-d"',
            'max_uses' => 'integer',
            'path' => 'required|max:255|alpha_dash|unique:ad_campaigns,path,'.$campaign->id,
            'active' => 'boolean',
        ]);

        $campaign->fill([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'start_date' => $request->input('start_date') . ' 00:00:00',
            'end_date' => $request->input('end_date') . ' 00:00:00',
            'max_uses' => $request->input('max_uses'),
            'path' => $request->input('path'),
            'active' => $request->input('active'),
        ])->save();

        $bonuses = $request->input('bonuses');

        foreach ($campaign->bonuses as $bonus) {
            if (!in_array($bonus->ad_campaign_bonus_type_id, array_keys($bonuses)) || empty($bonuses[$bonus->ad_campaign_bonus_type_id])) {
                $bonus->delete();
            } else {
                $bonus->bonus_amount = $bonuses[$bonus->ad_campaign_bonus_type_id];
                $bonus->save();
            }
            unset($bonuses[$bonus->ad_campaign_bonus_type_id]);
        }

        foreach ($bonuses as $type_id => $value) {
            if (!empty($value)) {
                $type = AdCampaignBonusType::find($type_id);
                $bonus = new AdCampaignBonus;
                $bonus->bonus_amount = $value;
                $bonus->type()->associate($type);
                $bonus->campaign()->associate($campaign);
                $bonus->save();
            }
        }

        // create discounted plans
        $recurring_discounts = $this->getDiscounts($campaign, Reward::TYPE_DISCOUNT_RECURRING_AMOUNT, Reward::TYPE_DISCOUNT_RECURRING_PERCENT);
        $initial_discounts = $this->getDiscounts($campaign, Reward::TYPE_DISCOUNT_INITIAL_AMOUNT, Reward::TYPE_DISCOUNT_INITIAL_PERCENT);
        $spark_plans = $this->getSparkPlans();

        // begin db transaction
        DB::beginTransaction();

        foreach ($spark_plans as $spark_plan) {
            // check if discounted rates are valid
            $sp_data = $spark_plan['plan'];
            $discounted_recurring = $this->applyDiscounts($sp_data->price, $recurring_discounts);
            $discounted_initial = $this->applyDiscounts($sp_data->price, $initial_discounts);

            if (!$discounted_recurring) {
                // recurring price is below 0
                $error = 'Discounted recurring price for Plan "' . $sp_data->name . '" is less than 0. The base price is ' . $this->currency . ' ' . $sp_data->price . '.';

                Log::error($error, [
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id,
                    'recurring_discounts' => $recurring_discounts,
                    'original_price' => $sp_data->price
                ]);

                DB::rollBack();

                return response()->json([
                    'status' => $error
                ], 422);
            } elseif (!$discounted_initial) {
                // initial month price is below 0
                $error = 'Discounted inital month price for Plan "' . $sp_data->name . '" is less than 0. The base price is ' . $this->currency . ' ' . $sp_data->price . '.';

                Log::error($error, [
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id,
                    'initial_discounts' => $initial_discounts,
                    'original_price' => $sp_data->price
                ]);

                DB::rollBack();

                return response()->json([
                    'status' => $error
                ], 422);
            } else {
                // create paypal plan
                $pp = $this->createDiscountedPaypalPlan($campaign, $spark_plan, $discounted_recurring);

                if (!$pp['success']) {
                    DB::rollBack();

                    return response()->json([
                        'status' => $pp['error']
                    ], 422);
                }

                // create coinbase checkout
                $cb = $this->createDiscountedCoinbaseCheckouts($campaign, $spark_plan, $discounted_initial, $discounted_recurring);

                if (!$cb['success']) {
                    DB::rollBack();

                    return response()->json([
                        'status' => $cb['error']
                    ], 422);
                }
            }
        }

        // no more errors, commit changes to db
        DB::commit();

        // TODO: % bonus on first deposit

        return response('Ok', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdCampaign $campaign)
    {
        $campaign->delete();
    }

    /**
     * Return all AdCampaignBonusType's.
     * @return mixed
     */
    public function bonusTypes()
    {
        return AdCampaignBonusType::all()->pluck('type', 'id')->toArray();
    }

    /**
     * Register a new User account with the benefits specified in this campaign.
     * @param string $path
     * @return \Illuminate\Http\Response
     */
    public function view(String $path = '')
    {
        $campaign = AdCampaign::where([
            ['path', '=', $path],
        ])->first();
        // Check if the provided path corresponds with a valid campaign
        if (empty($campaign)) {
            abort(404);
        } else {
            $invalid = false;
            // Check if the campaign is still valid/active.
            if (empty($campaign->active)) {
                $invalid = true;
            }
            // Check if the campaign is currently still running
            $current_time = time();
            if (strtotime($campaign->start_date) >= $current_time || strtotime($campaign->end_date) < $current_time) {
                $invalid = true;
            }
            // Check if the campaign has reached its quota
            if ($campaign->max_uses != 0 && $campaign->users()->count() >= $campaign->max_uses) {
                $invalid = true;
            }
            if (!empty($invalid)) {
                return redirect('register')->with('warning', 'Unfortunately this promotion is no longer available.
                Feel free to register as a regular user though!');
            } else {
                return redirect()->route('register', ['campaign' => $path]);
            }
        }
    }

    /**
     * Create PayPal plan at discounted rate for this campaign.
     * @param AdCampaign $campaign
     * @param array $spark_plan
     * @param float $price
     * @return mixed
     */
    private function createDiscountedPaypalPlan($campaign, $spark_plan, $price)
    {
        $sp_data = $spark_plan['plan'];

        // retrieve any existing plan pairing
        $pp_plan = PaypalPlan::where('plan_id', $sp_data->id)
            ->where('ad_campaign_id', $campaign->id)
            ->first();

        if ($pp_plan) {
            // delete existing paypal plan
            $existing_plan = Plan::get($pp_plan->paypal_plan_id, $this->paypal);
            $existing_plan->delete($this->paypal);
        } else {
            // create a new plan pairing
            $pp_plan = new PaypalPlan();
            $pp_plan->plan_id = $sp_data->id;
            $pp_plan->ad_campaign_id = $campaign->id;
        }

        $return_url = url('settings/subscription/checkout/paypal_recurring');

        // add trial definition if trial days is set
        if ($sp_data->trialDays > 0) {
            $trial_definition = new PaymentDefinition();
            $trial_definition->setName('Trial Subscription')
                ->setType('TRIAL')
                ->setFrequency('Day')
                ->setFrequencyInterval($sp_data->trialDays)
                ->setCycles("1")
                ->setAmount(new Currency(
                    array(
                        'currency' => $this->currency,
                        'value' => 0
                    )
                ));
        }

        // set payment definition
        $definition = new PaymentDefinition();
        $definition->setName('Subscription Payments (Discounted)')
            ->setType('REGULAR')
            ->setFrequency($spark_plan['billing_period'])
            ->setFrequencyInterval("1")
            ->setCycles("0")
            ->setAmount(new Currency(
                array(
                    'currency' => $this->currency,
                    'value' => $price
                )
            ));

        // set merchant preferences
        $preferences = new MerchantPreferences();
        $preferences->setReturnUrl($return_url . "/success")
            ->setCancelUrl($return_url . "/failure")
            ->setAutoBillAmount("YES")
            ->setInitialFailAmountAction("CANCEL")
            ->setMaxFailAttempts("3");
            /*
            ->setSetupFee(new Currency(
                array(
                    'currency' => $this->currency,
                    'value' => $sp_data->price
                )
            ));
            */

        // set up payment plan
        $plan = new Plan();
        $plan->setName(ucfirst($sp_data->interval) . ' Plan')
            ->setDescription('Regular Subscription')
            ->setType('INFINITE')
            ->setPaymentDefinitions(array($definition))
            ->setMerchantPreferences($preferences);

        // create plan
        try {
            $output = $plan->create($this->paypal);
        } catch (PayPalConnectionException $e) {
            $error = 'PayPal exception occurred while creating plan';

            Log::error($error, [
                'code' => $e->getCode(),
                'data' => $e->getData(),
                'message' => $e->getMessage(),
                'ad_campaign_id' => $campaign->id,
                'spark_plan_id' => $sp_data->id
            ]);
            
            return [
                'success' => false,
                'error' => $error
            ];
        } catch (Exception $e) {
            $error = 'Exception occurred while creating plan';

            Log::error($error, [
                'message' => $e->getMessage(),
                'ad_campaign_id' => $campaign->id,
                'spark_plan_id' => $sp_data->id
            ]);
            
            return [
                'success' => false,
                'error' => $error
            ];
        }

        // activate plan
        try {
            $patch = new Patch();

            $pvalue = new PayPalModel('{
                "state": "ACTIVE"
            }');

            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($pvalue);

            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);

            $plan->update($patchRequest, $this->paypal);
        } catch (PayPalConnectionException $e) {
            $error = 'PayPal exception occurred while activating plan';

            Log::error($error, [
                'code' => $e->getCode(),
                'data' => $e->getData(),
                'message' => $e->getMessage(),
                'ad_campaign_id' => $campaign->id,
                'spark_plan_id' => $sp_data->id
            ]);
            
            return [
                'success' => false,
                'error' => $error
            ];
        } catch (Exception $e) {
            $error = 'Exception occurred while activating plan';

            Log::error($error, [
                'message' => $e->getMessage(),
                'ad_campaign_id' => $campaign->id,
                'spark_plan_id' => $sp_data->id
            ]);
            
            return [
                'success' => false,
                'error' => $error
            ];
        }

        // update plan pairing
        $pp_plan->paypal_plan_id = $plan->getId();
        $pp_plan->save();

        return [
            'success' => true,
            'plan' => $pp_plan
        ];
    }

    /**
     * Create Coinbase checkouts at discounted rate for this campaign.
     * @param AdCampaign $campaign
     * @param array $spark_plan
     * @param float $price
     * @return mixed
     */
    private function createDiscountedCoinbaseCheckouts($campaign, $spark_plan, $initial_price, $recurring_price)
    {
        $sp_data = $spark_plan['plan'];

        // build checkout parameters for initial and recurring
        $price_data = [
            'initial' => [
                'price' => $initial_price,
                'discount_type' => CoinbaseCheckout::DISCOUNT_TYPE_INITIAL,
                'reward_types' => [
                    Reward::TYPE_DISCOUNT_INITIAL_PERCENT,
                    Reward::TYPE_DISCOUNT_INITIAL_AMOUNT
                ]
            ],
            'recurring' => [
                'price' => $recurring_price,
                'discount_type' => CoinbaseCheckout::DISCOUNT_TYPE_RECURRING,
                'reward_types' => [
                    Reward::TYPE_DISCOUNT_RECURRING_PERCENT,
                    Reward::TYPE_DISCOUNT_RECURRING_AMOUNT
                ]
            ]
        ];

        $checkouts = [];

        // create separate checkout for initial and recurring
        foreach ($price_data as $key => $data) {
            // retrieve any existing checkout pairing
            $cb_checkout = CoinbaseCheckout::where('plan_id', $sp_data->id)
                ->where('ad_campaign_id', $campaign->id)
                ->where('discount_type', $data['discount_type'])
                ->first();

            if (!$cb_checkout) {
                // create a new checkout pairing
                $cb_checkout = new CoinbaseCheckout();
                $cb_checkout->plan_id = $sp_data->id;
                $cb_checkout->ad_campaign_id = $campaign->id;
                $cb_checkout->discount_type = $data['discount_type'];
            }

            // Note: Skipping deletion of obsolete checkouts since there is no way to do this programmatically as of now

            // build checkout object
            $return_url = url('settings/subscription/checkout/coinbase');

            $params = array(
                'name'          => 'Gainrank Subscription',
                'amount'        => new Money($data['price'], $this->currency),
                'description'   => $sp_data->name,
                'successUrl'    => $return_url . "/success",
                'cancelUrl'     => $return_url . "/failure"
            );

            try {
                $checkout = new Checkout($params);
                $this->coinbase->createCheckout($checkout);
            } catch (AuthenticationException $e) {
                $error = 'Coinbase Authentication Exception occurred';

                Log::error($error, [
                    'message' => $e->getMessage(),
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id
                ]);
                
                return [
                    'success' => false,
                    'error' => $error
                ];
            } catch (InvalidTokenException $e) {
                $error = 'Coinbase Token Authentication Exception occurred';

                Log::error($error, [
                    'message' => $e->getMessage(),
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id
                ]);
                
                return [
                    'success' => false,
                    'error' => $error
                ];
            } catch (Exception $e) {
                $error = 'Exception occurred';

                Log::error($error, [
                    'message' => $e->getMessage(),
                    'ad_campaign_id' => $campaign->id,
                    'spark_plan_id' => $sp_data->id
                ]);
                
                return [
                    'success' => false,
                    'error' => $error
                ];
            }

            // update plan pairing
            $cb_checkout->checkout_id = $checkout->getId();
            $cb_checkout->checkout_embed_code = $checkout->getEmbedCode();
            $cb_checkout->save();

            $checkouts[$key] = $cb_checkout;
        }
            

        return [
            'success' => true,
            'plans' => $checkouts
        ];
    }

    /**
     * Get all Spark plans and billing periods
     * @return array
     */
    private function getSparkPlans()
    {
        $plans = [];

        $spark_plans = Spark::allPlans()->filter(function ($plan) {
            return $plan->price > 0;
        });

        foreach ($spark_plans as $spark_plan) {
            switch ($spark_plan->interval) {
                case 'monthly':
                    $billing_period = 'Month';
                    break;
                case 'yearly':
                    $billing_period = 'Year';
                    break;
            }

            $plans[] = [
                'plan' => $spark_plan,
                'billing_period' => $billing_period
            ];
        }

        return $plans;
    }

    /**
     * Apply discount to price
     * @param float $price
     * @param array $discounts
     * @return float
     */
    private function applyDiscounts($price, $discounts)
    {
        // apply flat amount discount
        if ($discounts['amount']) {
            $price = bcsub($price, $discounts['amount']);
        }

        // apply percentage discount
        if ($discounts['percent']) {
            $price = bcmul($price, bcsub(1, $discounts['percent']));
        }

        // return false if price is not greater than 0
        return $price > 0 ? $price : false;
    }

    /**
     * Get discounts for this AdCampaign that apply to a specific set of payments (RECURRING, INITIAL)
     * @param AdCampaign $campaign
     * @return array
     */
    private function getDiscounts($campaign, $amount_discount, $percent_discount)
    {
        $discount_amount_value = 0;
        $discount_percent_value = 0;

        // check if recurring rate is actually discounted
        $bonuses = $campaign->bonuses()
            ->whereHas('type', function ($q) use ($amount_discount, $percent_discount) {
                $q->whereIn('reward_type', [
                    $amount_discount,
                    $percent_discount,
                ]);
            })
            ->get();

        // if bonuses are found take note
        if (!$bonuses->isEmpty()) {
            foreach ($bonuses as $bonus) {
                if ($bonus->bonus_amount) {
                    switch ($bonus->type->reward_type) {
                        case $amount_discount:
                            $discount_amount_value = $bonus->bonus_amount;
                            break;
                        case $percent_discount:
                            $discount_percent_value = bcdiv($bonus->bonus_amount, 100);
                            break;
                    }
                }
            }
        }

        return [
            'amount' => $discount_amount_value,
            'percent' => $discount_percent_value
        ];
    }
}
