<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:administer posts', ['except' => ['view']]);
    }

    /**
     * If no Post slug is provided display the main blog else show the Post itself.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function view($slug = '')
    {
        if (empty($slug)) {
            return view('posts.index')->withPosts(Post::where('published', true)->with('tags')->orderBy('created_at', 'desc')->paginate(7));
        } else {
            // TODO 404 page if the slug doesn't exist (show() expects a Post otherwise we'll get a type error for null)
            return $this->show(Post::where('slug', $slug)->with('tags')->first());
        }
    }

    /**
     * Return a paginated list of Posts ordered by date.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Post::with(array(
            'tags' => function($query){
                $query->select('id','name');
            },
            'media'
        ))->orderBy('created_at', 'desc')->paginate(5);
    }

    /**
     * Create a new Post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255|unique:posts,title',
            'body' => 'required',
            'selectedFile.image' => 'required',
            'selectedFile.name' => 'required',
        ]);

        $tags = $request->get('selectedCategories');

        if (!empty($tags) && is_array($tags)) {
            foreach ($tags as $key => $tag) {
                $tags[$key] = strtolower($tag);
            }
        } else {
            $tags = ['general'];
        }

        $post = Post::create([
            'title' => $request->get('title'),
            'body' => $request->get('body'),
            'published' => $request->get('published'),
        ]);

        $post->syncTags($tags);
        $inputFile = $request->get('selectedFile');
        $filePath = PostController::base64ToFile($inputFile['image'], $inputFile['name']);

        /*
         * If addMedia() throws an \ErrorException about "rename(): (...) Operation not permitted", this is related to
         * https://github.com/spatie/laravel-medialibrary/issues/588
         * Leaving the note below for future reference but our PR has been merged so this should no longer be an issue.
         * If you need to check out an example of how to override a req in composer check the commit AFTER
         * db186c277ef3dee99dcd9e1fdf65c0afb1363ebb
         * * * * * * * * *
         * The problem is that rename() copies the file but cannot use chown() and chmod() across volumes,
         * so it throws a warning causing the exception.
         * Our workaround was to fork spatie/image, replace rename() with copy() and unlink(), and require the
         * fork in composer.json.
         * A MR has been submitted (https://github.com/spatie/image/pull/18) but, if you need to use a new version and
         * the MR hasn't been merged yet, you will have to update spyrosk/image by pulling the latest upstream
         * changes or fork spatie/image again, apply the fix, update composer.json and run composer update.
         */
        $post->addMedia($filePath)->toMediaCollection('images');
        // Easier than trying to empty the folder first and then call rmdir().
        system('rm -rf ' . escapeshellarg(dirname($filePath)));
        return $post;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        // TODO create a nice 404 page
        // TODO decide if this is needed since posts are reachable by slug in view() above.
        return view('posts.show')->with('post', $post)->with('posts', Post::where('published', true)->orderBy('created_at', 'desc')->take(3)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        Validator::make($request->all(), [
            'title' => [
                'required',
                'max:255',
                Rule::unique('posts')->ignore($post->id),
            ],
            'body' => [
                'required',
            ],
        ])->validate();

        $post->fill([
            'title' => $request->get('title'),
            'body' => $request->get('body'),
            'published' => $request->get('published'),
        ])->save();

        $tags = $request->get('selectedCategories');

        if (!empty($tags) && is_array($tags)) {
            foreach ($tags as $key => $tag) {
                $tags[$key] = strtolower($tag);
            }
        } else {
            $tags = ['general'];
        }
        $post->syncTags($tags);

        // If selectedFile is missing skip as we're not updating the image
        $inputFile = $request->get('selectedFile');
        if (!empty($inputFile['image']) && !empty($inputFile['name'])){
            $filePath = PostController::base64ToFile($inputFile['image'], $inputFile['name']);

            /*
             * If addMedia() throws an \ErrorException about "rename(): (...) Operation not permitted", check the
             * comment in the PostController::store() method and follow the instructions here as well.
             */
            $post->clearMediaCollection('images');
            $post->addMedia($filePath)->toMediaCollection('images');
            // Easier than trying to empty the folder first and then call rmdir().
            system('rm -rf ' . escapeshellarg(dirname($filePath)));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
    }

    /**
     * Return a list of Post categories (Tags).
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories()
    {
        // TODO: When working on i18n check all() instead of pluck() as the full response is localized
        return \Spatie\Tags\Tag::pluck('name');
    }

    /**
     * Decode and write to file a base64 encoded string.
     *
     * @param String $content
     * @param String $filename
     */
    public static function base64ToFile(String $content, String $filename)
    {
        // Decode the data
        $data = explode(',', $content);
        $decoded = base64_decode($data[1]);

        if (!empty($decoded)) {
            // Create a tmp dir for storing the file
            $dir = sys_get_temp_dir() . '/' . uniqid('gainrank_tmp_') . '/';
            // Ensure that we're not vulnerable to directory traversal attacks
            $filename = basename($filename);
            if (!empty($filename) && $filename != '..' && $filename != '.') {
                if (mkdir($dir)) {
                    $filepath = $dir . $filename;
                    if ($fp = fopen($filepath, "wb")) {
                        fwrite($fp, $decoded);
                        fclose($fp);
                        return $filepath;
                    } else {
                        throw new Exception("Could not decode base64 file. Target file could not be opened.");
                    }
                } else {
                    throw new Exception("Could not decode base64 file. Could not create directory.");
                }
            } else {
                throw new Exception("Could not decode base64 file. Unsupported filename.");
            }
        } else {
            throw new Exception("Could not decode base64 file. Provided file is empty or corrupted.");
        }
    }
}
