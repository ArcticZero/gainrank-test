<?php

namespace App\Http\Controllers;

use App\TrafficSubmission;
use App\TrafficSetting;
use App\TrafficStatus;
use App\Gainrank\TrafficAPI;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class TrafficController extends Controller
{
    protected $kiosk_per_page = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function test()
    {
        $api = TrafficAPI::initialize();


        // add
        // $api->add('www.tipidpc.com', 5);

        $api->update(1, 1);
    }

    /**
     * Get current version of API
     *
     * @return string
     */
    private function getCurrentVersion()
    {
        $version = TrafficSetting::where([
            ['version', '=', 0],
            ['name', 'current_version']
        ])
            ->select('value')
            ->pluck('value')
            ->first();
        ;

        return $version;
    }

    /**
     * Display the client view.
     *
     * @return Response
     */
    public function showTraffic(Request $request, $limit = 100)
    {
        $data = [
            'rows' => []
        ];

        // get submissions from db
        $rows = $request->user()
            ->trafficSubmissions()
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();

        foreach ($rows as $row) {
            $data['rows'][] = $row;
        }

        return view('services.traffic.traffic', $data);
    }

    /**
     * Display the client view with test data.
     *
     * @return Response
     */
    public function showTrafficTestData()
    {
        // this is dummy data
        $data = [
            // keyword rows
            'rows' => [
                [
                    'id' => 1,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 2,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 3,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 4,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 5,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 6,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 7,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 8,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 9,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 10,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 11,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 12,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 13,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 14,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 15,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 16,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 17,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 18,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 19,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ],
                [
                    'id' => 20,
                    'created_at' => '2017-09-27 23:12:29',
                    'url' => 'http://www.gainrank.com',
                    'amt_ordered' => 1000,
                    'amt_sent' => 500,
                    'status' => 'Ok'
                ]
            ]
        ];

        return view('services.traffic.traffic', $data);
    }

    /**
     * Add a new submission.
     *
     * @return Response
     */
    public function add()
    {
    }

    /**
     * Delete a submission.
     *
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Get a list of all submissions.
     *
     * @return Response
     */
    public function getSubmissions(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve rows
        $query = TrafficSubmission::orderBy('created_at', 'desc');

        if ($filter) {
            // get total rows (filtered)
            $total = TrafficSubmission::where('url', 'like', '%' . $filter . '%')
                ->orWhere('ip_addr', 'like', '%' . $filter . '%')
                ->orWhereHas('user', function ($query) use ($filter) {
                    $query->where('name', 'like', '%' . $filter . '%');
                })
                ->count();

            // apply filter to query
            $query->where('url', 'like', '%' . $filter . '%')
                ->orWhere('ip_addr', 'like', '%' . $filter . '%')
                ->orWhereHas('user', function ($query) use ($filter) {
                    $query->where('name', 'like', '%' . $filter . '%');
                });
        } else {
            // get total rows (all)
            $total = TrafficSubmission::count();
        }

        // limit and offset
        $rows = $query->offset(($page - 1) * $this->kiosk_per_page)
            ->limit($this->kiosk_per_page)
            ->get();

        // format results
        foreach ($rows as $row) {
            $rowdata = $row->toArray();
            $rowdata['user_name'] = $row->user->name;
            $rowdata['engine_name'] = $row->engine->name;
            $items[] = $rowdata;
        }

        // DEBUG: this is dummy data
        $items = [];
        $total = 25;

        for ($i = 1; $i <= 25; $i++) {
            $items[] = [
                'id' => $i,
                'created_at' => "2017-09-24 01:12:23",
                'user_id' => 1,
                'user_name' => "Kendrick Chan", // $row->user->name
                'submission_id' => 123,
                'url' => 'www.gainrank.com',
                'wanted_count' => 10,
                'sent_count' => 20,
                'cost_per_x' => 100,
                'cost_per_amt' => 1000,
                'ip_addr' => '208.67.222.222',
                'status_text' => "Ok" // $row->status->staff_message (?)
            ];
        }

        $items = array_slice($items, ($this->kiosk_per_page * ($page - 1)), $this->kiosk_per_page);
        // END dummy data

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->kiosk_per_page, $page);

        return $paginator;
    }

    /**
     * Get a list of all statuses.
     *
     * @return Response
     */
    public function getStatuses(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve rows
        $query = TrafficStatus::orderBy('id', 'asc');

        if ($filter) {
            // get total rows (filtered)
            $total = TrafficStatus::where('id', $filter)
                ->orWhere('name', 'like', '%' . $filter . '%')
                ->orWhere('code', 'like', '%' . $filter . '%')
                ->orWhere('cust_message', 'like', '%' . $filter . '%')
                ->orWhere('staff_message', 'like', '%' . $filter . '%')
                ->orWhere('cust_url', 'like', '%' . $filter . '%')
                ->count();

            // apply filter to query
            $query->where('id', $filter)
                ->orWhere('name', 'like', '%' . $filter . '%')
                ->orWhere('code', 'like', '%' . $filter . '%')
                ->orWhere('cust_message', 'like', '%' . $filter . '%')
                ->orWhere('staff_message', 'like', '%' . $filter . '%')
                ->orWhere('cust_url', 'like', '%' . $filter . '%');
        } else {
            // get total rows (all)
            $total = TrafficStatus::count();
        }

        // limit and offset
        $items = $query->offset(($page - 1) * $this->kiosk_per_page)
            ->limit($this->kiosk_per_page)
            ->get()
            ->toArray();

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->kiosk_per_page, $page);

        return $paginator;
    }

    /**
     * Create a new status.
     *
     * @return Response
     */
    public function addStatus(Request $request)
    {
        // validate input
        $this->validate($request, [
            'id' => 'required|unique:traffic_statuses,id|integer|digits_between:1,6',
            'name' => 'required|unique:traffic_statuses,name',
            'code' => 'required|unique:traffic_statuses,code',
            'cust_message' => 'required',
            'staff_message' => 'required',
            'cust_url' => 'required'
        ]);

        // save to db
        $status = new TrafficStatus;
        $status->id = $request->input('id');
        $status->name = $request->input('name');
        $status->code = $request->input('code');
        $status->cust_message = $request->input('cust_message');
        $status->staff_message = $request->input('staff_message');
        $status->cust_url = $request->input('cust_url');
        $status->save();

        // return
        return response('Ok', 200);
    }

    /**
     * Save an existing status.
     *
     * @return Response
     */
    public function updateStatus(Request $request, $id)
    {
        // validate input
        $this->validate($request, [
            'id' => 'exists:traffic_statuses|in:' . $id,
            'name' => 'required|unique:traffic_statuses,name, ' . $id,
            'code' => 'required|unique:traffic_statuses,code, ' . $id,
            'cust_message' => 'required',
            'staff_message' => 'required',
            'cust_url' => 'required'
        ]);

        // save to db
        $status = TrafficStatus::find($id);
        $status->name = $request->input('name');
        $status->code = $request->input('code');
        $status->cust_message = $request->input('cust_message');
        $status->staff_message = $request->input('staff_message');
        $status->cust_url = $request->input('cust_url');
        $status->save();

        // return
        return response('Ok', 200);
    }

    /**
     * Delete an existing status.
     *
     * @return Response
     */
    public function destroyStatus(Request $request, $id)
    {
        // delete row if it exists
        TrafficStatus::destroy($id);

        // return
        return response('Ok', 200);
    }

    /**
     * Get a list of all settings.
     *
     * @return Response
     */
    public function getSettings(Request $request)
    {
        // get current version
        $version = $this->getCurrentVersion();

        // get our settings for that version
        $indexed_settings = [
            'version' => $version
        ];

        $settings = [];

        $sdata = TrafficSetting::where('version', $version)
            ->select(['name', 'value'])
            ->get();

        foreach ($sdata as $srow) {
            $settings[$srow->name] = $srow->value;
        }

        return $settings;
    }

    /**
     * Save settings.
     *
     * @return Response
     */
    public function updateSettings(Request $request)
    {
        // get current version
        $version = $this->getCurrentVersion();

        // update settings if they exist
        foreach ($request->all() as $name => $value) {
            $setting = TrafficSetting::where('version', $version)
                ->where('name', $name)
                ->first();

            if ($setting) {
                $setting->value = $value;
                $setting->save();
            }
        }

        // return
        return response('Ok', 200);
    }
}
