<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\PaypalNotification;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class PaypalNotificationsController extends Controller
{
    protected $per_page = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:administer paypal');
    }

    /**
     * Get all notifications.
     *
     * @return Response
     */
    public function list(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve notifications
        $query = PaypalNotification::orderBy('created_at', 'desc');

        if ($filter) {
            // get total notifications (filtered)
            $total = PaypalNotification::where('request_details->payer_email', 'like', '%' . $filter . '%')
                ->orWhere('request_details->payer_id', 'like', '%' . $filter . '%')
                ->orWhere('request_details->txn_id', 'like', '%' . $filter . '%')
                ->count();

            // apply filter to query
            $query->where('request_details->payer_email', 'like', '%' . $filter . '%')
                ->orWhere('request_details->payer_id', 'like', '%' . $filter . '%')
                ->orWhere('request_details->txn_id', 'like', '%' . $filter . '%');
        } else {
            // get total notifications (all)
            $total = PaypalNotification::count();
        }

        // limit and offset
        $nrows = $query->offset(($page - 1) * $this->per_page)
            ->limit($this->per_page)
            ->get()
            ->toArray();

        // format results
        foreach ($nrows as $key => $nrow) {
            $ndata = json_decode($nrow['request_details'], true);
            $ndata['mc_fee'] = $ndata['currency_code'] . ' ' . ($ndata['mc_fee'] ?: '0.00');
            ksort($ndata);
            $items[] = $ndata;
        }

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->per_page, $page);

        return $paginator;
    }
}
