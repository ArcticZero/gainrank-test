<?php

namespace App\Http\Controllers\Kiosk;

use Carbon\Carbon;
use Laravel\Spark\Spark;
use App\Contracts\Repositories\PerformanceIndicatorsRepository;
use Laravel\Spark\Http\Controllers\Kiosk\PerformanceIndicatorsController as BaseController;

class PerformanceIndicatorsController extends BaseController
{
    /**
     * The performance indicators repository instance.
     *
     * @var PerformanceIndicatorsRepository
     */
    protected $indicators;

    /**
     * Create a new controller instance.
     *
     * @param  PerformanceIndicatorsRepository  $indicators
     * @return void
     */
    public function __construct(PerformanceIndicatorsRepository $indicators)
    {
        $this->indicators = $indicators;

        $this->middleware('auth');
        $this->middleware('dev');
    }

    /**
     * Get the subscriber counts by plan.
     *
     * @return Response
     */
    public function subscribers()
    {
        $plans = [];

        foreach (Spark::allPlans() as $plan) {
            $plans[] = [
                'name' => $plan->name,
                'interval' => $plan->interval,
                'count' => $this->indicators->subscribers($plan),
                'trialing' => $this->indicators->trialing($plan),
            ];
        }

        return collect($plans)->sortByDesc('count')->values()->all();
    }

    /**
     * Get the number of users who are on a generic trial.
     *
     * @return Response
     */
    public function trialUsers()
    {
        return Spark::user()
                        ->where('trial_ends_at', '>=', Carbon::now())
                        ->whereDoesntHave('subscriptions')
                        ->count();
    }
}
