<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\CoinbaseNotification;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class CoinbaseNotificationsController extends Controller
{
    protected $per_page = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:administer coinbase');
    }

    /**
     * Get all notifications.
     *
     * @return Response
     */
    public function list(Request $request)
    {
        $page = abs(intval($request->get('page')));
        $filter = $request->get('filter');

        $items = [];

        // retrieve notifications
        $query = CoinbaseNotification::orderBy('created_at', 'desc');

        if ($filter) {
            // get total notifications (filtered)
            $total = CoinbaseNotification::where('request_details->data->id', 'like', '%' . $filter . '%')
                ->orWhere('request_details->user->id', 'like', '%' . $filter . '%')
                ->count();

            // apply filter to query
            $query->where('request_details->data->id', 'like', '%' . $filter . '%')
                ->orWhere('request_details->user->id', 'like', '%' . $filter . '%');
        } else {
            // get total notifications (all)
            $total = CoinbaseNotification::count();
        }

        // limit and offset
        $nrows = $query->offset(($page - 1) * $this->per_page)
            ->limit($this->per_page)
            ->get()
            ->toArray();

        // format results
        foreach ($nrows as $key => $nrow) {
            $ndata = json_decode($nrow['request_details'], true);
            ksort($ndata);
            $items[] = $ndata;
        }

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->per_page, $page);

        return $paginator;
    }
}
