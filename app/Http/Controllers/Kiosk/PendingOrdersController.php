<?php

namespace App\Http\Controllers\Kiosk;

use App\Http\Controllers\Controller;
use App\PendingOrder;
use App\PendingOrderAction;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use Laravel\Spark\Spark;

use PendingOrderHandler;

class PendingOrdersController extends Controller
{
    protected $per_page = 20;
    protected $apiContext;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:administer paypal');
    }

    /**
     * Get all orders.
     *
     * @return Response
     */
    public function list(Request $request)
    {
        // validate page number and filter
        $this->validate($request, [
            'page' => 'required|integer|min:1',
            'filter' => 'alpha_dash'
        ]);

        $page = $request->input('page');
        $filter = $request->input('filter');

        $items = [];

        // retrieve orders
        $query = PendingOrder::where('status', PendingOrder::ORDER_STATUS['pending'])
            ->orderBy('created_at', 'desc');

        if (!empty($filter)) {
            // get total orders (filtered)
            $total = PendingOrder::where('status', PendingOrder::ORDER_STATUS['pending'])
                ->where(function ($q) use ($filter) {
                    $q->where('paypal_payer_email', 'like', '%' . $filter . '%')
                      ->orWhere('paypal_payer_id', 'like', '%' . $filter . '%')
                      ->orWhere('paypal_agreement_id', 'like', '%' . $filter . '%');
                })
                ->count();

            // apply filter to query
            $query->where(function ($q) use ($filter) {
                $q->where('paypal_payer_email', 'like', '%' . $filter . '%')
                  ->orWhere('paypal_payer_id', 'like', '%' . $filter . '%')
                  ->orWhere('paypal_agreement_id', 'like', '%' . $filter . '%');
            });
        } else {
            // get total orders (all)
            $total = PendingOrder::where('status', PendingOrder::ORDER_STATUS['pending'])
                ->count();
        }

        // limit and offset
        $nrows = $query->offset(($page - 1) * $this->per_page)
            ->limit($this->per_page)
            ->get()
            ->toArray();

        // format results
        foreach ($nrows as $key => $nrow) {
            $item = $nrow;

            // get order type text
            $item['order_type'] = ucfirst(array_search($item['order_type'], PendingOrder::ORDER_TYPES));

            // get order status text
            $item['status'] = ucfirst(array_search($item['status'], PendingOrder::ORDER_STATUS));

            if (!empty($nrow['request_details'])) {
                $ndata = json_decode($nrow['request_details'], true);
                ksort($ndata);
                $item['request_details'] = $ndata;
            }

            $items[] = $item;
        }

        // build custom paginator
        $paginator = new LengthAwarePaginator($items, $total, $this->per_page, $page);

        return $paginator;
    }

    /**
     * Approve an order
     *
     * @return Response
     */
    public function approve(Request $request)
    {
        $order_id = $request->input('id');

        // get order
        $order = PendingOrder::find($order_id);

        if (!$order) {
            return response('Order not found', 422);
        }

        // approve order
        $result = PendingOrderHandler::approve($order);

        // log this action
        $this->logAction($request->user, $order, PendingOrderAction::ORDER_ACTION['approve']);

        // return response
        return $result ? response('Ok', 200) : response('Pending subscription not found', 422);
    }

    /**
     * Deny an order
     *
     * @return Response
     */
    public function deny(Request $request)
    {
        $order_id = $request->input('id');

        if ($validator->fails()) {
            return Response::json($validator->getMessageBag()->toArray(), 422);
        }

        // get order
        $order = PendingOrder::find($order_id);

        if (!$order) {
            return response('Order not found', 422);
        }

        // deny order
        $result = PendingOrderHandler::deny($order);

        // log this action
        $this->logAction($request->user, $order, PendingOrderAction::ORDER_ACTION['deny']);

        // return response
        return response('Ok', 200);
    }

    /**
     * Log an order action
     *
     * @return Response
     */
    protected function logAction($user, $order, $action)
    {
        $action = new PendingOrderAction();
        $action->user()->associate($user);
        $action->order()->associate($order);
        $action->action = $action;
        $action->save();
    }
}
