<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PaymentProcessorService;
use App\UserPendingSubscription;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

use DateTime;
use Auth;

class SubscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    /**
     * Suspend an active subscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $billing_provider
     * @return \Illuminate\Http\Response
     */
    public function suspend(Request $request, string $billing_provider)
    {
        return $this->changeState($request, $billing_provider, 'suspend');
    }

    /**
     * Update an existing subscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $billing_provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $billing_provider)
    {
        return $this->changeState($request, $billing_provider, 'update');
    }

    /**
     * Change the state of an existing subscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $billing_provider
     * @param  string  $action
     * @return \Illuminate\Http\Response
     */
    private function changeState(Request $request, string $billing_provider, string $action)
    {
        // get all valid payment methods
        $providers = config('billing.providers');
        
        // invalid billing provider given
        if (!in_array($billing_provider, $providers)) {
            abort(422);
        }

        // handle suspension based on billing provider
        switch ($billing_provider) {
            case 'paypal_recurring':
                return $this->doPaypalRecurring($request, $action);
            case 'paypal_manual':
                return $this->doPaypalManual($request, $action);
            case 'coinbase':
                return $this->doCoinbase($request, $action);
        }
    }

    /**
     * Handle changing state of PayPal (Recurring Transactions) subscriptions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $action
     * @return \Illuminate\Http\Response
     */
    private function doPaypalRecurring(Request $request, string $action)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('paypal_recurring', $request);

        // update subscription state based on action
        switch ($action) {
            case 'suspend':
                $result = $processor->suspendSubscription();
                break;
            case 'update':
                $result = $processor->updateSubscription();
                break;
        }

        // return response
        if ($result) {
            return response('OK', 200);
        } else {
            return response('Error', 422);
        }
    }

    /**
     * Handle changing state of PayPal (Manual Transactions) subscriptions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $action
     * @return \Illuminate\Http\Response
     */
    private function doPaypalManual(Request $request, string $action)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('paypal_manual', $request);

        // update subscription state based on action
        switch ($action) {
            case 'suspend':
                $result = $processor->suspendSubscription();
                break;
            case 'update':
                $result = $processor->updateSubscription();
                break;
        }

        // return response
        if ($result) {
            return response('OK', 200);
        } else {
            return response('Error', 422);
        }
    }

    /**
     * Handle changing state of Coinbase subscriptions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $action
     * @return \Illuminate\Http\Response
     */
    private function doCoinbase(Request $request, string $action)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('coinbase', $request);

        // update subscription state based on action
        switch ($action) {
            case 'suspend':
                $result = $processor->suspendSubscription();
                break;
            case 'update':
                $result = $processor->updateSubscription();
                break;
        }

        // return response
        if ($result) {
            return response('OK', 200);
        } else {
            return response('Error', 422);
        }
    }

    /**
     * Retrieve any pending subscriptions
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPending(Request $request)
    {
        $subscription = UserPendingSubscription::where('user_id', Auth::user()->id)
            ->whereNotNull('agreement_id')
            ->first();

        if ($subscription) {
            return response()->json(['subscription' => $subscription]);
        } else {
            return response()->json(['subscription' => null]);
        }
    }

    /**
     * Activate any pending subscriptions
     * This is only for acceptance tests. Should not be available for normal use
     *
     * @param  string  $billing_provider
     * @return mixed
     */
    public function activateFirstPending(string $billing_provider)
    {
        // get all valid payment methods
        $providers = config('billing.providers');

        // validate billing provider
        $this->validate($request, [
            'billing_provider' => [
                'required',
                Rule::in($providers)
            ]
        ]);

        // check if we will run
        $display_flag = env('ACTIVATE_URL_DISPLAY', false);
        if (!$display_flag) {
            abort(404);
        }

        $user_plan = UserPendingSubscription::where('billing_provider', $billing_provider)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($user_plan) {
            $now = new DateTime();

            // find user
            $user = User::find($user_plan->user_id);

            // get spark plan
            $spark_plan = Spark::plans()->where('id', $user_plan->provider_plan_name)->first();

            if ($spark_plan->trialDays > 0 && !$user->has_used_trial) {
                $date_mod = '+' . $spark_plan->trialDays . ' day' . ($spark_plan->trialDays > 1 ? 's' : '');
                $trial_end_time = clone $now;
                $trial_end_time = $trial_end_time->modify($date_mod);
            }

            // update subscription
            $subscription = Subscription::firstOrNew(['user_id' => $user_plan->user_id]);
            $subscription->name = 'default';
            $subscription->quantity = '1';
            $subscription->trial_ends_at = isset($trial_end_time) ? $trial_end_time->format('Y-m-d H:i:s') : null;
            $subscription->paypal_agreement_id = $user_plan->agreement_id;
            $subscription->provider_plan_name = $spark_plan->id;
            $subscription->billing_provider = $user_plan->billing_provider;
            $subscription->last_payment_at = $now->format('Y-m-d H:i:s');

            // if eligible for trial, add trial end time
            if (isset($trial_end_time)) {
                $subscription->trial_ends_at = $trial_end_time->format('Y-m-d H:i:s');
            }

            // save subscription
            $subscription->save();

            // delete pending subscription as it is no longer necessary
            $user_plan->delete();

            // mark user as having used trial
            $user->has_used_trial = 1;
            $user->save();
        }
    }
}
