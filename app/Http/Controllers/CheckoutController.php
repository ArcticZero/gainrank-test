<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PaymentProcessorService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    /**
     * Start the checkout process
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request)
    {
        // get all valid payment methods
        $providers = config('billing.providers');

        // validate billing provider and plan
        $this->validate($request, [
            'plan' => 'required',
            'payment_method' => [
                'required',
                Rule::in($providers)
            ]
        ]);

        $billing_provider = $request->input('payment_method');

        // handle checkout based on billing provider
        switch ($billing_provider) {
            case 'paypal_recurring':
                return $this->doPaypalRecurring($request);
            case 'paypal_manual':
                return $this->doPaypalManual($request);
            case 'coinbase':
                return $this->doCoinbase($request);
        }
    }

    /**
     * Handle checkout for PayPal (Recurring Transactions)
     *
     * @return \Illuminate\Http\Response
     */
    private function doPaypalRecurring(Request $request)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('paypal_recurring', $request);

        // do checkout
        $result = $processor->subscriptionCheckout();

        // return response
        return response()->json($result);
    }

    /**
     * Handle checkout for PayPal (Manual Transactions)
     *
     * @return \Illuminate\Http\Response
     */
    private function doPaypalManual(Request $request)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('paypal_manual', $request);

        // do checkout
        $result = $processor->subscriptionCheckout();

        // return response
        return response()->json($result);
    }

    /**
     * Handle checkout for Coinbase
     *
     * @return \Illuminate\Http\Response
     */
    private function doCoinbase(Request $request)
    {
        // get payment processor instance
        $processor = PaymentProcessorService::getPaymentProcessor('coinbase', $request);

        // do checkout
        $result = $processor->subscriptionCheckout();

        // return response
        return response()->json($result);
    }
}
