<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyUser
{
    /**
     * Check if the user is verified (given the verified role).
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return $next($request);
        } else {
            if (!$request->user()->hasRole('verified')) {
                return redirect('/home');
            }

            return $next($request);
        }
    }
}
