<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request;
use App\Contracts\AffiliateContract;

class CheckReferralId
{
    protected $aff_handler;

    public function __construct(AffiliateContract $aff_handler)
    {
        $this->aff_handler = $aff_handler;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('ref')) {
            $ref_id = $request->input('ref');

            Log::info('Ref ID found in request', ['ref_id' => $ref_id]);

            // Check if an affiliate with this referral ID exists
            $affiliate = $this->aff_handler->getByReferralId($ref_id);

            // If this is valid affiliate, store in session
            if ($affiliate) {
                $referer_url = URL::previous();

                $request->session()->put('affiliate_id', $affiliate->id);
                $request->session()->put('ref_id', $ref_id);
                $request->session()->put('referer_url', $referer_url);

                Log::info('Referring affiliate saved to session', [
                    'affiliate_id'  => $affiliate->id,
                    'ref_id'        => $ref_id,
                    'referer_url'   => $referer_url,
                ]);
            }
        }

        return $next($request);
    }
}
