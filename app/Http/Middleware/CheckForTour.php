<?php

namespace App\Http\Middleware;

use Closure;

class CheckForTour
{
    /**
     * Check if an existing tour JSON file exists, and apply to template if necessary
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get directories
        $tour_dir = public_path('js/tours');
        $current_path = $request->path();

        // if blank, set to root
        if ($current_path == '/') {
            $current_path = 'root';
        }

        $tour_path = $current_path . '.js';

        // set tour URL
        if (file_exists($tour_dir . '/' . $tour_path)) {
            $tour_url = url('js/tours/' . $tour_path);
        } else {
            $tour_url = null;
        }

        // add to request
        $request->merge(['tour_url' => $tour_url]);

        return $next($request);
    }
}
