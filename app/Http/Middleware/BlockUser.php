<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Jrean\UserVerification\Exceptions\UserNotVerifiedException;

class BlockUser
{
    /**
     * Check if the user has been banned (given the blocked role).
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return $next($request);
        } else {
            if ($request->user()->hasRole('blocked')) {
                Auth::logout();

                // show banned view
                return response()->view('banned');
            }
            /*
            if (! $request->user()->verified) {
                throw new UserNotVerifiedException;
            }
            */
            return $next($request);
        }
    }
}
