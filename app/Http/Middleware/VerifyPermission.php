<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyPermission
{
    /**
     * Check if the user has been granted the necessary permission for this request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  String $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        if (Auth::guest()) {
            return $request->ajax() || $request->wantsJson()
                ? response('Unauthorized.', 401)
                : redirect()->guest('login');
        }
        if ($request->user()->hasRole('blocked')) {
            Auth::logout();
            abort(403, 'Permission Denied');
        }
        if ($request->user()->hasRole('admin')) {
            return $next($request);
        }
        /*
         * If $permission is empty then we are dealing with a check that was previously handled by Spark's
         * VerifyUserIsDeveloper.php middleware (originally defined as 'dev' in Kernel.php).
         * The only thing it did was check if the current user is authenticated and its email was in the $developers
         * array in SparkServiceProvider.php.
         */
        if (empty($permission)) {
            if ($request->is('spark/kiosk/users/*')) {
                if ($request->user()->can('administer users')) {
                    return $next($request);
                }
            }
            if ($request->is('spark/kiosk/announcements')) {
                if ($request->user()->can('administer announcements')) {
                    return $next($request);
                }
            }
            // TODO: Maybe add a permission for the performance metrics of the app?
            if ($request->is('spark/kiosk') || $request->is('spark/kiosk/*')) {
                if ($request->user()->can('access kiosk')) {
                    return $next($request);
                }
            }
            return $next($request);
        } else {
            if ($request->user()->can($permission)) {
                return $next($request);
            }
        }
        abort(403, 'Permission Denied');
    }
}
