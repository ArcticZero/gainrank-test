<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateTransaction extends Model
{
    /*
     * transaction status
     *
     * pending      - transaction hasn't matured
     * mature       - transaction has matured and reward has been implemented
     * cancelled    - transaction has not matured and has been cancelled, reward unimplemented
     * rolled back  - transaction has matured but cancelled, reward has been rolled back
     *
     */
    const STATUS_PENDING                = 1;
    const STATUS_MATURE                 = 2;
    const STATUS_CANCELLED              = 3;
    const STATUS_ROLLED_BACK            = 4;

    /*
     * transaction types
     *
     * subscribe        - subscription
     *
     */
    const TYPE_SUBSCRIBE                = 1;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'transacted_at',
        'mature_at'
    ];

    public function affiliate()
    {
        return $this->belongsTo('App\Affiliate');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getTypeText($type_id)
    {
        switch ($type_id) {
            case self::TYPE_SUBSCRIBE:
                return 'Referral subscription';

        }

        return 'Unkown action';
    }
}
