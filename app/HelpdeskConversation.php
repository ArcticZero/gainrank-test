<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpdeskConversation extends Model
{
    protected $fillable = [
        'user_id',
        'conv_id',
        'read_reply_count',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
