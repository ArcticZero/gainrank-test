<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTracking extends Model
{
    protected $table = 'rank_tracking';

    /**
     * Return the customer/user for the track
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Return the version the track
     *
     * @return mixed
     */
    public function version()
    {
        return $this->belongsTo('App\RankTrackingVersion');
    }

    /**
     * Return the engine used for the track
     *
     * @return mixed
     */
    public function engine()
    {
        return $this->belongsTo('App\RankTrackingEngine');
    }

    /**
     * Return ranks associated with the track
     *
     * @return mixed
     */
    public function ranks()
    {
        return $this->hasMany('App\RankTrackingRank', 'track_id');
    }

    /**
     * Return favorites associated with the track
     *
     * @return mixed
     */
    public function favorites()
    {
        return $this->hasMany('App\RankTrackingFavorite', 'track_id');
    }

    /**
     * Return status associated with the track
     *
     * @return mixed
     */
    public function status()
    {
        return $this->belongsTo('App\RankTrackingStatus', 'id_status');
    }

    public function domain()
    {
        return $this->belongsTo('App\RankTrackingDomain');
    }
}
