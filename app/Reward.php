<?php

namespace App;

class Reward
{
    /*
     * reward types
     *
     * payout               - rewards a payout to an affiliate
     * bonus keyword        - rewards bonus keywords to a user
     * discount amount      - rewards a discount based on amount
     * discount percent     - rewards a discount based on percentage of the original
     *
     */
    const TYPE_PAYOUT                       = 1;
    const TYPE_BONUS_KEYWORD                = 2;
    const TYPE_DISCOUNT_INITIAL_AMOUNT      = 3;
    const TYPE_DISCOUNT_INITIAL_PERCENT     = 4;
    const TYPE_DISCOUNT_RECURRING_AMOUNT    = 5;
    const TYPE_DISCOUNT_RECURRING_PERCENT   = 6;

    protected $type;
    protected $details;

    public function __construct($type, $details = [])
    {
        $this->setType($type);
        $this->details = $details;
    }

    protected function setType($type)
    {
        // check if valid
        switch ($type) {
            case self::TYPE_PAYOUT:
            case self::TYPE_BONUS_KEYWORD:
            case self::TYPE_DISCOUNT_INITIAL_AMOUNT:
            case self::TYPE_DISCOUNT_INITIAL_PERCENT:
            case self::TYPE_DISCOUNT_RECURRING_AMOUNT:
            case self::TYPE_DISCOUNT_RECURRING_PERCENT:
                $this->type = $type;
                return;
        }

        throw new Exception('Invalid reward type - ' . $type);
    }

    protected function setDetails($details = [])
    {
        switch ($this->type) {
            case self::TYPE_PAYOUT:
                if (!isset($this->details['payout_amount'])) {
                    throw new Exception("Missing 'payout_amount' reward detail");
                }
                break;
            case self::TYPE_BONUS_KEYWORD:
                if (!isset($this->details['keyword_count'])) {
                    throw new Exception("Missing 'keyword_count' reward detail");
                }
                break;
            case self::TYPE_DISCOUNT_INITIAL_AMOUNT:
                if (!isset($this->details['discount_amount'])) {
                    throw new Exception("Missing 'discount_amount' reward detail");
                }
                break;
            case self::TYPE_DISCOUNT_INITIAL_PERCENT:
                if (!isset($this->details['discount_percent'])) {
                    throw new Exception("Missing 'discount_percent' reward detail");
                }
                break;
            case self::TYPE_DISCOUNT_RECURRING_AMOUNT:
                if (!isset($this->details['discount_amount'])) {
                    throw new Exception("Missing 'discount_amount' reward detail");
                }
                break;
            case self::TYPE_DISCOUNT_RECURRING_PERCENT:
                if (!isset($this->details['discount_percent'])) {
                    throw new Exception("Missing 'discount_percent' reward detail");
                }
                break;
        }

        $this->details = $details;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function getFullText()
    {
        switch ($this->type) {
            case self::TYPE_PAYOUT:
                return '$' . $this->details['payout_amount'] . ' payout';
            case self::TYPE_BONUS_KEYWORD:
                return $this->details['keyword_count'] . ' bonus keywords';
            case self::TYPE_DISCOUNT_INITIAL_AMOUNT:
                return '$' . $this->details['discount_amount'] . ' discount on first month';
            case self::TYPE_DISCOUNT_INITIAL_PERCENT:
                return $this->details['discount_percent'] . '% discount on first month';
            case self::TYPE_DISCOUNT_RECURRING_AMOUNT:
                return '$' . $this->details['discount_amount'] . ' recurring discount';
            case self::TYPE_DISCOUNT_RECURRING_PERCENT:
                return $this->details['discount_percent'] . '% recurring discount';
        }

        return '';
    }
}
