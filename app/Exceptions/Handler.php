<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Jrean\UserVerification\Exceptions\UserNotVerifiedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \Jrean\UserVerification\Exceptions\UserNotVerifiedException::class,
    ];

    /**
     * Paths that will show custom error messages during AuthenticationException
     *
     * @var array
     */
    protected $showErrorOnGuest = [
        'helpdesk/tickets' => 'You must be logged in to use our support helpdesk. Alternatively, you may send an e-mail to <a href="mailto:support@gainrank.com">support@gainrank.com</a>.'
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        /*
        if ($exception instanceof UserNotVerifiedException) {
            Auth::logout();
            return redirect('/email-not-verified');
        }
        */

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        // if an error message is defined for this route, redirect with it
        if (array_key_exists($request->path(), $this->showErrorOnGuest)) {
            return redirect()->guest('login')->with('login_error', $this->showErrorOnGuest[$request->path()]);
        }

        return redirect()->guest('login');
    }
}
