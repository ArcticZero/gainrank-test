
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('spark-bootstrap');

// define global user timezone objects
if (Spark.state.user) {
    window.timezone = Spark.state.user.timezone ? Spark.state.user.timezone : 'UTC';
    window.mtz = require('moment-timezone');
}

require('./components/bootstrap');

Spark.forms.register = {
    is_subscribed: ''
};
//var VueQuillEditor = require('vue-quill-editor');
//Vue.use(VueQuillEditor);

/**
 * Load the fingerprintjs2 library.
 */
window.fingerprint = require('fingerprintjs2');

Vue.component('quill-editor', require('./../../../node_modules/vue-quill-editor/editor.vue'));
Vue.component('posts', require('./components/Posts.vue'));
Vue.component('chartist', require('./components/Chartist.vue'));
Vue.component('disable-tour', require('./components/DisableTour.vue'));

var app = new Vue({
    mixins: [require('spark')],

    data: function() {  
        return {
            notificationInterval: '',
            lastResentVerification: null
        }
    },

    mounted: function() {
    	this.refreshNotifications();
    },

    beforeDestroy() {
    	clearInterval(this.notificationInterval);
    },

    methods: {
    	// periodically refresh notifications and announcements
	    refreshNotifications: function() {
            /*
		    this.notificationInterval = setInterval(function () {
		    	this.getNotifications();
		    }.bind(this), 10000);
            */
		},

    	resendVerificationEmail() {
            var self = this;
            var now = new Date();
            var interval = 30;
            var elapsed = this.lastResentVerification ? (now - this.lastResentVerification) / 1000 : 0;

            if (!this.lastResentVerification || elapsed >= interval) {
                this.lastResentVerification = now;

                axios.post('/email-verification/resend-current')
                    .catch(error => {
                        if (error.response.status == 422) {
                            self.resendRateExceeded(); 
                        }
                    });

                swal({
                    title: 'Request Sent',
                    text: 'The verification message will be re-sent to your email address. Please check for it within the next few minutes.',
                    type: 'success'
                });
            } else {
               self.resendRateExceeded();
            }  
        },

        resendRateExceeded() {
            swal({
                title: 'Error',
                text: 'You just requested us to resend your verification code. Please wait a while before trying again.',
                type: 'error'
            });
        }
	}
});

/**
 * Load the hopscotch library.
 */
window.hopscotch = require('hopscotch');
