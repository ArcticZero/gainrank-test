var base = require('auth/register-stripe');

/*
 * Override some of the default methods defined in spark/resources/assets/js/auth/register-stripe.js to add custom
 * functionality.
 */
base.data = function () {
    return {
        query: null,

        coupon: null,
        campaign: null,
        invite: null,
        invalidCoupon: false,

        country: null,
        taxRate: 0,

        registerForm: $.extend(true, new SparkForm({
            stripe_token: '',
            plan: '',
            team: '',
            team_slug: '',
            name: '',
            email: '',
            password: '',
            password_confirmation: '',
            address: '',
            address_line_2: '',
            city: '',
            state: '',
            zip: '',
            country: 'US',
            vat_id: '',
            terms: false,
            coupon: null,
            campaign: null,
            invite: null,
            invitation: null
        }), Spark.forms.register),

        cardForm: new SparkForm({
            name: '',
            number: '',
            cvc: '',
            month: '',
            year: '',
        })
    };
}

base.created = function () {
    Stripe.setPublishableKey(Spark.stripeKey);

    this.getPlans();

    this.guessCountry();

    this.query = URI(document.URL).query(true);

    if (this.query.coupon) {
        this.getCoupon();

        this.registerForm.coupon = this.query.coupon;
    }

    if (this.query.campaign) {
        this.registerForm.campaign = this.query.campaign;
    }
    if (this.query.invite) {
        this.registerForm.invite = this.query.invite;
    }

    if (this.query.invitation) {
        this.getInvitation();

        this.registerForm.invitation = this.query.invitation;
    }
};

Vue.component('spark-register-stripe', {
    mixins: [base],

    methods: {

        /**
         * Select the appropriate default plan for registration.
         * Partially overwriting laravel\spark\resoures\assets\js\mixins\register.js to set free plan as default plan
         */
        selectAppropriateDefaultPlan() {
            this.selectFreePlan();
        }
    }        
});
