var base = require('kiosk/profile');

/*
 * Override some of the default methods defined in spark/resources/assets/js/kiosk/profile.js to add support for custom
 * functionality.
 */
base.data = function () {
    return {
        loading: false,
        profile: null,
        revenue: 0,
        currentRole: null,
        availableRoles: [],
        relatedData: {
            'users': [],
            'hashes': []
        },
        targetUserId: null,
        visibleRows: {},
        moment: window.mtz,
        timezone: window.timezone
    };
};

base.created = function () {
    var self = this;

    this.$parent.$on('showUserProfile', function(id) {
        self.getUserProfile(id);
        self.getRoles(id);
        self.targetUserId = id;
    });
};

/**
 * Get the profile user.
 */
base.methods.getUserProfile = function (id) {
    this.loading = true;

    axios.get('/spark/kiosk/users/' + id + '/profile')
      .then(response => {
        this.profile = response.data.user;
        this.revenue = response.data.revenue;

        this.loading = false;
        this.getRelatedUsers();
    });
};

/**
 * Get the user's roles.
 */
base.methods.getRoles = function (id) {
    axios.get('/users/' + id + '/roles')
      .then(response => {
        this.availableRoles = response.data.roles;
        this.currentRoles = response.data.current;
    });
};

/**
 * Get related accounts
 */
base.methods.getRelatedUsers = function () {
    var self = this;
    axios.get('/fingerprints/' + this.profile.email + '/related')
      .then(response => {
        _.forEach(response.data.users_by_hash, function(value, key) {
          Vue.set(self.visibleRows, key + '_hash', false);
          Vue.set(self.visibleRows, key + '_ip', false);
        });
        this.relatedData = response.data;
    });
};

/**
 * Update the user's role.
 */
base.methods.updateRoles = function () {
    Spark.put('/users/' + this.targetUserId + '/roles', new SparkForm({roles: this.currentRoles}))
      .then(response => {
        this.getRoles(this.targetUserId);
    });
};

/**
 * Toggle visibility for a row.
 */
base.methods.toggleVisibility = function (email) {
    Vue.set(this.visibleRows, email, !this.visibleRows[email]);
};

/**
 * Controls whether a specific table row should be hidden.
 * @param email
 * @returns {boolean}
 */
base.methods.isHidden = function (email) {
    return !this.visibleRows[email];
};

Vue.component('spark-kiosk-profile', {
    mixins: [base]
});
