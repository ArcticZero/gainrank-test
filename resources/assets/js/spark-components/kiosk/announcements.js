var base = require('kiosk/announcements');

Vue.component('spark-kiosk-announcements', {
    mixins: [base],

    data() {
        return {
        	moment: window.mtz,
            timezone: window.timezone
        };
    }
});
