var base = require('settings/subscription/cancel-subscription');

Vue.component('spark-cancel-subscription', {
    mixins: [
    	base,
    	require('./../../../../../../spark/resources/assets/js/mixins/plans'),
        require('./../../mixins/subscriptions')
    ],

    computed: {
        /**
         * Get the URL for the subscription cancellation.
         */
        urlForCancellation() {
        	var provider = this.activeSubscription.billing_provider.replace("_", "/");

            return this.billingUser
                            ? '/settings/subscription/' + provider
                            : `/settings/${Spark.pluralTeamString}/${this.team.id}/subscription/` + provider;
        }
    }
});
