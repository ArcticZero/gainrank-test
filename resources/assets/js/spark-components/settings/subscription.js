var base = require('settings/subscription');

Vue.component('spark-subscription', {
    mixins: [base],

    computed: {
    	/**
         * Determine if the current subscription is active.
         */
        hasAnySubscription() {
            return this.activeSubscription;
        },

    	/**
         * Determine if the current subscription is active.
         */
        subscriptionIsRecurring() {
            return this.activeSubscription && this.activeSubscription.billing_provider == 'paypal_recurring';
        },

        /**
         * Determine if the current subscription is cancelled.
         */
        subscriptionIsCancelled() {
        	return this.activeSubscription && this.activeSubscription.is_cancelled == 1 || (this.subscriptionIsRecurring && this.activeSubscription.ends_at);
        }
    }
});
