Vue.component('update-profile-notifications', {
    props: ['user'],

    data() {
        return {
            form: new SparkForm({
                is_subscribed: '',
                show_guide: ''
            })
        };
    },

    mounted() {
        this.form.is_subscribed = this.user.is_subscribed;
        this.form.show_guide = this.user.show_guide;
    },

    methods: {
        update() {
            Spark.put('/settings/profile/notifications', this.form)
                .then(response => {
                    Bus.$emit('updateUser');
                });
        }
    }
});
