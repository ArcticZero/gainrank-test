Vue.component('update-profile-timezone', {
    props: ['user', 'timezones', 'default_timezone'],

    data() {
        return {
            form: new SparkForm({
                timezone: ''
            })
        };
    },

    mounted() {
        if (!this.user.timezone) {
            this.user.timezone = this.default_timezone;
        }

        this.form.timezone = this.user.timezone;
    },

    methods: {
        update() {
            Spark.put('/settings/profile/timezone', this.form)
                .then(response => {
                    Bus.$emit('updateUser');
                });
        }
    }
});
