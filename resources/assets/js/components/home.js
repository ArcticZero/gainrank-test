Vue.component('home', {
    props: ['user', 'rows', 'ranks', 'dates', 'stats', 'colors'],

    data() {
    	return {
            rankdata: JSON.parse(this.ranks),
            series_colors: JSON.parse(this.colors),
    		statistics: JSON.parse(this.stats),
    		chart: {
    			rows: {},
    			rowsInChart: {},
    			data: {
    				labels: JSON.parse(this.dates),
    				series: [],
    			},
    			options: {
    				height: 250,
                    showArea: false,
                    showLine: true,
                    showPoint: true,
                    fullWidth: true,
                    lineSmooth: false,
                    onlyInteger: true,
                    axisX: {
                        showGrid: false
                    },
                    chartPadding: {
                        right: 40
                    },
                    tooltip: true,
                    axisY: {
                        labelInterpolationFnc: function(value) {
                            if (value === 0) {
                                value = -1;
                            }

                            return Math.floor(value < 0 ? -value : value);
                        }
                    }
    			}
    		}
    	}
    },

    /**
     * The component has been created by Vue.
     */
    created() {
    	this.getChartData();
    },

    mounted() {
        var keywordChart = this.$refs.keywords;

        keywordChart.chart.on('data', function(context) {
            context.data.series = context.data.series.map(function(series) {
                return series.map(function(value) {
                    if (value) {
                        return {
                            value: -value.value,
                            meta: {
                                id: value.meta.id,
                                domain: value.meta.domain,
                                keyword: value.meta.keyword
                            }
                        };
                    } else {
                        return null;
                    }
                });
            });
        });


        this.updateChartDisplay(true);
    },

    methods: {
    	/**
         * Get initial chart data.
         */
        getChartData() {
        	var rows = JSON.parse(this.rows);

        	for (var i = 0; i < rows.length; i++) {
        		if (rows[i].in_graph) {
        			this.chart.rowsInChart[rows[i].id] = rows[i];
        		}

        		this.chart.rows[rows[i].id] = rows[i];
        	}

        	this.updateChartData();
        },

        /**
         * Add row to chart.
         */
        addToChart(id) {
        	this.chart.rowsInChart[id] = this.chart.rows[id];
        	this.updateChartData(true);
        },

        /**
         * Remove row from chart.
         */
        removeFromChart(id) {
        	delete this.chart.rowsInChart[id];
        	this.updateChartData(true);
        },

        /**
         * Update the chart data.
         */
        updateChartData(autoUpdate = false) {
        	var series = [];

        	for (var id in this.chart.rowsInChart) {
                var rankdata = this.rankdata[id];
    			var myRanks = [];
    			var row = this.chart.rowsInChart[id];
    			var hasRank = false;

    			for (i in this.chart.data.labels) {
                    var rank = rankdata[this.chart.data.labels[i]];

                    if (rank) {
                        myRanks.push(
                            {
                                value: rank,
                                meta: {
                                    id: id,
                                    domain: row.domain,
                                    keyword: row.keyword
                                }
                            }
                        );
                    } else {
                        myRanks.push(null);
                    }
    			}

    			series.push(myRanks);
        	}

        	this.chart.data.series = series;

        	if (autoUpdate) {
        		this.updateChartDisplay();
        	}
        },

        /**
         * Update the chart display.
         */
        updateChartDisplay(initial = false) {
            var keywordChart = this.$refs.keywords;
            var newData = this.chart.data;
            var self = this;

            keywordChart.chart.on('draw', function(context) {
                if (context.type === 'point' || context.type === 'line') {
                    for (i in context.series) {
                        if (context.series[i]) {
                            var id = context.series[i].meta.id;
                            var color = self.getRowColor(id, true);

                            context.element.attr({
                                style: 'stroke: ' + color + ' !important;'
                            });

                            break;
                        }
                    }
                }
            });

            keywordChart.chart.update(newData);
/*
        	var keywordChart = this.$refs.keywords;
	    	var newData = this.chart.data;
	    	var self = this;

	    	keywordChart.chart.on('draw', function(context) {
	    		if (context.type === 'line' || context.type === 'point') {
	    			var id = context.series[context.index].meta.id;
	    			var color = self.getRowColor(id);

	    			context.element.attr({
		                style: 'stroke: ' + color + ' !important;'
		            });
	    		}
	    	});

	    	keywordChart.chart.update(newData);
*/

            // do reupdate to reverse chart on initial load
            if (initial) {
                keywordChart.chart.update(newData);
            }
        },

        /**
         * Toggle displaying a keyword row on graph.
         */
        toggleRow(id) {
            var row = $("tr[data-row-id=" + id + "]");
            var swatch = row.find('.fa-square');
            var charticon = row.find('.fa-area-chart');
            var chart = $("#keyword-rankings");

            if (charticon.hasClass('google-blue')) {
                swatch.addClass('invisible');
                charticon.removeClass('google-blue');
                this.removeFromChart(id);
            } else {
                swatch.removeClass('invisible');
                charticon.addClass('google-blue');
                this.addToChart(id);
            }
        },

        /**
         * Toggle favorite keyword.
         */
        toggleFavorite(id) {
        	/*
        	var row = $("tr[data-row-id=" + id + "]");
        	var star = row.find('.favorite-star');

        	if (this.chart.rows[id].favorite == true) {
        		star.addClass('fa-star-o').removeClass('fa-star gold');

        		this.chart.rows[id].favorite = false;

        		if (typeof this.chart.rowsInChart[id] !== 'undefined')
        			this.chart.rowsInChart[id].favorite = false;
        	} else {
        		star.addClass('fa-star gold').removeClass('fa-star-o');

        		this.chart.rows[id].favorite = true;

        		if (typeof this.chart.rowsInChart[id] !== 'undefined')
        			this.chart.rowsInChart[id].favorite = true;
        	}

        	this.updateChartData(true);

        	// TODO: Make AJAX call to actually toggle favorite status
        	*/
        	return true;
        },

        /**
         * Generate row color based on string.
         */
        getRowColor(id) {
            var row = $("#keyword-table tbody").find('tr[data-row-id=' + id + ']');
            var loop_id = row.data('loop-id')

            return this.series_colors[loop_id - 1];
        }
    }
});
