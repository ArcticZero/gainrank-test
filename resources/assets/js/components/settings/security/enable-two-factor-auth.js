Vue.component('enable-two-factor-auth', {
    props: ['user'],

    /**
     * The component's data.
     */
    data() {
        return {
            form: new SparkForm({
                verification_code: ''
            }),
            tfa_secret: '',
            tfa_secret_img: ''
        }
    },

    /**
     * The component has been created by Vue.
     */
    created() {
        this.generateSecret();
    },

    /**
     * The component has been mounted.
     */
    mounted() {
        this.form.verification_code = '';
    },

    methods: {
        /**
         * Generate TFA code and image
         */
        generateSecret() {
            axios.get('/settings/security/two-factor-auth/generate-secret')
                .then(response => {
                    this.tfa_secret = response.data.tfa_secret;
                    this.tfa_secret_img = response.data.tfa_secret_img;

                    $("#qr-code").prop('src', this.tfa_secret_img);
                    $("#tfa-secret").html(this.tfa_secret.toLowerCase().replace(/\W/gi, '').replace(/(.{4})/g, '$1 '));
                });
        },

        /**
         * Enable two-factor authentication for the user.
         */
        enable() {
            Spark.put('/settings/security/two-factor-auth', this.form)
                .then(() => {
                    Bus.$emit('updateUser');
                });
        }
    }
});