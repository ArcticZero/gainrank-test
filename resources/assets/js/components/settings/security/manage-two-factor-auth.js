Vue.component('manage-two-factor-auth', {
	props: ['user'],

    /**
     * The component's data.
     */
	data() {
		return {
			form: new SparkForm({}),
			backup_codes: [],
			generation_date: '',
			generate_error: false
		}
	},

	/**
     * The component has been created by Vue.
     */
    created() {
        this.getCodes();
    },

	methods: {
		/**
		 * Get existing backup codes.
		 */
		getCodes() {
			axios.get('/settings/security/two-factor-auth/get-backup-codes')
                .then(response => {
                	this.backup_codes = response.data.backup_codes;
                	this.generation_date = response.data.generation_date;
                	this.updateCodeBoxes();
                });
		},

		/**
		 * Generate new backup codes.
		 */
		generateCodes() {
			this.generate_error = false;

			axios.get('/settings/security/two-factor-auth/generate-backup-codes')
                .then(response => {
                	this.backup_codes = response.data.backup_codes;
                	this.generation_date = response.data.generation_date;
                	this.updateCodeBoxes();
                })
                .catch(error => {
                	this.generate_error = error.response.data.generate_codes[0];
                });
		},

		/**
		 * Update code boxes.
		 */
		updateCodeBoxes() {
			var self = this;

			$("#generation-date").html(this.generation_date);

			$(".backup-code").each(function(index, value) {
        		$(this).html(self.backup_codes[index].toString().toUpperCase().replace(/\W/gi, '').replace(/(.{4})/g, '$1 '));
        	});
		},

		/**
		 * Disable two-factor authentication for the user.
		 */
		disable() {
			var self = this;

			swal({
			  title: "Disable Two-Factor Authentication?",
			  text: "You will have to repeat the pairing procedure if you wish to resume using this feature later.",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Yes",
			  cancelButtonText: "No"
			}, function() {
				Spark.delete('/settings/security/two-factor-auth', self.form)
					.then(() => {
						Bus.$emit('updateUser');
					});
			});
		}
	}
});
