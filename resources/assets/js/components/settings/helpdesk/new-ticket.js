Vue.component('new-ticket', {
  props: ['user'],

  /**
   * The component's data.
   */
  data() {
    return {
      createForm: new SparkForm(this.ticketCreateForm()),
    };
  },


  /**
   * The component has been created by Vue.
   */
  created() {
    var self = this;
  },


  methods: {
    /**
     * Returns the form for inserting Invitations.
     */
    ticketCreateForm() {
      return {
        subject: '',
        body: '',
        attachments: [],
        busy: false,
      };
    },

    /**
     * Process the file upload.
     * @param e
     * @returns {*}
     */
    onFileChange(e) {
      var self = this;
      if (!e.target.files.length) {
        return;
      } else {
        var length = e.target.files.length;
        for (var i = 0; i < length; i++) {
          (function(file) {
            var reader = new FileReader();
            var uploadFile = {name: '', data: ''};
            uploadFile.name = file.name;
            reader.onload = function (event) {
              return uploadFile.data = event.target.result;
            };
            reader.readAsDataURL(file);
            self.createForm.attachments.push(uploadFile);
          })(e.target.files[i]);
        }
      }
    },

    createTicket() {
      this.createForm.busy = true;
      Spark.post('/helpdesk/tickets', this.createForm)
      .then(response => {
        console.log(response);
          if (response.hasOwnProperty('redirect') && response.redirect) {
            window.location = response.redirect;
          }
      });
    }
  }
});
