Vue.component('view-ticket', {
  props: ['user'],
  /**
   * The component's data.
   */
  data() {
    return {
      loading: false,
      savingReply: false,
      conversation: null,
      messages: [],
      replyForm: new SparkForm(this.messageCreateForm()),
    };
  },


  /**
   * The component has been created by Vue.
   */
  created() {
    var self = this;

    this.$parent.$on('showConversation', function(id) {
      self.getConversation(id);
    });
  },


  /**
   * Prepare the component.
   */
  mounted() {
    Mousetrap.bind('esc', e => this.showConversationsList());
  },

  methods: {
    /**
     * Get the conversation.
     */
    getConversation(id) {
      this.loading = true;

      axios.get('/helpdesk/tickets/' + id)
        .then(response => {
          this.processResponse(response.data);
          this.loading = false;
        });
    },

    processResponse(response) {
      this.conversation = response.conversation;
      this.messages = response.messages;

      switch (this.conversation.status) {
        case 'A':
          this.conversation.status = 'Answered';
          break;
        case 'P':
          this.conversation.status = 'Calling';
          break;
        case 'T':
          this.conversation.status = 'Chatting';
          break;
        case 'X':
          this.conversation.status = 'Deleted';
          break;
        case 'B':
          this.conversation.status = 'Spam';
          break;
        case 'I':
          this.conversation.status = 'Init';
          break;
        case 'C':
          this.conversation.status = 'Open';
          break;
        case 'R':
          this.conversation.status = 'Resolved';
          break;
        case 'N':
          this.conversation.status = 'New';
          break;
        case 'W':
          this.conversation.status = 'Postponed';
          break;
      }
    },

    /**
     * Show the search results and hide the user profile.
     */
    showConversationsList() {
      this.$parent.$emit('showConversationsList');

      this.conversation = null;
    },

    /**
     * Returns the form for adding messages to Conversations.
     */
    messageCreateForm() {
      return {
        body: '',
        attachments: [],
        busy: false,
      };
    },

    /**
     * Process the file upload.
     * @param e
     * @returns {*}
     */
    onFileChange(e) {
      var self = this;
      if (!e.target.files.length) {
        return;
      } else {
        var length = e.target.files.length;
        for (var i = 0; i < length; i++) {
          (function(file) {
            var reader = new FileReader();
            var uploadFile = {name: '', data: ''};
            uploadFile.name = file.name;
            reader.onload = function (event) {
              return uploadFile.data = event.target.result;
            };
            reader.readAsDataURL(file);
            self.replyForm.attachments.push(uploadFile);
          })(e.target.files[i]);
        }
      }
    },

    addMessage() {
      var self = this;
      self.savingReply = true;
      self.replyForm.busy = true;
      Spark.put('/helpdesk/tickets/' + this.conversation.conversationid, this.replyForm)
        .then(response => {
            self.processResponse(response);
            self.savingReply = false;
            self.replyForm = new SparkForm(self.messageCreateForm());
        });
    }
  },
});
