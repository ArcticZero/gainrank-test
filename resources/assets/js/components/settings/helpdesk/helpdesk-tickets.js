Vue.component('helpdesk-tickets', {
  props: ['user'],

  /**
   * The component's data.
   */
  data() {
    return {
      loading: false,
      noResults: false,
      conversations: [],
      showingConversation: false
    };
  },


  /**
   * The component has been created by Vue.
   */
  created() {
    var self = this;

    this.getConversations();

    this.$on('showConversationsList', function(){
      self.showConversationsList();
    });

    Bus.$on('sparkHashChanged', function (hash, parameters) {
      if (hash != 'tickets') {
        return true;
      }

      if (parameters && parameters.length > 0) {
        self.loadConversation({ conversationid: parameters[0] });
      } else {
        self.showConversationsList();
      }

      return true;
    });
  },


  mounted() {
    if (window.location.hash == '#request-service') {
      $("#helpdesk-tabs a[href='#new-ticket']").tab('show');
      $("input[name='subject']").val("[Service Request] - Your service name here").focus();
    }
  },

  methods: {
    /**
     * Loads all the conversations for the current user
     */
    getConversations() {
      var self = this;
      this.loading = true;
      this.noResults = false;
      axios.get('/helpdesk/tickets')
        .then(response => {
          this.conversations = response.data.conversations;
          this.noResults = this.conversations.length === 0;
          _.forEach(this.conversations, function(value, key) {
            switch (value.status) {
              case 'A':
                self.conversations[key].status = 'Answered';
                break;
              case 'P':
                self.conversations[key].status = 'Calling';
                break;
              case 'T':
                self.conversations[key].status = 'Chatting';
                break;
              case 'X':
                self.conversations[key].status = 'Deleted';
                break;
              case 'B':
                self.conversations[key].status = 'Spam';
                break;
              case 'I':
                self.conversations[key].status = 'Init';
                break;
              case 'C':
                self.conversations[key].status = 'Open';
                break;
              case 'R':
                self.conversations[key].status = 'Resolved';
                break;
              case 'N':
                self.conversations[key].status = 'New';
                break;
              case 'W':
                self.conversations[key].status = 'Postponed';
                break;
            }
          });
          this.loading = false;
        });
    },

    /**
     * Load a conversation
     * @param conversation_id
     */
    getConversation(conversation_id) {
      this.loading = true;

      axios.get('/helpdesk/tickets/' + conversation_id)
        .then(response => {
          console.log(response);

          this.loading = false;
        });
    },

    /**
     * Show the converstations list.
     */
    showConversationsList() {
      history.pushState(null, null, '#/tickets');

      this.showingConversation = false;
    },


    /**
     * Show the selected conversation.
     */
    showConversation(conversation) {
      history.pushState(null, null, '#/tickets/' + conversation.conversationid);

      this.loadConversation(conversation);
    },


    /**
     * Load a conversation.
     */
    loadConversation(conversation) {
      this.$emit('showConversation', conversation.conversationid);

      this.showingConversation = true;
    }
  }
});
