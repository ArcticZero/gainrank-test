Vue.component('invitations', {
  props: ['user'],

  /**
   * The component's data.
   */
  data() {
    return {
      invitations: [],
      deletingInvitation: null,
      createForm: new SparkForm(this.invitationCreateForm()),
      deleteForm: new SparkForm({}),
    };
  },


  /**
   * The component has been created by Vue.
   */
  created() {
    var self = this;

    Bus.$on('sparkHashChanged', function (hash, parameters) {
      if (hash == 'invitations' && self.invitations.length === 0) {
        self.getInvitations();
      }
    });
  },

  methods: {
    /**
     * Returns the form for inserting Invitations.
     */
    invitationCreateForm() {
      return {
        email: '',
      };
    },

    /**
     * Get all Invitations.
     */
    getInvitations() {
      self = this;
      axios.get('/users/' + this.user.id + '/invitations')
        .then(response => {
          this.invitations = response.data;
        });
    },

    /**
     * Create a new Invitation.
     */
    create() {
      Spark.post('/users/' + this.user.id + '/invitations', this.createForm)
        .then(() => {
            this.createForm = new SparkForm(this.invitationCreateForm());
            this.getInvitations();
        });
    },


    /**
     * Show the approval dialog for deleting a Invitation.
     */
    approveInvitationDelete(invitation) {
      this.deletingInvitation = invitation;

      $('#modal-delete-invitation').modal('show');
    },


    /**
     * Delete the specified Invitation.
     */
    deleteInvitation() {
      Spark.delete('/users/' + this.user.id + '/invitations/' + this.deletingInvitation.id, this.deleteForm)
        .then(() => {
            this.getInvitations();
            $('#modal-delete-invitation').modal('hide');
        });
    },
  }
});
