Vue.component('subscribe-base', {
    props: ['user', 'team', 'plans', 'billableType'],

    mixins: [
        require('./../../../../../../spark/resources/assets/js/mixins/plans'),
        require('./../../../../../../spark/resources/assets/js/mixins/subscriptions'),
        require('./../../../../../../spark/resources/assets/js/mixins/vat')
    ],

    data() {
        return {
            form: new SparkForm({
                payment_method: ''
            }),
            pendingDataReady: false,
            hasPending: false
        };
    },

    created() {
        this.getPendingSubscription();
    },

    methods: {
    	selectPlan(plan) {
            this.selectedPlan = plan;

            this.form.plan = this.selectedPlan.id;
        },

        showPlanDetails(plan) {
            this.$parent.$emit('showPlanDetails', plan);
        },

        hasSubscribed(plan) {
            return !!_.where(this.billable.subscriptions, {provider_plan: plan.id}).length
        },

        subscribe(e) {
            e.preventDefault();
            errors = {};

            paymentMethod = this.form.payment_method;
            selectedMethod = paymentMethod ? paymentMethod.value : null;

            // no payment method selected
            if (!paymentMethod)
                errors.payment_method = ['Select a payment method'];

            if (!$.isEmptyObject(errors))
                this.form.setErrors(errors);
            else
                Spark.post('/settings/subscription/checkout', this.form)
                    .then(response => {
                        if (response.success && response.approval_url)
                            window.location.href = response.approval_url;
                        else {
                            errors.form = [response.error];
                            this.form.setErrors(errors);
                        }
                    });
        },

        getPendingSubscription() {
            axios.get('/settings/subscription/pending')
                .then(response => {
                    this.pendingDataReady = true;

                    if (response.data.subscription)
                        this.hasPending = true;
                    else
                        this.hasPending = false;
                });

            return true;
        }
    },

    computed: {
        urlForNewSubscription() {
            return this.billingUser
                            ? '/settings/subscription'
                            : `/settings/${Spark.pluralTeamString}/${this.team.id}/subscription`;
        }
    }
});
