
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */

require('./../spark-components/bootstrap');

require('./home');

require('./../spark-components/settings/profile/update-profile-notifications');
require('./../spark-components/settings/profile/update-profile-timezone');

require('./kiosk/paypal/notifications');
require('./settings/payment-method-paypal');

require('./settings/subscription/subscribe-base');
require('./settings/subscription/subscribe-paypal');
require('./settings/subscription/subscribe-coinbase');

require('./settings/subscription/subscription-details');

require('./settings/payment-method/update-payment-method-paypal');
require('./settings/invitations');
require('./settings/security/enable-two-factor-auth');
require('./settings/security/manage-two-factor-auth');

require('./helpdesk');

// kiosk stuff

require('./kiosk/coinbase/notifications');
require('./kiosk/orders/pending');
require('./fingerprints');
require('./settings/helpdesk/new-ticket');
require('./settings/helpdesk/helpdesk-tickets');
require('./settings/helpdesk/view-ticket');
require('./campaigns');
require('./affiliate');

// services

require('./services/ranktracker/ranktracker');
require('./services/ranktracker/domain');
require('./services/alexa/alexa');
require('./services/ctr/ctr');
require('./services/traffic/traffic');
require('./services/wayback/wayback');

require('./kiosk/ranktracker/ranktracker');
require('./kiosk/alexa/alexa');
require('./kiosk/ctr/ctr');
require('./kiosk/traffic/traffic');
require('./kiosk/wayback/wayback');
