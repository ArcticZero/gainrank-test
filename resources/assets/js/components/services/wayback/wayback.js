import Datepicker from 'vuejs-datepicker';

Vue.component('wayback', {
    props: ['user', 'rows'],

    components: {
        Datepicker
    },

    data() {
    	return {
    		form: new SparkForm({
                url: '',
                wayback_date: ''
            })
    	}
    },

    methods: {
        /**
         * Delete a request
         */
        deleteRow(id) {
        	swal({
                title: "Confirmation",
                text: "Are you sure you want to delete this request?",
                icon: "warning",
                dangerMode: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            }, function() {
                var row = $("tr[data-row-id=" + id + "]");
                //axios.delete('/wayback' + id);
                $(row).fadeOut('fast');
            });
        },

        /**
         * Submit the form.
         */
        submitForm() {
        	var modal = $("#add-row-modal");
            Spark.post('/wayback', this.form)
                .then(response => {
                    swal({
                        title: 'Success!',
                        text: 'Request has been sent successfully.',
                        type: 'success'
                    }, function() {
                        modal.modal('hide');
                        location.reload();

                        // TODO: Bind table rows similar to kiosk views, to take advantage of reactivity
                    });
                });
        }
    }

});
