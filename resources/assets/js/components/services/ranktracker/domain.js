Vue.component('domain', {
    props: ['user', 'rows'],

    data() {
    	return {
    		form: new SparkForm({
                url: '',
            })
    	}
    },

    methods: {
        /**
         * Delete a domain
         */
        deleteRow(id) {
        	swal({
                title: "Confirmation",
                text: "Are you sure you want to delete this domain?",
                icon: "warning",
                dangerMode: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            }, function() {
                var row = $("tr[data-row-id=" + id + "]");
                //axios.delete('/ctr' + id);
                $(row).fadeOut('fast');
            });
        },

        /**
         * Submit the form.
         */
        submitForm() {
        	var modal = $("#add-row-modal");
            Spark.post('/ranktracker/domains', this.form)
                .then(response => {
                    swal({
                        title: 'Success!',
                        text: 'Domain has been saved successfully.',
                        type: 'success'
                    }, function() {
                        modal.modal('hide');
                        location.reload();

                        // TODO: Bind table rows similar to kiosk views, to take advantage of reactivity
                    });
                });
        }
    }

});
