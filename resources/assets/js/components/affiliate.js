Vue.component('affiliate-transactions', {
    data() {
        return {
            transactions: {
                data: [],
                current_page: 0,
                last_page: 0,
                per_page: 0,
                from: 0,
                to: 0,
                total: 0,
                next_page_url: null,
                prev_page_url: null
            },
            isLoadingData: false,
            filter: ''
        };
    },

    created() {
        var self = this;

        self.getTransactions();
    },

    methods: {
        getTransactions(page) {
            if (typeof page === 'undefined') {
                page = 1;
            }

            this.isLoadingData = true;

            axios.get('/affiliates/referral_transactions?page=' + page + '&filter=' + this.filter)
                .then(response => {
                    this.transactions = response.data;
                    this.isLoadingData = false;
                });
        },

        prevPage() {
            page = this.transactions.current_page - 1;
            this.selectPage(page);
        },

        nextPage() {
            page = this.transactions.current_page + 1;
            this.selectPage(page);
        },

        jumpToPage() {
            page = $("#page-number").val();
            this.selectPage(page);
        },

        selectPage(page) {
            if (this.isValidPage(page)) {
                $("#page-number").val(page);
                this.transactions.current_page = page;
                this.getTransactions(page);
            } else {
                alert('Invalid page number');
                $("#page-number").val(this.transactions.current_page);
            }
        },

        filterRows() {
            this.filter = $("#filter").val();
            this.getTransactions();
        },

        resetRows() {
            $("#filter").val("");
            this.filterRows();
        },

        isValidPage(page) {
            return /^\+?(0|[1-9]\d*)$/.test(page) && page > 0 && page <= this.transactions.last_page;
        }

    }
});
