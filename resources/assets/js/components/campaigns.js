Vue.component('campaigns', {
    /**
     * The component's data.
     */
    data() {
        return {
            campaigns: [],
            updatingCampaign: null,
            deletingCampaign: null,

            createForm: new SparkForm(this.campaignsCreateForm()),
            updateForm: new SparkForm(this.campaignsCreateForm()),

            deleteForm: new SparkForm({}),

            bonusesList: [],
        };
    },


    /**
     * The component has been created by Vue.
     */
    created() {
        var self = this;

        Bus.$on('sparkHashChanged', function (hash, parameters) {
            if (hash == 'campaigns' && self.campaigns.length === 0) {
                self.getCampaigns();
            }
        });
        this.getBonuses();
    },

    methods: {
       /**
        * Returns the form for inserting Campaigns.
        */
        campaignsCreateForm() {
            return {
                name: '',
                description: '',
                start_date: '',
                end_date: '',
                max_uses: 0,
                path: '',
                active: 1,
                bonuses: {},
            };
        },

        /**
         * Get all Campaigns.
         */
        getCampaigns() {
            this.page = this.page || 0;
            this.last_page = this.last_page || 0;
            axios.get('/campaigns?page=' + this.page)
              .then(response => {
                this.page = response.data.current_page;
                this.last_page = response.data.last_page;
                this.campaigns = response.data.data;
            });
        },

        /**
         * Get all bonus types.
         */
        getBonuses() {
            axios.get('/campaign-bonus-types')
              .then(response => {
                this.bonusesList = response.data;
            });
        },

        /**
         * Display the next page of Campaigns.
         */
        nextPage() {
            this.page += 1;
            this.getCampaigns();
        },


        /**
         * Display the previous page of Campaigns.
         */
        previousPage() {
            this.page -= 1;
            this.getCampaigns();
        },

        /**
         * Create a new Campaign.
         */
        create() {
            axios.post('/campaigns', this.createForm)
              .then(() => {
                this.createForm = new SparkForm(this.campaignsCreateForm());
                $("#creatFormError").addClass('hide');
                this.getCampaigns();
              })
              .catch(error => {
                if (error.response.status == 422) {
                  $("#createFormError").html(error.response.data.status).removeClass('hide');
                }
              });
        },


        /**
         * Edit the given campaign.
         */
        editCampaign(campaign) {
          var self = this;
          this.updatingCampaign = campaign;

          this.updateForm.name = campaign.name;
          _.forEach(campaign.bonuses, function (bonus) {
            self.updateForm.bonuses[bonus.ad_campaign_bonus_type_id] = bonus.bonus_amount;
          });
          this.updateForm.description = campaign.description;
          this.updateForm.start_date = campaign.start_date.replace(' 00:00:00', '');
          this.updateForm.end_date = campaign.end_date.replace(' 00:00:00', '');
          this.updateForm.max_uses = campaign.max_uses;
          this.updateForm.path = campaign.path;
          this.updateForm.active = campaign.active;

          $('#modal-update-campaign').modal('show');
        },


        /**
         * Update the specified Campaign.
         */
        update() {
            axios.put('/campaigns/' + this.updatingCampaign.id, this.updateForm)
              .then(() => {
                  this.getCampaigns();
                  $("#updateFormError").addClass('hide');
                  $('#modal-update-campaign').modal('hide');
              })
              .catch(error => {
                if (error.response.status == 422) {
                  $("#updateFormError").html(error.response.data.status).removeClass('hide');
                }
              });
        },


        /**
         * Show the approval dialog for deleting a Campaign.
         */
        approveCampaignDelete(campaign) {
            this.deletingCampaign = campaign;

            $('#modal-delete-campaign').modal('show');
        },


        /**
         * Delete the specified Campaign.
         */
        deleteCampaign() {
            Spark.delete('/campaigns/' + this.deletingCampaign.id, this.deleteForm)
              .then(() => {
                  this.getCampaigns();
                  $('#modal-delete-campaign').modal('hide');
              });
        },
    }
});
