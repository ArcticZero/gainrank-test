Vue.component('kiosk-orders-pending', {
    /**
     * The component's data.
     */
    data() {
        return {
            orders: {
                data: [],
                current_page: 0,
                last_page: 0,
                per_page: 0,
                from: 0,
                to: 0,
                total: 0,
                next_page_url: null,
                prev_page_url: null
            },
            isLoadingData: false,
            filter: '',
            approvingOrder: false,
            denyingOrder: false,
            orderId: null,
            moment: window.mtz,
            timezone: window.timezone
        };
    },

    /**
     * The component has been created by Vue.
     */
    created() {
        var self = this;

        Bus.$on('sparkHashChanged', function (hash, parameters) {
            if (hash == 'orders-pending') {
                self.getOrders();
            }
        });
    },


    methods: {
        /**
         * Get all orders.
         */
        getOrders(page) {
            if (typeof page === 'undefined') {
                page = 1;
            }

            this.isLoadingData = true;

            axios.get('/kiosk/orders/pending?page=' + page + '&filter=' + this.filter)
                .then(response => {
                    this.orders = response.data;
                    this.isLoadingData = false;
                });
        },

        /**
         * Pagination methods
         */
        prevPage() {
            page = this.orders.current_page - 1;
            this.selectPage(page);
        },

        nextPage() {
            page = this.orders.current_page + 1;
            this.selectPage(page);
        },

        jumpToPage() {
            page = $("#page-number").val();
            this.selectPage(page);
        },

        selectPage(page) {
            if (this.isValidPage(page)) {
                $("#page-number").val(page);
                this.orders.current_page = page;
                this.getOrders(page);
            } else {
                alert('Invalid page number');
                $("#page-number").val(this.orders.current_page);
            }
        },

        /**
         * Filter results
         */
        filterRows() {
            this.filter = $("#order-filter").val();
            this.getOrders();
        },

        resetRows() {
            $("#order-filter").val("");
            this.filterRows();
        },

        /**
         * Check if valid page number
         */
        isValidPage(page) {
            return /^\+?(0|[1-9]\d*)$/.test(page) && page > 0 && page <= this.orders.last_page;
        },

        /**
         * Approve/deny order
         */
        confirmApproveOrder(id) {
            console.log(this.orderId);
            this.denyingOrder = false;
            this.approvingOrder = true;
            this.orderId = id;
            $("#modal-process-order").modal('show');
        },

        confirmDenyOrder(id) {
            this.approvingOrder = false;
            this.denyingOrder = true;
            this.orderId = id;
            $("#modal-process-order").modal('show');
        },

        approveOrder() {
            $("#modal-process-order").modal('hide');
            $("#order" + this.orderId).fadeOut();

            axios.post('/kiosk/orders/approve', {id: this.orderId});

            this.approvingOrder = false;
            this.orderId = null;
        },

        denyOrder() {
            $("#modal-process-order").modal('hide');
            $("#order" + this.orderId).fadeOut();

            axios.post('/kiosk/orders/deny', {id: this.orderId});

            this.denyingOrder = false;
            this.orderId = null;
        },

        approvingOrder() {
            return this.approvingOrder;
        },

        denyingOrder() {
            return this.denyingOrder;
        }
    }
});

