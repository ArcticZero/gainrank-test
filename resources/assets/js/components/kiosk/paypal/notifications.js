Vue.component('kiosk-paypal-notifications', {
    /**
     * The component's data.
     */
    data() {
        return {
            notifications: {
                data: [],
                current_page: 0,
                last_page: 0,
                per_page: 0,
                from: 0,
                to: 0,
                total: 0,
                next_page_url: null,
                prev_page_url: null
            },
            isLoadingData: false,
            filter: '',
            moment: window.mtz,
            timezone: window.timezone
        };
    },

    /**
     * The component has been created by Vue.
     */
    created() {
        var self = this;

        Bus.$on('sparkHashChanged', function (hash, parameters) {
            if (hash == 'paypal-notifications') {
                self.getNotifications();
            }
        });
    },


    methods: {
        /**
         * Get all notifications.
         */
        getNotifications(page) {
            if (typeof page === 'undefined') {
                page = 1;
            }

            this.isLoadingData = true;

            axios.get('/kiosk/paypal/notifications?page=' + page + '&filter=' + this.filter)
                .then(response => {
                    this.notifications = response.data;
                    this.isLoadingData = false;
                });
        },

        /**
         * Pagination methods
         */
        prevPage() {
            page = this.notifications.current_page - 1;
            this.selectPage(page);
        },

        nextPage() {
            page = this.notifications.current_page + 1;
            this.selectPage(page);
        },

        jumpToPage() {
            page = $("#page-number").val();
            this.selectPage(page);
        },

        selectPage(page) {
            if (this.isValidPage(page)) {
                $("#page-number").val(page);
                this.notifications.current_page = page;
                this.getNotifications(page);
            } else {
                alert('Invalid page number');
                $("#page-number").val(this.notifications.current_page);
            }
        },

        /**
         * Filter results
         */
        filterRows() {
            this.filter = $("#filter").val();
            this.getNotifications();
        },

        resetRows() {
            $("#filter").val("");
            this.filterRows();
        },

        /**
         * Check if valid page number
         */
        isValidPage(page) {
            return /^\+?(0|[1-9]\d*)$/.test(page) && page > 0 && page <= this.notifications.last_page;
        }
    }
});

