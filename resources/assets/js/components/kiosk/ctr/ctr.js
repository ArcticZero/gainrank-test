Vue.component('kiosk-ctr', {
    /**
     * The component's data.
     */
    data() {
        return {
            submissions: {
                data: [],
                current_page: 0,
                last_page: 0,
                per_page: 0,
                from: 0,
                to: 0,
                total: 0,
                next_page_url: null,
                prev_page_url: null
            },
            statuses: {
                data: [],
                current_page: 0,
                last_page: 0,
                per_page: 0,
                from: 0,
                to: 0,
                total: 0,
                next_page_url: null,
                prev_page_url: null
            },
            settings: {},
            isLoadingData: false,
            submissionFilter: '',
            statusFilter: '',
            statusForm: new SparkForm({
                id: '',
                name: '',
                code: '',
                cust_message: '',
                cust_url: '',
                staff_message: ''
            }),
            statusFormId: null,
            statusFormTitle: 'Add Status',
            settingsForm: '',
            moment: window.mtz,
            timezone: window.timezone
        };
    },

    /**
     * The component has been created by Vue.
     */
    created() {
        var self = this;

        Bus.$on('sparkHashChanged', function (hash, parameters) {
            if (hash == 'ctr') {
                self.getSubmissions();
                self.getSettings();
                self.getStatuses();
            }  
        });
    },

    methods: {
        /**
         * Get all rows.
         */
        getSubmissions(page) {
            if (typeof page === 'undefined') {
                page = 1;
            }

            this.isLoadingData = true;

            axios.get('/kiosk/ctr/submissions?page=' + page + '&filter=' + this.submissionFilter)
                .then(response => {
                    this.submissions = response.data;
                    this.isLoadingData = false;
                });
        },

        getSettings(page) {
            this.isLoadingData = true;

            axios.get('/kiosk/ctr/settings')
                .then(response => {
                    this.settings = response.data
                    this.isLoadingData = false;
                    this.settingsForm = new SparkForm(this.settings);
                });
        },

        getStatuses(page) {
            if (typeof page === 'undefined') {
                page = 1;
            }

            this.isLoadingData = true;

            axios.get('/kiosk/ctr/statuses?page=' + page + '&filter=' + this.statusFilter)
                .then(response => {
                    this.statuses = response.data;
                    this.isLoadingData = false;
                });
        },

        /**
         * Pagination methods
         */
        submissionsPrevPage() {
            page = this.submissions.current_page - 1;
            this.submissionsSelectPage(page);
        },

        submissionsNextPage() {
            page = this.submissions.current_page + 1;
            this.submissionsSelectPage(page);
        },

        submissionsJumpToPage(page) {
            if (page === undefined) {
                page = $("#ctr-submissions-page-number").val();
            }

            this.submissionsSelectPage(page);
        },

        submissionsSelectPage(page) {
            if (this.isValidSubmissionsPage(page)) {
                $("#ctr-submissions-page-number").val(page);
                this.submissions.current_page = page;
                this.getSubmissions(page);
            } else {
                swal({
                    title: 'Whoops',
                    text: 'You have specified an invalid page number.',
                    type: 'warning',
                    timer: 2000
                });
                $("#ctr-submissions-page-number").val(this.submissions.current_page);
            }
        },

        statusesPrevPage() {
            page = this.statuses.current_page - 1;
            this.statusesSelectPage(page);
        },

        statusesNextPage() {
            page = this.statuses.current_page + 1;
            this.statusesSelectPage(page);
        },

        statusesJumpToPage(page) {
            if (page === undefined) {
                page = $("#ctr-statuses-page-number").val();
            }

            this.statusesSelectPage(page);
        },

        statusesSelectPage(page) {
            if (this.isValidStatusesPage(page) || page == 1) {
                $("#ctr-statuses-page-number").val(page);
                this.statuses.current_page = page;
                this.getStatuses(page);
            } else {
                swal({
                    title: 'Whoops',
                    text: 'You have specified an invalid page number.',
                    type: 'warning',
                    timer: 2000
                });
                $("#ctr-statuses-page-number").val(this.statuses.current_page);
            }
        },

        /**
         * Filter results
         */
        filterSubmissions() {
            var minLength = 4;

            if (this.submissionFilter.length >= minLength) {
                this.getSubmissions();
            } else {
                swal({
                    title: 'Whoops',
                    text: 'Your search term must be at least ' + minLength + ' characters long.',
                    type: 'warning',
                    timer: 2000
                });
            }
        },

        resetSubmissions() {
            this.submissionFilter = '';
            this.getSubmissions();
        },

        filterStatuses() {
            var minLength = 3;

            if (this.statusFilter.length >= minLength) {
                this.getStatuses();
            } else {
                swal({
                    title: 'Whoops',
                    text: 'Your search term must be at least ' + minLength + ' characters long.',
                    type: 'warning',
                    timer: 2000
                });
            }
        },

        resetStatuses() {
            this.statusFilter = '';
            this.getStatuses();
        },

        /**
         * Check if valid page number
         */
        isValidSubmissionsPage(page) {
            return /^\+?(0|[1-9]\d*)$/.test(page) && page > 0 && page <= this.submissions.last_page;
        },

        isValidStatusesPage(page) {
            return /^\+?(0|[1-9]\d*)$/.test(page) && page > 0 && page <= this.statuses.last_page;
        },

        /**
         * Show edit submission form
         */
        showSubmissionForm(id) {
            // TODO: Build this form
        },

        /**
         * Show edit form
         */
        showStatusForm(id) {
            var modal = $("#ctr-status-modal");
            var row = false;

            // find that status
            for (var i = 0; i < this.statuses.data.length; i++) {
                if (this.statuses.data[i].id == id) {
                    row = this.statuses.data[i];
                    break;
                }
            }

            // reset form status
            this.statusForm.resetStatus();
            
            if (row) {
                // update form view
                this.statusFormTitle = 'Edit Status';
                this.statusFormId = row.id;

                // update form data
                this.statusForm.id = row.id;
                this.statusForm.name = row.name;
                this.statusForm.code = row.code;
                this.statusForm.cust_message = row.cust_message;
                this.statusForm.cust_url = row.cust_url;
                this.statusForm.staff_message = row.staff_message;

                $("#ctr-field-status-id").prop('readonly', true);
            } else {
                // update form view
                this.statusFormTitle = 'Add Status';
                this.statusFormId = null;

                // update form data
                this.statusForm.id = '';
                this.statusForm.name = '';
                this.statusForm.code = '';
                this.statusForm.cust_message = '';
                this.statusForm.cust_url = '';
                this.statusForm.staff_message = '';

                $("#ctr-field-status-id").prop('readonly', false);
            }

            $("#ctr-status-modal").modal('show');
        },

        /**
         * Save status
         */
        saveStatus() {
            var modal = $("#ctr-status-modal");
            var self = this;

            if (this.statusFormId) {
                Spark.put('/kiosk/ctr/statuses/' + this.statusFormId, this.statusForm)
                    .then(response => {
                        swal({
                            title: 'Success!',
                            text: 'Status has been saved successfully.',
                            type: 'success'
                        }, function() {
                            self.statusesJumpToPage(self.statuses.current_page);
                            modal.modal('hide');
                        });
                });
            } else {
                Spark.post('/kiosk/ctr/statuses', this.statusForm)
                    .then(response => {
                        swal({
                            title: 'Success!',
                            text: 'Status has been added successfully.',
                            type: 'success'
                        }, function() {
                            self.statusesJumpToPage(1);
                            modal.modal('hide');
                        });
                });
            }
        },

        /**
         * Save settings
         */
        saveSettings() {
            Spark.put('/kiosk/ctr/settings', this.settingsForm);

            swal({
                title: 'Success!',
                text: 'Service settings have been updated successfully.',
                type: 'success'
            });
        },

        /**
         * Delete status
         */
        deleteStatus(id) {
            var self = this;

            swal({
                title: "Confirmation",
                text: "Are you sure you want to delete this status?",
                icon: "warning",
                dangerMode: true,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            }, function() {
                var row = $("#ctr-statuses-table").find("tr[data-status-id=" + id + "]");
                axios.delete('/kiosk/ctr/statuses/' + id);

                $(row).fadeOut('fast', function() {
                    self.statuses.total--;
                    if (self.statuses.total == 0) {
                        self.statusesJumpToPage(1);
                    }
                });
            });
        },

        /**
         * Issue a refund
         */
        issueRefund(id) {
            // TODO: Build this function
        }
    }
});

