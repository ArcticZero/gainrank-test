Vue.component('fingerprints', {
    props: ['user'],

    /**
     * The component's data.
     */
    data() {
        return {
            plans: [],
            hashes: [],

            hashID: null,

            searching: false,
            noSearchResults: false,
            searchResults: [],
            moment: window.mtz,
            timezone: window.timezone
        };
    },


    /**
     * The component has been created by Vue.
     */
    created() {
        var self = this;

        this.$on('showSearch', function(){
            self.navigateToSearch();
        });
    },


    methods: {
        /**
         * Display the next page of Hashes.
         */
        nextPage() {
            this.page += 1;
            this.search();
        },


        /**
         * Display the previous page of Hashes.
         */
        previousPage() {
            this.page -= 1;
            this.search();
        },

        /**
         * Load a specific hash's details.
         */
        viewHash(hash) {
            this.hashID = hash;
            this.search();
        },

        /**
         * Perform a search for the given query.
         */
        search() {
            var self = this;
            this.searching = true;
            this.noSearchResults = false;
            this.page = this.page || 0;
            this.last_page = this.last_page || 0;
            axios.get('/fingerprints/' + this.hashID + '?page=' + this.page)
              .then(response => {
                this.page = response.data.current_page;
                this.last_page = response.data.last_page;
                if (this.hashID == '*') {
                    this.hashes = response.data.data;
                    this.noSearchResults = this.hashes.length === 0;
                } else {
                    self.searchResults = [];
                    var userList = {};
                    _.forEach(response.data, function (user) {
                        if (!userList[user.email]) {
                            userList[user.email] = {'id': user.id, 'dates': []};
                        }
                        userList[user.email].dates.push(user.pivot.created_at);
                    });
                    _.map(userList, function(info, email) {
                        self.searchResults.push({'email': email, 'id': info.id, 'dates': info.dates});
                    });
                    this.noSearchResults = this.searchResults.length === 0;
                }
                this.searching = false;
              });
        },


        /**
         * Show the search results and update the browser history.
         */
        navigateToSearch() {
            history.pushState(null, null, '#/fingerprints');

            this.showSearch();
        },


        /**
         * Show the search results.
         */
        showSearch() {
            this.showingUserProfile = false;

            Vue.nextTick(function () {
                $('#fingerprints').focus();
            });
        },
    }
});
