<spark-security :user="user" inline-template>
	<div>
	    <!-- Update Password -->
	    @include('spark::settings.security.update-password')

	    <!-- Two-Factor Authentication -->
	    <div v-if="user && !user.tfa_enable">
        	@include('settings.security.enable-two-factor-auth')
        </div>

        <div v-if="user && user.tfa_enable">
        	@include('settings.security.manage-two-factor-auth')
        </div>
    </div>
</spark-security>
