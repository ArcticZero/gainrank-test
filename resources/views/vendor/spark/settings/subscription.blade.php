<spark-subscription :user="user" :team="team" :billable-type="billableType" inline-template>
    <div>
        <div v-if="plans.length > 0">
            <!-- Trial Expiration Notice -->
            @include('spark::settings.subscription.trial-expiration-notice')

            <!-- New Subscription -->
            <div v-if="needsSubscription">
                @include('spark::settings.subscription.subscribe')
            </div>

            <!-- Update Subscription -->
            <div v-if="subscriptionIsActive && subscriptionIsRecurring">
                @include('spark::settings.subscription.update-subscription')
            </div>

            <!-- Subscription Details -->
            <div v-if="subscriptionIsOnGracePeriod && !subscriptionIsRecurring">
                @include('settings.subscription.subscription-details')
            </div>

            <!-- Resume Subscription -->
            <div v-if="(subscriptionIsOnGracePeriod && subscriptionIsRecurring) || (!subscriptionIsRecurring && subscriptionIsCancelled)">
                @include('spark::settings.subscription.resume-subscription')
            </div>

            <!-- Cancel Subscription -->
            <div v-if="hasAnySubscription && !subscriptionIsCancelled">
                @include('spark::settings.subscription.cancel-subscription')
            </div>
        </div>

        <!-- Plan Features Modal -->
        @include('spark::modals.plan-details')
    </div>
</spark-subscription>
