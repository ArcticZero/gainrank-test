<new-ticket inline-template>
  <div>
    <div class="panel panel-default">
      @can('open new support ticket')
      <div class="panel-heading">
        Create New Ticket
      </div>

      <div class="panel-body">
        <form class="form-horizontal" role="form">
          <!-- Subject -->
          <div class="form-group" :class="{'has-error': createForm.errors.has('subject')}">
            <label class="col-md-4 control-label">Subject</label>

            <div class="col-md-6">
              <input type="text" class="form-control" name="subject" v-model="createForm.subject">

              <span class="help-block" v-show="createForm.errors.has('subject')">
                  @{{ createForm.errors.get('subject') }}
              </span>
            </div>
          </div>

          <!-- Body -->
          <div class="form-group" :class="{'has-error': createForm.errors.has('body')}">
            <label class="col-md-4 control-label">Body</label>

            <div class="col-md-6">
              <textarea class="form-control" v-model="createForm.body" rows="10"></textarea>
              <span class="help-block" v-show="createForm.errors.has('body')">
                  @{{ createForm.errors.get('body') }}
              </span>
            </div>
          </div>

          <!-- Attachments -->
          <div class="form-group" :class="{'has-error': createForm.errors.has('attachments')}">
            <label class="col-md-4 control-label">Attachments</label>

            <div class="col-md-6">
              <input type="file" id="attachments" ref="attachments" @change="onFileChange($event)" multiple/>
              <span class="help-block" v-show="createForm.errors.has('attachments')">
                  @{{ createForm.errors.get('attachments') }}
              </span>
            </div>
          </div>

          <!-- Create Button -->
          <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
              <button type="submit" class="btn btn-primary"
                      @click.prevent="createTicket"
                      :disabled="createForm.busy">
                Create
              </button>
            </div>
          </div>
        </form>
      </div>
      @else
        <div class="panel-heading">
          Error
        </div>

        <div class="panel-body">
          <div class="alert alert-warning">
            You are not allowed to create any more tickets.
          </div>
        </div>
      @endcan
    </div>
  </div>
</new-ticket>
