<helpdesk-tickets inline-template>
  <div>
    <div class="panel panel-default" v-if="loading && ! showingConversation">
      <div class="panel-heading">My Tickets </div>
      <!-- Loading Indicator -->
      <div class="panel-body">
        <i class="fa fa-btn fa-spinner fa-spin"></i>Loading Tickets
      </div>
    </div>

    <!-- No Tickets -->
    <div class="panel panel-warning" v-if=" ! loading && noResults && ! showingConversation">
      <div class="panel-heading">My Tickets</div>

      <div class="panel-body">
        No tickets submitted yet.
      </div>
    </div>

    <!-- Ticket list -->
    <div class="panel panel-default" v-if=" ! loading && conversations.length > 0 && ! showingConversation">
      <div class="panel-heading">My Tickets </div>
      <div class="panel-body">
        <table class="table table-striped table-condensed">
          <thead>
            <th>Subject</th>
            <th>Date</th>
            <th>Unread Replies</th>
            <th>Status</th>
            <th></th>
          </thead>

          <tbody>
            <tr v-for="conversation in conversations">
              <!-- Title -->
              <td>
                <div class="btn-table-align">
                  @{{ conversation.subject }}
                </div>
              </td>

              <!-- Date -->
              <td>
                <div class="btn-table-align">
                  @{{ conversation.datecreated | datetime }}
                </div>
              </td>

              <!-- Unread Replies -->
              <td>
                <div class="btn-table-align">
                  @{{ conversation.messagegroupsout - conversation.readgroupsout }}
                </div>
              </td>

              <!-- Status -->
              <td>
                <div class="btn-table-align">
                  @{{ conversation.status }}
                </div>
              </td>
              <td>
                <!-- View Button -->
                <button class="btn btn-success-outline" @click="showConversation(conversation)">
                  <i class="fa fa-eye"></i>
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Conversation Details -->
    <div v-show="showingConversation">
      @include('spark::settings.helpdesk.view-ticket')
    </div>
  </div>
</helpdesk-tickets>
