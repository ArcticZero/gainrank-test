<view-ticket inline-template>
    <div>
        <!-- Loading Indicator -->
        <div class="row" v-if="loading">
            <div class="panel panel-default">
                <div class="panel-body">
                    <i class="fa fa-btn fa-spinner fa-spin"></i>Loading
                </div>
            </div>
        </div>

        <!-- Helpdesk Ticket -->
        <div v-if=" ! loading && conversation">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-btn fa-times" style="cursor: pointer;" @click="showConversationsList"></i>
                    @{{ conversation.subject }}
                </div>
                <div class="panel-body">
                    <ul>
                        <li>Status: @{{ conversation.status }}</li>
                        <li>Created: @{{ conversation.datecreated | datetime }}</li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default" v-for="message in messages">
                <div class="panel-heading">
                    <div>@{{ message.user }}</div>
                </div>
                <div class="panel-body" v-html="message.message"></div>
            </div>
            @can('reply to support ticket')
            <div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div v-if="! savingReply">Add a Reply</div>
                        <div v-if="savingReply"><i class="fa fa-btn fa-spinner fa-spin"></i>Saving</div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form">
                            <!-- Body -->
                            <div class="form-group" :class="{'has-error': replyForm.errors.has('body')}">
                                <label class="col-md-4 control-label">Body</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" v-model="replyForm.body" rows="10"></textarea>
                                    <span class="help-block" v-show="replyForm.errors.has('body')">
                                        @{{ replyForm.errors.get('body') }}
                                    </span>
                                </div>
                            </div>

                            <!-- Attachments -->
                            <div class="form-group" :class="{'has-error': replyForm.errors.has('attachments')}">
                                <label class="col-md-4 control-label">Attachments</label>

                                <div class="col-md-6">
                                    <input type="file" id="attachments" ref="attachments" @change="onFileChange($event)" multiple/>
                                    <span class="help-block" v-show="replyForm.errors.has('attachments')">
                                        @{{ replyForm.errors.get('attachments') }}
                                    </span>
                                </div>
                            </div>

                            <!-- Create Button -->
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-6">
                                    <button type="submit" class="btn btn-primary"
                                            @click.prevent="addMessage"
                                            :disabled="replyForm.busy">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endcan
        </div>
    </div>
</view-ticket>