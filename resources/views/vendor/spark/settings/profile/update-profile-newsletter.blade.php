<update-profile-notifications :user="user" inline-template>
    <div class="panel panel-default">
        <div class="panel-heading">Notifications</div>

        <div class="panel-body">
            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your notification settings have been updated!
            </div>

            <form class="form-horizontal" role="form">
                <!-- Newsletter -->
                <div class="form-group" :class="{'has-error': form.errors.has('is_subscribed')}">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_subscribed" v-model="form.is_subscribed">
                                Keep subscribed to our newsletter
                            </label>

                            <span class="help-block" v-show="form.errors.has('is_subscribed')">
                                @{{ form.errors.get('is_subscribed') }}
                            </span>
                        </div>
                    </div>
                </div>

                <!-- Guided Tour -->
                <div class="form-group" :class="{'has-error': form.errors.has('show_guide')}">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="show_guide" v-model="form.show_guide">
                                Show guided tour
                            </label>

                            <span class="help-block" v-show="form.errors.has('show_guide')">
                                @{{ form.errors.get('show_guide') }}
                            </span>
                        </div>
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="update"
                                :disabled="form.busy">

                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</update-profile-notifications>
