<spark-profile :user="user" inline-template>
    <div>
        <!-- Update Profile Photo -->
        {{-- @include('spark::settings.profile.update-profile-photo') --}}

        <!-- Update Contact Information -->
        @include('spark::settings.profile.update-contact-information')

        <!-- Update Timezone Settings -->
        @include('settings.profile.update-profile-timezone')

        <!-- Update Notification Settings -->
        @include('settings.profile.update-profile-notifications')
    </div>
</spark-profile>

@section('scripts-bottom')
    <script type="text/javascript">
        window.fingerprint({excludeUserAgent: true}).get(function(result, components){
            axios.post('/fingerprints', {hash: result});
        });
    </script>
@endsection
