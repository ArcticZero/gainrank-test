<invitations :user="user" inline-template>
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Invite Users
            </div>

            <div class="panel-body">
                <form class="form-horizontal" role="form">
                    <!-- Email -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('email')}">
                        <label class="col-md-4 control-label">Email</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="email" v-model="createForm.email">

                            <span class="help-block" v-show="createForm.errors.has('email')">
                                @{{ createForm.errors.get('email') }}
                            </span>
                        </div>
                    </div>

                    <!-- Create Button -->
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6">
                            <button type="submit" class="btn btn-primary"
                                    @click.prevent="create"
                                    :disabled="createForm.busy">

                                Send
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-default" v-if="invitations.length > 0">
            <div class="panel-heading">Sent Invitations</div>

            <div class="panel-body">
                <table class="table table-borderless m-b-none">
                    <thead>
                    <th>Email</th>
                    <th>Used</th>
                    <th></th>
                    </thead>

                    <tbody>
                    <tr v-for="invitation in invitations">
                        <!-- Email -->
                        <td>
                            <div class="btn-table-align">
                                @{{ invitation.email }}
                            </div>
                        </td>

                        <!-- Used At -->
                        <td>
                            <div class="btn-table-align">
                                <span v-if="invitation.used">
                                    Yes
                                </span>

                                <span v-else>
                                    No
                                </span>
                            </div>
                        </td>

                        <!-- Delete Button -->
                        <td>
                            <button :disabled="!!invitation.used" class="btn btn-danger-outline" @click="approveInvitationDelete(invitation)">
                            <i class="fa fa-times"></i>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Delete Invitation Modal -->
        <div class="modal fade" id="modal-delete-invitation" tabindex="-1" role="dialog">
            <div class="modal-dialog" v-if="deletingInvitation">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <h4 class="modal-title">
                            Delete Invitation (@{{ deletingInvitation.email }})
                        </h4>
                    </div>

                    <div class="modal-body">
                        Are you sure you want to delete this invitation? If deleted, the recipient's invitation will no
                        longer be valid and they won't be able to sign up using it.
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No, Go Back</button>

                        <button type="button" class="btn btn-danger" @click="deleteInvitation">
                        Yes, Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</invitations>
