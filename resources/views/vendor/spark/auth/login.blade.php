@extends('layouts.auth')

@section('content')
<div class="container auth-form">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <h5>Sign in to Gainrank</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            @if (session('login_error'))
                <div class="alert alert-danger">
                    {!! session('login_error') !!}
                </div>
            @endif
            @include('spark::shared.errors')

            <form class="form-horizontal" role="form" method="POST" action="/login" id="login">
                {{ csrf_field() }}

                <div class="input-cluster">
                    <!-- E-Mail Address -->
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" autofocus>
                        </div>
                    </div>

                    <!-- Password -->
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                        </div>
                    </div>
                </div>

                <!-- Remember Me -->
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"> Remember me
                            </label>
                        </div>
                    </div>
                </div>

                <!-- Buttons -->
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-block">
                            Log in
                        </button>
                        <a class="btn btn-link btn-block" href="{{ url('/password/reset') }}">Forgot password?</a>

                        <small class="separator">Do not have an account?</small>

                        <a class="btn btn-block btn-default" href="{{ url('/register') }}">Create an account</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
