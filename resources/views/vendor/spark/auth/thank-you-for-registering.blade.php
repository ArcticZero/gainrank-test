@extends('spark::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thank you</div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        <p>Thank you for creating an account on Gainrank. A message with further instructions
                            has been sent to your email address.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
