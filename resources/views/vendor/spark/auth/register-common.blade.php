<!-- Coupon -->
<div class="row" v-if="coupon">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">Discount</div>

            <div class="panel-body">
                The coupon's @{{ discount }} discount will be applied to your subscription!
            </div>
        </div>
    </div>
</div>

<!-- Invalid Coupon -->
<div class="row" v-if="invalidCoupon">
    <div class="col-md-12">
        <div class="alert alert-danger">
            Whoops! This coupon code is invalid.
        </div>
    </div>
</div>

<!-- Invitation -->
<div class="row" v-if="invitation">
    <div class="col-md-12">
        <div class="alert alert-success">
            We found your invitation to the <strong>@{{ invitation.team.name }}</strong> {{ Spark::teamString() }}!
        </div>
    </div>
</div>

<!-- Invalid Invitation -->
<div class="row" v-if="invalidInvitation">
    <div class="col-md-12">
        <div class="alert alert-danger">
            Whoops! This invitation code is invalid.
        </div>
    </div>
</div>

<!-- Basic Profile -->
<div class="row">
    <div class="col-md-12">
        <!-- Generic Error Message -->
        <div class="alert alert-danger" v-if="registerForm.errors.has('form')">
            @{{ registerForm.errors.get('form') }}
        </div>

        <!-- Invitation Code Error -->
        <div class="alert alert-danger" v-if="registerForm.errors.has('invitation')">
            @{{ registerForm.errors.get('invitation') }}
        </div>

        <!-- Registration Form -->
        @include('spark::auth.register-common-form')
    </div>
</div>
