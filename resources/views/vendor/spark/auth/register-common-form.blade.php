<form class="form-horizontal" role="form">
    @if (Spark::usesTeams() && Spark::onlyTeamPlans())
        <div class="input-cluster">
            <!-- Team Name -->
            <div class="form-group" :class="{'has-error': registerForm.errors.has('team')}" v-if=" ! invitation">
                <div class="col-md-12">
                    <input type="name" placeholder="{{ ucfirst(Spark::teamString()) }} Name" class="form-control" name="team" v-model="registerForm.team" autofocus>

                    <span class="help-block" v-show="registerForm.errors.has('team')">
                        @{{ registerForm.errors.get('team') }}
                    </span>
                </div>
            </div>

            @if (Spark::teamsIdentifiedByPath())
                <!-- Team Slug (Only Shown When Using Paths For Teams) -->
                <div class="form-group" :class="{'has-error': registerForm.errors.has('team_slug')}" v-if=" ! invitation">
                    <div class="col-md-12">
                        <input type="name" placeholder="{{ ucfirst(Spark::teamString()) }} Slug" class="form-control" name="team_slug" v-model="registerForm.team_slug" autofocus>

                        <p class="help-block" v-show=" ! registerForm.errors.has('team_slug')">
                            This slug is used to identify your team in URLs.
                        </p>

                        <span class="help-block" v-show="registerForm.errors.has('team_slug')">
                            @{{ registerForm.errors.get('team_slug') }}
                        </span>
                    </div>
                </div>
            @endif
        </div>
    @endif

    @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
    @endif

    <div class="input-cluster">
        <!-- Name -->
        <div class="form-group" :class="{'has-error': registerForm.errors.has('name')}">
            <div class="col-md-12">
                <input type="text" placeholder="Name" class="form-control" name="name" v-model="registerForm.name" autofocus>

                <span class="help-block" v-show="registerForm.errors.has('name')">
                    @{{ registerForm.errors.get('name') }}
                </span>
            </div>
        </div>

        <!-- E-Mail Address -->
        <div class="form-group" :class="{'has-error': registerForm.errors.has('email')}">
            <div class="col-md-12">
                <input type="email" placeholder="Email" class="form-control" name="email" v-model="registerForm.email">

                <span class="help-block" v-show="registerForm.errors.has('email')">
                    @{{ registerForm.errors.get('email') }}
                </span>
            </div>
        </div>
    </div>

    <div class="input-cluster">
        <!-- Password -->
        <div class="form-group" :class="{'has-error': registerForm.errors.has('password')}">
            <div class="col-md-12">
                <input type="password" placeholder="Password" class="form-control" name="password" v-model="registerForm.password">

                <span class="help-block" v-show="registerForm.errors.has('password')">
                    @{{ registerForm.errors.get('password') }}
                </span>
            </div>
        </div>

        <!-- Password Confirmation -->
        <div class="form-group" :class="{'has-error': registerForm.errors.has('password_confirmation')}">
            <div class="col-md-12">
                <input type="password" placeholder="Confirm password" class="form-control" name="password_confirmation" v-model="registerForm.password_confirmation">

                <span class="help-block" v-show="registerForm.errors.has('password_confirmation')">
                    @{{ registerForm.errors.get('password_confirmation') }}
                </span>
            </div>
        </div>
    </div>

    <!-- Newsletter -->
    <div class="form-group" :class="{'has-error': registerForm.errors.has('is_subscribed')}">
        <div class="col-md-12">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="is_subscribed" v-model="registerForm.is_subscribed">
                    Sign-up for our newsletter
                </label>

                <span class="help-block" v-show="registerForm.errors.has('is_subscribed')">
                    @{{ registerForm.errors.get('is_subscribed') }}
                </span>
            </div>
        </div>
    </div>

    <!-- Terms And Conditions -->
    <div v-if=" ! selectedPlan || selectedPlan.price == 0">
        <div class="form-group" :class="{'has-error': registerForm.errors.has('terms')}">
            <div class="col-md-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="terms" v-model="registerForm.terms">
                        I Accept The <a href="/terms" target="_blank">Terms Of Service</a>
                    </label>

                    <span class="help-block" v-show="registerForm.errors.has('terms')">
                        @{{ registerForm.errors.get('terms') }}
                    </span>
                </div>
            </div>
        </div>

        <!-- Buttons -->
        <div class="form-group">
            <div class="col-md-12 text-center">
                <button class="btn btn-primary btn-block" @click.prevent="register" :disabled="registerForm.busy">
                    <span v-if="registerForm.busy">
                        <i class="fa fa-btn fa-spinner fa-spin"></i>Registering
                    </span>

                    <span v-else>
                        <i class="fa fa-btn fa-check-circle"></i>Register
                    </span>
                </button>

                <small class="separator">Already have an account?</small>

                <a class="btn btn-block btn-default" href="{{ url('/login') }}">Sign in</a>
            </div>
        </div>
    </div>
</form>