 <!-- Left Side Of Navbar -->
<li{!! Request::path() == 'home' ? ' class="active"' : '' !!}><a href="/home"><i class="gr-dashboard gr-spaced" aria-hidden="true"></i>Dashboard</a></li>
<li{!! starts_with(Request::path(), 'ranktracker') ? ' class="active"' : '' !!}><a href="/ranktracker"><i class="gr-documents gr-spaced" aria-hidden="true"></i>Ranktracker</a></li>
<!--
<li{!! Request::path() == 'ctr' ? ' class="active"' : '' !!}><a href="/ctr"><i class="gr-actions gr-spaced" aria-hidden="true"></i>CTR</a></li>
<li{!! Request::path() == 'wayback' ? ' class="active"' : '' !!}><a href="/wayback"><i class="gr-activity gr-spaced" aria-hidden="true"></i>Wayback</a></li>
<li{!! Request::path() == 'alexa' ? ' class="active"' : '' !!}><a href="/alexa"><i class="gr-emoji gr-spaced" aria-hidden="true"></i>Alexa</a></li>
<li{!! Request::path() == 'traffic' ? ' class="active"' : '' !!}><a href="/traffic"><i class="gr-electricity gr-spaced" aria-hidden="true"></i>Traffic</a></li>
-->

