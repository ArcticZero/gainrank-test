@extends('spark::layouts.app')

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
@endsection

@section('content')
<spark-kiosk :user="user" inline-template>
    <div class="container-fluid">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-4">
                <div class="panel panel-default panel-flush">
                    <div class="panel-heading">
                        Kiosk
                    </div>

                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <!-- Announcements Link -->
                                @can('administer announcements')
                                  <li role="presentation" class="active">
                                      <a href="#announcements" aria-controls="announcements" role="tab" data-toggle="tab">
                                          <i class="fa fa-fw fa-btn fa-bullhorn"></i>Announcements
                                      </a>
                                  </li>
                                @endcan

                                <!-- Metrics Link -->
                                <li role="presentation">
                                    <a href="#metrics" aria-controls="metrics" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-bar-chart"></i>Metrics
                                    </a>
                                </li>

                                @can('administer users')
                                  <!-- Users Link -->
                                  <li role="presentation">
                                      <a href="#users" aria-controls="users" role="tab" data-toggle="tab">
                                          <i class="fa fa-fw fa-btn fa-user"></i>Users
                                      </a>
                                  </li>
                                @endcan

                                @can('administer posts')
                                  <!-- Blog Posts Link -->
                                  <li role="presentation">
                                      <a href="#posts" aria-controls="blog posts" role="tab" data-toggle="tab">
                                          <i class="fa fa-fw fa-btn fa-file-text-o"></i>Blog Posts
                                      </a>
                                  </li>
                                @endcan

                                @can('administer coinbase')
                                <!-- Coinbase Notifications Link -->
                                <li role="presentation">
                                    <a href="#coinbase-notifications" aria-controls="coinbase-notifications" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-bitcoin"></i>Coinbase Notification History
                                    </a>
                                </li>
                                @endcan

                                @can('administer paypal')
                                <!-- PayPal Notifications Link -->
                                <li role="presentation">
                                    <a href="#paypal-notifications" aria-controls="paypal-notifications" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-paypal"></i>PayPal Notification History
                                    </a>
                                </li>
                                @endcan

                                @can('administer paypal')
                                <!-- Pending Orders Link -->
                                <li role="presentation">
                                    <a href="#orders-pending" aria-controls="orders-pending" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-shopping-cart"></i>Pending Orders
                                    </a>
                                </li>
                                @endcan

                                @can('view fingerprints')
                                <!-- Fingerprints -->
                                <li role="presentation">
                                    <a href="#fingerprints" aria-controls="fingerprints" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-id-badge"></i>Fingerprints
                                    </a>
                                </li>
                                @endcan

                                @can('administer ad campaigns')
                                <!-- Ad Campaigns -->
                                <li role="presentation">
                                    <a href="#campaigns" aria-controls="campaigns" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-compass"></i>Ad Campaigns
                                    </a>
                                </li>
                                @endcan
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default panel-flush">
                    <div class="panel-heading">
                        Services
                    </div>
                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <!-- Ranktracker -->
                                <li role="presentation">
                                    <a href="#ranktracker" aria-controls="ranktracker" role="tab" data-toggle="tab">
                                        <i class="gr-documents gr-fw gr-btn"></i>Ranktracker
                                    </a>
                                </li>

                                <!-- CTR -->
                                <li role="presentation">
                                    <a href="#ctr" aria-controls="ctr" role="tab" data-toggle="tab">
                                        <i class="gr-actions gr-fw gr-btn"></i>CTR
                                    </a>
                                </li>

                                <!-- Wayback Restorer -->
                                <li role="presentation">
                                    <a href="#wayback" aria-controls="wayback" role="tab" data-toggle="tab">
                                        <i class="gr-activity gr-fw gr-btn"></i>Wayback Restorer
                                    </a>
                                </li>

                                <!-- Alexa -->
                                <li role="presentation">
                                    <a href="#alexa" aria-controls="alexa" role="tab" data-toggle="tab">
                                        <i class="gr-emoji gr-fw gr-btn"></i>Alexa
                                    </a>
                                </li>

                                <!-- Traffic -->
                                <li role="presentation">
                                    <a href="#traffic" aria-controls="traffic" role="tab" data-toggle="tab">
                                        <i class="gr-electricity gr-fw gr-btn"></i>Traffic
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panels -->
            <div class="col-md-8">
                <div class="tab-content">
                    @can('administer announcements')
                      <!-- Announcements -->
                      <div role="tabpanel" class="tab-pane active" id="announcements">
                          @include('spark::kiosk.announcements')
                      </div>
                    @endcan

                    <!-- Metrics -->
                    <div role="tabpanel" class="tab-pane" id="metrics">
                        @include('spark::kiosk.metrics')
                    </div>

                    @can('administer users')
                      <!-- User Management -->
                      <div role="tabpanel" class="tab-pane" id="users">
                          @include('spark::kiosk.users')
                      </div>
                    @endcan

                    @can('administer posts')
                      <!-- Blog Posts -->
                      <div role="tabpanel" class="tab-pane" id="posts">
                          @include('spark::kiosk.posts')
                      </div>
                    @endcan

                    @can('administer coinbase')
                    <!-- Coinbase Notification History -->
                    <div role="tabpanel" class="tab-pane" id="coinbase-notifications">
                        @include('kiosk.coinbase.notifications')
                    </div>
                    @endcan

                    @can('administer paypal')
                    <!-- PayPal Notification History -->
                    <div role="tabpanel" class="tab-pane" id="paypal-notifications">
                        @include('kiosk.paypal.notifications')
                    </div>
                    @endcan
 
                    @can('administer paypal')
                    <!-- Pending Orders -->
                    <div role="tabpanel" class="tab-pane" id="orders-pending">
                        @include('kiosk.orders.pending')
                    </div>
                    @endcan

                    @can('view fingerprints')
                    <!-- Fingerprints -->
                    <div role="tabpanel" class="tab-pane" id="fingerprints">
                        @include('spark::kiosk.fingerprints')
                    </div>
                    @endcan

                    @can('administer ad campaigns')
                    <!-- Ad Campaigns -->
                    <div role="tabpanel" class="tab-pane" id="campaigns">
                        @include('spark::kiosk.campaigns')
                    </div>
                    @endcan

                    <!-- Ranktracker -->
                    <div role="tabpanel" class="tab-pane" id="ranktracker">
                        @include('kiosk.ranktracker.ranktracker')
                    </div>

                    <!-- CTR -->
                    <div role="tabpanel" class="tab-pane" id="ctr">
                        @include('kiosk.ctr.ctr')
                    </div>

                    <!-- Wayback Restorer -->
                    <div role="tabpanel" class="tab-pane" id="wayback">
                        @include('kiosk.wayback.wayback')
                    </div>

                    <!-- Alexa -->
                    <div role="tabpanel" class="tab-pane" id="alexa">
                        @include('kiosk.alexa.alexa')
                    </div>

                    <!-- Traffic -->
                    <div role="tabpanel" class="tab-pane" id="traffic">
                        @include('kiosk.traffic.traffic')
                    </div>
                </div>
            </div>
        </div>
    </div>
</spark-kiosk>
@endsection
