<campaigns inline-template>
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">Create Ad Campaign</div>

            <div class="panel-body">
                <form class="form-horizontal" role="form">
                    <div class="alert alert-danger hide" id="createFormError"></div>
                    <!-- Name -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('name')}">
                        <label class="col-md-4 control-label">Name</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" v-model="createForm.name">

                                <span class="help-block" v-show="createForm.errors.has('name')">
                                    @{{ createForm.errors.get('name') }}
                                </span>
                        </div>
                    </div>

                    <!-- Bonuses -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Bonuses</label>

                        <div class="col-md-6">
                            <div class="row">
                                <div v-for="(type, id) in bonusesList" class="col-md-6 height-80" :class="{'has-error': createForm.errors.has('bonuses.'+id)}">
                                    <label class="control-label">@{{ type }}</label>
                                    <input type="text" class="form-control" v-model="createForm.bonuses[id]">
                                    <span class="help-block" v-show="createForm.errors.has('bonuses.'+id)">
                                        @{{ createForm.errors.get('bonuses.'+id) }}
                                    </span>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Description -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('description')}">
                        <label class="col-md-4 control-label">Description</label>

                        <div class="col-md-6">

                            <textarea class="form-control" name="description" v-model="createForm.description"></textarea>

                            <span class="help-block" v-show="createForm.errors.has('description')">
                                @{{ createForm.errors.get('description') }}
                            </span>
                        </div>
                    </div>

                    <!-- Start Date -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('start_date')}">
                        <label class="col-md-4 control-label">Start Date (YYYY-mm-dd)</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="start_date" v-model="createForm.start_date">

                                <span class="help-block" v-show="createForm.errors.has('start_date')">
                                    @{{ createForm.errors.get('start_date') }}
                                </span>
                        </div>
                    </div>

                    <!-- End Date -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('end_date')}">
                        <label class="col-md-4 control-label">End Date (YYYY-mm-dd)</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="end_date" v-model="createForm.end_date">

                                <span class="help-block" v-show="createForm.errors.has('end_date')">
                                    @{{ createForm.errors.get('end_date') }}
                                </span>
                        </div>
                    </div>

                    <!-- Max Uses -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('max_uses')}">
                        <label class="col-md-4 control-label">Max Uses</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="max_uses" v-model="createForm.max_uses">

                                <span class="help-block" v-show="createForm.errors.has('max_uses')">
                                    @{{ createForm.errors.get('max_uses') }}
                                </span>
                        </div>
                    </div>

                    <!-- URL Path -->
                    <div class="form-group" :class="{'has-error': createForm.errors.has('path')}">
                        <label class="col-md-4 control-label">URL Path</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="path" v-model="createForm.path">

                                <span class="help-block" v-show="createForm.errors.has('path')">
                                    @{{ createForm.errors.get('path') }}
                                </span>
                        </div>
                    </div>

                    <!-- Create Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label text-right">Active</label>
                        <div class="col-md-6">
                            <input type="checkbox" name="active" v-model="createForm.active">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6">
                            <button type="submit" class="btn btn-primary"
                                    @click.prevent="create"
                                    :disabled="createForm.busy">

                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Recent Campaigns List -->
        <div class="panel panel-default" v-if="campaigns.length > 0">
            <div class="panel-heading">Campaigns</div>

            <div class="panel-body">
                <table class="table table-borderless m-b-none">
                    <thead>
                    <th>Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Max Uses</th>
                    <th>Path</th>
                    <th>Active</th>
                    <th></th>
                    </thead>

                    <tbody>
                        <tr v-for="campaign in campaigns">
                            <!-- Name -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ campaign.name }}
                                </div>
                            </td>

                            <!-- Start Date -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ campaign.start_date.replace(' 00:00:00', '') }}
                                </div>
                            </td>

                            <!-- End Date -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ campaign.end_date.replace(' 00:00:00', '') }}
                                </div>
                            </td>

                            <!-- Max Uses -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ campaign.max_uses }}
                                </div>
                            </td>

                            <!-- Path -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ campaign.path }}
                                </div>
                            </td>

                            <!-- Active -->
                            <td>
                                <i class="fa fa-check-square-o" v-if="campaign.active"></i>
                                <i class="fa fa-square-o" v-else></i>
                            </td>

                            <td>
                                <!-- Edit Button -->
                                <button class="btn btn-primary" @click="editCampaign(campaign)">
                                <i class="fa fa-pencil"></i>
                                </button>

                                <!-- Delete Button -->
                                <button class="btn btn-danger-outline" @click="approveCampaignDelete(campaign)">
                                <i class="fa fa-times"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <button type="button" class="btn btn-primary" @click="previousPage" :disabled="page <= 1">
                Previous
                </button>
                <button type="button" class="btn btn-primary" @click="nextPage" :disabled="page >= last_page">
                Next
                </button>
            </div>
        </div>

        <!-- Edit Campaign Modal -->
        <div class="modal fade" id="modal-update-campaign" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" v-if="updatingCampaign">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <h4 class="modal-title">
                            Update Campaign
                        </h4>
                    </div>

                    <div class="modal-body">
                        <div class="alert alert-danger hide" id="updateFormError"></div>
                        <!-- Update Campaign -->
                        <form class="form-horizontal" role="form">
                            <!-- Name -->
                            <div class="form-group" :class="{'has-error': updateForm.errors.has('name')}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" v-model="updateForm.name">

                                <span class="help-block" v-show="updateForm.errors.has('name')">
                                    @{{ updateForm.errors.get('name') }}
                                </span>
                                </div>
                            </div>

                            <!-- Bonuses -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Bonuses</label>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div v-for="(type, id) in bonusesList" class="col-md-6 height-80" :class="{'has-error': updateForm.errors.has('bonuses.'+id)}">
                                            <label class="control-label">@{{ type }}</label>
                                            <input type="text" class="form-control" v-model="updateForm.bonuses[id]">
                                    <span class="help-block" v-show="updateForm.errors.has('bonuses.'+id)">
                                        @{{ updateForm.errors.get('bonuses.'+id) }}
                                    </span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Description -->
                            <div class="form-group" :class="{'has-error': updateForm.errors.has('description')}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">

                                    <textarea class="form-control" name="description" v-model="updateForm.description"></textarea>

                            <span class="help-block" v-show="updateForm.errors.has('description')">
                                @{{ updateForm.errors.get('description') }}
                            </span>
                                </div>
                            </div>

                            <!-- Start Date -->
                            <div class="form-group" :class="{'has-error': updateForm.errors.has('start_date')}">
                                <label class="col-md-4 control-label">Start Date (YYYY-mm-dd)</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="start_date" v-model="updateForm.start_date">

                                <span class="help-block" v-show="updateForm.errors.has('start_date')">
                                    @{{ updateForm.errors.get('start_date') }}
                                </span>
                                </div>
                            </div>

                            <!-- End Date -->
                            <div class="form-group" :class="{'has-error': updateForm.errors.has('end_date')}">
                                <label class="col-md-4 control-label">End Date (YYYY-mm-dd)</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="end_date" v-model="updateForm.end_date">

                                <span class="help-block" v-show="updateForm.errors.has('end_date')">
                                    @{{ updateForm.errors.get('end_date') }}
                                </span>
                                </div>
                            </div>

                            <!-- Max Uses -->
                            <div class="form-group" :class="{'has-error': updateForm.errors.has('max_uses')}">
                                <label class="col-md-4 control-label">Max Uses</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="max_uses" v-model="updateForm.max_uses">

                                <span class="help-block" v-show="updateForm.errors.has('max_uses')">
                                    @{{ updateForm.errors.get('max_uses') }}
                                </span>
                                </div>
                            </div>

                            <!-- URL Path -->
                            <div class="form-group" :class="{'has-error': updateForm.errors.has('path')}">
                                <label class="col-md-4 control-label">URL Path</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="path" v-model="updateForm.path">

                                <span class="help-block" v-show="updateForm.errors.has('path')">
                                    @{{ updateForm.errors.get('path') }}
                                </span>
                                </div>
                            </div>

                            <!-- Create Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label text-right">Active</label>
                                <div class="col-md-6">
                                    <input type="checkbox" name="active" v-model="updateForm.active">
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                        <button type="button" class="btn btn-primary" @click="update" :disabled="updateForm.busy">
                        Update
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete Campaigns Modal -->
        <div class="modal fade" id="modal-delete-campaign" tabindex="-1" role="dialog">
            <div class="modal-dialog" v-if="deletingCampaign">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <h4 class="modal-title">
                            Delete Campaign
                        </h4>
                    </div>

                    <div class="modal-body">
                        Are you sure you want to delete this campaign?

                        <div class="alert alert-danger">
                            All User rewards distributed through this Campaign will be revoked as well!
                        </div>
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No, Go Back</button>

                        <button type="button" class="btn btn-danger" @click="deleteCampaign" :disabled="deleteForm.busy">
                        Yes, Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</campaigns>
