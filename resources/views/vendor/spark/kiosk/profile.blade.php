<spark-kiosk-profile :user="user" :plans="plans" inline-template>
    <div>
        <!-- Loading Indicator -->
        <div class="row" v-if="loading">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <i class="fa fa-btn fa-spinner fa-spin"></i>Loading
                    </div>
                </div>
            </div>
        </div>

        <!-- User Profile -->
        <div v-if=" ! loading && profile">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <!-- User Name -->
                            <div class="pull-left">
                                <div class="btn-table-align">
                                    <i class="fa fa-btn fa-times" style="cursor: pointer;" @click="showSearch"></i>
                                    @{{ profile.name }}
                                </div>
                            </div>

                            <!-- Profile Actions -->
                            <div class="pull-right" style="padding-top: 2px;">
                                <div class="btn-group" role="group">
                                    <!-- Apply Discount -->
                                    <button class="btn btn-default" v-if="spark.usesStripe && profile.stripe_id" @click="addDiscount(profile)">
                                        <i class="fa fa-gift"></i>
                                    </button>

                                    <!-- Impersonate Button -->
                                    <button class="btn btn-default" @click="impersonate(profile)" :disabled="user.id === profile.id">
                                        <i class="fa fa-user-secret"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <!-- Profile Photo -->
                                <!--
                                <div class="col-md-3 text-center">
                                    <img :src="profile.photo_url" class="spark-profile-photo-xl">
                                </div>
                                -->

                                <div class="col-md-12">
                                    <!-- Email Address -->
                                    <p>
                                        <strong>Email Address:</strong> <a :href="'mailto:'+profile.email">@{{ profile.email }}</a>
                                    </p>

                                    <!-- Joined Date -->
                                    <p>
                                        <strong>Joined:</strong> @{{ this.moment(profile.created_at).tz(this.timezone).format() | datetime }}
                                    </p>

                                    <!-- Subscription -->
                                    <p>
                                        <strong>Subscription:</strong>

                                        <span v-if="activePlan(profile)">
                                            <a :href="customerUrlOnBillingProvider(profile)" target="_blank">
                                                @{{ activePlan(profile).name }} (@{{ activePlan(profile).interval | capitalize }})
                                            </a>
                                        </span>

                                        <span v-else>
                                            None
                                        </span>
                                    </p>

                                    <!-- Total Revenue -->
                                    <p>
                                        <strong>Total Revenue:</strong> @{{ revenue | currency(spark.currencySymbol) }}
                                    </p>

                                    @can('assign roles')
                                      <div class=" col-md-3 p-l-none">
                                          <h4>Roles</h4>
                                          <div v-for="role in availableRoles">
                                              <input type="checkbox" :id="role.name" :value="role.name" v-model="currentRoles" @change="updateRoles()">
                                              <label :for="role.name">@{{ role.name }}</label>
                                           </div>
                                      </div>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Teams -->
            <div class="row" v-if="spark.usesTeams && profile.owned_teams.length > 0">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ ucfirst(str_plural(Spark::teamString())) }}
                        </div>

                        <div class="panel-body">
                            <table class="table table-borderless m-b-none">
                                <thead>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Subscription</th>
                                </thead>

                                <tbody>
                                    <tr v-for="team in profile.owned_teams">
                                        <!-- Photo -->
                                        <td>
                                            <img :src="team.photo_url" class="spark-team-photo">
                                        </td>

                                        <!-- Team Name -->
                                        <td>
                                            <div class="btn-table-align">
                                                @{{ team.name }}
                                            </div>
                                        </td>

                                        <!-- Subscription -->
                                        <td>
                                            <div class="btn-table-align">
                                                <span v-if="activePlan(team)">
                                                    <a :href="customerUrlOnBillingProvider(team)" target="_blank">
                                                        @{{ activePlan(team).name }} (@{{ activePlan(team).interval | capitalize }})
                                                    </a>
                                                </span>

                                                <span v-else>
                                                    None
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Apply Discount Modal -->
        <div>
            @include('spark::kiosk.modals.add-discount')
        </div>

        @can('view fingerprints')
          <!-- Related Users -->
          <div class="panel panel-default">
              <div class="panel-heading">Related Users by Fingerprint</div>

              <div class="panel-body">
                  <table class="table table-condensed table-hover fingerprints" v-for="(user, email) in relatedData.users_by_hash">
                      <thead @click="toggleVisibility(email + '_hash')">
                        <th>@{{ email }}</th>
                        <th>Total Hashes: @{{ user.hashes.length }}</th>
                        <th><a v-bind:href="'/users/' + email" target="_blank">View Profile</th>
                      </thead>
                      <tbody v-bind:class="{ hidden: isHidden(email + '_hash') }">
                        <tr v-for="hash in user.hashes">
                            <td>@{{ hash.hash }}</td>
                            <td>@{{ hash.created_at }}</td>
                            <td></td>
                        </tr>
                      </tbody>
                  </table>
              </div>
          </div>

          <!-- Related Hashes -->
          <div class="panel panel-default">
              <div class="panel-heading">Related Users by IP Address</div>

              <div class="panel-body">
                  <table class="table table-condensed table-hover fingerprints" v-for="(user, email) in relatedData.users_by_ip">
                      <thead @click="toggleVisibility(email + '_ip')">
                      <th>@{{ email }}</th>
                      <th>Total IP Addresses: @{{ user.ip_addresses.length }}</th>
                      <th><a v-bind:href="'/users/' + email" target="_blank">View Profile</th>
                      </thead>
                      <tbody v-bind:class="{ hidden: isHidden(email + '_ip') }">
                      <tr v-for="ip in user.ip_addresses">
                          <td>@{{ ip.ip_address }}</td>
                          <td>@{{ ip.created_at }}</td>
                          <td></td>
                      </tr>
                      </tbody>
                  </table>
              </div>
          </div>
        @endcan
    </div>
</spark-kiosk-profile>
