<fingerprints inline-template>
    <div>
        <div>
            <!-- Search Field Panel -->
            <div class="panel panel-default panel-flush" style="border: 0;">
                <div class="panel-body">
                    <form class="form-horizontal p-b-none" role="form" @submit.prevent>
                        <!-- Search Field -->
                        <div class="form-group m-b-none">
                            <div class="col-md-12">
                                <input type="text" id="fingerprints-search" class="form-control"
                                        name="search"
                                        placeholder="Search by fingerprint hash"
                                        v-model="hashID"
                                        @keyup.enter="search">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Recent Hashes List -->
            <div class="panel panel-default" v-if="hashes.length > 0">
                <div class="panel-heading">Recent Hashes</div>

                <div class="panel-body">
                    <table class="table table-borderless m-b-none">
                        <thead>
                        <th>Hash</th>
                        <th>First Recording Date</th>
                        <th></th>
                        </thead>

                        <tbody>
                        <tr v-for="hash in hashes">
                            <!-- Title -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ hash.hash }}
                                </div>
                            </td>

                            <!-- Date -->
                            <td>
                                <div class="btn-table-align">
                                    @{{ this.moment(hash.created_at).tz(this.timezone).format() | datetime }}
                                </div>
                            </td>

                            <td>
                                <!-- Search Button -->
                                <button class="btn btn-danger-outline" @click="viewHash(hash.hash)">
                                <i class="fa fa-fw fa-btn fa-user"></i>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <button type="button" class="btn btn-primary" @click="previousPage" :disabled="page <= 1">
                    Previous
                    </button>
                    <button type="button" class="btn btn-primary" @click="nextPage" :disabled="page >= last_page">
                    Next
                    </button>
                </div>
            </div>

            <!-- Searching -->
            <div class="panel panel-default" v-if="searching">
                <div class="panel-heading">Search Results</div>

                <div class="panel-body">
                    <i class="fa fa-btn fa-spinner fa-spin"></i>Searching
                </div>
            </div>

            <!-- No Search Results -->
            <div class="panel panel-warning" v-if=" ! searching && noSearchResults">
                <div class="panel-heading">Search Results</div>

                <div class="panel-body">
                    No fingerprints found with that hash.
                </div>
            </div>

            <!-- Hash Search Results -->
            <div class="panel panel-default" v-if=" ! searching && searchResults.length > 0">
                <div class="panel-heading">Search Results</div>

                <div class="panel-body">
                    <table class="table table-borderless m-b-none">
                        <thead>
                            <th>Email</th>
                            <th>Recording Dates</th>
                        </thead>

                        <tbody>
                            <tr v-for="searchUser in searchResults">
                                <!-- E-Mail Address -->
                                <td>
                                    <div class="btn-table-align">
                                        @{{ searchUser.email }}
                                    </div>
                                </td>

                                <!-- Recording Date -->
                                <td>
                                    <div class="btn-table-align">
                                          <p v-for="searchUserDate in searchUser.dates">
                                            @{{ searchUserDate  }}<br/>
                                          </p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</fingerprints>
