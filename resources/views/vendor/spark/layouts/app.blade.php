<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', config('app.name'))</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="/css/gainrank-font.css" rel="stylesheet">
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/hopscotch.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Gainrank">
    <meta name="application-name" content="Gainrank">
    <meta name="theme-color" content="#ffffff">

    @yield('scripts', '')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>
</head>
<body class="with-navbar">
    <div id="spark-app" v-cloak>
        <!-- Navigation -->
        @if (Auth::check())
            @include('spark::nav.user')
        @else
            @include('spark::nav.guest')
        @endif

        <!-- Unverified Notice -->
        @if (Auth::check() && !Auth::user()->hasRole('verified'))
        <div class="container-fluid universal-alert">
            <div class="row">
                <div class="col-xs-12 alert alert-danger text-center boxed">
                    Your account is currently unverified and has been limited. A message with the verification link was sent to your email address when you registered. You may also <a role="button" v-on:click="resendVerificationEmail()">resend the verification message</a>.
                </div>
            </div>
        </div>
        @endif

        <!-- Main Content -->
        @yield('content')

        <!-- Application Level Modals -->
        @if (Auth::check())
            @include('spark::modals.notifications')
            @include('spark::modals.support')
            @include('spark::modals.session-expired')
        @endif

        <disable-tour></disable-tour>
    </div>

    <!-- JavaScript -->
    <script src="/js/app.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    @yield('scripts-bottom', '')

    @if (Auth::check() && Auth::user()->referrer_affiliate_id && is_null(Auth::user()->referralLog))
        <!-- This is a referred user's first visit. Track details -->
        <script>
            $(function() {
                window.fingerprint().get(function(result, components) {
                    axios.post('/affiliates/referral', { hash: result, components: components });
                });
            });
        </script>
    @endif
    <!-- Scripts -->

    @if (((Auth::check() && Auth::user()->show_guide) || !Auth::check()) && app('request')->input('tour_url') != false)
    <!-- Guided Tour -->
    <script src="{{ app('request')->input('tour_url') }}"></script>
    <script>
        $(function() {
            window.hopscotch.startTour(tour);
        });
    </script>
    @endif

</body>
</html>
