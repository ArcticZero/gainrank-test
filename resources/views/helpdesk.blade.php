@extends('spark::layouts.app')

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.4.6/mousetrap.min.js"></script>
@endsection

@section('content')
<helpdesk :user="user" inline-template>
    <div class="spark-screen container">
        <div class="row">
            <!-- Tabs -->
            <div class="col-md-3">
                <!-- Support Tabs -->
                <div class="panel panel-default panel-flush">
                    <div class="panel-heading">
                        Helpdesk
                    </div>

                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            <ul class="nav spark-settings-stacked-tabs" id="helpdesk-tabs" role="tablist">
                                <!-- My Tickets Link -->
                                <li role="presentation" class="active">
                                    <a href="#tickets" aria-controls="tickets" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-history"></i>My Tickets
                                    </a>
                                </li>
                                <!-- New Ticket Link -->
                                <li role="presentation">
                                    <a href="#new-ticket" aria-controls="new-ticket" role="tab" data-toggle="tab">
                                        <i class="fa fa-fw fa-btn fa-ticket"></i>New Ticket
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab Panels -->
            <div class="col-md-9">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tickets">
                        @include('spark::settings.helpdesk.tickets')
                    </div>

                    <div role="tabpanel" class="tab-pane" id="new-ticket">
                        @include('spark::settings.helpdesk.new-ticket')
                    </div>
                </div>
            </div>
        </div>
    </div>
</helpdesk>
@endsection
