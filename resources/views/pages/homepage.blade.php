@extends('layouts.frontend')
@section('title', 'Gainrank Home')
@section('content')
  <div class="jumbotron home text-center">
    <div class="container">
      <div class="row">
        <h1><strong>SEO</strong> JUST <br class="visible-xxs" /> GOT <span>EASIER.</span></h1>
        <h2><strong>One</strong> Dashboard <br class="visible-xxs" /> with all your SEO tools</h2>
      </div>

      <div class="row">
        @if (env('SOFT_LAUNCH') == 1)
          <a class="btn large btn-primary" role="button" data-toggle="modal" data-target="#shortlist-modal">Request Access</a>
        @else
          <a class="btn large btn-primary" href="/register" role="button">Join the Free Beta</a>
        @endif
        <a class="btn large btn-blank btn-inverse" href="#" role="button">Features</a>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="shortlist-modal" tabindex="-1" role="dialog" aria-labelledby="shortlist-modal-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="shortlist-modal-1">Request Access</h4>
            </div>
            <div class="modal-body">
              <p>Gainrank is currently in private beta.</p>
              <p>Please add your email below <br/>to be notified when we launch!</p>
              <form id="shortlist" role="form" method="POST" action="/shortlist">
                {{ csrf_field() }}

                <div class="form-group">
                  <label>E-Mail Address</label>
                  <input type="email" class="form-control" name="email">
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa m-r-xs fa-sign-in"></i>Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row text-center margin top large">
        <img src="{!! asset('images/tablet.png') !!}" alt="Tablet Image" class="img-responsive" id="tablet-img"/>
      </div>
    </div>
  </div><!-- /.jumbotron -->

  <div class="row" role="main">
    <div class="row row-eq-height">
      <div class="col-xs-12 col-sm-12 xs-full-width">
        <div class="content-box no-margin faded-text">
          <h1>Total SEO Automation</h1>
          <p>
              When we say ‘Total SEO Automation’ we mean it.  You use our proxies, 
              our servers and our in house software to run the tools that you will 
              find within our dashboard.  This means no more need for subscriptions 
              to proxy services, no more pesky software that you have to baby sit 
              and lastly no more virtual private servers to run all of your software. 
              We created GainRank with one thing in mind: making your Search 
              Engine Optimization more efficient, for both business owners and SEOs.  
          </p>
          <p>
              We want to make it as easy as possible to do anything your business
              needs, so while we're in beta we will be making services and tools 
              <strong>you</strong> need.  
              If we don't have it, let us know and we'll work with you to add it.  
              No cost, no strings.  
          </p>
          <div class="button-row">
            <a class="btn large btn-primary" href="{{ url('helpdesk') }}/#request-service" role="button">Request Service</a><br class="visible-xxs" />
            <a class="btn large btn-blank" href="#" role="button">Join the Free Beta</a>
          </div>
        </div>
      </div>
      <!--
      <div class="col-xs-12 col-sm-4 video-section xxs-full-width row-eq-height">
        <div class="flex-container center">
          <a class="btn large btn-primary" role="button" data-toggle="modal" data-target="#video-modal">Watch our video</a>
        </div>
      </div>
      -->

      <!-- Modal -->
      <div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="video-modal-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="video-modal-1">Video</h4>
            </div>
            <div class="modal-body">
              <p>Video Goes here</p>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="row row-eq-height">
      <div class="col-xs-12 col-sm-4 eq-height-col">
        <div class="content-box">
          <h4 class="blue">Built-in Proxies</h4>
          <p>Cancel your proxy subscription, we have it under control with our network of millions of residential proxies that are safely used to run our services.  </p>
        </div>
      </div>

      <div class="col-xs-12 col-sm-4 eq-height-col">
        <div class="content-box">
          <h4 class="blue">Services We Make for You</h4>
          <p>Don’t see what you’re looking for?  We will make anything you need, just ask!  </p>
        </div>
      </div>

      <div class="col-xs-12 col-sm-4 eq-height-col">
        <div class="content-box no-margin">
          <h4 class="blue">Our Servers</h4>
          <p>We take the brunt of the work and put our servers to work for you when using our services.
And I think thats all that is needed for now?  Until maybe you make the features page, but I would need to see the format for that</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="content-box text-center margin left right small xs-full-width">
        <div class="row">
          <p class="lead">No more <strong>Proxies</strong>, <strong>VPS</strong> or <strong>Software</strong></p>
          <h1>Join the SEO Revolution</h1>
        </div>
        <div class="row">
          <div class="col-xs-12 text-center">
            <a class="btn large btn-primary" href="#" role="button">Take the tour</a>
          </div>
        </div>
      </div>
    </div>

    <!--
    <div class="row">
      <h1 class="text-center margin top bottom large">What are our <br class="visible-xxs" /> Customers saying?</h1>

      <div class="col-xs-12 col-sm-4 testimonial">
        <div class="thumbnail">
          <img src="{!! asset('images/testimonials.jpg') !!}" />
          <div class="caption">
            <h4 class="light"><strong class="blue">Jamie R.</strong> - MegaSales Ltd</h4>
            <div class="row rating">
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
            </div>

            <div class="row">
              <p>It’s time you regained control over your website and online services. urabitur sceleri sque mi dolor, eget e mi dolor, eget...</p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-4 testimonial hidden-xs">
        <div class="thumbnail">
          <img src="{!! asset('images/testimonials.jpg') !!}" />
          <div class="caption">
            <h4 class="light"><strong class="blue">Jamie R.</strong> - MegaSales Ltd</h4>
            <div class="row rating">
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
            </div>

            <div class="row">
              <p>It’s time you regained control over your website and online services. urabitur sceleri sque mi dolor, eget e mi dolor, eget...</p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-4 testimonial hidden-xs hidden-sm">
        <div class="thumbnail no-margin">
          <img src="{!! asset('images/testimonials.jpg') !!}" />
          <div class="caption">
            <h4 class="light"><strong class="blue">Jamie R.</strong> - MegaSales Ltd</h4>
            <div class="row rating">
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
            </div>

            <div class="row">
              <p>It’s time you regained control over your website and online services. urabitur sceleri sque mi dolor, eget e mi dolor, eget...</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    -->

    @if (count($posts))
      <!--
      <div class="row blog-row">
        <div class="col-xs-12">
          <h1 class="text-center margin top bottom large">The latest <br class="visible-xxs" /> from our Blog</h1>
          <div id="blog-posts-slider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              @for($i = 0; $i < count($posts); $i++)
                <li data-target="#blog-posts-slider" data-slide-to="{{ $i }}"{!! $i === 0 ? ' class="active"' : '' !!}></li>
              @endfor
            </ol>

            <div class="carousel-inner" role="listbox">
              @foreach($posts as $post)
                <div class="item {{ $loop->first ? 'active' : '' }}">
                  <a href="{{ url('/blog/'.$post->slug) }}">
                    <img src="{!! $post->getMedia('images')->first()->getUrl('hero') !!}" class="img-responsive"/>
                    <div class="carousel-caption">
                      <h2>{{ $post->title }}</h2>
                      <p>{{ strip_tags(str_limit($post->body, 300)) }}</p>
                    </div>
                  </a>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      -->
    @endif

    <!--
    <div class="row">
      <div class="col-xs-12">
        <div class="content-box text-center no-margin">
          <h1>What are you waiting for?</h1>
          <p>It's time you regained control over your website and online services.</p>
          <a class="btn large btn-default no-margin" href="" role="button">Get your free trial</a>
        </div>
      </div>
    </div>
    -->
  </div>
@endsection
