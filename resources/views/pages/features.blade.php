@extends('layouts.frontend-internal')
@section('title', 'Gainrank Features')
@section('content')
<div class="container-fluid full-page">
	<div class="content-box">
		<section id="feature-list" class="features">
			<div class="row intro">
				<div class="col-md-8">
					<h1>Keyword Tracking</h1>
					<p>
						To have a successful website you need to be found and the best way is to be number one on Google, Bing and Yahoo. The problem? You don't know or don't have time to check all of them daily.  We do.
					</p>
					<p>
						With GainRank's Rank Tracking service you can track all the keywords you want for your site to see how well your site ranks in all of the top search engines. Nothing to download, no logins to your website required.
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/no-downloads.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>No downloads</h4>
							<p>All of the keyword tracking is done on our servers so you have nothing to install, download or worry about.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/no-proxies.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>No proxies</h4>
							<p>With some keyword trackers you need to bring your own "proxies" or what is essentially another payment for you. With GainRank, we handle that on our side as well. No proxies. Ever.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/daily-tracking.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Daily tracking</h4>
							<p>Everyday we will search and record the results for your domains and keywords, all automatically.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/easy-to-understand.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Easy to understand</h4>
							<p>All of your keywords will be shown in an easy to understand chart. See how your keyword ranks change daily for the past week. The past 2 weeks.  Past month... Past year. You choose!</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/choose-your-location.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Choose your location</h4>
							<p>Google, Bing, Yahoo and other search engines all have their main search page (usually .com) but did you know there are local ones too? It's incredibly important to track keywords in your local countries search engines too!  We can track there too, no worries.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/no-hassle-reporting.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>No hassle reporting</h4>
							<p>Need to show your clients or boss how well the site is doing? We got you covered wiith easy to export reports. We'll even export to other formats so you can make your own!</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="amazing-support" class="features">
			<div class="row semi-intro">
				<div class="col-md-8">
					<h1>Amazing Support</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/knowledgeable-agents.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Knowledgeable agents</h4>
							<p>Support personnel are trained in everything we offer and primarily speak English. No "I dont know" or lost in translation issues.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/developer-access.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Developer access</h4>
							<p>Our developers also provide support! When something is a bit more complex or requires a custom solution they can step in and fix your issue directly.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/custom-services.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Custom services</h4>
							<p>We will build your ideas! If we don't currently have something you need to accomplish your SEO or marketing endeavor then drop us a message.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-xs-2">
							<img src="{{ asset('images/features/custom-features.png') }}" alt="">
						</div>
						<div class="col-xs-10">
							<h4>Custom features</h4>
							<p>Similar to services, we will modify our dashboard to fit your needs! New API endpoints, new features, more information. Let us know!</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
@endsection