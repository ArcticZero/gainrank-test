@extends('spark::layouts.app')

@section('content')
 <div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="alert alert-danger">We were unable to verify your order. Please make sure your confirmation key is correct.</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <a class="btn btn-default" href="{{ url('home') }}" role="button">Return Home</a>
        </div>
    </div>
</div>
@endsection