@extends('spark::layouts.app')

@section('content')
 <div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="alert alert-success">Your order has been verified successfully. Thank you!</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <a class="btn btn-default" href="{{ url('home') }}" role="button">Return Home</a>
        </div>
    </div>
</div>

<!-- Take a fingerprint of this action -->
<script>
	$(function() {
		window.fingerprint({excludeUserAgent: true}).get(function(result, components){
            axios.post('/fingerprints', {hash: result});
        });
	});
</script>
@endsection