<div class="container-fluid">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <a class="navbar-brand" href="/">
      <img src="{!! asset('images/gainrank-logo.png') !!}" alt="GainRank Logo" />
    </a>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="navbar-collapse">
    <ul class="nav navbar-nav navbar-right">
      <li class="active"><a href="{{ url('/') }}" class="btn btn-blank btn-inverse">Home</a></li>
      <!--<li><a href="{{ url('/blog') }}" class="btn btn-blank btn-inverse">Blog</a></li>-->
      <li><a href="{{ url('/features') }}" class="btn btn-blank btn-inverse">Features</a></li>
      <!--<li><a href="#" class="btn btn-blank btn-inverse">Pricing</a></li>-->
      <li><a href="{{ url('/helpdesk') }}" class="btn btn-blank btn-inverse">Support</a></li>
      @if (Auth::check())
      <li><a href="{{ url('/home') }}" class="btn btn-primary">Account</a></li>
      @else
      <li><a href="{{ url('/login') }}" class="btn btn-blank btn-inverse">Login</a></li>
      <li><a href="{{ url('/register') }}" class="btn btn-primary">Sign Up</a></li>
      @endif

    </ul>
  </div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
