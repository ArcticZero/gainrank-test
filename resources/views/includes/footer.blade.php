<div class="row text-center row-eq-height">
  <div class="col-xs-12 footer-menu hidden-md hidden-lg hidden-xl hidden-xxl hidden-sm">
    <ul>
      <li><a href="#">Home</a></li>
      <li><a href="#">Features</a></li>
      <li><a href="#">Pricing</a></li>
      <li><a href="#">Support</a></li>
      <li><a href="#" class="blue">Register</a></li>
      <li><a href="#" class="blue">Login</a></li>
    </ul>
  </div>
  <div class="col-xs-12 col-sm-4 copyright flex-container left">
    <p>&copy; Copyright {{ date("Y") }} <br class="hidden-lg hidden-xl hidden-xxl hidden-xxs" />GainRank - All rights reserved.</p>
  </div>
  <div class="col-xs-12 col-sm-4 col-md-4 social-media flex-container center">
    <ul>
      <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
      <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
      <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    </ul>
  </div>
  <div class="col-xs-12 col-sm-5 footer-menu hidden-sm hidden-xs hidden-md flex-container right text-right">
    <ul>
      <li><a href="#">Home</a></li>
      <li><a href="#">Features</a></li>
      <li><a href="#">Pricing</a></li>
      <li><a href="#">Support</a></li>
      <li><a href="#" class="blue">Register</a></li>
      <li><a href="#" class="blue">Login</a></li>
    </ul>
  </div>
  <div class="col-xs-12 col-sm-4 footer-menu hidden-xxl hidden-xs hidden-lg">
    <div>
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">Features</a></li>
        <li><a href="#">Pricing</a></li>
      </ul>
    </div>
    <br />
    <div>
      <ul>
        <li><a href="#">Support</a></li>
        <li><a href="#" class="blue">Register</a></li>
        <li><a href="#" class="blue">Login</a></li>
      </ul>
    </div>
  </div>
</div>
