<p>Hello!</p>

<p>{{ $invitation->sender->email }} has invited you to try out Gainrank.</p>
<p>Please <a href="{{ env('APP_URL').'/invitations/'.$invitation->token }}">click here</a> to create your account.</p>

<p>Sincerely,<br/>
The Gainrank Team</p>