Hi {{ explode(' ', $user->name)[0] }}!

<br><br>

Your order has been automatically cancelled due to failure to respond to our
verification e-mail within the given {{ $expiry_days }} {{ $expiry_days > 1 ? 'days' : 'day' }}.

<br><br>

Thanks!

<br>

{{ $product }}

