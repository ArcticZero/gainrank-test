Hi {{ explode(' ', $user->name)[0] }}!

<br><br>

We have recorded a payment attempt using your PayPal account from the e-mail address
{{ $user->email }}. If this was made by you, please click on the link below to verify
this transaction and complete your order.

<br><br>

{{ $verify_url }}

<br><br>

This link will remain valid for {{ $expiry_days }} {{ $expiry_days > 1 ? 'days' : 'day' }}, after which this order will be automatically
cancelled.

<br><br>

Thanks!

<br>

{{ $product }}

