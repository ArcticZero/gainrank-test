Hi {{ explode(' ', $user->name)[0] }}!

<br><br>

@isset ($days)
	Your current subscription will expire in {{ $days }} days.

	@isset ($pay_url)
		Please follow the link below to extend your subscription.

		<br><br>

		{{ $pay_url }}
	@endisset

	<br><br>
@endisset

Your invoice details are listed below.

<br><br>

<strong>Items:</strong>

<br>

<table style="width: 100%; border: 1px solid #333;">
	<thead>
		<tr>
			<th>Item</th>
			<th>Quantity</th>
			<th>Price</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($invoice->items as $item)
			<tr>
				<td>{{ $item['description'] }}</td>
				<td>{{ $item['quantity'] }}</td>
				<td style="text-align: right;">{{ $invoice->currency . number_format($item['price'], 2) }}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2" style="text-align: right; font-weight: 500;">TOTAL:</td>
			<td style="text-align: right;">{{ $invoice->currency . number_format($invoice->total_amount, 2) }}
		</td>
	</tfoot>
</table>

<br><br>

Thanks for your continued support. This email message may serve as your invoice for your records.
Please let us know if you have any questions or concerns.

<br><br>

Thanks!

@isset ($company)
	<br>

	{{ $company }}
@endisset
