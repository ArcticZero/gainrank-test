<update-profile-timezone :user="user" :timezones="{{ json_encode($timezones) }}" :default_timezone="'{{ config('app.timezone') }}'" inline-template>
    <div class="panel panel-default">
        <div class="panel-heading">Timezone</div>

        <div class="panel-body">
            <!-- Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                Your timezone settings have been updated!
            </div>

            <form class="form-horizontal" role="form">
                <!-- Newsletter -->
                <div class="form-group" :class="{'has-error': form.errors.has('timezone')}">
                    <label class="col-md-4 control-label">Timezone</label>
                    
                    <div class="col-md-6">
                        <select name="timezone" v-model="form.timezone" class="form-control">
                            <option v-for="tz in timezones" :value="tz.zone">
                                @{{ tz.diff_from_gmt + " - " + tz.zone }}
                            </option>
                        </select>

                        <span class="help-block" v-show="form.errors.has('timezone')">
                            @{{ form.errors.get('timezone') }}
                        </span>
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="update"
                                :disabled="form.busy">

                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</update-profile-timezone>
