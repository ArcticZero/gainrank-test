<manage-two-factor-auth :user="user" inline-template>
    <div class="panel panel-default">
        <div class="panel-heading">Two-Factor Authentication</div>

        <div class="panel-body">
            <form class="form-horizontal" role="form">
                <!-- Backup Codes -->
                <div class="form-group" :class="{'has-error': generate_error}">
                    <label class="col-md-4 control-label">
                        Backup Codes
                        <div class="text-info">
                            <small>
                                <p>Keep your backup codes somewhere safe but accessible. You can only use each backup code once.</p>
                                <p>You last generated codes on:<br /><strong id="generation-date"></strong></p>
                            </small>
                        </div>
                    </label>
                    <div class="col-md-6">
                        <div class="alert alert-info backup-codes">
                            <div class="row">
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                                <div class="col-md-6"><code class="backup-code">&nbsp;</code></div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary" @click.prevent="generateCodes" :disabled="form.busy">
                            <span v-if="form.busy">
                                <i class="fa fa-btn fa-spinner fa-spin"></i>Please wait
                            </span>

                            <span v-else>
                                Generate New Codes
                            </span>
                        </button>

                        <span class="help-block" v-show="generate_error">
                            @{{ generate_error }}
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button class="btn btn-danger-outline" @click.prevent="disable" :disabled="form.busy">
                            <span v-if="form.busy">
                                <i class="fa fa-btn fa-spinner fa-spin"></i>Please wait
                            </span>

                            <span v-else>
                                Disable Two-Factor Authentication
                            </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</manage-two-factor-auth>
