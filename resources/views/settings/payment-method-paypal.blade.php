<payment-method-paypal :user="user" :team="team" :billable-type="billableType" inline-template>
    <div>
        <!-- Update Card -->
        @include('settings.payment-method.update-payment-method-paypal')

        <div>
            <div v-if="billable.current_billing_plan">
                <!-- Redeem Coupon -->
                @include('spark::settings.payment-method.redeem-coupon')
            </div>
        </div>
    </div>
</payment-method-paypal>
