<div class="radio">
    <label><input type="radio" id="payment-paypal-recurring" name="payment_method" value="paypal_recurring" v-model="form.payment_method"> PayPal (Recurring Payments)</label>
</div>
<div class="radio">
    <label><input type="radio" id="payment-paypal-manual" name="payment_method" value="paypal_manual" v-model="form.payment_method"> PayPal (Manual Payments)</label>
</div>