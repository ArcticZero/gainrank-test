<subscribe-base :user="user" :team="team"
                :plans="plans" :billable-type="billableType" inline-template>

    <div v-if="pendingDataReady">
        <!-- Common Subscribe Form Contents -->
        <div v-if="hasPending">
            <div class="alert alert-warning">
                Your subscription is currently pending, and will be activated once payment has been cleared.
            </div>
        </div>
        <div v-else>
            @include('spark::settings.subscription.subscribe-common')
        </div>

        <!-- Billing Information -->
        <div class="panel panel-default" v-show="selectedPlan">
            <div class="panel-heading"><i class="fa fa-btn fa-credit-card"></i>Billing Information</div>

            <div class="panel-body">
                <!-- Generic 500 Level Error Message / Threw Exception -->
                <div class="alert alert-danger" v-if="form.errors.has('form')">
                    Your payment has failed. Please check your payment gateway account.
                </div>

                <form class="form-horizontal" role="form" @submit.prevent="subscribe">

                    <div class="form-group" :class="{'has-error': form.errors.has('payment_method')}">
                        <label class="col-md-4 control-label">Select a payment method</label>
                        <div class="col-md-6">

                            @include('settings.subscription.subscribe-paypal')
                            @include('settings.subscription.subscribe-coinbase')

                            <span class="help-block" v-show="form.errors.has('payment_method')">
                                @{{ form.errors.get('payment_method') }}
                            </span>
                        </div>
                    </div>

                    <!-- Subscribe Button -->
                    <div class="form-group m-b-none p-b-none">
                        <div class="col-sm-6 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary" :disabled="form.busy || form.successful">
                            <span v-if="form.busy || form.successful">
                                <i class="fa fa-btn fa-spinner fa-spin"></i>Processing
                            </span>

                            <span v-else>
                                Checkout
                            </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</subscribe-base>
