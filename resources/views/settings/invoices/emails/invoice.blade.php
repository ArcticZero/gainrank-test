Hi {{ explode(' ', $billable->name)[0] }}!

<br><br>

Your current subscription will expire in {{ $days }} days.
Please follow the link below to extend your subscription

<br><br>

{{ $pay_url }}

<br><br>

Thanks for your continued support. We've attached a copy of your invoice for your records.
Please let us know if you have any questions or concerns!

<br><br>

Thanks!

<br>

{{ $invoiceData['product'] }}

