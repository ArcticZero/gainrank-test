Hi {{ explode(' ', $user->name)[0] }}!

<br><br>

Your subscription payment is incomplete. You have an outstanding balance of {{ $data['balance'] . ' ' . $data['currency'] }} which must be paid before your subscription can be activated or extended.
Please follow the link below to complete payment

<br><br>

{{ $data['pay_url'] }}

<br><br>

Thanks for your continued support.
Please let us know if you have any questions or concerns!

<br><br>

Thanks!

<br>

{{ $invoiceData['product'] }}

