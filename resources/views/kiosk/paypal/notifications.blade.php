<kiosk-paypal-notifications inline-template>
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-7 col-sm-6">PayPal Notification History</div>
                    <div class="col-lg-5 col-sm-6 text-right">
                        <form v-on:submit.prevent="filterRows()" v-on:reset.prevent="resetRows()" class="form-inline">
                            <input type="text" name="filter" id="filter" class="form-control input-sm" placeholder="Email, TXN ID, Payer ID ...">
                            <button class="btn btn-primary" type="submit">Search</button>
                            <button class="btn btn-danger" type="reset">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>Date Received</th>
                                <th>Transaction ID</th>
                                <th>Payer ID</th>
                                <th>Payer Email</th>
                                <th>Payer Status</th>
                                <th>PayPal Fees</th>
                                <th>IPN Track ID</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="notifications.total == 0">
                                <td colspan="8" class="text-center">No notifications to display.</td>
                            </tr>
                            <template v-if="notifications.data" v-for="notification in notifications.data">
                                <tr>
                                    <td>@{{ this.moment(notification['created_at']).tz(this.timezone).format() | datetime }}</td>
                                    <td>@{{ notification['txn_id'] }}</td>
                                    <td>@{{ notification['payer_id'] }}</td>
                                    <td>@{{ notification['payer_email'] }}</td>
                                    <td>@{{ notification['payer_status'] }}</td>
                                    <td>@{{ notification['mc_fee'] }}</td>
                                    <td>@{{ notification['ipn_track_id'] }}</td>
                                    <td><a role="button" data-toggle="collapse" :href="'#payload-' + notification['ipn_track_id']" aria-expanded="false" :aria-controls="'payload-' + notification['ipn_track_id']">View Payload</a></td>
                                </tr>
                                <tr class="collapse" :id="'payload-' + notification['ipn_track_id']">
                                    <td colspan="8">
                                        <div class="well">
                                            <table class="table table-condensed table-bordered">
                                                <tbody>
                                                    <tr v-for="(value, key) in notification">
                                                        <td><small>@{{ key }}</small></td>
                                                        <td><small>@{{ value }}</small></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-xs-3 text-left">
                        <button type="button" class="btn btn-sm" aria-label="Previous" v-on:click="prevPage()">&larr;<span class="hidden-xs"> Previous</span></button>
                    </div>
                    <div class="col-sm-4 col-xs-6 text-center">
                        <form class="form-inline" v-on:submit.prevent="jumpToPage()">
                            <div class="input-group">
                                <input type="number" min="1" step="1" class="form-control text-right input-sm" value="1" id="page-number">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">Go to Page</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4 col-xs-3 text-right">
                        <button type="button" class="btn btn-sm" aria-label="Next" v-on:click="nextPage()"><span class="hidden-xs">Next </span>&rarr;</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</kiosk-paypal-notifications>

