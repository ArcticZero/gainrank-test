<kiosk-orders-pending inline-template>
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-7 col-sm-6">Pending Orders</div>
                    <div class="col-lg-5 col-sm-6 text-right">
                        <form v-on:submit.prevent="filterRows()" v-on:reset.prevent="resetRows()" class="form-inline">
                            <input type="text" name="order-filter" id="order-filter" class="form-control input-sm" placeholder="Email, Payer ID, Agreement ID ...">
                            <button class="btn btn-primary" type="submit">Search</button>
                            <button class="btn btn-danger" type="reset">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-borderless table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>Date Received</th>
                                <th>Order Type</th>
                                <th>PayPal Payer ID</th>
                                <th>PayPal Payer Email</th>
                                <th>PayPal Agreement ID</th>
                                <th>Status</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="orders.total == 0">
                                <td colspan="9" class="text-center">No orders to display.</td>
                            </tr>
                            <template v-if="orders.data" v-for="order in orders.data">
                                <tr :id="'order' + order['id']">
                                    <td>@{{ this.moment(order['created_at']).tz(this.timezone).format() | datetime }}</td>
                                    <td>@{{ order['order_type']}}</td>
                                    <td>@{{ order['paypal_payer_id'] }}</td>
                                    <td>@{{ order['paypal_payer_email'] }}</td>
                                    <td>@{{ order['paypal_agreement_id'] }}</td>
                                    <td>@{{ order['status'] }}</td>
                                    <td><a v-if="order.request_details" role="button" data-toggle="collapse" :href="'#payload-' + order['id']" aria-expanded="false" :aria-controls="'payload-' + order['id']">View Payload</a></td>
                                    <td><a role="button" :id="'#approve-' + order['id']" v-on:click="confirmApproveOrder(order['id'])">Approve</a></td>
                                    <td><a role="button" :id="'#deny-' + order['id']" v-on:click="confirmDenyOrder(order['id'])">Deny</a></td>
                                </tr>
                                <tr class="collapse" :id="'payload-' + order['id']">
                                    <td colspan="8">
                                        <div class="well">
                                            <table class="table table-condensed table-bordered">
                                                <tbody>
                                                    <tr v-for="(value, key) in order.request_details">
                                                        <td><small>@{{ key }}</small></td>
                                                        <td><small>@{{ value }}</small></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-xs-3 text-left">
                        <button type="button" class="btn btn-sm" aria-label="Previous" v-on:click="prevPage()">&larr;<span class="hidden-xs"> Previous</span></button>
                    </div>
                    <div class="col-sm-4 col-xs-6 text-center">
                        <form class="form-inline" v-on:submit.prevent="jumpToPage()">
                            <div class="input-group">
                                <input type="number" min="1" step="1" class="form-control text-right input-sm" value="1" id="page-number">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">Go to Page</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4 col-xs-3 text-right">
                        <button type="button" class="btn btn-sm" aria-label="Next" v-on:click="nextPage()"><span class="hidden-xs">Next </span>&rarr;</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Process Order Modal -->
        <div class="modal fade" id="modal-process-order" tabindex="-1" role="dialog">
            <div class="modal-dialog" v-if="approvingOrder">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <h4 class="modal-title">
                            Approve Order
                        </h4>
                    </div>

                    <div class="modal-body">
                        Are you sure you want to approve this order?
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No, Go Back</button>

                        <button type="button" class="btn btn-danger" @click="approveOrder">
                            Yes, Approve
                        </button>
                    </div>
                </div>
            </div>

            <div class="modal-dialog" v-if="denyingOrder">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <h4 class="modal-title">
                            Deny Order
                        </h4>
                    </div>

                    <div class="modal-body">
                        Are you sure you want to deny this order?
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No, Go Back</button>

                        <button type="button" class="btn btn-danger" @click="denyOrder">
                            Yes, Deny
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</kiosk-orders-pending>

