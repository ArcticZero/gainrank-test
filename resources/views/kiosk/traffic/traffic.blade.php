<kiosk-traffic inline-template>
    <div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Traffic
            </div>
            <div class="panel-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#traffic-submissions" aria-controls="traffic" role="tab" data-toggle="tab">Submissions</a></li>
                    <li role="presentation"><a href="#traffic-statuses" aria-controls="traffic-statuses" role="tab" data-toggle="tab">Statuses</a></li>
                    <li role="presentation"><a href="#traffic-settings" aria-controls="traffic-settings" role="tab" data-toggle="tab">Settings</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="traffic-submissions"> 
                        <div class="row" style="margin: 20px 0;">
                            <div class="col-sm-6 col-sm-offset-6 text-right">
                                <form v-on:submit.prevent="filterSubmissions()" v-on:reset.prevent="resetSubmissions()" class="form-inline">
                                    <input type="text" name="filter" v-model="submissionFilter" class="form-control input-sm" placeholder="URL, IP Address ...">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date Submitted</th>
                                        <th>Customer</th>
                                        <th>URL</th>
                                        <th>Wanted Count</th>
                                        <th>Sent Count</th>
                                        <th>Cost Per X</th>
                                        <th>Amount per Cost</th>
                                        <th>IP Address</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if="submissions.total == 0">
                                        <td colspan="11" class="text-center">No submissions to display.</td>
                                    </tr>
                                    <template v-if="submissions.data" v-for="submission in submissions.data">
                                        <tr>
                                            <td>@{{ submission['submission_id'] }}</td>
                                            <td>@{{ this.moment(submission['created_at']).tz(this.timezone).format() | datetime }}</td>
                                            <td>@{{ submission['user_name'] }}</td>
                                            <td>@{{ submission['url'] }}</td>
                                            <td>@{{ submission['wanted_count'] }}</td>
                                            <td>@{{ submission['sent_count'] }}</td>
                                            <td>@{{ submission['cost_per_x'] }}</td>
                                            <td>@{{ submission['cost_per_amt'] }}</td>
                                            <td>@{{ submission['ip_addr'] }}</td>
                                            <td>@{{ submission['status_text'] }}</td>
                                            <td>
                                                <i class="fa fa-pencil-square-o fa-fw" role="button" title="Edit" v-on:click="showSubmissionForm(status['id'])"></i>
                                                <i class="fa fa-trash fa-fw" role="button" title="Stop submission" v-on:click="stopSubmission(submission['id'])"></i>
                                                <!--<i class="fa fa-undo fa-fw" role="button" title="Issue refund" v-on:click="issueRefund(submission['id'])"></i>-->
                                            </td>
                                        </tr>
                                    </template>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-xs-3 text-left">
                                <button type="button" class="btn btn-sm" aria-label="Previous" v-on:click="submissionsPrevPage()">&larr;<span class="hidden-xs"> Previous</span></button>
                            </div>
                            <div class="col-sm-4 col-xs-6 text-center">
                                <form class="form-inline" v-on:submit.prevent="submissionsJumpToPage()">
                                    <div class="input-group">
                                        <input type="number" min="1" step="1" class="form-control text-right input-sm" value="1" id="traffic-submissions-page-number">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">Go to Page</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-4 col-xs-3 text-right">
                                <button type="button" class="btn btn-sm" aria-label="Next" v-on:click="submissionsNextPage()"><span class="hidden-xs">Next </span>&rarr;</button>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="traffic-statuses"> 
                        <div class="row" style="margin: 20px 0;">
                            <div class="col-sm-6">
                                <button type="button" v-on:click="showStatusForm()" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-spaced"></i>Add Status</button>
                            </div>
                            <div class="col-sm-6 text-right">
                                <form v-on:submit.prevent="filterStatuses()" v-on:reset.prevent="resetStatuses()" class="form-inline">
                                    <input type="text" name="filter" v-model="statusFilter" class="form-control input-sm" placeholder="ID, Name, Internal ...">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless table-striped table-condensed" id="traffic-statuses-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Internal Name</th>
                                        <th>Customer Message</th>
                                        <th>Admin Message</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if="statuses.total == 0">
                                        <td colspan="6" class="text-center">No statuses to display.</td>
                                    </tr>
                                    <template v-if="statuses.data" v-for="status in statuses.data">
                                        <tr :data-status-id="status['id']">
                                            <td>@{{ status['id'] }}</td>
                                            <td>@{{ status['name'] }}</td>
                                            <td>@{{ status['code'] }}</td>
                                            <td>@{{ status['cust_message'] }}</td>
                                            <td>@{{ status['staff_message'] }}</td>
                                            <td>
                                                <i class="fa fa-pencil-square-o fa-fw" role="button" title="Edit" v-on:click="showStatusForm(status['id'])"></i>
                                                <i class="fa fa-trash fa-fw" role="button" title="Delete" v-on:click="deleteStatus(status['id'])"></i>
                                            </td>
                                        </tr>
                                    </template>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-xs-3 text-left">
                                <button type="button" class="btn btn-sm" aria-label="Previous" v-on:click="statusesPrevPage()">&larr;<span class="hidden-xs"> Previous</span></button>
                            </div>
                            <div class="col-sm-4 col-xs-6 text-center">
                                <form class="form-inline" v-on:submit.prevent="statusesJumpToPage()">
                                    <div class="input-group">
                                        <input type="number" min="1" step="1" class="form-control text-right input-sm" value="1" id="traffic-statuses-page-number">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">Go to Page</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-4 col-xs-3 text-right">
                                <button type="button" class="btn btn-sm" aria-label="Next" v-on:click="statusesNextPage()"><span class="hidden-xs">Next </span>&rarr;</button>
                            </div>
                        </div>

                        <div class="modal fade" id="traffic-status-modal" role="dialog" aria-labelledby="traffic-status-modal-title">
                            <div class="modal-dialog" role="document">
                                <form action="" method="post" v-on:submit.prevent="saveStatus">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="traffic-status-modal-title">@{{ statusFormTitle }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert alert-success" v-if="statusForm.successful">
                                                Status has been saved successfully!
                                            </div>
                                            <div class="form-group" :class="{'has-error': statusForm.errors.has('id')}">
                                                <label for="traffic-field-status-id">ID</label>
                                                <input name="id" id="traffic-field-status-id" type="text" class="form-control" v-model="statusForm.id" required readonly>

                                                <span class="help-block" v-show="statusForm.errors.has('id')">
                                                    @{{ statusForm.errors.get('id') }}
                                                </span>
                                            </div>
                                            <div class="form-group" :class="{'has-error': statusForm.errors.has('name')}">
                                                <label for="traffic-field-status-name">Name</label>
                                                <input name="name" id="traffic-field-status-name" type="text" class="form-control" v-model="statusForm.name" required>

                                                <span class="help-block" v-show="statusForm.errors.has('name')">
                                                    @{{ statusForm.errors.get('name') }}
                                                </span>
                                            </div>
                                            <div class="form-group" :class="{'has-error': statusForm.errors.has('code')}">
                                                <label for="traffic-field-status-code">Internal Name</label>
                                                <input name="code" id="traffic-field-status-code" type="text" class="form-control" v-model="statusForm.code" required>

                                                <span class="help-block" v-show="statusForm.errors.has('code')">
                                                    @{{ statusForm.errors.get('code') }}
                                                </span>
                                            </div>
                                            <div class="form-group" :class="{'has-error': statusForm.errors.has('cust_message')}">
                                                <label for="traffic-field-status-cust-message">Customer Message</label>
                                                <input name="cust_message" id="traffic-field-status-cust-message" type="text" class="form-control" v-model="statusForm.cust_message" required>

                                                <span class="help-block" v-show="statusForm.errors.has('cust_message')">
                                                    @{{ statusForm.errors.get('cust_message') }}
                                                </span>
                                            </div>
                                            <div class="form-group" :class="{'has-error': statusForm.errors.has('cust_url')}">
                                                <label for="traffic-field-status-cust-url">Customer URL</label>
                                                <input name="cust_url" id="traffic-field-status-cust-url" type="text" class="form-control" v-model="statusForm.cust_url" required>

                                                <span class="help-block" v-show="statusForm.errors.has('cust_url')">
                                                    @{{ statusForm.errors.get('cust_url') }}
                                                </span>
                                            </div>
                                            <div class="form-group" :class="{'has-error': statusForm.errors.has('staff_message')}">
                                                <label for="traffic-field-status-staff-message">Staff Message</label>
                                                <input name="staff_message" id="traffic-field-status-staff-message" type="text" class="form-control" v-model="statusForm.staff_message" required>

                                                <span class="help-block" v-show="statusForm.errors.has('staff_message')">
                                                    @{{ statusForm.errors.get('staff_message') }}
                                                </span>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary large" :disabled="statusForm.busy">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="traffic-settings"> 
                        <form action="" method="post" class="form-horizontal" v-on:submit.prevent="saveSettings()">
                            <table class="table table-borderless table-striped table-condensed" style="margin-top: 20px;">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if="!settings || Object.keys(settings).length == 0">
                                        <td colspan="2" class="text-center">No settings to display.</td>
                                    </tr>
                                    <template v-if="Object.keys(settings).length > 0" v-for="(value, key) in settings">
                                        <tr>
                                            <td class="col-xs-2">
                                                <label>@{{ key }}</label>
                                            </td>
                                            <td>
                                                <input type="text" :name="key" :value="value" class="form-control" v-model="settingsForm[key]">
                                            </td>
                                        </tr>
                                    </template>
                                </tbody>
                            </table>
                            <div class="row" v-if="Object.keys(settings).length">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-primary" type="submit">Save Settings</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</kiosk-traffic>
