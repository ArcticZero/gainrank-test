@extends('spark::layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="alert alert-danger">This account has been banned for a terms of service violation.</div>
        </div>
    </div>
</div>
@endsection
