@extends('spark::layouts.app')
@section('title', 'Gainrank Affiliates')
@section('content')
    <div class="row" role="main">
        <div class="content-box">
            <h1>Be an Affiliate</h1>

            <h2>Guidelines</h2>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed iaculis in dui vel aliquam. Vestibulum pretium vestibulum tempor. Morbi lectus sem, ultrices at sagittis vel, vulputate ac odio. Donec vel pharetra nulla. Ut eget tincidunt nunc. Duis ex orci, feugiat nec urna ac, gravida dapibus nunc. Aliquam eget ligula non arcu aliquet molestie sit amet in sapien.
            </p>

            <p>
            Nullam ut diam mattis, efficitur erat a, lacinia arcu. Sed tristique enim nibh, nec pretium nulla dictum eu. Maecenas quam risus, auctor sed lorem sit amet, bibendum sollicitudin dui. Nullam quis viverra odio. Mauris at lobortis ante, at sagittis mauris. Donec at magna nunc. In quam nunc, sollicitudin nec enim in, mollis blandit nulla. Donec eget nunc scelerisque, consequat massa ultrices, tempus turpis. Mauris consequat elit non ultricies iaculis. Donec ullamcorper elementum nibh eu sodales. Etiam feugiat ligula risus, eu commodo enim gravida eget. Donec mattis quam ac tincidunt dictum. Pellentesque tempus felis rhoncus nisi consequat efficitur. Proin consequat dolor et elementum rhoncus.
            </p>

            <p>
            Mauris pulvinar velit nibh, at scelerisque urna dapibus eu. Maecenas elementum, tellus vel blandit tincidunt, velit justo porttitor ante, in convallis orci nibh et quam. Suspendisse sed nisi mattis, tempor ligula id, sagittis justo. Vestibulum nec congue metus. Ut eu tempus felis. Phasellus consequat sapien nec neque ornare, ac aliquet nisi sagittis. Praesent sollicitudin, ex at pellentesque congue, leo velit dapibus tellus, eu gravida tellus ligula quis nisl.
            </p>

            <p>
            Vivamus posuere eget sem eu semper. Fusce dapibus augue dolor, ac pretium lectus dignissim rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut id rutrum ipsum, a scelerisque nunc. Curabitur maximus sapien vitae sapien rhoncus, sit amet fringilla massa bibendum. Mauris sed blandit quam. Nunc faucibus dictum neque, vel volutpat enim. Etiam vehicula interdum dui, quis suscipit lorem placerat eu. Integer posuere sapien purus, vitae dignissim lacus hendrerit convallis.
            </p>

            <p>
            Pellentesque mollis et quam ut posuere. Praesent tellus arcu, ullamcorper at facilisis sit amet, pretium ut eros. Etiam vitae neque ut diam cursus facilisis eget eget ipsum. Sed tempus, nulla vitae tincidunt luctus, nulla metus tristique nisi, id dictum metus lorem eget nulla. Etiam sagittis turpis lectus, ac ultricies enim vulputate id. Fusce et consectetur lectus, id vulputate purus. Integer in velit sed ex commodo luctus. Proin lobortis orci quis magna laoreet semper.
            </p>

            <h2>Terms and Conditions</h2>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed iaculis in dui vel aliquam. Vestibulum pretium vestibulum tempor. Morbi lectus sem, ultrices at sagittis vel, vulputate ac odio. Donec vel pharetra nulla. Ut eget tincidunt nunc. Duis ex orci, feugiat nec urna ac, gravida dapibus nunc. Aliquam eget ligula non arcu aliquet molestie sit amet in sapien.
            </p>

            <p>
            Nullam ut diam mattis, efficitur erat a, lacinia arcu. Sed tristique enim nibh, nec pretium nulla dictum eu. Maecenas quam risus, auctor sed lorem sit amet, bibendum sollicitudin dui. Nullam quis viverra odio. Mauris at lobortis ante, at sagittis mauris. Donec at magna nunc. In quam nunc, sollicitudin nec enim in, mollis blandit nulla. Donec eget nunc scelerisque, consequat massa ultrices, tempus turpis. Mauris consequat elit non ultricies iaculis. Donec ullamcorper elementum nibh eu sodales. Etiam feugiat ligula risus, eu commodo enim gravida eget. Donec mattis quam ac tincidunt dictum. Pellentesque tempus felis rhoncus nisi consequat efficitur. Proin consequat dolor et elementum rhoncus.
            </p>

            <p>
            Mauris pulvinar velit nibh, at scelerisque urna dapibus eu. Maecenas elementum, tellus vel blandit tincidunt, velit justo porttitor ante, in convallis orci nibh et quam. Suspendisse sed nisi mattis, tempor ligula id, sagittis justo. Vestibulum nec congue metus. Ut eu tempus felis. Phasellus consequat sapien nec neque ornare, ac aliquet nisi sagittis. Praesent sollicitudin, ex at pellentesque congue, leo velit dapibus tellus, eu gravida tellus ligula quis nisl.
            </p>

            <p>
            Vivamus posuere eget sem eu semper. Fusce dapibus augue dolor, ac pretium lectus dignissim rutrum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut id rutrum ipsum, a scelerisque nunc. Curabitur maximus sapien vitae sapien rhoncus, sit amet fringilla massa bibendum. Mauris sed blandit quam. Nunc faucibus dictum neque, vel volutpat enim. Etiam vehicula interdum dui, quis suscipit lorem placerat eu. Integer posuere sapien purus, vitae dignissim lacus hendrerit convallis.
            </p>

            <p>
            Pellentesque mollis et quam ut posuere. Praesent tellus arcu, ullamcorper at facilisis sit amet, pretium ut eros. Etiam vitae neque ut diam cursus facilisis eget eget ipsum. Sed tempus, nulla vitae tincidunt luctus, nulla metus tristique nisi, id dictum metus lorem eget nulla. Etiam sagittis turpis lectus, ac ultricies enim vulputate id. Fusce et consectetur lectus, id vulputate purus. Integer in velit sed ex commodo luctus. Proin lobortis orci quis magna laoreet semper.
            </p>

            <form method="post" action="{{ url('/affiliate_apply') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="accept_terms" value="1">
                                Accept Terms and Conditions
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                        <button type="submit" class="btn btn-primary">
                            Become an Affiliate
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection
