<affiliate-transactions inline-template>
<div class="content-box backend">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box backend w-header">
                <div class="box-header">
                    <h5><strong>Referral Transactions</strong></h5>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Details</th>
                                        <th>Maturity</th>
                                        <th>Mature Date</th>
                                        <th>Reward</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if="transactions.total == 0">
                                        <td colspan="4" class="text-center">No transactions to display.</td>
                                    </tr>
                                    <template v-if="transactions.data" v-for="transaction in transactions.data">
                                    <tr>
                                        <td>@{{ transaction['transacted_at'] }}</td>
                                        <td>@{{ transaction['action_text'] }}</td>
                                        <td>@{{ transaction['maturity'] }}</td>
                                        <td>@{{ transaction['mature_at'] }}</td>
                                        <td>@{{ transaction['reward'] }}</td>
                                    </tr>
                                    </template>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-xs-3 text-left">
                            <button type="button" class="btn btn-sm" aria-label="Previous" v-on:click="prevPage()">&larr;<span class="hidden-xs"> Previous</span></button>
                        </div>
                        <div class="col-sm-4 col-xs-6 text-center">
                            <form class="form-inline" v-on:submit.prevent="jumpToPage()">
                                <div class="input-group">
                                    <input type="number" min="1" step="1" class="form-control text-right input-sm" value="1" id="page-number">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">Go to Page</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-4 col-xs-3 text-right">
                            <button type="button" class="btn btn-sm" aria-label="Next" v-on:click="nextPage()"><span class="hidden-xs">Next </span>&rarr;</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</affiliate-transactions>
