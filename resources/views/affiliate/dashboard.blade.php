@extends('spark::layouts.app')
@section('title', 'Gainrank Affiliates')
@section('content')
    <div class="row" role="main">
        <div class="content-box">
            <h1>Affiliate Dashboard</h1>
        </div>
    </div>
    <div class="content-box backend">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-success">
                    <div class="panel-heading text-center">Total Payout</div>
                    <div class="panel-body text-center">${{ number_format($payout_total, 2) }}</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-success">
                    <div class="panel-heading text-center">Available Payout</div>
                    <div class="panel-body text-center">${{ number_format($payout_available, 2) }}</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-success">
                    <div class="panel-heading text-center">Pending Payout</div>
                    <div class="panel-body text-center">${{ number_format($payout_pending, 2) }}</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading text-center">Total Referals</div>
                    <div class="panel-body text-center">{{ $total_referals }}</div>
            </div>
        </div>
    </div>
    @include('affiliate.transactions')
@endsection
