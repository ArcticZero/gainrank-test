@extends('layouts.frontend-internal')
@section('title', 'Blog Posts')

@section('content')

  <!-- Static blog posts page -->
  <div class="row" role="main">
    @foreach( $posts as $post)
      @if( $loop->first)
        <div class="row blog-post">
          <div class="thumbnail featured">
            <p class="featured-tag">Featured</p>
            <a href="{{ url('/blog/'.$post->slug) }}">
              <img src="{!! $post->getMedia('images')->first()->getUrl('hero') !!}"/>
              <div class="caption">
                <p class="category-tag light">
                  @foreach( $post->tags as $tag)
                    {{ $tag->name }}@unless( $loop->last),@endunless
                  @endforeach
                </p>
                <h3>{{ $post->title }}</h3>
              </div>
            </a>
          </div>
        </div>
      @endif
    @endforeach

    <div class="row blog-posts">
      @foreach( $posts as $post)
        @if( ! $loop->first)
          <div class="col-xs-12 col-sm-4 blog-post">
            <div class="thumbnail">
              <a href="{{ url('/blog/'.$post->slug) }}">
                <img src="{!! $post->getMedia('images')->first()->getUrl('thumb-large') !!}"/>
                <div class="caption">
                  <p class="category-tag light">
                    @foreach( $post->tags as $tag)
                      {{ $tag->name }}@unless( $loop->last),@endunless
                    @endforeach
                  </p>
                  <h3>{{ $post->title }}</h3>
                </div>
              </a>
            </div>
          </div>
        @endif
      @endforeach
    </div>
  </div>
@endsection