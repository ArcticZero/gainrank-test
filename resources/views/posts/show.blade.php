@extends('layouts.frontend-internal')
@section('title', $post->title)

@section('content')
  <div class="row single-post" role="main">
    <div class="jumbotron">
      <img src="{!! $post->getMedia('images')->first()->getUrl('hero') !!}" class="img-responsive"/>
      <div class="post-details">
        <h1>{{ $post->title }}</h1>
        <h4 class="category-tag light">
          @foreach( $post->tags as $tag)
            {{ $tag->name }}@unless( $loop->last),@endunless
          @endforeach
        </h4>
        <h4>{{ $post->user->name }} | {{ $post->created_at->format('F d, Y') }}</h4>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-md-8 post-content">
        <div class="content-box">
          <!-- Remember to use @{!! $var !!} (without the @ sign) instead of @{{ $var }} as the latter escapes all HTML entities -->
          {!! $post->body !!}
        </div>
      </div>

      <div class="col-md-4 hidden-xs hidden-sm sidebar">
        <div class="row latest-posts">
          <h3>Latest Articles</h3>
          @foreach( $posts as $latest)
            <div class="content-box">
              <div class="row">
                <a href="{{ url('/blog/'.$latest->slug) }}">
                  <img src="{!! $latest->getMedia('images')->first()->getUrl('thumb-small') !!}" class="img-responsive"/>
                  <div class="post-info">
                    <h4>{{ $latest->title }}</h4>
                    <p class="light">{{ $latest->created_at->format('F d, Y') }}</p>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        </div>

        <div class="row post-share">
          <h3>Share Article</h3>
          <div class="content-box text-center">
            <ul>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="Share this on Facebook"><img src="{!! asset('images/facebook.png') !!}" alt="Facebook"/></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="Share this on Google+"><img src="{!! asset('images/google+.png') !!}" alt="Google+"/></a></li>
              <li><a href="" data-toggle="tooltip" data-placement="top" title="Share this on Twitter"><img src="{!! asset('images/twitter.png') !!}" alt="Twitter"/></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script type="text/javascript">
      //Initialize tooltips
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
      });
    </script>
@endsection