@extends('spark::layouts.app')

@section('content')
<alexa :user="user" rows="{{ json_encode($rows) }}" inline-template>
    <div class="container-fluid back-content">
        <div id="main-content" role="main">
            <div class="content-box backend">
                <div class="row">
                    <div class="col-md-6"><p><strong>Alexa Submissions</strong></p></div>
                    <div class="col-md-6"><button type="button" data-toggle="modal" data-target="#add-row-modal" class="btn btn-primary large pull-right"><i class="fa fa-plus fa-spaced"></i>Add Submission</button></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-condensed" id="keyword-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Date Added</th>
                                        <th>URL</th>
                                        <th>Amount Ordered</th>
                                        <th>Amount Sent</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($rows))
                                        @foreach ($rows as $row)
                                            <tr>
                                                <td>
                                                    <i role="button" class="fa fa-fw fa-trash" title="Delete this submission" v-on:click="deleteRow({{ $row['id'] }})"></i>
                                                </td>
                                                <td>{{ Auth::user()->applyTz($row['created_at'])->format('m-d-Y h:i A') }}</td>
                                                <td>{{ $row['url'] }}</td>
                                                <td>{{ $row['wanted_count'] }}</td>
                                                <td>{{ $row['sent_count'] }}</td>
                                                <td>{{ $row['status'] }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">No submissions to display.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-row-modal" role="dialog" aria-labelledby="add-row-modal-title">
            <div class="modal-dialog" role="document">
                <form action="" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="add-row-modal-title">Add Submission</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-success" v-if="form.successful">
                                Your request has been submitted successfully!
                            </div>
                            <div class="form-group" :class="{'has-error': form.errors.has('url')}">
                                <div class="input-group">
                                    <span class="input-group-addon" id="domain-protocol">http(s)://</span>
                                    <input name="url" type="text" class="form-control" placeholder="Your URL here" aria-describedby="domain-protocol" v-model="form.url" required>
                                </div>

                                <span class="help-block" v-show="form.errors.has('url')">
                                    @{{ form.errors.get('url') }}
                                </span>
                            </div>
                            <div class="form-group" :class="{'has-error': form.errors.has('amt')}">
                                <label for="field-amt">Amount</label>
                                <input name="amt" id="field-amt" type="number" step="1" class="form-control" v-model="form.amt" required>

                                <span class="help-block" v-show="form.errors.has('amt')">
                                    @{{ form.errors.get('amt') }}
                                </span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary large" @click="submitForm" :disabled="form.busy">Submit to Alexa</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</alexa>
@endsection

@section('scripts-bottom')
    <script type="text/javascript">
        $('#side-nav').hover(function() {
            if ($('#side-nav').hasClass('visible')) {
                $(".nav-menu-item").css({
                    'display': 'none',
                });
                $("#side-nav").animate({
                    width: "80",
                }, 100);
                $("#main-content").animate({
                    'margin-left': "80",
                }, 100);
                $("#side-nav .fixed-bottom a").css({
                    'width': 'auto',
                })
            } else {
                $("#side-nav").animate({
                    width: "250",
                }, 100, function () {
                    $(".nav-menu-item").css({
                        'display': 'inline-block',
                    });
                });

                $("#main-content").animate({
                    'margin-left': "250",
                }, 100);

                $("#side-nav .fixed-bottom a").css({
                    'width': '250px',
                })
            }
            $("#side-nav").toggleClass('visible');
        }, function() {
            $(".nav-menu-item").css({
                'display': 'none',
            });
            $("#side-nav").animate({
                width: "80",
            }, 100);
            $("#main-content").animate({
                'margin-left': "80",
            }, 100);
            $("#side-nav .fixed-bottom a").css({
                'width': 'auto',
            })
            $("#side-nav").removeClass('visible');
        });

        $("#add-row-modal").on('hidden.bs.modal', function(e) {
            $(this).find('form').trigger('reset');
        })

        window.fingerprint({excludeUserAgent: true}).get(function(result, components){
            axios.post('/fingerprints', {hash: result});
        });
    </script>

@endsection
