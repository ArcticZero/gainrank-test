@extends('spark::layouts.app')

@section('content')
<ranktracker :user="user" rows="{{ json_encode($rows) }}" ranks="{{ json_encode($ranks) }}" dates="{{ json_encode($dates) }}" stats="{{ json_encode($stats) }}" colors="{{ json_encode(config('chartist.series_colors')) }}" inline-template>
    <div class="container-fluid">
        <div id="main-content" role="main">
            <div class="row data-box-row">
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-arrow-up fa-spaced"></i>Keywords Up
                        </div>
                        <h1{!! $stats['keywords_up']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['keywords_up']['total'] }}</h1>
                        @if ($stats['keywords_up']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['keywords_up']['delta']) . '%' }}</span> from last period
                        @elseif ($stats['keywords_up']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['keywords_up']['delta']) . '%' }}</span> from last period
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last period
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-heart fa-spaced"></i>Top 3
                        </div>
                        <h1{!! $stats['top_3']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['top_3']['total'] }}</h1>
                        @if ($stats['top_3']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top_3']['delta']) . '%' }}</span> from last period
                        @elseif ($stats['top_3']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top_3']['delta']) . '%' }}</span> from last period
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last period
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-check fa-spaced"></i>Top 10
                        </div>
                        <h1{!! $stats['top_10']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['top_10']['total'] }}</h1>
                        @if ($stats['top_10']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top_10']['delta']) . '%' }}</span> from last period
                        @elseif ($stats['top_10']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top_10']['delta']) . '%' }}</span> from last period
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last period
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-trophy fa-spaced"></i>Top 20
                        </div>
                        <h1{!! $stats['top_20']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['top_20']['total'] }}</h1>
                        @if ($stats['top_20']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top_20']['delta']) . '%' }}</span> from last period
                        @elseif ($stats['top_20']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top_20']['delta']) . '%' }}</span> from last period
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last period
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-road fa-spaced"></i>Top 30
                        </div>
                        <h1{!! $stats['top_30']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['top_30']['total'] }}</h1>
                        @if ($stats['top_30']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top_30']['delta']) . '%' }}</span> from last period
                        @elseif ($stats['top_30']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top_30']['delta']) . '%' }}</span> from last period
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last period
                        @endif
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-signal fa-spaced"></i>Top 100
                        </div>
                        <h1{!! $stats['top_100']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['top_100']['total'] }}</h1>
                        @if ($stats['top_100']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top_100']['delta']) . '%' }}</span> from last period
                        @elseif ($stats['top_100']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top_100']['delta']) . '%' }}</span> from last period
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last period
                        @endif
                    </div>
                </div>
            </div>
            <div class="content-box backend">
                <div class="row">
                    <div class="col-md-6"><p><strong>Keyword Rankings</strong> Last {{ $past_x_days }} days</p></div>
                    <div class="col-md-6"><button type="button" v-on:click="showForm()" class="btn btn-primary large pull-right"><i class="fa fa-plus fa-spaced"></i>Add Keywords</button></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <chartist id="keyword-rankings" ref="keywords" class="ct-fill-only hide" :type="'Line'" :data="chart.data"
                            :options="chart.options"></chartist>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed" id="keyword-table">
                                <thead>
                                    <tr>
                                        <th><i class="fa fa-area-chart fa-fw" title="Show on chart"></i></th>
                                        <th></th>
                                        <th>Keyword</th>
                                        <th><i class="fa fa-google fa-fw google-blue"></i></th>
                                        <th>1 Day</th>
                                        <th>7 Days</th>
                                        <th>30 Days</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($rows))
                                        @foreach ($rows as $row)
                                            <tr data-domain="{{ $row['domain'] }}" data-keyword="{{ $row['keyword'] }}" data-loop-id="{{ $loop->iteration }}" data-row-id="{{ $row['id'] }}">
                                                <td>
                                                    <i role="button" class="fa fa-area-chart fa-fw" title="Show on chart" v-on:click="toggleRow({{ $row['id'] }})"></i>
                                                </td>
                                                <td><i class="fa fa-square fa-fw invisible" :style="{ color: getRowColor({{ $row['id'] }}) }"></i></td>
                                                <td>{{ $row['keyword'] }}</td>
                                                <td>{{ $row['last_rank'] }}</td>
                                                <td>{!! $row['deltas']['1'] != 0 ? '<i class="fa fa-fw ' . ($row['deltas']['1'] < 0 ? 'fa-arrow-down text-danger' : 'fa-arrow-up light-green') . '"></i>' . abs($row['deltas']['1']) : '-' !!}</td>
                                                <td>{!! $row['deltas']['7'] != 0 ? '<i class="fa fa-fw ' . ($row['deltas']['7'] < 0 ? 'fa-arrow-down text-danger' : 'fa-arrow-up light-green') . '"></i>' . abs($row['deltas']['7']) : '-' !!}</td>
                                                <td>{!! $row['deltas']['30'] != 0 ? '<i class="fa fa-fw ' . ($row['deltas']['30'] < 0 ? 'fa-arrow-down text-danger' : 'fa-arrow-up light-green') . '"></i>' . abs($row['deltas']['30']) : '-' !!}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">No submissions to display.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-row-modal" role="dialog" aria-labelledby="add-row-modal-title">
            <div class="modal-dialog" role="document">
                <form action="" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="add-row-modal-title">Add Keywords</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-success" v-if="form.successful">
                                Keywords have been added successfully!
                            </div>
                            <!--
                            <div class="form-group" :class="{'has-error': form.errors.has('domain')}">
                                <div class="input-group">
                                    <span class="input-group-addon" id="domain-protocol">http(s)://</span>
                                    <input name="domain" type="text" class="form-control" placeholder="domain.com" aria-describedby="domain-protocol" v-model="form.domain" required>
                                </div>
                            </div>
                            -->
                            <div class="form-group" :class="{'has-error': form.errors.has('keywords')}">
                                <textarea class="form-control" rows="8" name="keywords" v-model="form.keywords" required>Input one keyword per line</textarea>

                                <span class="help-block" v-show="form.errors.has('keywords')">
                                    @{{ form.errors.get('keywords') }}
                                </span>
                            </div>
                            <div class="form-group" :class="{'has-error': form.errors.has('google_region')}">
                                <label for="google-region">Google Region</label>
                                <select id="google-region" name="google_region" class="form-control" v-model="form.google_region" options="regions" required>
                                    <option v-for="region in regions" :value="region.value">
                                        @{{ region.text }}
                                    </option>
                                </select>

                                <span class="help-block" v-show="form.errors.has('google_region')">
                                    @{{ form.errors.get('google_region') }}
                                </span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary large" @click="submitForm" :disabled="form.busy">Add Keywords</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</ranktracker>
@endsection

@section('scripts-bottom')
    <script type="text/javascript">
        $('#side-nav').hover(function() {
            if ($('#side-nav').hasClass('visible')) {
                $(".nav-menu-item").css({
                    'display': 'none',
                });
                $("#side-nav").animate({
                    width: "80",
                }, 100);
                $("#main-content").animate({
                    'margin-left': "80",
                }, 100);
                $("#side-nav .fixed-bottom a").css({
                    'width': 'auto',
                })
            } else {
                $("#side-nav").animate({
                    width: "250",
                }, 100, function () {
                    $(".nav-menu-item").css({
                        'display': 'inline-block',
                    });
                });

                $("#main-content").animate({
                    'margin-left': "250",
                }, 100);

                $("#side-nav .fixed-bottom a").css({
                    'width': '250px',
                })
            }
            $("#side-nav").toggleClass('visible');
        }, function() {
            $(".nav-menu-item").css({
                'display': 'none',
            });
            $("#side-nav").animate({
                width: "80",
            }, 100);
            $("#main-content").animate({
                'margin-left': "80",
            }, 100);
            $("#side-nav .fixed-bottom a").css({
                'width': 'auto',
            })
            $("#side-nav").removeClass('visible');
        });

        $("#add-row-modal").on('hidden.bs.modal', function(e) {
            $(this).find('form').trigger('reset');
        })

        window.fingerprint({excludeUserAgent: true}).get(function(result, components){
            axios.post('/fingerprints', {hash: result});
        });
    </script>

@endsection
