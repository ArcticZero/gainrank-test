@extends('spark::layouts.app')

@section('content')
<domain :user="user" rows="{{ json_encode($rows) }}" inline-template>
    <div class="container-fluid">
        <div id="main-content" role="main">
            <div class="content-box backend">
                <div class="row">
                    <div class="col-md-6"><p><strong>Ranktracker Domains</strong></p></div>
                    <div class="col-md-6"><button type="button" data-toggle="modal" data-target="#add-row-modal" class="btn btn-primary large pull-right"><i class="fa fa-plus fa-spaced"></i>Add Domain</button></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-condensed" id="keyword-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Domain URL</th>
                                        <th>Date Added</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($rows))
                                        @foreach ($rows as $row)
                                            <tr>
                                                <td>
                                                    <i role="button" class="fa fa-fw fa-trash" title="Delete this Domain" v-on:click="deleteRow({{ $row['id'] }})"></i>
                                                </td>
                                                <td><a href="/ranktracker/domains/{{ $row['id'] }}">{{ $row['url'] }}</a></td>
                                                <td>{{ Auth::user()->applyTz($row['created_at'])->format('m-d-Y h:i A') }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" class="text-center">No domain to display.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-row-modal" role="dialog" aria-labelledby="add-row-modal-title">
            <div class="modal-dialog" role="document">
                <form action="" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="add-row-modal-title">Add Domain</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-success" v-if="form.successful">
                                Your request has been submitted successfully!
                            </div>
                            <div class="form-group" :class="{'has-error': form.errors.has('url')}">
                                <div class="input-group">
                                    <span class="input-group-addon" id="domain-protocol">http(s)://</span>
                                    <input name="url" type="text" class="form-control" placeholder="Domain URL here" aria-describedby="domain-protocol" v-model="form.url" required>
                                </div>

                                <span class="help-block" v-show="form.errors.has('url')">
                                    @{{ form.errors.get('url') }}
                                </span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary large" @click="submitForm" :disabled="form.busy">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</domain>
@endsection

@section('scripts-bottom')
    <script type="text/javascript">
        $('#side-nav').hover(function() {
            if ($('#side-nav').hasClass('visible')) {
                $(".nav-menu-item").css({
                    'display': 'none',
                });
                $("#side-nav").animate({
                    width: "80",
                }, 100);
                $("#main-content").animate({
                    'margin-left': "80",
                }, 100);
                $("#side-nav .fixed-bottom a").css({
                    'width': 'auto',
                })
            } else {
                $("#side-nav").animate({
                    width: "250",
                }, 100, function () {
                    $(".nav-menu-item").css({
                        'display': 'inline-block',
                    });
                });

                $("#main-content").animate({
                    'margin-left': "250",
                }, 100);

                $("#side-nav .fixed-bottom a").css({
                    'width': '250px',
                })
            }
            $("#side-nav").toggleClass('visible');
        }, function() {
            $(".nav-menu-item").css({
                'display': 'none',
            });
            $("#side-nav").animate({
                width: "80",
            }, 100);
            $("#main-content").animate({
                'margin-left': "80",
            }, 100);
            $("#side-nav .fixed-bottom a").css({
                'width': 'auto',
            })
            $("#side-nav").removeClass('visible');
        });

        $("#add-row-modal").on('hidden.bs.modal', function(e) {
            $(this).find('form').trigger('reset');
        })

        window.fingerprint({excludeUserAgent: true}).get(function(result, components){
            axios.post('/fingerprints', {hash: result});
        });
    </script>

@endsection
