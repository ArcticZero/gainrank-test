<!--[if IE 9]>  <html class="ie9" lang="en"> <![endif]-->
<html lang="en">
  <head>
    @include('includes.head')
  </head>
  <body>
    <nav class="navbar navbar-default">
      @include('includes.header')
    </nav>
    <section id="main">
      @yield('content')
    </section>

    <footer id="footer">
      @include('includes.footer')
    </footer>
  </body>
</html>
