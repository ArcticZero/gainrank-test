<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', config('app.name'))</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="/css/gainrank-font.css" rel="stylesheet">
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/hopscotch.css" rel="stylesheet">

    @yield('scripts', '')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(),
    []
        )); ?>;
    </script>
</head>
<body class="with-navbar auth-body">
    <div id="spark-app" v-cloak>
        <!-- Main Content -->
        <div class="row">
            <div class="col-md-12 text-center brand-row">
                <a href="{{ url('/') }}"><img src="{!! asset('images/gainrank-logo-color.png') !!}" alt="GainRank" /></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @yield('content')
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 copyright">&copy; Copyright {{ date('Y') }} Gainrank<br />All Rights Reserved</div>
        </div>
    </div>

    <!-- JavaScript -->
    <script src="/js/app.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    @yield('scripts-bottom', '')

    @if (Auth::check() && Auth::user()->referrer_affiliate_id && is_null(Auth::user()->referralLog))
        <!-- This is a referred user's first visit. Track details -->
        <script>
            $(function() {
                window.fingerprint().get(function(result, components) {
                    axios.post('/affiliates/referral', { hash: result, components: components });
                });
            });
        </script>
    @endif
    <!-- Scripts -->

    @if (((Auth::check() && Auth::user()->show_guide) || !Auth::check()) && app('request')->input('tour_url') != false)
    <!-- Guided Tour -->
    <script src="{{ app('request')->input('tour_url') }}"></script>
    <script>
        $(function() {
            window.hopscotch.startTour(tour);
        });
    </script>
    @endif

</body>
</html>
