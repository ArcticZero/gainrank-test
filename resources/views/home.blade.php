@extends('spark::layouts.app')

@section('content')
<home :user="user" ranks="{{ json_encode($ranks) }}" rows="{{ json_encode($rows) }}" dates="{{ json_encode($dates) }}" stats="{{ json_encode($stats) }}" colors="{{ json_encode(config('chartist.series_colors')) }}" inline-template>
    <div class="container-fluid">
        <div id="main-content" role="main">
            <div class="row data-box-row">
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-bullseye fa-spaced"></i>Total Keywords
                        </div>
                        <h1>{{ $stats['keyword_total'] }}</h1>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-globe fa-spaced"></i>Total Websites
                        </div>
                        <h1>{{ $stats['website_total'] }}</h1>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-line-chart fa-spaced"></i>Keyword Movement
                        </div>
                        <h1{!! $stats['keyword_movement']['total'] <= 0 ? ' class="light-red"' : '' !!}>{{ $stats['keyword_movement']['total'] . '%' }}</h1>
                        <!--
                        @if ($stats['keyword_movement']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['keyword_movement']['delta']) . '%' }}</span> from last week
                        @elseif ($stats['keyword_movement']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['keyword_movement']['delta']) . '%' }}</span> from last week
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last week
                        @endif
                        -->
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-trophy fa-spaced"></i>Rank 1
                        </div>
                        <h1>{{ $stats['top1']['total'] }}</h1>
                        <!--
                        @if ($stats['top1']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top1']['delta']) . '%' }}</span> from last week
                        @elseif ($stats['top1']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top1']['delta']) . '%' }}</span> from last week
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last week
                        @endif
                        -->
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-heart fa-spaced"></i>Top 3
                        </div>
                        <h1>{{ $stats['top3']['total'] }}</h1>
                        <!--
                        @if ($stats['top3']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top3']['delta']) . '%' }}</span> from last week
                        @elseif ($stats['top3']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top3']['delta']) . '%' }}</span> from last week
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last week
                        @endif
                        -->
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="data-box">
                        <div class="data-box-title">
                            <i class="fa fa-check fa-spaced"></i>Top 10
                        </div>
                        <h1>{{ $stats['top10']['total'] }}</h1>
                        <!--
                        @if ($stats['top10']['delta'] > 0)
                            <i class="fa fa-fw fa-chevron-up light-green"></i>Up <span class="light-green">{{ abs($stats['top10']['delta']) . '%' }}</span> from last week
                        @elseif ($stats['top10']['delta'] < 0)
                            <i class="fa fa-fw fa-chevron-down light-red"></i>Down <span class="light-red">{{ abs($stats['top10']['delta']) . '%' }}</span> from last week
                        @else
                            <i class="fa fa-fw fa-minus"></i>No change from last week
                        @endif
                        -->
                    </div>
                </div>
            </div>
            <div class="content-box backend">
                <div class="row">
                    <div class="col-xs-12"><p><strong>Top Performers</strong> Last {{ $past_x_days }} days</p></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <chartist id="keyword-rankings" ref="keywords" class="ct-fill-only" :type="'Line'" :data="chart.data"
                            :options="chart.options"></chartist>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed" id="keyword-table">
                                <thead>
                                    <tr>
                                        <th><i class="fa fa-area-chart fa-fw" title="Show on chart"></i></th>
                                        <th></th>
                                        <th>Domain</th>
                                        <th>Keyword</th>
                                        <th><i class="fa fa-google fa-fw google-blue"></i></th>
                                        <th>1 Day</th>
                                        <th>7 Days</th>
                                        <th>30 Days</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($rows))
                                        @foreach ($rows as $row)
                                            <tr data-domain="{{ $row['domain'] }}" data-keyword="{{ $row['keyword'] }}" data-loop-id="{{ $loop->iteration }}" data-row-id="{{ $row['id'] }}">
                                                <td>
                                                    <i role="button" class="fa fa-area-chart fa-fw google-blue" title="Show on chart" v-on:click="toggleRow({{ $row['id'] }})"></i>
                                                </td>
                                                <td><i class="fa fa-square fa-fw" :style="{ color: getRowColor({{ $row['id'] }}) }"></i></td>
                                                <td>{{ $row['domain'] }}</td>
                                                <td>{{ $row['keyword'] }}</td>
                                                <td>{{ $row['last_rank'] }}</td>
                                                <td>{!! $row['deltas']['1'] != 0 ? '<i class="fa fa-fw ' . ($row['deltas']['1'] < 0 ? 'fa-arrow-down text-danger' : 'fa-arrow-up light-green') . '"></i>' . abs($row['deltas']['1']) : '-' !!}</td>
                                                <td>{!! $row['deltas']['7'] != 0 ? '<i class="fa fa-fw ' . ($row['deltas']['7'] < 0 ? 'fa-arrow-down text-danger' : 'fa-arrow-up light-green') . '"></i>' . abs($row['deltas']['7']) : '-' !!}</td>
                                                <td>{!! $row['deltas']['30'] != 0 ? '<i class="fa fa-fw ' . ($row['deltas']['30'] < 0 ? 'fa-arrow-down text-danger' : 'fa-arrow-up light-green') . '"></i>' . abs($row['deltas']['30']) : '-' !!}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8" class="text-center">No submissions to display.</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <a href="{{ url('ranktracker') }}">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>
@endsection

@section('scripts-bottom')
    <script type="text/javascript">
        $('#side-nav').hover(function() {
            if ($('#side-nav').hasClass('visible')) {
                $(".nav-menu-item").css({
                    'display': 'none',
                });
                $("#side-nav").animate({
                    width: "80",
                }, 100);
                $("#main-content").animate({
                    'margin-left': "80",
                }, 100);
                $("#side-nav .fixed-bottom a").css({
                    'width': 'auto',
                })
            } else {
                $("#side-nav").animate({
                    width: "250",
                }, 100, function () {
                    $(".nav-menu-item").css({
                        'display': 'inline-block',
                    });
                });

                $("#main-content").animate({
                    'margin-left': "250",
                }, 100);

                $("#side-nav .fixed-bottom a").css({
                    'width': '250px',
                })
            }
            $("#side-nav").toggleClass('visible');
        }, function() {
            $(".nav-menu-item").css({
                'display': 'none',
            });
            $("#side-nav").animate({
                width: "80",
            }, 100);
            $("#main-content").animate({
                'margin-left': "80",
            }, 100);
            $("#side-nav .fixed-bottom a").css({
                'width': 'auto',
            })
            $("#side-nav").removeClass('visible');
        });

        $("#add-row-modal").on('hidden.bs.modal', function(e) {
            $(this).find('form').trigger('reset');
        })

        window.fingerprint({excludeUserAgent: true}).get(function(result, components){
            axios.post('/fingerprints', {hash: result});
        });
    </script>

@endsection
