@extends('spark::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login via Backup Code</div>

                <div class="panel-body">
                    @include('spark::shared.errors')

                    <!-- Warning Message -->
                    <div class="alert alert-warning">
                        You may use one of your pre-generated backup codes to log in to your account. Backup codes are <strong>single-use only</strong> and will expire once successfully used to log in.
                    </div>

                    <form class="form-horizontal" role="form" method="POST" action="/login/token/backup-code">
                        {{ csrf_field() }}

                        <!-- Backup Code -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Backup Code</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="token" autofocus>
                            </div>
                        </div>

                        <!-- Backup Code Login Button -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>Login
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
