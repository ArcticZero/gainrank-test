@extends('spark::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Two-Factor Authentication</div>

                <div class="panel-body">
                    <div class="alert alert-info clearfix">
                        <img src="{{ asset('img/authenticator.png') }}" alt="" class="pull-left tfa-logo">Your account has two-factor authentication enabled, and requires further verification to log in. You can get this token from your <strong>Google Authenticator</strong> mobile app.
                    </div>

                    @include('spark::shared.errors')

                    <form class="form-horizontal" role="form" method="POST" action="/login/token">
                        {{ csrf_field() }}

                        <!-- Token -->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Enter Gainrank Auth Code</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="token" autofocus>
                            </div>
                        </div>

                        <!-- Verify Button -->
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Verify
                                </button>

                                <a class="btn btn-link" href="{{ url('login/token/backup-code') }}">
                                    Use Backup Code
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
