<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PagesController@homepage');
Route::get('/features', 'PagesController@features');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register', 'Auth\RegisterController@register');
Route::get('/thank-you-for-registering', 'Auth\RegisterController@showRegistrationThankYou');
Route::get('/email-not-verified', 'Auth\RegisterController@emailNotVerified');
Route::post('/email-verification/resend', 'Auth\RegisterController@resendVerification');
Route::post('/email-verification/resend-current', 'Auth\RegisterController@resendVerificationForCurrentUser');
Route::get('/home', 'HomeController@show');

// settings blocks
Route::put('/settings/profile/notifications', 'ProfileNotificationsController@update');
Route::put('/settings/profile/timezone', 'ProfileTimezoneController@update');
Route::get('/settings/subscription/pending', 'SubscriptionController@getPending');

// two-factor authentication
Route::post('/login', 'Auth\LoginController@login');
Route::get('/login/token', 'Auth\LoginController@showTokenForm');
Route::post('/login/token', 'Auth\LoginController@verifyToken');
Route::get('/login/token/backup-code', 'Auth\LoginController@showBackupCodeForm');
Route::post('/login/token/backup-code', 'Auth\LoginController@loginByBackupCode');
Route::put('/settings/security/two-factor-auth', 'TwoFactorAuthController@enable');
Route::delete('/settings/security/two-factor-auth', 'TwoFactorAuthController@disable');
Route::get('/settings/security/two-factor-auth/generate-secret', 'TwoFactorAuthController@generateSecret');
Route::get('/settings/security/two-factor-auth/generate-backup-codes', 'TwoFactorAuthController@generateBackupCodes');
Route::get('/settings/security/two-factor-auth/get-backup-codes', 'TwoFactorAuthController@getBackupCodes');

Route::get('/users/{user}/roles', 'ProfileRoleController@get');
Route::put('/users/{user}/roles', 'ProfileRoleController@update');
Route::get('/users/{email}', 'ProfileRoleController@loadByEmail');

// notification handlers
Route::post('/settings/subscription/paypal/ipn', 'PaymentNotificationController@listenPaypal');
Route::post('/settings/subscription/coinbase/notification', 'PaymentNotificationController@listenCoinbase');

// subscription checkout
Route::post('/settings/subscription/checkout', 'CheckoutController@checkout');

// subscription post-checkout success
Route::get('/settings/subscription/checkout/{billing_provider}/success', 'PostCheckoutController@doCheckoutSuccess');

// subscription post-checkout failure
Route::get('/settings/subscription/checkout/{billing_provider}/failure', 'PostCheckoutController@doCheckoutFailure');

// suspend subscription
Route::delete('/settings/subscription/{billing_provider}', 'SubscriptionController@suspend');

// update or resume subscription
Route::put('/settings/subscription/{billing_provider}', 'SubscriptionController@update');

// activate subscription (only for acceptance tests)
Route::get('/settings/subscription/activate/{billing_provider}', 'SubscriptionController@activateFirstPending');

// disable tour
Route::post('/settings/tour/disable', 'ProfileNotificationsController@disableTour');

// website routes
Route::get('/blog/{slug?}', 'PostController@view');
Route::get('/posts/categories', 'PostController@getCategories');
Route::resource('/posts', 'PostController');

// affiliates
/*
Route::get('/affiliates', 'AffiliateController@view');
Route::post('/affiliate_apply', 'AffiliateController@apply');
Route::post('/affiliates/referral', 'AffiliateController@trackReferral');
Route::get('/affiliates/referral_transactions', 'AffiliateController@transactions');
*/

// kiosk overrides
Route::get('/spark/kiosk/performance-indicators/plans', 'Kiosk\\PerformanceIndicatorsController@subscribers');
Route::get('/spark/kiosk/performance-indicators/revenue', 'Kiosk\\PerformanceIndicatorsController@revenue');
Route::get('/spark/kiosk/performance-indicators/plans', 'Kiosk\\PerformanceIndicatorsController@subscribers');
Route::get('/spark/kiosk/performance-indicators/trialing', 'Kiosk\\PerformanceIndicatorsController@trialUsers');

// kiosk payment notification management routes
Route::get('/kiosk/coinbase/notifications', 'Kiosk\\CoinbaseNotificationsController@list');
Route::get('/kiosk/paypal/notifications', 'Kiosk\\PaypalNotificationsController@list');

// order routes
Route::get('/kiosk/orders/pending', 'Kiosk\\PendingOrdersController@list');
Route::post('/kiosk/orders/approve', 'Kiosk\\PendingOrdersController@approve');
Route::post('/kiosk/orders/deny', 'Kiosk\\PendingOrdersController@deny');
Route::get('/orders/confirm/{key}', 'PendingOrderConfirmationController@confirm');

// fingerprinting routes
Route::post('/fingerprints', 'FingerprintRecordingController@store');
Route::get('/fingerprints/{hash}', 'FingerprintRecordingController@view');
Route::get('/fingerprints/{email}/related', 'FingerprintRecordingController@relatedUsers');

// soft launch invites
Route::get('/invitations/{token}', 'SoftLaunchInvitationController@register');
Route::get('/users/{user}/invitations', 'SoftLaunchInvitationController@index');
Route::post('/users/{user}/invitations', 'SoftLaunchInvitationController@store');
Route::delete('/users/{user}/invitations/{invitation}', 'SoftLaunchInvitationController@destroy');
Route::post('/shortlist', 'SoftLaunchInvitationController@addToShortlist');

// verified overrides
Route::get('settings', '\\Laravel\\Spark\\Http\\Controllers\\Settings\\DashboardController@show')->middleware('verified');

// Kiosk search override
Route::post('/spark/kiosk/users/search', 'Kiosk\\SearchController@performBasicSearch');

// Helpdesk
Route::get('/helpdesk', 'HelpdeskController@show');
Route::get('/helpdesk/tickets', 'HelpdeskController@index');
Route::get('/helpdesk/tickets/{conversation_id}', 'HelpdeskController@view');
Route::post('/helpdesk/tickets', 'HelpdeskController@store');
Route::put('/helpdesk/tickets/{conversation_id}', 'HelpdeskController@update');

// rank tracking
Route::get('/ranktracker', 'RankTrackingController@showDomainPage');
Route::post('/ranktracker/domains', 'RankTrackingController@addDomain');
Route::get('/ranktracker/domains/{domain_id}', 'RankTrackingController@showKeywordPage');
// Route::get('/ranktracker/domains/{domain_id}', 'RankTrackingController@showRankTrackerTestData');
Route::post('/ranktracker/domains/{domain_id}', 'RankTrackingController@addKeyword');
Route::get('/ranktracker/test', 'RankTrackingController@test');
Route::post('/ranktracker/google-regions', 'RankTrackingController@getGoogleRegions');
Route::delete('/ranktracker/{id}', 'RankTrackingController@destroy');
Route::get('/kiosk/ranktracker/submissions', 'RankTrackingController@getSubmissions');
//Route::get('/kiosk/ranktracker/submissions', 'RankTrackingController@getTestSubmissions');
Route::get('/kiosk/ranktracker/settings', 'RankTrackingController@getSettings');
Route::put('/kiosk/ranktracker/settings', 'RankTrackingController@updateSettings');
Route::get('/kiosk/ranktracker/statuses', 'RankTrackingController@getStatuses');
Route::post('/kiosk/ranktracker/statuses', 'RankTrackingController@addStatus');
Route::put('/kiosk/ranktracker/statuses/{id}', 'RankTrackingController@updateStatus');
Route::delete('/kiosk/ranktracker/statuses/{id}', 'RankTrackingController@destroyStatus');


// alexa
Route::get('/alexa', 'AlexaController@showAlexa');
//Route::get('/alexa', 'AlexaController@showAlexaTestData');
Route::post('/alexa', 'AlexaController@add');
Route::delete('/alexa', 'AlexaController@destroy');
Route::get('/alexa/test', 'AlexaController@test');
Route::get('/kiosk/alexa', 'AlexaController@showSubmissions');
Route::get('/kiosk/alexa/submissions', 'AlexaController@getSubmissions');
//Route::get('/kiosk/alexa/submissions', 'AlexaController@getTestSubmissions');
Route::get('/kiosk/alexa/settings', 'AlexaController@getSettings');
Route::put('/kiosk/alexa/settings', 'AlexaController@updateSettings');
Route::get('/kiosk/alexa/statuses', 'AlexaController@getStatuses');
Route::post('/kiosk/alexa/statuses', 'AlexaController@addStatus');
Route::put('/kiosk/alexa/statuses/{id}', 'AlexaController@updateStatus');
Route::delete('/kiosk/alexa/statuses/{id}', 'AlexaController@destroyStatus');

// ctr
Route::get('/ctr', 'CtrController@showCtr');
//Route::get('/ctr', 'CtrController@showCtrTestData');
Route::get('/ctr/test', 'CtrController@test');
Route::post('/ctr', 'CtrController@add');
Route::delete('/ctr', 'CtrController@destroy');
Route::get('/kiosk/ctr', 'CtrController@showSubmissions');
Route::get('/kiosk/ctr/submissions', 'CtrController@getSubmissions');
//Route::get('/kiosk/ctr/submissions', 'CtrController@getTestSubmissions');
Route::get('/kiosk/ctr/settings', 'CtrController@getSettings');
Route::put('/kiosk/ctr/settings', 'CtrController@updateSettings');
Route::get('/kiosk/ctr/statuses', 'CtrController@getStatuses');
Route::post('/kiosk/ctr/statuses', 'CtrController@addStatus');
Route::put('/kiosk/ctr/statuses/{id}', 'CtrController@updateStatus');
Route::delete('/kiosk/ctr/statuses/{id}', 'CtrController@destroyStatus');

// wayback
Route::get('/wayback', 'WaybackController@showWayback');
//Route::get('/wayback', 'WaybackController@showWaybackTestData');
Route::post('/wayback', 'WaybackController@add');
Route::delete('/wayback', 'WaybackController@destroy');
Route::get('/kiosk/wayback', 'WaybackController@showSubmissions');
Route::get('/kiosk/wayback/submissions', 'WaybackController@getSubmissions');
//Route::get('/kiosk/wayback/settings', 'WaybackController@getSettings');
Route::put('/kiosk/wayback/settings', 'WaybackController@updateSettings');
Route::get('/kiosk/wayback/statuses', 'WaybackController@getStatuses');
Route::post('/kiosk/wayback/statuses', 'WaybackController@addStatus');
Route::put('/kiosk/wayback/statuses/{id}', 'WaybackController@updateStatus');
Route::delete('/kiosk/wayback/statuses/{id}', 'WaybackController@destroyStatus');

// traffic
Route::get('/traffic', 'TrafficController@showTraffic');
//Route::get('/traffic', 'TrafficController@showTrafficTestData');
Route::get('/traffic/test', 'TrafficController@test');
Route::post('/traffic', 'TrafficController@add');
Route::delete('/traffic', 'TrafficController@destroy');
Route::get('/kiosk/traffic', 'TrafficController@showSubmissions');
Route::get('/kiosk/traffic/submissions', 'TrafficController@getSubmissions');
//Route::get('/kiosk/traffic/submissions', 'TrafficController@getTestSubmissions');
Route::get('/kiosk/traffic/settings', 'TrafficController@getSettings');
Route::put('/kiosk/traffic/settings', 'TrafficController@updateSettings');
Route::get('/kiosk/traffic/statuses', 'TrafficController@getStatuses');
Route::post('/kiosk/traffic/statuses', 'TrafficController@addStatus');
Route::put('/kiosk/traffic/statuses/{id}', 'TrafficController@updateStatus');
Route::delete('/kiosk/traffic/statuses/{id}', 'TrafficController@destroyStatus');

// ad campaign routes
Route::resource('campaigns', 'AdCampaignController');
Route::get('campaign/{path}', 'AdCampaignController@view');
Route::get('campaign-bonus-types', 'AdCampaignController@bonusTypes');
Route::get('{path}', 'AdCampaignController@view');

// settings override
Route::get('/settings', 'Settings\\DashboardController@show');

// dummy ranktracker API
Route::get('/ranktracker/dummyapi/domain/{domain}/bulk', 'RankTrackingController@dummyApi');
